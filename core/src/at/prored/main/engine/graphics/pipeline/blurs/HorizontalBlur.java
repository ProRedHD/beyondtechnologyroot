package at.prored.main.engine.graphics.pipeline.blurs;

import at.prored.main.engine.graphics.pipeline.FrameBufferProcessor;

/**
 * Created by ProRed on 12.11.2018.
 */
public class HorizontalBlur extends BaseBlur {

    public HorizontalBlur(FrameBufferProcessor frameBufferProcessor, float radius) {
        super(frameBufferProcessor, radius);
    }

    @Override
    boolean isVertical() {
        return false;
    }

}
