package at.prored.main.engine.graphics.pipeline;

import at.prored.main.engine.Engine;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;

/**
 * Created by ProRed on 12.11.2018.
 */
public abstract class FrameBufferProcessor {

    private FrameBufferProcessor fbp;
    private FrameBuffer fbo;
    private boolean top = true;

    public FrameBufferProcessor(FrameBufferProcessor frameBufferProcessor){
        this.fbp = frameBufferProcessor;
        this.fbp.notifyOfParent();
    }

    public FrameBufferProcessor(FrameBuffer fbo){
        this.fbo = fbo;
    }

    public FrameBuffer render(Engine e){
        return process(fbp == null ? fbo : fbp.render(e), e);
    }

    /**
     * Notifies that this element is not the end of the pipeline
     */
    public void notifyOfParent(){
        this.top = false;
    }

    /**
     * Can be used to set this element to the top (ex. passthrough processors)
     */
    public void setAsTop(){
        this.top = true;
    }

    public boolean isTop() {
        return top;
    }

    public FrameBufferProcessor getFrameBufferProcessor() {
        return fbp;
    }

    public abstract FrameBuffer process(FrameBuffer fbo, Engine e);
}
