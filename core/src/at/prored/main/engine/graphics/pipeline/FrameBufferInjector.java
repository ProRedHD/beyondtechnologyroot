package at.prored.main.engine.graphics.pipeline;

import at.prored.main.engine.Engine;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;

/**
 * Created by ProRed on 12.11.2018.
 */
public class FrameBufferInjector extends FrameBufferProcessor {

    public FrameBufferInjector(Pixmap.Format format, int width, int height, boolean hasDepth) {
        super(new FrameBuffer(format, width, height, hasDepth));
    }

    public FrameBufferInjector(FrameBuffer fbo){
        super(fbo);
    }

    @Override
    public FrameBuffer process(FrameBuffer fbo, Engine e) {
        return fbo;
    }
}
