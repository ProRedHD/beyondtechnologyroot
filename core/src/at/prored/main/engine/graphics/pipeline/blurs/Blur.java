package at.prored.main.engine.graphics.pipeline.blurs;

import at.prored.main.engine.Engine;
import at.prored.main.engine.graphics.pipeline.FrameBufferProcessor;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;

/**
 * Created by ProRed on 29.11.2018.
 */
public class Blur extends FrameBufferProcessor {

    public Blur(FrameBufferProcessor frameBufferProcessor, float radius) {
        super(new VerticalBlur(new HorizontalBlur(frameBufferProcessor, radius), radius));

        //Set to top
        getFrameBufferProcessor().setAsTop();
    }

    @Override
    public FrameBuffer process(FrameBuffer fbo, Engine e) {
        return fbo;
    }
}
