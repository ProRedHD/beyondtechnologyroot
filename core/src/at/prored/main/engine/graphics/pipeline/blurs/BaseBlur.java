package at.prored.main.engine.graphics.pipeline.blurs;

import at.prored.main.engine.Engine;
import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.graphics.pipeline.FrameBufferProcessor;
import at.prored.main.engine.graphics.shaders.Shaders;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

import static com.badlogic.gdx.graphics.GL20.GL_COLOR_BUFFER_BIT;
import static com.badlogic.gdx.graphics.GL20.GL_DEPTH_BUFFER_BIT;

/**
 * Created by ProRed on 12.11.2018.
 */
public abstract class BaseBlur extends FrameBufferProcessor {

    private FrameBuffer newfbo;

    //Properties
    private float radius;
    private float linearBlurDivider = 2;

    //If there is another blur before it -> do not reduce of fbo twice
    private boolean prevBlur;

    OrthographicCamera camera;
    SpriteBatch ownBatch;

    public BaseBlur(FrameBufferProcessor frameBufferProcessor, float radius) {
        super(frameBufferProcessor);
        this.radius = radius;
        this.prevBlur = frameBufferProcessor instanceof BaseBlur;
        this.camera = new OrthographicCamera();
        this.ownBatch = new SpriteBatch();
    }

    @Override
    public FrameBuffer process(FrameBuffer fbo, Engine e) {
        float resMultiplier = 1f/(prevBlur ? 1 : linearBlurDivider);

        //Create a new fbo
        if(newfbo == null){
            newfbo = new FrameBuffer(Pixmap.Format.RGBA8888, (int) (fbo.getWidth()*resMultiplier), (int) (fbo.getHeight()*resMultiplier), true);
        }

        //Set camera for proper rendering
        camera.setToOrtho(true, newfbo.getWidth(), newfbo.getHeight());

        //Initiate shader
        ShaderProgram shader = Shaders.instance().getBlurShader();
        shader.begin();
        shader.setUniformf("dir", isVertical() ? 0f : 1f, isVertical() ? 1f : 0f);
        shader.setUniformf("radius", radius);
        shader.setUniformf("resolution", Graphics.V_HEIGHT*resMultiplier);
        shader.end();

        //Flip texture if not top
        TextureRegion textureRegion = new TextureRegion(fbo.getColorBufferTexture());
        textureRegion.flip(false,!isTop());

        //Set the batch projection matrix to match fbo size
        ownBatch.setProjectionMatrix(camera.combined);

        //Begin rendering with shader
        newfbo.begin();
        ownBatch.setShader(shader);

        Gdx.gl.glClearColor(0f, 0f, 0f, 0f);
        Gdx.gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        ownBatch.begin();
        ownBatch.draw(textureRegion, 0, 0, newfbo.getWidth(), newfbo.getHeight());
        ownBatch.end();

        newfbo.end();
        ownBatch.setShader(null);

        return newfbo;
    }

    abstract boolean isVertical();
}
