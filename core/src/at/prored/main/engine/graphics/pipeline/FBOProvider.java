package at.prored.main.engine.graphics.pipeline;

import com.badlogic.gdx.graphics.glutils.FrameBuffer;

/**
 * Created by ProRed on 12.11.2018.
 */
public interface FBOProvider {

    FrameBuffer get();

}
