package at.prored.main.engine.graphics.screens;

import at.prored.main.engine.Engine;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by ProRed on 01.02.2016.
 */
public class GameScreenManager {

    private Engine engine;

    private ObjectMap<String, AbstractScreen> screens;
    private AbstractScreen activeScreen;

    public GameScreenManager(Engine engine){
        this.engine = engine;
        this.screens = new ObjectMap<String, AbstractScreen>();
    }

    public void registerScreen(String key, AbstractScreen screen){
        screens.put(key, screen);

        if(activeScreen == null)
            activeScreen = screen;
    }

    public void unregisterScreen(String key){
        screens.remove(key);
    }

    public void update(){
        if(activeScreen != null)
            activeScreen.update();
    }

    public void setScreen(String key){
        AbstractScreen screen = screens.get(key);
        engine.getGame().setScreen(screen);
        activeScreen = screen;
    }

}
