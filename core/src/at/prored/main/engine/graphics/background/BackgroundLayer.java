package at.prored.main.engine.graphics.background;

import at.prored.main.engine.Engine;
import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.graphics.pipeline.FrameBufferInjector;
import at.prored.main.engine.graphics.pipeline.FrameBufferProcessor;
import at.prored.main.engine.graphics.pipeline.blurs.Blur;
import at.prored.main.engine.tiles.storage.GameMap;
import at.prored.main.engine.utils.AssetUtils;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

import static com.badlogic.gdx.graphics.GL20.GL_COLOR_BUFFER_BIT;

/**
 * Created by ProRed on 08.03.2018.
 */
public class BackgroundLayer {

    private float speedMultiplier;
    private int yOffset;

    private TextureRegion[] textures;
    private float disiredWidth;
    private Array<Sprite> activeTextures;

    private float lastX, lastY;

    //Rendering
    private FrameBuffer layerFrameBuffer;
    private FrameBufferProcessor pipeline;

    public BackgroundLayer(AssetManager assetManager, String[] textureNames, float speedMultiplier, int yOffset, float disiredWidth, float blur) {
        this.speedMultiplier = speedMultiplier;
        this.yOffset = yOffset;
        this.activeTextures = new Array<>();
        this.disiredWidth = disiredWidth;

        this.textures = new TextureRegion[textureNames.length];
        for (int i = 0; i < textureNames.length; i++) {
            this.textures[i] = AssetUtils.loadTextureRegion(assetManager, textureNames[i]);
        }

        layerFrameBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, Graphics.V_WIDTH, Graphics.V_HEIGHT, false);
        pipeline = new Blur(new FrameBufferInjector(layerFrameBuffer), blur);
    }

    public void update(GameMap gameMap){
        Vector3 camera = Graphics.instance().getCamera().getWorldCamera().position;

        //Create first texture
        if(activeTextures.size == 0){
            TextureRegion selectedTexture = textures[MathUtils.random(0, textures.length-1)];

            Sprite sprite = new Sprite(selectedTexture);
            sprite.setPosition(-disiredWidth / 2f, -(disiredWidth/Graphics.V_WIDTH)*Graphics.V_HEIGHT / 2f+yOffset);
            sprite.setSize(disiredWidth, (disiredWidth/Graphics.V_WIDTH)*Graphics.V_HEIGHT);
            activeTextures.add(sprite);
        }

        Array<Sprite> toBeDeleted = new Array<>();
        if(camera.x != lastX || camera.y != lastY){
            for (Sprite activeTexture : activeTextures) {
                float x = activeTexture.getX();
                float y = activeTexture.getY();

                activeTexture.setPosition(x + (camera.x - lastX) * -speedMultiplier, y + (camera.y - lastY) * speedMultiplier * -.2f);

                if(activeTexture.getX() <= -disiredWidth*2)
                    toBeDeleted.add(activeTexture);
            }

            //Remove values
            for (Sprite sprite : toBeDeleted) {
                activeTextures.removeValue(sprite, true);
            }

            lastX = camera.x;
            lastY = camera.y;
        }

        //Create new textures if old one is off screen
        while(activeTextures.size != (Math.ceil(Graphics.V_WIDTH/disiredWidth))+2){
            TextureRegion selectedTexture = textures[MathUtils.random(0, textures.length-1)];

            Sprite sprite = new Sprite(selectedTexture);
            sprite.setPosition(activeTextures.get(activeTextures.size-1).getX() + disiredWidth, activeTextures.get(0).getY());
            sprite.setSize(disiredWidth, (disiredWidth/Graphics.V_WIDTH)*Graphics.V_HEIGHT);
            activeTextures.add(sprite);
        }
    }

    public void render(SpriteBatch batch, Engine e){
        //Render layer to a frame buffer
        layerFrameBuffer.begin();

        Gdx.gl.glClearColor(0f, 0f, 0f, 0f);
        Gdx.gl.glClear(GL_COLOR_BUFFER_BIT);

        batch.begin();
        for (Sprite activeTexture : activeTextures) {
            activeTexture.draw(batch);
        }
        batch.end();

        layerFrameBuffer.end();

        //Put rendered screen through blurring pipeline
        FrameBuffer finalFbo = pipeline.render(e);

        int x = (int) (-Graphics.V_WIDTH / 2f);
        int y = (int) (-Graphics.V_HEIGHT / 2f);

        //Render finished to scene
        batch.begin();
        batch.draw(finalFbo.getColorBufferTexture(), x,y,Graphics.V_WIDTH, Graphics.V_HEIGHT);
        batch.end();
    }

    public float getSpeedMultiplier() {
        return speedMultiplier;
    }

}
