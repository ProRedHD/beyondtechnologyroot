package at.prored.main.engine.graphics.background;

import at.prored.main.engine.tiles.storage.GameMap;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by ProRed on 08.03.2018.
 */
public class BackgroundRenderer {

    private GameMap gameMap;

    private ObjectMap<Integer, BackgroundLayer> layers;

    public BackgroundRenderer(GameMap gameMap){
        this.gameMap = gameMap;
        this.layers = new ObjectMap<>();

        this.layers.put(0, new BackgroundLayer(gameMap.getEngine().getAssetManager(), new String[]{"rockymountains"}, .25f, 230, 860,1f));
        this.layers.put(1, new BackgroundLayer(gameMap.getEngine().getAssetManager(), new String[]{"medium"}, .4f, 130, 1200,1f));
    }

    public void update(){
        for (BackgroundLayer backgroundLayer : layers.values()) {
            backgroundLayer.update(gameMap);
        }
    }

    public void render(SpriteBatch batch){
        for (BackgroundLayer backgroundLayer : layers.values()) {
            backgroundLayer.render(batch, gameMap.getEngine());
        }
    }

}
