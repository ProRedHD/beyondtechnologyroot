package at.prored.main.engine.graphics.shaders;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.ShaderProgramLoader;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

/**
 * Created by ProRed on 21.11.2017.
 */
public class Shaders {

    private static Shaders instance;

    private ShaderProgram silhouette;
    private ShaderProgram lightShader;
    private ShaderProgram blurShader;

    private Shaders(){}

    public void setupShaders(){
        silhouette = createShader("lightShaderVert.glsl", "shadowShaderFrag.glsl");
        lightShader = createShader("lightShaderVert.glsl", "lightShaderFrag.glsl");
        blurShader = createShader("lightShaderVert.glsl","blurShaderFrag.glsl");

        initDefaultUniforms();
    }

    private void initDefaultUniforms(){
        blurShader.begin();
        blurShader.setUniformf("dir", 0f, 0f); //direction of blur; nil for now
        blurShader.setUniformf("resolution", 1024); //size of FBO texture
        blurShader.setUniformf("radius", 4f); //radius of blur
        blurShader.end();
    }

    public static Shaders instance(){
        return instance == null ? (instance = new Shaders()) : instance;
    }

    public static ShaderProgram createShader(String vert, String frag){
        ShaderProgram shaderProgram = new ShaderProgram(Gdx.files.internal("shaders/" + vert), Gdx.files.internal("shaders/" + frag));
        if(shaderProgram.isCompiled()){
            Gdx.app.log("Shader",  String.format("ShaderProgram[%s,%s] was successfully compiled.", vert, frag));
        } else {
            Gdx.app.error("Shader", shaderProgram + shaderProgram.getLog());
        }

        return shaderProgram;
    }

    public ShaderProgram getSilhouette() {
        return silhouette;
    }

    public ShaderProgram getBlurShader() {
        return blurShader;
    }
}
