package at.prored.main.engine.graphics.renderer;

import at.prored.main.engine.tiles.storage.GameMap;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;

/**
 * Created by ProRed on 03.01.2018.
 */
public class RenderModeManager {

    //POWER-Constants
    public static final int POWER_KEY = Input.Keys.F5;
    public static final Color POWER_TILE_COLOR = new Color(.5f, .5f, .5f, 1f);

    public enum RenderMode {
        NORMAL, POWER
    }

    private GameMap gameMap;
    private static RenderMode renderMode;

    public RenderModeManager(GameMap gameMap) {
        this.gameMap = gameMap;
        renderMode = RenderMode.NORMAL;
    }

    public static RenderMode getRenderMode(){
        return renderMode;
    }

    public void switchRenderMode(RenderMode switchto){
        renderMode = switchto;
        Gdx.app.debug("RenderMode", "Changed render mode to " + switchto);
    }

    public void updateKeybinds(){
        if(Gdx.input.isKeyJustPressed(POWER_KEY)){
            if(getRenderMode() == RenderMode.POWER)
                switchRenderMode(RenderMode.NORMAL);
            else
                switchRenderMode(RenderMode.POWER);
        }


    }






}
