package at.prored.main.engine.graphics.renderer;

import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.tiles.storage.GameMap;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by ProRed on 19.06.2017.
 */
public class GameMapTileRenderer {

    private GameMap gameMap;

    private ShaderProgram tileShader;

    public GameMapTileRenderer(GameMap gameMap) {
        this.gameMap = gameMap;
    }

    /**
     * Setup shaders
     */
    public void setupShader() {
        ShaderProgram.pedantic = false;
        tileShader = new ShaderProgram(Gdx.files.internal("shaders/lightShaderVert.glsl"), Gdx.files.internal("shaders/lightShaderFrag.glsl"));

        if (tileShader.isCompiled()) {
            Gdx.app.log("TileRenderer", "LightShader was successfully compiled!");
        } else {
            Gdx.app.error("TileRenderer", "LightShader errored while compiling: \n" + tileShader.getLog());
            System.exit(1);
        }

        tileShader.begin();

        tileShader.setUniformf("u_resolution", Graphics.V_WIDTH, Graphics.V_HEIGHT);
        tileShader.setUniformi("u_tileLight", 1);
        tileShader.setUniformf("u_ambientColor", 0.8f, 0.8f, 0.8f, 0.3f);

        tileShader.end();
    }


    /**
     * Renders tiles
     */
    public void renderTiles() {
        Gdx.gl.glActiveTexture(GL20.GL_TEXTURE1);
        gameMap.getLightProcessor().getTexture().bind();
        Gdx.gl.glActiveTexture(GL20.GL_TEXTURE0);

        Graphics.instance().getWorldBatch().setShader(tileShader);

        tileShader.begin();

        tileShader.setUniformi("u_renderMode", RenderModeManager.getRenderMode().ordinal());

        Graphics.instance().getWorldBatch().begin();

        for (Vector2 vec : gameMap.getActiveChunks().keys()) {

            gameMap.getChunks().get(vec).renderTiles(gameMap.getEngine().getTileRegistry(), (int) vec.x, (int) vec.y, 1);

            if (RenderModeManager.getRenderMode() != RenderModeManager.RenderMode.POWER)
                gameMap.getChunks().get(vec).renderTiles(gameMap.getEngine().getTileRegistry(), (int) vec.x, (int) vec.y, 2);

            gameMap.getChunks().get(vec).renderTiles(gameMap.getEngine().getTileRegistry(), (int) vec.x, (int) vec.y, 0);
            Graphics.instance().getWorldBatch().setColor(Color.WHITE);
        }
        Graphics.instance().getWorldBatch().end();

        tileShader.end();

        Graphics.instance().getWorldBatch().setShader(null);
    }

    /**
     *
     */
    public void renderBackgroundTiles(){

    }

    public void renderOverlayTiles() {
        switch (RenderModeManager.getRenderMode()) {
            case NORMAL:
                break;
            case POWER:
                Graphics.instance().getWorldBatch().begin();
                for (Vector2 vec : gameMap.getActiveChunks().keys()) {
                    gameMap.getChunks().get(vec).renderTiles(gameMap.getEngine().getTileRegistry(), (int) vec.x, (int) vec.y, 2);
                }
                Graphics.instance().getWorldBatch().end();
                break;
        }
    }
}
