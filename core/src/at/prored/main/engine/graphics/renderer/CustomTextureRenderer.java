package at.prored.main.engine.graphics.renderer;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.TextureStorageComponent;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by ProRed on 07.03.2018.
 */
public interface CustomTextureRenderer {

    void render(SpriteBatch batch, float x, float y, float scaleX, float scaleY, OrthographicCamera camera, Properties properties, Definition definition, Engine engine, TextureStorageComponent textureStorageComponent);

}