package at.prored.main.engine.graphics.renderer;

import at.prored.main.engine.graphics.background.BackgroundRenderer;
import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.graphics.pipeline.FrameBufferInjector;
import at.prored.main.engine.graphics.pipeline.FrameBufferProcessor;
import at.prored.main.engine.graphics.pipeline.blurs.Blur;
import at.prored.main.engine.tiles.storage.Chunk;
import at.prored.main.engine.tiles.storage.GameMap;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ObjectMap;

import static at.prored.main.engine.tiles.storage.GameMap.DEBUG;
import static com.badlogic.gdx.graphics.GL20.GL_COLOR_BUFFER_BIT;

/**
 * Created by ProRed on 07.01.2018.
 */
public class GameMapRenderer {

    private GameMap gameMap;

    private GameMapTileRenderer tileRenderer;
    private BackgroundRenderer backgroundRenderer;

    private FrameBuffer fbo;

    private FrameBufferProcessor blurPipeline;

    public GameMapRenderer(GameMap gameMap) {
        this.gameMap = gameMap;

        this.tileRenderer = new GameMapTileRenderer(gameMap);
        this.tileRenderer.setupShader();

        this.backgroundRenderer = new BackgroundRenderer(gameMap);


        resize();
    }

    public void resize() {
        fbo = new FrameBuffer(Pixmap.Format.RGBA8888, Graphics.V_WIDTH, Graphics.V_HEIGHT, true);
        blurPipeline = new Blur(new FrameBufferInjector(fbo), 4f);
    }

    public void renderToFrameBuffer() {

        ObjectMap<Vector2, Chunk> chunks = gameMap.getChunks();
        ObjectMap<Vector2, Chunk> activeChunks = gameMap.getActiveChunks();

        //Render tiles and entities
        fbo.begin();

        Gdx.gl.glClearColor(0f, .2f, .5f, .0f);
        Gdx.gl.glClear(GL_COLOR_BUFFER_BIT);

        tileRenderer.renderTiles();

        Graphics.instance().getWorldBatch().begin();
        for (Vector2 vec : activeChunks.keys()) {
            chunks.get(vec).renderEntities(gameMap.getEngine().getEntityRegistry());
        }
        Graphics.instance().getWorldBatch().end();

        fbo.end();

    }

    public void render() {

        renderToFrameBuffer();
        renderToScreen();

        if (DEBUG) {

            ObjectMap<Vector2, Chunk> chunks = gameMap.getChunks();
            ObjectMap<Vector2, Chunk> activeChunks = gameMap.getActiveChunks();

            for (Vector2 vec : activeChunks.keys()) {
                chunks.get(vec).renderBoundingBoxes();
            }

            for (Vector2 vec : activeChunks.keys()) {
                chunks.get(vec).renderEntityBoundingBoxes(gameMap.getEngine().getEntityRegistry());
            }
        }
    }

    public void renderToScreen() {
        int x = (int) (-Graphics.V_WIDTH / 2f);
        int y = (int) (-Graphics.V_HEIGHT / 2f);

        SpriteBatch batch = Graphics.instance().getHudBatch();

        backgroundRenderer.render(batch);

        batch.begin();
        FrameBuffer fboFinal;
        if(RenderModeManager.getRenderMode() != RenderModeManager.RenderMode.NORMAL) {
            fboFinal = blurPipeline.render(gameMap.getEngine());
        } else {
            fboFinal = fbo;
        }

        batch.draw(fboFinal.getColorBufferTexture(), x, y, fboFinal.getWidth(), fboFinal.getHeight(), 0, 0, fboFinal.getWidth(), fboFinal.getHeight(), false, true);
        batch.end();
    }

    public void update(){
        backgroundRenderer.update();
    }

    public BackgroundRenderer getBackgroundRenderer() {
        return backgroundRenderer;
    }
}
