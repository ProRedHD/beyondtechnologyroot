package at.prored.main.engine.graphics.camera;

/**
 * Created by ProRed on 18.05.2017.
 */

import at.prored.main.engine.graphics.shaders.Shaders;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Array;

/**
 * Singelton class for all graphic related things
 */
public class Graphics {

    public static final int V_WIDTH = 1280;
    public static final int V_HEIGHT = 720;

    private static final int DELTA_AVG = 60;

    //Singelton instance
    private static Graphics graphics;

    private BTCamera camera;

    private SpriteBatch batch;
    private SpriteBatch hudBatch;

    private ShapeRenderer shapeBatch;
    private ShapeRenderer hudShapeBatch;

    //DeltaTime
    private Array<Float> deltaArray;
    private float avg_delta;

    public Graphics(){
        this.camera = new BTCamera(Graphics.V_WIDTH, Graphics.V_HEIGHT);

        this.batch = new SpriteBatch();
        this.hudBatch = new SpriteBatch();

        this.shapeBatch = new ShapeRenderer();
        this.hudShapeBatch = new ShapeRenderer();

        this.deltaArray = new Array<>();

        Shaders.instance().setupShaders();
    }

    /**
     * Returns the instance of the singelton class
     * @return instance of this class
     */
    public static Graphics instance(){
        if(graphics == null)
            graphics = new Graphics();

        return graphics;
    }

    public void update(){
        this.batch.setProjectionMatrix(camera.combinedWorld());
        this.hudBatch.setProjectionMatrix(camera.combinedHud());

        this.shapeBatch.setProjectionMatrix(camera.combinedWorld());
        this.hudShapeBatch.setProjectionMatrix(camera.combinedHud());

        updateDeltaTime();
    }

    /**
     * Generates the average delta time of 100 game loops
     */
    private void updateDeltaTime() {
        deltaArray.add(Gdx.graphics.getDeltaTime());

        if (deltaArray.size >= DELTA_AVG) {
            float sum = 0;
            for (float f : deltaArray) {
                sum += f;
            }

            avg_delta = sum / deltaArray.size;
            deltaArray.clear();
        }
    }

    public BTCamera getCamera() {
        return camera;
    }

    public float getDeltaTime(){
        if(avg_delta == 0.0f){
            this.avg_delta = Gdx.graphics.getDeltaTime();
        }
        return avg_delta;
    }

    public SpriteBatch getWorldBatch() {
        return batch;
    }

    public SpriteBatch getHudBatch() {
        return hudBatch;
    }

    public ShapeRenderer getWorldShapeBatch() {
        return shapeBatch;
    }

    public ShapeRenderer getHudShapeBatch() {
        return hudShapeBatch;
    }
}
