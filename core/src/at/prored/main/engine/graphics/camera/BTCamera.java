package at.prored.main.engine.graphics.camera;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.StretchViewport;

/**
 * Created by ProRed on 01.09.2016.
 */
public class BTCamera {

    private OrthographicCamera worldCamera;
    private OrthographicCamera hudCamera;

    private StretchViewport worldViewport;
    private StretchViewport hudViewport;

    public BTCamera(int virtualWidth, int virtualHeight) {
        worldCamera = new OrthographicCamera();
        worldViewport = new StretchViewport(virtualWidth, virtualHeight, worldCamera);
        worldViewport.apply();
        worldCamera.zoom = .7f;
        worldCamera.update();

        hudCamera = new OrthographicCamera();
        hudViewport = new StretchViewport(virtualWidth, virtualHeight, hudCamera);
        hudViewport.apply();

        hudCamera.position.set(0, 0, 0);
        hudCamera.update();
    }

    /**
     * Updates the camera when resized
     */
    public void update(int width, int height) {
        worldViewport.update(width, height);
        hudViewport.update(width, height);
    }

    /**
     * Returns the combined matrix of the world camera
     */
    public Matrix4 combinedWorld() {
        return worldCamera.combined;
    }

    /**
     * Returns the combined matrix of the hud camera
     */
    public Matrix4 combinedHud() {
        return hudCamera.combined;
    }

    /**
     * Returns the world camera
     */
    public OrthographicCamera getWorldCamera() {
        return worldCamera;
    }

    /**
     * Returns the world viewport
     */
    public StretchViewport getWorldViewport() {
        return worldViewport;
    }

    /**
     * Returns the hud camera
     */
    public OrthographicCamera getHudCamera() {
        return hudCamera;
    }

    /**
     * Returns the hud viewport
     */
    public StretchViewport getHudViewport() {
        return hudViewport;
    }

    /**
     * Returns the unprojected mouse coordinates (Hud)
     */
    public Vector2 getMouseCoordinates() {
        Vector3 vec = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
        hudCamera.unproject(vec);

        return new Vector2(vec.x, vec.y);
    }

    /**
     * Returns the unprojected mouse coordinates (World)
     */
    public Vector2 getUnprojectedMouseCoordinates() {
        float mX = Gdx.input.getX();
        float mY = Gdx.input.getY();

        Vector3 vec = new Vector3(mX, mY, 0);
        worldCamera.unproject(vec);

        return new Vector2(vec.x, vec.y);
    }
}
