package at.prored.main.engine.graphics.font;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

public class Fonts {

    private static Fonts instance;
    private static GlyphLayout layout = new GlyphLayout();

    private BitmapFont tinyFont;
    private BitmapFont normalFont;
    private BitmapFont smallFont;

    private BitmapFont recipeFont;




    private Fonts(){
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("font/lunchds.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 14;
        parameter.color = Color.BLACK;
        recipeFont = generator.generateFont(parameter);

        generator = new FreeTypeFontGenerator(Gdx.files.internal("font/AftaSerifThin-Regular.otf"));
        parameter.size = 10;
        parameter.color = Color.BLACK;
        tinyFont = generator.generateFont(parameter);

        parameter.size = 18;
        normalFont = generator.generateFont(parameter);

        parameter.size = 14;
        smallFont = generator.generateFont(parameter);
    }

    public BitmapFont getNormalFont() {
        return normalFont;
    }

    public BitmapFont getSmallFont() {
        return smallFont;
    }

    public BitmapFont getRecipeFont() {
        return recipeFont;
    }

    public BitmapFont getTinyFont() {
        return tinyFont;
    }

    public static Fonts instance(){
        if (instance == null) {
            instance = new Fonts();
        }

        return instance;
    }

    public static GlyphLayout getLayout() {
        return layout;
    }
}
