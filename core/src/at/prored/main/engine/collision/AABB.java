package at.prored.main.engine.collision;

import at.prored.main.engine.utils.Pools;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by ProRed on 24.09.2016.
 */
public class AABB {
    private Vector2 center, halfSize;

    public AABB(Vector2 center, Vector2 halfSize) {
        this.halfSize = halfSize;
        this.center = center;
    }

    public AABB(float x, float y, float width, float height) {
        this(new Vector2(x + 16, y + 16), new Vector2(width / 2f, height / 2f));
    }

    public AABB(){
        this(new Vector2(), new Vector2());
    }

    public AABB set(float x, float y, float halfwidth, float halfheight){
        this.center.x = x;
        this.center.y = y;
        this.halfSize.x = halfwidth;
        this.halfSize.y = halfheight;

        return this;
    }

    /**
     * Collides with box2 and returns collision data
     */
    public Collision getCollision(AABB box2) {
        Vector2 distance = box2.center.cpy().sub(center);

        distance.x = Math.abs(distance.x);
        distance.y = Math.abs(distance.y);

        distance.sub(halfSize.cpy().add(box2.halfSize));

        return Pools.instance().getCollisionPool().obtain().set(distance, distance.x < 0 && distance.y < 0);
    }

    /**
     * Extends the bounding box by the given amount
     *
     * @param amount amount in px
     */
    public void extendX(float amount) {
        this.getCenter().add(amount / 2f, 0);
        this.getHalfSize().add(amount / 2f, 0);
    }

    /**
     * Extends the bounding box by the given amount
     *
     * @param amount amount in px
     */
    public void extendY(float amount) {
        this.getCenter().add(0, amount / 2f);
        this.getHalfSize().add(0, amount / 2f);
    }

    /**
     * Corrects the position of an aabb with the collision data
     * @return direction that the entity got pushed
     */
    public Vector2 correctPosition(AABB aabb2, Collision data) {
        Vector2 correction = aabb2.center.cpy().sub(center);

        Vector2 direction = Pools.instance().getVector2Pool().obtain().setZero();

        if(data.getDistance().x > data.getDistance().y){
            if(correction.x > 0){
                center.add(data.getDistance().x, 0);
                direction.x = -1;
            } else {
                center.add(-data.getDistance().x, 0);
                direction.x = 1;
            }
        } else {
            if(correction.y > 0){
                center.add(0, data.getDistance().y);
                direction.y = -1;
            } else {
                center.add(0, -data.getDistance().y);
                direction.y = 1;
            }
        }

        return direction;
    }

    /**
     * Returns the half_extend of the AABB
     */
    public Vector2 getHalfSize() {
        return halfSize;
    }

    /**
     * Returns the center of the AABB
     */
    public Vector2 getCenter() {
        return center;
    }

    @Override
    public String toString() {
        return "AABB{" +
                "center=" + center +
                ", halfSize=" + halfSize +
                '}';
    }
}
