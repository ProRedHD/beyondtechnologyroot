package at.prored.main.engine.collision;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;

/**
 * Created by ProRed on 24.09.2016.
 */
public class Collision implements Pool.Poolable{

    private Vector2 distance;
    private boolean isIntersecting;

    public Collision(Vector2 distance, boolean isIntersecting){
        this.distance = distance;
        this.isIntersecting = isIntersecting;
    }

    public Collision(){}

    public Collision set(Vector2 distance, boolean isIntersecting){
        this.distance = distance;
        this.isIntersecting = isIntersecting;

        return this;
    }

    public Vector2 getDistance() {
        return distance;
    }

    public boolean isIntersecting() {
        return isIntersecting;
    }

    @Override
    public void reset() {
        this.distance.set(0, 0);
        this.isIntersecting = false;
    }

}
