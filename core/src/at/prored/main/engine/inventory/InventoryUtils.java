package at.prored.main.engine.inventory;

import at.prored.main.engine.items.ItemStack;

/**
 * Created by ProRed on 27.08.2017.
 */
public class InventoryUtils {

    /**
     * Adds an itemstack to the inventory
     *
     * @param stackToAdd itemstack that should be added
     * @param inv  inventory
     * @return success or not
     */
    public static boolean addItem(ItemStack stackToAdd, Inventory inv) {

        ItemStack[] inventory = inv.getData();

        if (stackToAdd == null)
            return false;

        inv.notifyBeforeChange();

        for (int i = 0; i < inventory.length; i++) {
            if (inventory[i] == null) {
                inventory[i] = stackToAdd;
                inv.setData(inventory);
                inv.notifyAfterChange();
                return true;
            } else if (compareItemStack(inventory[i], stackToAdd)) {
                inventory[i].getProperties().addAmount(stackToAdd.getAmount());
                inv.setData(inventory);
                inv.notifyAfterChange();
                return true;
            }
        }



        return false;
    }

    /**
     * Checks if a certain amount of items are in the given inventory
     *
     * @param stack     s
     * @param inv inventory
     * @return success or not
     */
    public static boolean hasItemStack(ItemStack stack, Inventory inv) {

        ItemStack[] inventory = inv.getData();

        int amount = stack.getAmount();
        for (int i = 0; i < inventory.length; i++) {
            if (inventory[i] != null && inventory[i].equals(stack)) {
                amount -= inventory[i].getAmount();
            }
        }

        return amount <= 0;
    }

    /**
     * Removes a given amount of items from the inventory
     *
     * @param st++++
     *          e stack to remove
     * @param inv     inventory
     * @return success or not
     */
    public static boolean removeItemStack(ItemStack stackToRemove, Inventory inv) {

        ItemStack[] inventory = inv.getData();

        if (!hasItemStack(stackToRemove, inv))
            return false;

        inv.notifyBeforeChange();

        int amount = stackToRemove.getAmount();
        for (int i = 0; i < inventory.length; i++) {
            if (inventory[i] != null && inventory[i].equals(stackToRemove)) {
                if (amount > inventory[i].getAmount()) {
                    amount -= inventory[i].getAmount();
                    inventory[i] = null;
                } else {
                    inventory[i].getProperties().addAmount(-amount);
                    if (inventory[i].getProperties().getAmount() <= 0)
                        inventory[i] = null;
                    break;
                }
            }
        }

        inv.notifyAfterChange();

        return amount <= 0;
    }

    public static boolean compareItemStack(ItemStack s1, ItemStack s2) {

        //Return false when one of them is false but true when both of them are null
        if (s1 == null || s2 == null)
            return s1 == null && s2 == null;

        //Return false when they don't have the same reason
        if (s1.getID() != s2.getID())
            return false;

        //Return false when the properties are not the same
        if (!s1.getProperties().equals(s2.getProperties()))
            return false;

        return true;
    }
}

