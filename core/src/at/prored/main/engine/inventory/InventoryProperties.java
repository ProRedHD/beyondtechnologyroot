package at.prored.main.engine.inventory;

import at.prored.main.engine.items.ItemStack;

/**
 * Created by ProRed on 19.06.2018.
 */
public class InventoryProperties {
    private ItemStack floating;

    public InventoryProperties() {
    }

    public ItemStack getFloating() {
        return floating;
    }

    public void setFloating(ItemStack floating) {
        this.floating = floating;
    }
}
