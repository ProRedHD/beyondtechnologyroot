package at.prored.main.engine.inventory;

import at.prored.main.engine.items.ItemStack;
import com.badlogic.gdx.utils.Array;

/**
 * Created by ProRed on 20.06.2018.
 */
public class Inventory {

    private ItemStack[] data;
    private Array<InventoryObserver> observers;

    public Inventory(int size) {
        this.data = new ItemStack[size];
        this.observers = new Array<InventoryObserver>();
    }

    public void registerObserver(InventoryObserver observer){
        observers.add(observer);
    }

    public void deregisterObserver(InventoryObserver observer){
        observers.removeValue(observer, true);
    }

    public void notifyBeforeChange(){
        for (InventoryObserver observer : observers) {
            observer.beforeChange();
        }
    }

    public void notifyAfterChange(){
        for (InventoryObserver observer : observers) {
            observer.afterChange();
        }
    }

    public ItemStack[] getData() {
        return data;
    }

    public void setData(ItemStack[] data) {
        this.data = data;
    }
}
