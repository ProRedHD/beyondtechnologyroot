package at.prored.main.engine.inventory;

/**
 * Created by ProRed on 20.06.2018.
 */
public interface InventoryObserver {

    void beforeChange();

    void afterChange();

}
