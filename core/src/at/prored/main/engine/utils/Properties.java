package at.prored.main.engine.utils;

/**
 * Created by ProRed on 03.01.2018.
 */
public interface Properties {

    void setInteger(String key, int value);

    void setFloat(String key, float value);

    void setData(String key, Object value);

    void setBoolean(String key, boolean value);

    int getInteger(String key);

    float getFloat(String key);

    boolean getBoolean(String key);

    boolean hasData(String key);

    Object getData(String key);

    void clear();
}