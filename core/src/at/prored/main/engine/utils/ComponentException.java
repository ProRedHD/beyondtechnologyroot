package at.prored.main.engine.utils;

import at.prored.main.engine.components.Component;

/**
 * Created by ProRed on 21.08.2017.
 */
public class ComponentException extends RuntimeException {

    public ComponentException() {}

    public ComponentException(Class<? extends Component> compClass, String message){
        super(compClass.getSimpleName() + " cannot be constructed! | " + message);
    }
}
