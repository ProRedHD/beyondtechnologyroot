package at.prored.main.engine.utils;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;

/**
 * Created by ProRed on 16.08.2017.
 */
public class StringUtils {

    private static GlyphLayout glyphLayout;

    /**
     * Turns an string array into an sentence
     * ["hello", "this", "is", "cool"] -> hello, this, is and cool
     *
     * @param stringArray string array
     * @return sentence
     */
    public static String arrayToSentence(Array<String> stringArray) {
        StringBuilder stringBuilder = new StringBuilder();

        if (stringArray.size == 0) {
            return "";
        } else if (stringArray.size == 1) {
            return stringArray.first();
        } else {
            if (stringArray.size > 2) {
                for (int i = 0; i < stringArray.size - 2; i++) {
                    stringBuilder.append(stringArray.get(i));
                    stringBuilder.append(", ");
                }
            }

            stringBuilder.append(stringArray.get(stringArray.size - 2));
            stringBuilder.append(" and ");
            stringBuilder.append(stringArray.get(stringArray.size - 1));
        }

        return stringBuilder.toString();
    }

    public static String[] stringToArray(String array) {

        //Check for brackets
        if ((!array.startsWith("[") || !array.endsWith("]")) && (!array.startsWith("{") || !array.endsWith("}")))
            throw new GdxRuntimeException("Component cannot be constructed! (\"" + array + "\")");

        //Remove brackets
        array = array.substring(1, array.length() - 1);

        return array.split(",");
    }

    /**
     * Calculates the width of a text
     * @param text the text for width calculation
     * @param font font of text
     * @return width of text
     */
    public static float getTextWidth(String text, BitmapFont font){
        if(glyphLayout == null)
            glyphLayout = new GlyphLayout();

        glyphLayout.reset();
        glyphLayout.setText(font, text);

        return glyphLayout.width;
    }

    /**
     * Calculates the height of a text
     * @param text the text for height calculation
     * @param font font of text
     * @return height of text
     */
    public static float getTextHeight(String text, BitmapFont font){
        if(glyphLayout == null)
            glyphLayout = new GlyphLayout();

        glyphLayout.reset();
        glyphLayout.setText(font, text);

        return glyphLayout.height;
    }

    /**
     * Truncates a string until it fits into the given width
     * @param str str
     * @param width max width for the str to fit
     * @param font font used
     * @return truncated string
     */
    public static String truncateString(String str, float width, BitmapFont font){

        while(getTextWidth(str, font) > width){
            str = str.substring(0, str.length() - 3) + "..";
        }

        return str;
    }



}
