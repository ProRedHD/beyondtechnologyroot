package at.prored.main.engine.utils;

import at.prored.main.engine.collision.Collision;
import at.prored.main.engine.components.behavior.behaviors.events.TileChangeEvent;
import at.prored.main.engine.entities.EntityProperties;
import at.prored.main.engine.items.ItemProperties;
import at.prored.main.engine.tiles.TileProperties;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Pool;

/**
 * Created by ProRed on 18.05.2017.
 */
public class Pools {

    private static Pools instance;

    private Pool<TileProperties> tilePropertiesPool;

    private Pool<EntityProperties> entityPropertiesPool;

    private Pool<ItemProperties> itemPropertiesPool;

    private Pool<Collision> collisionPool;

    private Pool<Vector2> vector2Pool;

    private Pool<ObjectMap<String, Object>> hotbarItemStoragePool;

    private Pool<TileChangeEvent> tileChangeEventPool;

    private Pools() {
        initPools();
    }

    public static Pools instance() {
        if (instance == null)
            instance = new Pools();

        return instance;
    }

    private void initPools() {
        this.tilePropertiesPool = new Pool<TileProperties>() {
            @Override
            protected TileProperties newObject() {
                return new TileProperties();
            }
        };

        this.entityPropertiesPool = new Pool<EntityProperties>() {
            @Override
            protected EntityProperties newObject() {
                return new EntityProperties();
            }
        };

        this.itemPropertiesPool = new Pool<ItemProperties>() {
            @Override
            protected ItemProperties newObject() {
                return new ItemProperties();
            }
        };

        this.collisionPool = new Pool<Collision>() {
            @Override
            protected Collision newObject() {
                return new Collision();
            }
        };

        this.vector2Pool = new Pool<Vector2>() {
            @Override
            protected Vector2 newObject() {
                return new Vector2();
            }
        };

        this.hotbarItemStoragePool = new Pool<ObjectMap<String, Object>>() {
            @Override
            protected ObjectMap<String, Object> newObject() {
                return new ObjectMap<String, Object>();
            }
        };

        this.tileChangeEventPool = new Pool<TileChangeEvent>() {
            @Override
            protected TileChangeEvent newObject() {
                return new TileChangeEvent();
            }
        };
    }

    public Vector2 getVector2() {
        return getVector2Pool().obtain();
    }

    public void freeVector2(Vector2 vector2){
        this.getVector2Pool().free(vector2);
    }

    public Pool<TileProperties> getTilePropertiesPool() {
        return tilePropertiesPool;
    }

    public Pool<EntityProperties> getEntityPropertiesPool() {
        return entityPropertiesPool;
    }

    public Pool<ItemProperties> getItemPropertiesPool() {
        return itemPropertiesPool;
    }

    public Pool<Collision> getCollisionPool() {
        return collisionPool;
    }

    public Pool<Vector2> getVector2Pool() {
        return vector2Pool;
    }

    public Pool<ObjectMap<String, Object>> getHotbarItemStoragePool() {
        return hotbarItemStoragePool;
    }

    public Pool<TileChangeEvent> getTileChangeEventPool() {
        return tileChangeEventPool;
    }
}
