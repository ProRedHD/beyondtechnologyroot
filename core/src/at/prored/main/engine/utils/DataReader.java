package at.prored.main.engine.utils;

import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.saving.data.*;
import at.prored.main.engine.saving.data.base.Data;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.ObjectMap;


/**
 * Created by ProRed on 02.01.2017.
 */
public class DataReader {

    private ObjectMap<String, Data> dataMap;

    public DataReader(Array<Data> dataArray){
        this.dataMap = new ObjectMap<String, Data>();

        createDataMap(dataArray);
    }

    private void createDataMap(Array<Data> dataArray){
        for(Data data : dataArray){
            dataMap.put(data.getDataName(), data);
        }
    }

    public int getInt(String name) {
        if(dataMap.containsKey(name)) {
            return ((IntData) dataMap.get(name)).getValue();
        } else {
            throw new GdxRuntimeException("No data with name: " + name);
        }
    }

    public float getFloat(String name) {
        if(dataMap.containsKey(name)) {
            return ((FloatData) dataMap.get(name)).getValue();
        } else {
            throw new GdxRuntimeException("No data with name: " + name);
        }
    }

    public boolean getBoolean(String name) {
        if(dataMap.containsKey(name)) {
            return ((BooleanData) dataMap.get(name)).getValue();
        } else {
            throw new GdxRuntimeException("No data with name: " + name);
        }
    }

    public String getString(String name) {
        if(dataMap.containsKey(name)) {
            return ((StringData) dataMap.get(name)).getValue();
        } else {
            throw new GdxRuntimeException("No data with name: " + name);
        }
    }

    public ItemStack getItemStack(String name) {
        if(dataMap.containsKey(name)) {
            return ((ItemStackData) dataMap.get(name)).getValue();
        } else {
            throw new GdxRuntimeException("No data with name: " + name);
        }
    }

    public Data get(String name) {
        if(dataMap.containsKey(name)) {
            return dataMap.get(name);
        } else {
            throw new GdxRuntimeException("No data with name: " + name);
        }
    }

    public boolean hasData(String name){
        return dataMap.containsKey(name);
    }



}
