package at.prored.main.engine.utils;

import com.badlogic.gdx.assets.AssetManager;

/**
 * Created by ProRed on 18.05.2017.
 */
public interface AssetLoader {

    void queueAssets(AssetManager assetManager);

}
