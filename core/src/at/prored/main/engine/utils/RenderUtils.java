package at.prored.main.engine.utils;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.MathUtils;

/**
 * Created by ProRed on 10.12.2017.
 */
public class RenderUtils {

    /**
     * Draws lines with spritebatch (Credit goes to Cero on www.java-gaming.org)
     * @param batch batch
     * @param x1 x1
     * @param y1 y1
     * @param x2 x2
     * @param y2 y2
     * @param thickness thickness of the line
     * @param tex texture
     */
    public static void drawLine(SpriteBatch batch, float x1, float y1, float x2, float y2, float thickness, TextureRegion tex)
    {
        float length = Vector2.dst(x1, y1, x2, y2); // get distance between those 2 points
        float dx = x1;
        float dy = y1;
        dx = dx - x2;
        dy = dy - y2;
        float angle = MathUtils.radiansToDegrees*MathUtils.atan2(dy, dx);
        angle = angle-180;
        batch.draw(tex, x1, y1, 0f, thickness*0.5f, length, thickness, tex.getRegionWidth(), tex.getRegionHeight(), angle);
    }
}
