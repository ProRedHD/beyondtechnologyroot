package at.prored.main.engine.utils;

public interface Trigger {

    void trigger();

}
