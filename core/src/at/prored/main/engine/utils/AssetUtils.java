package at.prored.main.engine.utils;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by ProRed on 18.05.2017.
 */
public class AssetUtils {

    private AssetUtils(){}

    public static TextureAtlas loadAtlas(AssetManager assetManager){
        return assetManager.get("assets.atlas", TextureAtlas.class);
    }

    public static TextureRegion loadTextureRegion(AssetManager manager, String name){
        return loadAtlas(manager).findRegion(name);
    }

    public static NinePatch loadNinePatch(AssetManager assetManager, String name, int left, int right, int top, int bottom){
        return new NinePatch(loadTextureRegion(assetManager, name), left, right, top, bottom);
    }
}
