package at.prored.main.engine.utils;

import at.prored.main.engine.components.Component;
import com.badlogic.gdx.utils.Array;

/**
 * Created by ProRed on 17.08.2017.
 */
public interface Definition {

    enum Type {
        TILE, ENTITY, ITEM
    }

    int getID();
    <T extends Component> T getComponent(Class<T> component);
    boolean hasComponent(Class<? extends Component> component);
    Array<Component> getComponents();
    void addComponent(Component c);
    String getDefID();
    void setDefID(String defid);
}
