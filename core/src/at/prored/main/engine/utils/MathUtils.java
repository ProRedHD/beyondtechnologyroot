package at.prored.main.engine.utils;

/**
 * Created by ProRed on 18.05.2017.
 */
public class MathUtils {

    /**
     * Gets the relative block position of a chunk (X)
     */
    public static int getRelChunkPosX(int x) {
        return x < 0 ? 16 - ((x * -1) % 16) == 16 ? 0 : 16 - ((x * -1) % 16) : x % 16;
    }

    /**
     * Gets the relative block position of a chunk (Y)
     */
    public static int getRelChunkPosY(int y) {
        return y < 0 ? 16 - ((y * -1) % 16) == 16 ? 0 : 16 - ((y * -1) % 16) : y % 16;
    }
}
