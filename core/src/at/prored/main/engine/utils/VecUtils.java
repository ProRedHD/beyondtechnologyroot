package at.prored.main.engine.utils;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;

/**
 * Created by ProRed on 31.08.2017.
 */
public class VecUtils {

    private static Pool<Vector2> vecPool = Pools.instance().getVector2Pool();

    /**
     * Uses the sub method from the Vector2 class
     * @param vec1 vec1
     * @param vec2 vec2
     * @return pooled vector2 instance
     */
    public static Vector2 sub(Vector2 vec1, Vector2 vec2){
        return poolCpy(vec1).sub(vec2);
    }

    /**
     * Uses the add method from the Vector2 class
     * @param vec1 vec1
     * @param vec2 vec2
     * @return pooled vector2 instance
     */
    public static Vector2 add(Vector2 vec1, Vector2 vec2){
        return poolCpy(vec1).add(vec2);
    }

    /**
     * Adds the given value to x and y of the vector
     * @param vec vec
     * @param value value to add
     * @return vec (for chaining)
     */
    public static Vector2 add(Vector2 vec, float value){
        vec.add(value, value);

        return vec;
    }

    /**
     * Uses the scl method from the Vector2 class
     * @param vec1 vec1
     * @param scalar scalar
     * @return pooled vector2 instance
     */
    public static Vector2 scl(Vector2 vec1, float scalar){
        return poolCpy(vec1).scl(scalar);
    }

    private static Vector2 poolCpy(Vector2 vec){
        return vecPool.obtain().set(vec);
    }

    public static void free(Vector2 vec){
        vecPool.free(vec);
    }

}
