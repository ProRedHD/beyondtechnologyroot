package at.prored.main.engine.utils;

import com.badlogic.gdx.math.Vector2;

public enum Direction {
    UP(0,1),
    DOWN(0,-1),
    LEFT(-1,0),
    RIGHT(1,0);

    private int xoffset, yoffset;

    Direction(int xoffset, int yoffset) {
        this.xoffset = xoffset;
        this.yoffset = yoffset;
    }

    public int getXoffset() {
        return xoffset;
    }

    public int getYoffset() {
        return yoffset;
    }
}
