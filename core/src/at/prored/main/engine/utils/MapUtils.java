package at.prored.main.engine.utils;

import at.prored.main.engine.components.Component;
import at.prored.main.engine.components.DropComponent;
import at.prored.main.engine.components.behavior.BehaviorManager;
import at.prored.main.engine.components.behavior.EventType;
import at.prored.main.engine.components.behavior.behaviors.events.TileChangeEvent;
import at.prored.main.engine.entities.Entity;
import at.prored.main.engine.generation.WorldGenerator;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.tiles.TileDef;
import at.prored.main.engine.tiles.TileProperties;
import at.prored.main.engine.tiles.layers.LayerRegistry;
import at.prored.main.engine.tiles.storage.Chunk;
import at.prored.main.engine.tiles.storage.GameMap;
import at.prored.main.engine.tiles.tilecomponents.LayerComponent;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by ProRed on 04.06.2017.
 */
public class MapUtils {

    public static void addEntity(Entity e, GameMap map) {
        int chunkX = com.badlogic.gdx.math.MathUtils.floor(e.getEntityProperties().getPosition().x / 32f / (float) Chunk.CHUNK_SIZE);
        int chunkY = com.badlogic.gdx.math.MathUtils.floor(e.getEntityProperties().getPosition().y / 32f / (float) Chunk.CHUNK_SIZE);

        //Init entity
        map.getEngine().getEntityRegistry().initEntity(e);

        Vector2 help = Pools.instance().getVector2Pool().obtain();
        ObjectMap<Vector2, Chunk> chunks = map.getChunks();

        //Create Chunk if not there
        if (!chunks.containsKey(help.set(chunkX, chunkY))) {
            createChunk(map, chunkX, chunkY);
        }

        Chunk ch = chunks.get(help);
        ch.addEntity(e);

        Pools.instance().getVector2Pool().free(help);
    }

    public static void removeEntity(Entity e, GameMap map) {
        int chunkX = com.badlogic.gdx.math.MathUtils.floor(e.getEntityProperties().getPosition().x / 32f / (float) Chunk.CHUNK_SIZE);
        int chunkY = com.badlogic.gdx.math.MathUtils.floor(e.getEntityProperties().getPosition().y / 32f / (float) Chunk.CHUNK_SIZE);

        Vector2 help = Pools.instance().getVector2Pool().obtain().set(chunkX, chunkY);
        ObjectMap<Vector2, Chunk> chunks = map.getChunks();

        Chunk ch = chunks.get(help);
        ch.removeEntity(e);

        Pools.instance().getVector2Pool().free(help);
    }

    public static void placeTile(GameMap gameMap, int x, int y, int tileid, int layer) {
        int chunkX = com.badlogic.gdx.math.MathUtils.floor(x / (float) Chunk.CHUNK_SIZE);
        int chunkY = com.badlogic.gdx.math.MathUtils.floor(y / (float) Chunk.CHUNK_SIZE);

        //Obtain helper vector
        Vector2 help = Pools.instance().getVector2Pool().obtain();

        int airID = gameMap.getEngine().getTileRegistry().getTileDef("air").getID();

        ObjectMap<Vector2, Chunk> chunks = gameMap.getChunks();

        //Create Chunk if not there
        if (!chunks.containsKey(help.set(chunkX, chunkY))) {
            createChunk(gameMap, chunkX, chunkY);
        }

        //Check if the tile is allowed to exist on the layer
        TileDef def = gameMap.getEngine().getTileRegistry().getTileDef(tileid);
        LayerComponent layerComponent = def.getComponent(LayerComponent.class);

        if (!layerComponent.getLayers().contains(layer, false))
            return;

        Chunk ch = chunks.get(help);
        ch.setTileType(x, y, tileid, layer);
        ch.populateTileBoundingBoxes(gameMap.getEngine().getTileRegistry(), chunkX, chunkY);

        //Fire event when removed
        if (tileid == airID) {
            TileChangeEvent.ChangeType type = TileChangeEvent.ChangeType.REMOVED;
            fireTileChangeEvent(type, 0, 0, gameMap, x, y, layer, true, true);
        }

        //Fire event when added
        if (tileid != airID) {
            TileChangeEvent.ChangeType type = TileChangeEvent.ChangeType.ADDED;
            fireTileChangeEvent(type, 0, 0, gameMap, x, y, layer, true, true);
        }

        //Notify comps of addition
        for (Component component : def.getComponents()) {
            component.onTileAdded(ch.getTileProperties(x, y, layer));
        }

        Pools.instance().getVector2Pool().free(help);
    }

    public static void removeTile(GameMap gameMap, int x, int y, int layer) {

        int id = MapUtils.getTileType(gameMap, x, y, layer);
        TileProperties properties = MapUtils.getTileProperties(gameMap, x, y, layer);
        TileDef def = gameMap.getEngine().getTileRegistry().getTileDef(id);

        //Drop decide
        if(def.hasComponent(DropComponent.class)){
            DropComponent dc = def.getComponent(DropComponent.class);
            Array<ItemStack> itemStacks = dc.generateLoot(gameMap.getEngine(), def, properties);

            for (ItemStack itemStack : itemStacks) {

                //create item entity
                Entity e = Entity.createInstance(gameMap.getEngine().getEntityRegistry().getEntityDef("item").getID(), gameMap.getEngine());
                e.getEntityProperties().getPosition().set(x * 32 + MathUtils.random(4,12), y * 32 + 8);
                e.getEntityProperties().setData("stack", itemStack);

                //add it to the world
                addEntity(e, gameMap);
            }
        }

        //Remove tile
        placeTile(gameMap, x, y, gameMap.getEngine().getTileRegistry().getTileDef("air").getID(), layer);

    }

    public static int getTileType(GameMap gameMap, int x, int y, int layer) {
        int chunkX = com.badlogic.gdx.math.MathUtils.floor(x / (float) Chunk.CHUNK_SIZE);
        int chunkY = com.badlogic.gdx.math.MathUtils.floor(y / (float) Chunk.CHUNK_SIZE);

        Vector2 help = Pools.instance().getVector2Pool().obtain().set(chunkX, chunkY);

        Chunk ch = gameMap.getChunks().get(help);

        //Null check
        if (ch == null) {
            throw new GdxRuntimeException("Chunk [" + x + "|" + y + "] does not exist!");
        }

        int type = ch.getTileType(x, y, layer);

        Pools.instance().getVector2Pool().free(help);

        return type;
    }

    public static TileProperties getTileProperties(GameMap gameMap, int x, int y, int layer) {
        int chunkX = com.badlogic.gdx.math.MathUtils.floor(x / (float) Chunk.CHUNK_SIZE);
        int chunkY = com.badlogic.gdx.math.MathUtils.floor(y / (float) Chunk.CHUNK_SIZE);

        Vector2 help = Pools.instance().getVector2Pool().obtain();
        help.set(chunkX, chunkY);

        ObjectMap<Vector2, Chunk> chunks = gameMap.getChunks();

        Chunk ch = chunks.get(help);

        //Null check
        if (ch == null) {
            throw new GdxRuntimeException("Chunk [" + chunkX + "|" + chunkY + "] does not exist!");
        }

        TileProperties prop = ch.getTileProperties(x, y, layer);

        Pools.instance().getVector2Pool().free(help);

        return prop;
    }

    /**
     * Fires a tile change event
     *
     * @param changeType  changeType
     * @param gameMap     gameMap
     * @param x           x of target
     * @param y           y of target
     * @param layer       layer of target
     * @param layerSpread if other layers should get informed too
     */
    public static void fireTileChangeEvent(TileChangeEvent.ChangeType changeType, int srcOffsetX, int srcOffsetY, GameMap gameMap, int x, int y, int layer, boolean layerSpread, boolean neighbourSpread) {

        //Fire tile change event
        TileChangeEvent event = Pools.instance().getTileChangeEventPool().obtain().set(changeType, srcOffsetX, srcOffsetY);
        BehaviorManager.instance().broadcastTile(EventType.TILE_CHANGE, event, gameMap, x, y, layer);
        Pools.instance().getTileChangeEventPool().free(event);

        //Notify other layers too
        TileChangeEvent.ChangeType layerType;

        switch (changeType) {
            case ADDED:
                layerType = TileChangeEvent.ChangeType.LAYER_ADDED;
                break;
            case REMOVED:
                layerType = TileChangeEvent.ChangeType.LAYER_REMOVED;
                break;
            default:
            case CHANGED:
                layerType = TileChangeEvent.ChangeType.LAYER_CHANGED;
        }

        if (layerSpread) {
            for (int i = 0; i < LayerRegistry.getLayerCount(); i++) {
                if (i != layer)
                    fireTileChangeEvent(layerType, 0, 0, gameMap, x, y, i, false, false);
            }
        }

        if (neighbourSpread) {
            TileChangeEvent.ChangeType type;

            switch (changeType) {
                case ADDED:
                    type = TileChangeEvent.ChangeType.NEIGHBOUR_ADDED;
                    break;
                case REMOVED:
                    type = TileChangeEvent.ChangeType.NEIGHBOUR_REMOVED;
                    break;
                default:
                case CHANGED:
                    type = TileChangeEvent.ChangeType.NEIGHBOUR_CHANGED;
            }

            fireTileChangeEvent(type, 1, 0, gameMap, x - 1, y, layer, false, false);
            fireTileChangeEvent(type, -1, 0, gameMap, x + 1, y, layer, false, false);
            fireTileChangeEvent(type, 0, 1, gameMap, x, y - 1, layer, false, false);
            fireTileChangeEvent(type, 0, -1, gameMap, x, y + 1, layer, false, false);
        }
    }

    public static void createChunk(GameMap gameMap, int cx, int cy) {
        gameMap.getChunks().put(new Vector2(cx, cy), WorldGenerator.generateChunk(cx, cy, gameMap.getEngine()));
    }
}
