package at.prored.main.engine.init;

import at.prored.main.engine.crafting.Recipe;
import at.prored.main.engine.crafting.template.RecipeTemplate;
import at.prored.main.engine.entities.EntityDef;
import at.prored.main.engine.items.ItemDef;
import at.prored.main.engine.tiles.TileDef;
import at.prored.main.engine.utils.Definition;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;

public class ContentPackage {

    public enum ContentPackageSize {
        SMALL(16), MEDIUM(64), BIG(256), LARGE(1024), EXTREME(4096);
        private int size;

        ContentPackageSize(int size) {
            this.size = size;
        }

        public int getSize() {
            return size;
        }
    }

    private String packageID;
    private ContentPackageSize packageSize;
    private String packageVersion;

    private Array<Definition> tileDefs;
    private Array<Definition> entityDefs;
    private Array<Definition> itemDefs;
    private Array<RecipeTemplate> recipes;

    private ObjectMap<String, Definition> baseDefs;

    public ContentPackage(String packageID, ContentPackageSize packageSize, String packageVersion) {
        this.packageID = packageID;
        this.packageSize = packageSize;
        this.packageVersion = packageVersion;

        tileDefs = new Array<>();
        entityDefs = new Array<>();
        itemDefs = new Array<>();
        recipes = new Array<>();

        baseDefs = new ObjectMap<>();
    }

    public void addTileDef(TileDef def){
        tileDefs.add(def);
    }

    public void addEntityDef(EntityDef def){
        entityDefs.add(def);
    }

    public void addItemDef(ItemDef def){
        itemDefs.add(def);
    }

    public void addRecipe(RecipeTemplate recipe){
        recipes.add(recipe);
    }

    public ObjectMap<String, Definition> getBaseDefs() {
        return baseDefs;
    }

    /**
     * Returns a definition with the given defid
     * @param defid defid
     * @param type definition type
     * @return definition
     */
    public Definition getDefinition(String defid, Definition.Type type){

        Array<Definition> toBeSearched = getDefinitions(type);

        for (Definition definition : toBeSearched) {
            if(definition.getDefID().equals(defid))
                return definition;
        }

        return null;
    }

    /**
     * Returns a definition with the given id
     * @param id id
     * @param type definition type
     * @return definition
     */
    public Definition getDefinition(int id, Definition.Type type){

        Array<Definition> toBeSearched = getDefinitions(type);

        for (Definition definition : toBeSearched) {
            if(definition.getID() == id)
                return definition;
        }

        return null;
    }

    /**
     * Returns a array of all definition of that type
     * @param type type
     * @return array of definitions
     */
    public Array<Definition> getDefinitions(Definition.Type type){

        switch (type) {
            case TILE:
                return tileDefs;
            case ENTITY:
                return entityDefs;
            case ITEM:
                return itemDefs;
        }

        return null;
    }

    public String getPackageID() {
        return packageID;
    }

    public ContentPackageSize getPackageSize() {
        return packageSize;
    }

    public Array<RecipeTemplate> getRecipes() {
        return recipes;
    }
}
