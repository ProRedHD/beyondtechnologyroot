package at.prored.main.engine.init;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.Component;
import at.prored.main.engine.crafting.template.ItemStackTemplate;
import at.prored.main.engine.crafting.template.RecipeTemplate;
import at.prored.main.engine.entities.EntityDef;
import at.prored.main.engine.items.ItemDef;
import at.prored.main.engine.tiles.TileDef;
import at.prored.main.engine.utils.Definition;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.XmlReader;

import java.io.IOException;

public class ContentPackageLoader {

    private static ObjectMap<Component, XmlReader.Element> cacheMap = new ObjectMap<>();

    /**
     * Loads a content package from a file
     *
     * @param dir root directory
     * @param e   engine
     * @return loaded content package
     * @throws IOException
     */
    public static ContentPackage loadContentPackage(FileHandle dir, Engine e) throws IOException {
        ContentPackage contentPackage = createContentPackage(dir, e);
        e.getContentPackageRegistry().registerContentPackage(contentPackage);

        loadDefinitions(dir.child("/tiledefs"), Definition.Type.TILE, contentPackage, e);
        loadDefinitions(dir.child("/entitydefs"), Definition.Type.ENTITY, contentPackage, e);
        loadDefinitions(dir.child("/itemdefs"), Definition.Type.ITEM, contentPackage, e);

        loadRecipes(dir.child("/recipes"), contentPackage, e);

        return contentPackage;
    }

    /**
     * Creates a content package object an sets all data (id, size, version)
     *
     * @param root root directory
     * @param e    engine
     * @return content package
     * @throws IOException
     */
    private static ContentPackage createContentPackage(FileHandle root, Engine e) throws IOException {
        FileHandle properties = root.child("contentPackage.btcpp");

        XmlReader reader = new XmlReader();
        XmlReader.Element rootElement = reader.parse(properties);

        String packageID = rootElement.getChildByName("packageID").getAttribute("value");
        String packageSize = rootElement.getChildByName("packageSize").getAttribute("value");
        String packageVersion = rootElement.getChildByName("packageVersion").getAttribute("value");

        return new ContentPackage(packageID, ContentPackage.ContentPackageSize.valueOf(packageSize), packageVersion);
    }

    private static void loadRecipes(FileHandle dir, ContentPackage contentPackage, Engine e) throws IOException {
        XmlReader reader = new XmlReader();

        for (FileHandle fileHandle : dir.list(".btr")) {
            loadRecipe(fileHandle, contentPackage, reader, e);
        }
    }

    private static void loadRecipe(FileHandle fileHandle, ContentPackage contentPackage, XmlReader reader, Engine e) throws IOException {

        //don't load non existent defs
        if (!fileHandle.exists())
            return;

        XmlReader.Element rootElement = reader.parse(fileHandle);
        String type = fileHandle.nameWithoutExtension();

        for (XmlReader.Element recipe : rootElement.getChildrenByName("recipe")) {
            int tier = recipe.getInt("tier", 0);

            int minTime, maxTime;
            try {
                minTime = maxTime = recipe.getInt("time");
            } catch(GdxRuntimeException gre) {
                minTime = recipe.getInt("minTime", 1);
                maxTime = recipe.getInt("maxTime", 1);
            }

            RecipeTemplate rt = new RecipeTemplate(minTime, maxTime, type, tier, contentPackage.getPackageID());

            rt.setResult(ItemStackTemplate.createFromXML(recipe.getChildByName("result")));

            for (XmlReader.Element ingredient : recipe.getChildrenByName("ingredient")) {
                rt.getIngredients().add(ItemStackTemplate.createFromXML(ingredient));
            }

            //Register template
            contentPackage.addRecipe(rt);
        }
    }



    /**
     * Loads definitions into a definition and stores them into a content package
     *
     * @param dir            directory
     * @param contentPackage contentpackage
     * @param e              engine
     * @throws IOException
     */
    private static void loadDefinitions(FileHandle dir, Definition.Type type, ContentPackage contentPackage, Engine e) throws IOException {
        XmlReader reader = new XmlReader();

        int defCnt = 0;
        int startID = e.getContentPackageRegistry().getBeginID(contentPackage);
        for (FileHandle fileHandle : dir.list(".btd")) {
            loadDefinition(fileHandle, reader, startID, defCnt, type, contentPackage, e);

            //Find next free id
            defCnt = getNextFreeID(defCnt, startID, type, contentPackage);
        }
    }

    private static void loadDefinition(FileHandle fileHandle, XmlReader reader, int startID, int defCnt, Definition.Type type, ContentPackage contentPackage, Engine e) throws IOException {

        //don't load non existent defs
        if (!fileHandle.exists())
            return;

        XmlReader.Element rootElement = reader.parse(fileHandle);
        String defid = fileHandle.nameWithoutExtension();

        //Don't load defs twice
        if (contentPackage.getDefinition(defid, type) != null)
            return;

        //check if id is forced
        int forceID = rootElement.getIntAttribute("id", -1);

        Definition def = null;
        int id = forceID == -1 ? (startID + defCnt) : forceID;
        switch (type) {
            case TILE:
                def = new TileDef(id);
                break;
            case ENTITY:
                def = new EntityDef(id);
                break;
            case ITEM:
                def = new ItemDef(id);
                break;
        }
        def.setDefID(defid);

        //Copy components from parents
        String parent = rootElement.getAttribute("parent", null);
        String parameter = rootElement.getAttribute("parameter", null);
        if (parent != null) {
            if (parameter != null) {
                //load from template
                loadFromTemplate(fileHandle.sibling(parent + ".bttd"), reader, parameter, defCnt, startID, type, contentPackage, def, e);
            } else {
                int nextID = getNextFreeID(defCnt, startID, type, contentPackage);
                loadDefinition(fileHandle.sibling(parent + ".btd"), reader, startID, nextID, type, contentPackage, e);

                for (Component component : copyComponents(parent, type, contentPackage, e)) {
                    def.addComponent(component);
                }
            }
        }

        readComponents(rootElement, def, e);

        boolean shouldLoad = rootElement.getBooleanAttribute("load", true);
        if (shouldLoad) {
            switch (type) {
                case TILE:
                    contentPackage.addTileDef(((TileDef) def));
                    break;
                case ENTITY:
                    contentPackage.addEntityDef(((EntityDef) def));
                    break;
                case ITEM:
                    contentPackage.addItemDef(((ItemDef) def));
                    break;
            }
        } else {
            contentPackage.getBaseDefs().put(def.getDefID(), def);
        }
    }

    /**
     * Searches the next id and makes sure that there is no other definition with that id
     *
     * @param defCnt         current defCnt
     * @param startID        startID of the contentpackage
     * @param type           definition type
     * @param contentPackage content package
     * @return next free id
     */
    private static int getNextFreeID(int defCnt, int startID, Definition.Type type, ContentPackage contentPackage) {

        do {
            defCnt++;
        } while (contentPackage.getDefinition(startID + defCnt, type) != null);

        return defCnt;
    }

    /**
     * Reads components and adds them to the definition
     *
     * @param rootElement root element
     * @param def         definition
     * @param e           engine
     */
    private static void readComponents(XmlReader.Element rootElement, Definition def, Engine e) {

        cacheMap.clear();
        for (int i = 0; i < rootElement.getChildCount(); i++) {
            XmlReader.Element comp = rootElement.getChild(i);
            String name = comp.getName();

            //Read name and create object
            Component c = e.getComponentRegistry().createInstance(name);
            def.addComponent(c);

            cacheMap.put(c, comp);
        }

        //Construct all components
        for (Component component : def.getComponents()) {
            if (cacheMap.containsKey(component))
                component.constructComponent(cacheMap.get(component), e, def);
        }

        for (Component component : def.getComponents()) {
            if (cacheMap.containsKey(component))
                component.onFinishedCompLoading(def);
        }

    }

    /**
     * Loads all components from a template to the def
     *
     * @param template  filehandle to root of template
     * @param reader    reader
     * @param parameter parameters for the template
     * @param def       definition
     * @param e         engine
     * @throws IOException
     */
    private static void loadFromTemplate(FileHandle template, XmlReader reader, String parameter, int defCnt, int startID, Definition.Type type, ContentPackage contentPackage, Definition def, Engine e) throws IOException {

        //Construct parameter array
        String[] para = parameter.split(";");
        for (int i = 0; i < para.length; i++) {
            para[i] = para[i].trim();
        }

        XmlReader.Element rootElement = reader.parse(template);

        //Copy components from parents
        String parent = rootElement.getAttribute("parent", null);
        if (parent != null) {

            int nextID = getNextFreeID(defCnt, startID, type, contentPackage);
            loadDefinition(template.sibling(parent + ".btd"), reader, startID, nextID, type, contentPackage, e);

            for (Component component : copyComponents(parent, type, contentPackage, e)) {
                def.addComponent(component);
            }
        }

        cacheMap.clear();
        for (int i = 0; i < rootElement.getChildCount(); i++) {
            XmlReader.Element comp = rootElement.getChild(i);
            String name = comp.getName();

            //Read name and create object
            Component c = e.getComponentRegistry().createInstance(name);
            def.addComponent(c);

            //Replace all templates with values
            if (comp.getAttributes() != null) {
                //Replace attributes
                for (String s : comp.getAttributes().keys()) {
                    String value = comp.get(s);
                    for (int j = 0; j < para.length; j++) {
                        value = value.replace("{" + (j + 1) + "}", para[j]);
                    }
                    comp.getAttributes().remove(s);
                    comp.getAttributes().put(s, value);
                }
            }

            //Replace text
            String text = comp.getText();
            if (text != null) {
                for (int j = 0; j < para.length; j++) {
                    text = text.replace("{" + (j + 1) + "}", para[j]);
                }

                comp.setText(text);
            }

            cacheMap.put(c, comp);
        }

        //Construct all components
        for (Component component : def.getComponents()) {
            if (cacheMap.containsKey(component))
                component.constructComponent(cacheMap.get(component), e, null);
        }

        for (Component component : def.getComponents()) {
            if (cacheMap.containsKey(component))
                component.onFinishedCompLoading(def);
        }
    }

    /**
     * Copys all components from a def
     *
     * @param defid          defid
     * @param type           type (TILE/ENTITY/ITEM)
     * @param contentPackage current content package
     * @param e              engine
     * @return array of copied components
     */
    private static Array<Component> copyComponents(String defid, Definition.Type type, ContentPackage contentPackage, Engine e) {

        Definition def = null;
        Array<Component> componentsCopy = new Array<Component>();

        //Search own base components and components
        if (!defid.contains(":")) {
            def = contentPackage.getBaseDefs().get(defid);

            if (def == null) {
                def = contentPackage.getDefinition(defid, type);
            }

        }

        //Search content package
        if (defid.contains(":")) {
            String[] parts = defid.split(":");

            String packageID = parts[0];
            String defidpart = parts[1];

            ContentPackage contentPackage1 = e.getContentPackageRegistry().getContentPackage(packageID);
            def = contentPackage1.getDefinition(defidpart, type);
        }

        //Copy components if found
        if (def != null) {
            for (Component component : def.getComponents()) {
                componentsCopy.add(component.cpy());
            }
        }

        return componentsCopy;
    }

}
