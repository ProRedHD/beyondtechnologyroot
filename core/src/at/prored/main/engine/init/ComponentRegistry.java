package at.prored.main.engine.init;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.*;
import at.prored.main.engine.components.conduit.PowerConduitComponent;
import at.prored.main.engine.entities.components.*;
import at.prored.main.engine.items.components.HotbarActionComponent;
import at.prored.main.engine.items.components.TileComponent;
import at.prored.main.engine.items.components.ToolComponent;
import at.prored.main.engine.tiles.light.LightComponent;
import at.prored.main.engine.tiles.tilecomponents.*;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.ObjectMap;

import java.lang.reflect.InvocationTargetException;

public class ComponentRegistry {

    private Engine e;
    private ObjectMap<String, Class<? extends Component>> components;

    public ComponentRegistry(Engine e){
        this.e = e;

        components = new ObjectMap<String, Class<? extends Component>>();

        register(RenderableComponent.class);
        register(TextureStorageComponent.class);
        register(LocalizableComponent.class);
        register(PhysicsComponent.class);
        register(LightComponent.class);
        register(MaterialComponent.class);
        register(LayerComponent.class);
        register(RestrictedPlacementComponent.class);
        register(GrowableComponent.class);
        register(ConnectedTextureComponent.class);
        register(PowerConduitComponent.class);
        register(StateComponent.class);
        register(StatedAnimationComponent.class);
        register(KineticComponent.class);
        register(RotateComponent.class);
        register(FlipComponent.class);
        register(GravityComponent.class);
        register(UserControlComponent.class);
        register(InventoryComponent.class);
        register(HotbarComponent.class);
        register(ItemComponent.class);
        register(HotbarActionComponent.class);
        register(ToolComponent.class);
        register(TileComponent.class);
        register(InventoryIconComponent.class);
        register(SlotIconComponent.class);
        register(CreativeInventoryComponent.class);
        register(DropComponent.class);
        register(RecipeDiscoverComponent.class);
        register(ReplaceableComponent.class);
        register(TooltipComponent.class);
    }

    public Component createInstance(String name){

        try {
            return components.get(name).getConstructor().newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (NullPointerException e){
            throw new GdxRuntimeException("Component " + name + " is not registered");
        }

        throw new GdxRuntimeException("Could not create component! (" + name + ")");
    }

    public void register(Class<? extends Component> clazz){
        components.put(clazz.getSimpleName(), clazz);
    }
}
