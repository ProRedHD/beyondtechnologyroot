package at.prored.main.engine.init;

import at.prored.main.engine.Engine;
import at.prored.main.engine.crafting.template.RecipeTemplate;
import at.prored.main.engine.entities.EntityDef;
import at.prored.main.engine.items.ItemDef;
import at.prored.main.engine.tiles.TileDef;
import at.prored.main.engine.utils.Definition;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.ObjectMap;

import java.io.FilenameFilter;
import java.io.IOException;

/**
 * Created by ProRed on 11.06.2018.
 */
public class ContentPackageRegistry {

    private Engine e;

    private ObjectMap<String, ContentPackage> contentPackages;
    private ObjectMap<String, Integer> beginId;
    private int newID = 0;

    public ContentPackageRegistry(Engine e) {
        this.e = e;

        this.contentPackages = new ObjectMap<String, ContentPackage>();
        this.beginId = new ObjectMap<String, Integer>();
    }

    /**
     * Returns the content package of the content id
     * @param id id
     * @return content package
     */
    public ContentPackage getContentPackageFromContentID(int id){
        ContentPackage found = null;
        for (ContentPackage contentPackage : contentPackages.values()) {
            if(id >= getBeginID(contentPackage))
                found = contentPackage;
        }

        return found;
    }

    public void readAllContentPackages(){
        FileHandle dir = Gdx.files.internal("Beyond Technology/contentpackages");

        FilenameFilter fnf = (dir1, name) -> dir1.isDirectory();

        FileHandle[] list = dir.list(fnf);

        for (FileHandle fileHandle : list) {
            try {
                ContentPackage contentPackage = ContentPackageLoader.loadContentPackage(fileHandle, e);

                for (Definition tileDef : contentPackage.getDefinitions(Definition.Type.TILE)) {
                    e.getTileRegistry().registerTileDef(tileDef.getID(), (TileDef) tileDef);
                }

                for (Definition entityDef : contentPackage.getDefinitions(Definition.Type.ENTITY)) {
                    e.getEntityRegistry().registerEntityDef(entityDef.getID(), (EntityDef) entityDef);
                }

                for (Definition itemDef : contentPackage.getDefinitions(Definition.Type.ITEM)) {
                    e.getItemRegistry().registerItemDef(itemDef.getID(), (ItemDef) itemDef);
                }

                for (RecipeTemplate recipeTemplate : contentPackage.getRecipes()) {
                    e.getRecipeRegistry().registerRecipe(recipeTemplate);
                }
            } catch (IOException e1) {
                Gdx.app.error("ContentPackage", "Error loading content package: " + fileHandle.name());
            }
        }

        Gdx.app.log("ContentLoader", "Read " + e.getTileRegistry().size() + " tile definitions.");
        Gdx.app.log("ContentLoader", "Read " + e.getEntityRegistry().size() + " entity definitions.");
        Gdx.app.log("ContentLoader", "Read " + e.getItemRegistry().size() + " item definitions.");
        Gdx.app.log("ContentLoader", "Read " + e.getRecipeRegistry().size() + " recipe definitions.");
    }

    public ContentPackage getContentPackage(String packageID){
        return contentPackages.get(packageID);
    }

    public void registerContentPackage(ContentPackage contentPackage){
        contentPackages.put(contentPackage.getPackageID(), contentPackage);
        beginId.put(contentPackage.getPackageID(), newID);
        newID += contentPackage.getPackageSize().getSize();
    }

    public int getBeginID(ContentPackage contentPackage){
        return beginId.get(contentPackage.getPackageID());
    }


}
