package at.prored.main.engine.generation;

import at.prored.main.engine.Engine;
import at.prored.main.engine.tiles.TileDef;
import at.prored.main.engine.tiles.storage.Chunk;
import at.prored.main.engine.utils.FastNoise;
import at.prored.main.engine.utils.Pools;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by ProRed on 04.06.2017.
 */
public class WorldGenerator {

    private static final FastNoise noiseGen = new FastNoise((int) (System.currentTimeMillis() / 1000));
    private static final int EXCESS_NULL = -255525;

    private static ObjectMap<Vector2, Integer> excessBlocks = new ObjectMap<Vector2, Integer>();

    private static float highest = 0.6f;

    public static Chunk generateChunk(int cx, int cy, Engine e) {
        Chunk ch = new Chunk();

        TileDef air = e.getTileRegistry().getTileDef("air");
        TileDef stone = e.getTileRegistry().getTileDef("stone");
        TileDef dirt = e.getTileRegistry().getTileDef("dirt");
        TileDef grass = e.getTileRegistry().getTileDef("grass");
        TileDef flower = e.getTileRegistry().getTileDef("flower");
        TileDef bush = e.getTileRegistry().getTileDef("bush");
        TileDef tallgrass = e.getTileRegistry().getTileDef("tallgrass");

        if (cy < 0) {
            for (int x = 0; x < 16; x++) {
                for (int y = 0; y < 16; y++) {
                    noiseGen.SetFractalType(FastNoise.FractalType.Billow);
                    float value = FastNoise.generate(noiseGen, (cx * 16 + x), (cy * 16 + y), 1.5f, 0.1f, 5);
                    noiseGen.SetFractalType(FastNoise.FractalType.FBM);
                    value = (value + 1) / 2;

                    highest = Math.max(highest, value);

                    if (value > highest / 2f) {
                        ch.setTileTypeRelative(x, y, dirt.getID(), 0);
                    } else {
                        ch.setTileTypeRelative(x, y, stone.getID(), 0);
                    }

                    int id;
                    if ((id = getExcess(cx, cy, x, y)) != EXCESS_NULL)
                        ch.setTileType(x, y, id, 0);
                }
            }

            //Cave generation
            for (int y = 0; y < 16; y++) {
                for (int x = 0; x < 16; x++) {
                    if (CaveGenerator.solid(cx * 16 + x, cy * 16 + y)) {
                        ch.setTileType(x, y, air.getID(), 0);
                    } else if (CaveGenerator.solid2(cx * 16 + x, cy * 16 + y)) {
                        ch.setTileType(x, y, air.getID(), 0);
                    }
                }
            }


        } else {
            for (int x = 0; x < 16; x++) {
                int terrainHeight = (int) (32 * Math.abs((FastNoise.generate(noiseGen, cx * Chunk.CHUNK_SIZE + x, 0, 2f, .04f, 8) + 1) / 2));
                for (int y = 0; y < 16; y++) {
                    float tY = (cy * Chunk.CHUNK_SIZE) + y;
                    if (tY < terrainHeight) {
                        ch.setTileTypeRelative(x, y, dirt.getID(), 0);
                    } else if (tY == terrainHeight) {
                        ch.setTileTypeRelative(x, y, grass.getID(), 0);
                    } else if (tY == terrainHeight + 1) {
                        int random = MathUtils.random(0, 4);
                        if (random == 0) {
                            ch.setTileTypeRelative(x, y, flower.getID(), 0);
                        } else if(random == 1){
                            ch.setTileTypeRelative(x, y, tallgrass.getID(), 0);

                         //   tallgrass.getComponent(GrowableComponent.class).setStage(ch.getTilePropertiesRelative(x,y,0), MathUtils.random(0, 2));
                            } else if(random == 2){
                            ch.setTileTypeRelative(x, y, bush.getID(), 0);
                        }
                    }
                }
            }
        }

        //Smooth out border between dirt and stone
        if (cy == 0) {
            for (int x = 0; x < 16; x++) {
                int stoneHeight = (int) (16 * Math.abs((FastNoise.generate(noiseGen, (cx * Chunk.CHUNK_SIZE + x) * 16, 0, 2f, .005f, 5) + 1) / 2));
                for (int y = 0; y < 16; y++) {

                    int id;
                    if ((id = getExcess(cx, cy, x, y)) != EXCESS_NULL) {
                        ch.setTileType(x, y, id, 0);
                        continue;
                    }

                    if (y <= stoneHeight) {
                        ch.setTileType(x, y, stone.getID(), 0);
                    }
                }
            }
        }

        return ch;
    }

    private static void putIntoExcess(int cx, int cy, int rx, int ry, int id) {
        int tX = cx * Chunk.CHUNK_SIZE + rx;
        int tY = cy * Chunk.CHUNK_SIZE + ry;

        Vector2 vec = Pools.instance().getVector2();
        vec.set(tX, tY);

        excessBlocks.put(new Vector2(tX, tY), id);
    }

    private static int getExcess(int cx, int cy, int rx, int ry) {
        int tX = cx * Chunk.CHUNK_SIZE + rx;
        int tY = cy * Chunk.CHUNK_SIZE + ry;

        Vector2 vec = Pools.instance().getVector2();
        vec.set(tX, tY);

        if (excessBlocks.containsKey(vec)) {
            Integer id = excessBlocks.get(vec);
            excessBlocks.remove(vec);

            Pools.instance().freeVector2(vec);

            return id;
        }

        Pools.instance().freeVector2(vec);

        return EXCESS_NULL;
    }

    public static boolean solid(int cy, int ry) {
        return (cy < 0) || (cy == 0 && ry < 8);
    }
}


