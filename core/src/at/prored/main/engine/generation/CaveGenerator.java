package at.prored.main.engine.generation;

import at.prored.main.engine.utils.FastNoise;

import java.util.Random;

public class CaveGenerator {

    private static FastNoise noise = new FastNoise(new Random().nextInt());
    private static FastNoise noise2 = new FastNoise(new Random().nextInt());

    private static float highestCaveLayerValue = 0;
    private static float highestCaveValue = 0;
    private static float highestCaveValue2 = 0;

    private static final int CAVE_HEIGHT = 12;
    private static final int CAVE_FLOOR_DEPTH = 9;
    private static final int CAVE_OFFSET = 45;

    public static boolean solid(int x, int y){

        noise.SetFractalType(FastNoise.FractalType.Billow);
        float value = FastNoise.generate(noise, x, y, 2f, .25f, 0.08f, 5);
        value = (value + 1)/2f;

        highestCaveValue = Math.max(highestCaveValue, value);

        return value > highestCaveValue/2;
    }

    public static boolean solid2(int x, int y){
        noise2.SetFractalType(FastNoise.FractalType.Billow);
        float value = FastNoise.generate(noise2, x, y, 0f, .1f, 0.07f, 2);
        value = (value + 1)/2f;

        highestCaveValue2 = Math.max(highestCaveValue2, value);

        return value > highestCaveValue2/2;
    }

    public static int getCaveHeight(int x, int y){
        x = Math.abs(x);

        noise.SetFractalType(FastNoise.FractalType.FBM);

        //float chance = FastNoise.generate(noise, x, 0, 2f, 0.3f, 1);
        float chance1 = FastNoise.generate(noise, x, 0, 2f, 0.003f, 1);
        float chance = (float) Math.sin(Math.toRadians(x*11.25f));
        chance = (chance*chance1 + 1) / 2f;

        int caveHeight = (int) (CAVE_HEIGHT * ((FastNoise.generate(noise, x, y*8192, 1f, .2f, 4)+1)/2));
        caveHeight = (int) (caveHeight * (chance));

        return caveHeight > 1 ? caveHeight : 0;
    }

    public static int getCaveFloorDepth(int x, int y){
        x = Math.abs(x);

        noise.SetFractalType(FastNoise.FractalType.FBM);

        float chance1 = FastNoise.generate(noise, x, 0, 2f, 0.003f, 1);
        float chance = (float) Math.sin(Math.toRadians(x*11.25f));
        chance = (chance*chance1 + 1) / 2f;

        int caveFloor = (int) (CAVE_FLOOR_DEPTH * ((FastNoise.generate(noise, x, y*1024, 1f, .002f, 1)+1)/2));
        caveFloor = (int) (caveFloor * (chance));

        return caveFloor;
    }

    public static int getCaveOffset(int x, int y){
        x = Math.abs(x);

        noise.SetFractalType(FastNoise.FractalType.FBM);

        float chance1 = FastNoise.generate(noise, x, 0, 2f, 0.01f, 5);
        float chance = (float) Math.sin(Math.toRadians(x*35f));
        chance = (chance*chance1 + 1) / 2f;

        return (int) (chance * CAVE_OFFSET); //(int) (CAVE_OFFSET * ((FastNoise.generate(noise, x, y*8192, 2f, .02f, 5)+1)/2));
    }


    public static boolean isCaveLayer(int y){
        noise.SetFractalType(FastNoise.FractalType.Billow);
        float value = FastNoise.generate(noise, y*1024, 0, 2f, .04f, 1);
        value = (value + 1) / 2f;

        //find highest
        highestCaveLayerValue = Math.max(highestCaveLayerValue, value);
        return value > (highestCaveLayerValue - highestCaveLayerValue / 32);
    }

}
