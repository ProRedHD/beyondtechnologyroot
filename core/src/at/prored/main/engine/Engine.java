package at.prored.main.engine;

import at.prored.main.engine.crafting.RecipeRegistry;
import at.prored.main.engine.entities.EntityRegistry;
import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.graphics.screens.GameScreenManager;
import at.prored.main.engine.guis.GUIManager;
import at.prored.main.engine.init.ComponentRegistry;
import at.prored.main.engine.init.ContentPackageRegistry;
import at.prored.main.engine.items.ItemRegistry;
import at.prored.main.engine.tiles.TileRegistry;
import at.prored.main.engine.tiles.layers.LayerRegistry;
import at.prored.main.engine.utils.AssetLoader;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.FPSLogger;

/**
 * Created by ProRed on 18.05.2017.
 */
public class Engine {

    private Game game;

    private GameScreenManager gameScreenManager;
    private GUIManager guiManager;

    private AssetManager assetManager;
    private AssetLoader assetLoader;
    private InputMultiplexer inputMultiplexer;

    private TileRegistry tileRegistry;
    private EntityRegistry entityRegistry;
    private ItemRegistry itemRegistry;
    private LayerRegistry layerRegistry;
    private RecipeRegistry recipeRegistry;
    private ComponentRegistry componentRegistry;
    private ContentPackageRegistry contentPackageRegistry;

    private FPSLogger logger;

    public Engine(Game game) {
        this.game = game;
        this.gameScreenManager = new GameScreenManager(this);
        this.guiManager = new GUIManager(this);

        this.assetManager = new AssetManager();

        this.tileRegistry = new TileRegistry(this);
        this.entityRegistry = new EntityRegistry(this);
        this.itemRegistry = new ItemRegistry(this);
        this.layerRegistry = new LayerRegistry();
        this.recipeRegistry = new RecipeRegistry(this);
        this.componentRegistry = new ComponentRegistry(this);
        this.contentPackageRegistry = new ContentPackageRegistry(this);

        this.logger = new FPSLogger();
    }

    public void init(){
        this.inputMultiplexer = new InputMultiplexer();
        Gdx.input.setInputProcessor(inputMultiplexer);

        //Initializes GUIManager
        guiManager.init(assetManager);
    }

    public void update() {
        Graphics.instance().update();
        gameScreenManager.update();
        guiManager.update();

        logger.log();
    }

    public void render() {

    }

    public Game getGame() {
        return game;
    }

    public GameScreenManager getGameScreenManager() {
        return gameScreenManager;
    }

    public GUIManager getGUIManager() {
        return guiManager;
    }

    public AssetManager getAssetManager() {
        return assetManager;
    }

    public InputMultiplexer getInputMultiplexer() {
        return inputMultiplexer;
    }

    public TileRegistry getTileRegistry() {
        return tileRegistry;
    }

    public EntityRegistry getEntityRegistry() {
        return entityRegistry;
    }

    public ItemRegistry getItemRegistry() {
        return itemRegistry;
    }

    public LayerRegistry getLayerRegistry() {
        return layerRegistry;
    }

    public RecipeRegistry getRecipeRegistry() {
        return recipeRegistry;
    }

    public ComponentRegistry getComponentRegistry() {
        return componentRegistry;
    }

    public ContentPackageRegistry getContentPackageRegistry() {
        return contentPackageRegistry;
    }

    public void setAssetLoader(AssetLoader assetLoader) {
        this.assetLoader = assetLoader;

        if (assetLoader != null) {
            assetLoader.queueAssets(this.assetManager);
            this.assetManager.finishLoading();
        }
    }
}
