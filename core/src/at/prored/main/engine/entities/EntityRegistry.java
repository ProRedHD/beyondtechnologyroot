package at.prored.main.engine.entities;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.Component;
import at.prored.main.engine.init.ContentPackage;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by ProRed on 18.05.2017.
 */
public class EntityRegistry {

    private Engine engine;

    private ObjectMap<Integer, EntityDef> entityDefs;
    private ObjectMap<String, Integer> defIDs;

    public EntityRegistry(Engine engine) {
        this.engine = engine;

        this.entityDefs = new ObjectMap<>();
        this.defIDs = new ObjectMap<>();
    }

    public int size(){
        return entityDefs.size;
    }

    /**
     * Loads up values to the entities properties object
     *
     * @param e entity
     */
    public void initEntity(Entity e) {
        if (!hasEntityDef(e.getID()))
            return;

        EntityDef def = getEntityDef(e.getID());

        for (Component component : def.getComponents()) {
            component.onEntityAdded(e);
        }
    }

    /**
     * Registers a entity definition
     *
     * @param id        id of this entity
     * @param entityDef entity def for this entity
     */
    public void registerEntityDef(int id, EntityDef entityDef) {
        this.entityDefs.put(id, entityDef);

        //Store defid
        ContentPackage contentPackage = engine.getContentPackageRegistry().getContentPackageFromContentID(id);
        this.defIDs.put(contentPackage.getPackageID() + ":" + entityDef.getDefID(), id);
    }

    /**
     * Unregisters a entity definition
     *
     * @param id id of the entity
     */
    public void unregisterEntityDef(int id) {
        this.entityDefs.remove(id);
    }

    /**
     * Checks if a entity id is registered / if a entity def is available
     *
     * @param id id of the entity
     * @return true or false
     */
    public boolean hasEntityDef(int id) {
        return this.entityDefs.containsKey(id);
    }

    /**
     * Returns a entity definition
     *
     * @param id id of the entity
     * @return entity definition
     */
    public EntityDef getEntityDef(int id) {
        return this.entityDefs.get(id);
    }

    public EntityDef getEntityDef(String defid) {
        if (!defid.contains(":"))
            defid = "btcontent:" + defid;


        Integer id = defIDs.get(defid);

        if(id == null)
            return null;

        return getEntityDef(id);
    }

    public Engine getEngine() {
        return engine;
    }
}
