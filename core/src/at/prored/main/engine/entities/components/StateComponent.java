package at.prored.main.engine.entities.components;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.Component;
import at.prored.main.engine.entities.Entity;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.tiles.TileProperties;
import at.prored.main.engine.tiles.storage.GameMap;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import at.prored.main.engine.utils.StringUtils;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

import java.util.Arrays;

/**
 * Created by ProRed on 03.07.2017.
 */
public class StateComponent extends EntityComponent {

    private String[] states;
    private int defaultState;

    public StateComponent() {
    }

    public StateComponent(String[] states, int defaultState) {
        this.states = states;
        this.defaultState = defaultState;
    }

    @Override
    public void onEntityAdded(Entity entity) {
        if (!entity.getEntityProperties().hasData("state"))
            entity.getEntityProperties().setInteger("state", defaultState);
    }

    @Override
    public void onTileAdded(TileProperties tileProperties) {
        if (!tileProperties.hasData("state"))
            tileProperties.setInteger("state", defaultState);
    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {

        this.states = element.getText().split(",");
        for (int i = 0; i < states.length; i++) {
            states[i] = states[i].trim();
        }

        this.defaultState = Integer.parseInt(element.getAttribute("default", "0"));

        Gdx.app.debug("StateComponent", "Successfully loaded state" + (this.states.length > 1 ? "s " : " ") + StringUtils.arrayToSentence(new Array<String>(this.states)));
    }

    @Override
    public Component cpy() {
        return new StateComponent(Arrays.copyOf(states, states.length), defaultState);
    }

    /**
     * Sets the state of the entity to the given state
     *
     * @param state state
     * @param p     properties
     */
    public void setState(int state, Properties p) {
        p.setInteger("state", state);
        p.setFloat("stateTime", 0);
        Gdx.app.debug("StateComponent", "Set state of entity/tile to " + states[state] + "(" + state + ")");
    }

    /**
     * Returns the state of the entity
     *
     * @param p properties
     * @return state
     */
    public int getState(Properties p) {
        return p.hasData("state") ? p.getInteger("state") : defaultState;
    }

    /**
     * Returns the name of the state
     *
     * @param state state index
     * @return state name
     */
    public String getStateName(int state) {
        return this.states[state];
    }

    @Override
    public void update(Entity e, GameMap gameMap) {

    }
}
