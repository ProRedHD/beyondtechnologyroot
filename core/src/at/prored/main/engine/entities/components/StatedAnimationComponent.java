package at.prored.main.engine.entities.components;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.Component;
import at.prored.main.engine.components.RenderableComponent;
import at.prored.main.engine.components.TextureStorageComponent;
import at.prored.main.engine.entities.Entity;
import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.graphics.renderer.CustomTextureRenderer;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.tiles.storage.GameMap;
import at.prored.main.engine.tiles.tilecomponents.GrowableComponent;
import at.prored.main.engine.utils.AssetUtils;
import at.prored.main.engine.utils.ComponentException;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 17.08.2017.
 */
public class  StatedAnimationComponent extends EntityComponent {

    private static final String PLAY_MODE = "playMode";

    private ObjectMap<Integer, Animation<TextureRegion>> animations;

    public StatedAnimationComponent() {
        this.animations = new ObjectMap<Integer, Animation<TextureRegion>>();
    }

    public StatedAnimationComponent(ObjectMap<Integer, Animation<TextureRegion>> animations) {
        this.animations = animations;
    }

    public Animation<TextureRegion> getAnimation(int state) {
        return animations.get(state);
    }

    public void setPlayMode(Properties p, Animation.PlayMode playMode){
        p.setData(PLAY_MODE, playMode);
    }

    public Animation.PlayMode getPlayMode(Properties p){
        return p.hasData(PLAY_MODE) ? (Animation.PlayMode) p.getData(PLAY_MODE) : Animation.PlayMode.NORMAL;
    }

    @Override
    public void onFinishedCompLoading(Definition def) {
        if(!def.hasComponent(TextureStorageComponent.class))
            throw new ComponentException(this.getClass(), "Required Component not found: TextureStorageComponent");

        if(def.hasComponent(RenderableComponent.class))
            throw new ComponentException(this.getClass(), "Not compatible with RenderableComponent");

        if(def.hasComponent(GrowableComponent.class))
            throw new ComponentException(this.getClass(), "Not compatible with GrowableComponent");

        def.getComponent(TextureStorageComponent.class).setCustomTextureRenderer(new AnimationRenderer());
    }

    @Override
    public void onEntityAdded(Entity entity) {
        if (!entity.getEntityProperties().hasData("stateTime"))
            entity.getEntityProperties().setFloat("stateTime", 0f);
    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {

        Array<XmlReader.Element> animations = element.getChildrenByName("animation");

        for(XmlReader.Element element1 : animations) {
            try {
                int state = Integer.parseInt(element1.getAttribute("state"));
                String path = element1.getAttribute("path");
                float duration = Float.parseFloat(element1.getAttribute("duration", "1"));
                int width = Integer.parseInt(element1.getAttribute("width", "32"));
                int height = Integer.parseInt(element1.getAttribute("height", "32"));

                TextureRegion regions = AssetUtils.loadAtlas(e.getAssetManager()).findRegion(path);
                Animation<TextureRegion> animationObj = new Animation<TextureRegion>(duration, regions.split(width, height)[0]);

                this.animations.put(state, animationObj);
            } catch (NumberFormatException ex) {
                throw new GdxRuntimeException("StatedAnimationComponent cannot be constructed! (id:" + def.getID() + "), (" + ex.getMessage() + ")");
            }

        }

        Gdx.app.debug("StatedAnimationComponent", "Successfully loaded animations for id " + def.getID());
    }

    @Override
    public Component cpy() {
        ObjectMap<Integer, Animation<TextureRegion>> anis = new ObjectMap<Integer, Animation<TextureRegion>>();
        for (Integer integer : animations.keys()) {
            anis.put(integer, animations.get(integer));
        }
        return new StatedAnimationComponent(anis);
    }

    @Override
    public void update(Entity e, GameMap gameMap) {

    }

    public class AnimationRenderer implements CustomTextureRenderer {

        @Override
        public void render(SpriteBatch batch, float x, float y, float scaleX, float scaleY, OrthographicCamera camera, Properties properties, Definition definition, Engine engine, TextureStorageComponent textureStorageComponent) {
            StatedAnimationComponent sAComp = definition.getComponent(StatedAnimationComponent.class);
            StateComponent stateComp = definition.getComponent(StateComponent.class);

            //Render first texture when not in world
            if(properties == null){
                Animation<TextureRegion> animation = sAComp.getAnimation(0);
                TextureRegion textureRegion = animation.getKeyFrame(0, true);

                textureStorageComponent.draw(textureRegion, batch, x, y, scaleX, scaleY, camera, null, definition, engine);
                return;
            }

            int state = stateComp.getState(properties);
            Animation<TextureRegion> animation = sAComp.getAnimation(state);

            //Return if animation doesn't exist
            if (animation == null)
                return;

            animation.setPlayMode(getPlayMode(properties));

            float stateTime = properties.hasData("stateTime") ? properties.getFloat("stateTime") : 0f;

            TextureRegion textureRegion = animation.getKeyFrame(stateTime, true);
            textureStorageComponent.draw(textureRegion, batch, x, y, scaleX, scaleY, camera, properties, definition, engine);

            //Increase StateTime
            properties.setFloat("stateTime", stateTime + Graphics.instance().getDeltaTime());

        }
    }
}
