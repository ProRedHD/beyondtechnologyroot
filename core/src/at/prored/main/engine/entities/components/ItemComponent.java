package at.prored.main.engine.entities.components;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.Component;
import at.prored.main.engine.components.InventoryComponent;
import at.prored.main.engine.components.TextureStorageComponent;
import at.prored.main.engine.entities.Entity;
import at.prored.main.engine.entities.EntityProperties;
import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.graphics.renderer.CustomTextureRenderer;
import at.prored.main.engine.inventory.InventoryUtils;
import at.prored.main.engine.items.ItemRenderer;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.saving.data.ItemStackData;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.tiles.storage.GameMap;
import at.prored.main.engine.utils.*;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 16.01.2018.
 */
public class ItemComponent extends EntityComponent {

    public ItemComponent() {
    }

    @Override
    public void onFinishedCompLoading(Definition def) {
        if(!def.hasComponent(TextureStorageComponent.class))
            throw new ComponentException(this.getClass(), "Required Component not found: TextureStorageComponent");

        def.getComponent(TextureStorageComponent.class).setCustomTextureRenderer(new CustomItemRenderer());
    }

    public void setItemStack(ItemStack stack, EntityProperties properties){
        properties.setData("stack", stack);
    }

    public ItemStack getItemStack(Properties properties){
        return (ItemStack) properties.getData("stack");
    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {
        if (data != null) {
            data.add(new ItemStackData("stack", (ItemStack) properties.getData("stack")));
        }
    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {
        DataReader reader = new DataReader(data);
        properties.setData("stack", reader.getItemStack("stack"));
    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {

    }

    @Override
    public Component cpy() {
        return new ItemComponent();
    }

    @Override
    public void update(Entity e, GameMap gameMap) {
        Entity player = gameMap.getPlayer();

        Vector2 playerPos = player.getEntityProperties().getPosition().cpy().add(24,0);
        float dst = playerPos.dst(e.getEntityProperties().getPosition());

        if(dst < 5) {
            InventoryUtils.addItem(getItemStack(e.getEntityProperties()), InventoryComponent.getInventory(player));
            MapUtils.removeEntity(e, gameMap);
        } else if(dst < 68){
            Vector2 traj = VecUtils.sub(playerPos, e.getEntityProperties().getPosition());
            e.getEntityProperties().getPosition().add(traj.nor().scl(96*Graphics.instance().getDeltaTime()));
            VecUtils.free(traj);
        }
    }

    class CustomItemRenderer implements CustomTextureRenderer {

        @Override
        public void render(SpriteBatch batch, float x, float y, float scaleX, float scaleY, OrthographicCamera camera, Properties properties, Definition definition, Engine engine, TextureStorageComponent textureStorageComponent) {
            ItemComponent itemComponent = definition.getComponent(ItemComponent.class);
            ItemStack stack = itemComponent.getItemStack(properties);

            ItemRenderer.renderItem(batch, x - 8, y - 8, 1, 1, engine, camera, stack, true);
        }
    }
}
