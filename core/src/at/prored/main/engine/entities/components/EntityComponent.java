package at.prored.main.engine.entities.components;

import at.prored.main.engine.components.Component;
import at.prored.main.engine.entities.Entity;
import at.prored.main.engine.tiles.storage.GameMap;

/**
 * Created by ProRed on 21.05.2017.
 */
public abstract class EntityComponent extends Component {

    public abstract void update(Entity e, GameMap gameMap);

}
