package at.prored.main.engine.entities.components;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.Component;
import at.prored.main.engine.entities.Entity;
import at.prored.main.engine.entities.EntityProperties;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.tiles.storage.GameMap;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 25.05.2017.
 */
public class PhysicsComponent extends EntityComponent {

    private static final float MAX_VEL = 600;

    private static final String VELX = "velX";
    private static final String VELY = "velY";
    private static final String ACCX = "accX";
    private static final String ACCY = "accY";
    private static final String JUMP = "canJump";

    private float xOffset, yOffset;
    private float width, height;

    public PhysicsComponent() {
    }

    public PhysicsComponent(float xOffset, float yOffset, float width, float height) {
        this.xOffset = xOffset;
        this.yOffset = yOffset;
        this.width = width;
        this.height = height;
    }

    /**
     * Sets the x velocity of an entity
     *
     * @param entity entity
     * @param velX   velocity x
     */
    public void setVelocityX(Entity entity, float velX) {
        EntityProperties properties = entity.getEntityProperties();
        properties.setFloat(VELX, Math.min(velX, MAX_VEL));
    }

    /**
     * Sets the y velocity of an entity
     *
     * @param entity entity
     * @param velY   velocity y
     */
    public void setVelocityY(Entity entity, float velY) {
        EntityProperties properties = entity.getEntityProperties();
        properties.setFloat(VELY, Math.min(velY, MAX_VEL));
    }

    /**
     * Sets the x acceleration of an entity
     *
     * @param entity entity
     * @param accX   acceleration x
     */
    public void setAccelerationX(Entity entity, float accX) {
        EntityProperties properties = entity.getEntityProperties();
        properties.setFloat(ACCX, accX);
    }

    /**
     * Sets the y acceleration of an entity
     *
     * @param entity entity
     * @param accY   acceleration y
     */
    public void setAccelerationY(Entity entity, float accY) {
        EntityProperties properties = entity.getEntityProperties();
        properties.setFloat(ACCY, accY);
    }

    /**
     * Adds velocity to the entity
     *
     * @param entity entity
     * @param velX   x velocity
     */
    public void addVelocityX(Entity entity, float velX) {
        setVelocityX(entity, getVelocityX(entity) + velX);
    }

    /**
     * Adds velocity to the entity
     *
     * @param entity entity
     * @param velY   y velocity
     */
    public void addVelocityY(Entity entity, float velY) {
        setVelocityY(entity, getVelocityY(entity) + velY);
    }

    /**
     * Adds acceleration to the entity
     *
     * @param entity entity
     * @param accX   x acceleration
     */
    public void addAccelerationX(Entity entity, float accX) {
        setAccelerationX(entity, getAccelerationX(entity) + accX);
    }

    /**
     * Adds acceleration to the entity
     *
     * @param entity entity
     * @param accY   y acceleration
     */
    public void addAccelerationY(Entity entity, float accY) {
        setAccelerationY(entity, getAccelerationY(entity) + accY);
    }

    /**
     * Returns the velocity x of an entity
     *
     * @param entity entity
     * @return velocity x
     */
    public float getVelocityX(Entity entity) {
        EntityProperties properties = entity.getEntityProperties();
        return properties.hasData(VELX) ? properties.getFloat(VELX) : 0f;
    }

    /**
     * Returns the velocity y of an entity
     *
     * @param entity entity
     * @return velocity y
     */
    public float getVelocityY(Entity entity) {
        EntityProperties properties = entity.getEntityProperties();
        return properties.hasData(VELY) ? properties.getFloat(VELY) : 0f;
    }

    /**
     * Returns the acceleration x of an entity
     *
     * @param entity entity
     * @return acceleration x
     */
    public float getAccelerationX(Entity entity) {
        EntityProperties properties = entity.getEntityProperties();
        return properties.hasData(ACCX) ? properties.getFloat(ACCX) : 0f;
    }

    /**
     * Returns the acceleration y of an entity
     *
     * @param entity entity
     * @return acceleration y
     */
    public float getAccelerationY(Entity entity) {
        EntityProperties properties = entity.getEntityProperties();
        return properties.hasData(ACCY) ? properties.getFloat(ACCY) : 0f;
    }

    /**
     * Sets the canJump boolean
     * @param entity entity
     * @param canJump new value
     */
    public void setCanJump(Entity entity, boolean canJump){
        entity.getEntityProperties().setBoolean(JUMP, canJump);
    }

    /**
     * Returns if a entity can jump
     * @param entity whether or not the entity can jump
     */
    public boolean canJump(Entity entity){
        EntityProperties properties = entity.getEntityProperties();
        return properties.hasData(JUMP) && properties.getBoolean(JUMP);
    }

    /**
     * Returns the xOffset of the collision box
     * @return xOffset
     */
    public float getXOffset() {
        return xOffset;
    }

    /**
     * Returns the yOffset of the collision box
     * @return yOffset
     */
    public float getYOffset() {
        return yOffset;
    }

    /**
     * Returns the width of the collision box
     * @return width of box
     */
    public float getWidth() {
        return width;
    }

    /**
     * Returns the height of the collision box
     * @return height of box
     */
    public float getHeight() {
        return height;
    }

    @Override
    public void onEntityAdded(Entity entity) {
        super.onEntityAdded(entity);

        //Set default values when the entity gets added to the world
        entity.getEntityProperties().setFloat(VELX, 0f);
        entity.getEntityProperties().setFloat(VELY, 0f);

        entity.getEntityProperties().setFloat(ACCX, 0f);
        entity.getEntityProperties().setFloat(ACCY, 0f);

        entity.getEntityProperties().setBoolean(JUMP, false);
    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {
        this.xOffset = Float.parseFloat(element.getAttribute("xOffset", "0"));
        this.yOffset = Float.parseFloat(element.getAttribute("yOffset", "0"));
        this.width = Float.parseFloat(element.getAttribute("width", "32"));
        this.height = Float.parseFloat(element.getAttribute("height", "32"));
    }

    @Override
    public Component cpy() {
        return new PhysicsComponent(xOffset, yOffset, width, height);
    }

    @Override
    public void update(Entity e, GameMap gameMap) {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PhysicsComponent that = (PhysicsComponent) o;

        if (Float.compare(that.xOffset, xOffset) != 0) return false;
        if (Float.compare(that.yOffset, yOffset) != 0) return false;
        if (Float.compare(that.width, width) != 0) return false;
        return Float.compare(that.height, height) == 0;
    }

    @Override
    public int hashCode() {
        int result = (xOffset != +0.0f ? Float.floatToIntBits(xOffset) : 0);
        result = 31 * result + (yOffset != +0.0f ? Float.floatToIntBits(yOffset) : 0);
        result = 31 * result + (width != +0.0f ? Float.floatToIntBits(width) : 0);
        result = 31 * result + (height != +0.0f ? Float.floatToIntBits(height) : 0);
        return result;
    }
}
