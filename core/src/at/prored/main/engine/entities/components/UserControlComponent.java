package at.prored.main.engine.entities.components;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.Component;
import at.prored.main.engine.components.behavior.Behavior;
import at.prored.main.engine.components.behavior.BehaviorManager;
import at.prored.main.engine.components.behavior.EventType;
import at.prored.main.engine.components.behavior.behaviors.UserControlBehavior;
import at.prored.main.engine.components.behavior.behaviors.events.ControlEvent;
import at.prored.main.engine.entities.Entity;
import at.prored.main.engine.entities.EntityDef;
import at.prored.main.engine.saving.data.BooleanData;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.tiles.storage.GameMap;
import at.prored.main.engine.utils.DataReader;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 25.05.2017.
 */
public class UserControlComponent extends EntityComponent {

    private ControlEvent controlEvent;
    private Vector2 lastDirection;

    public UserControlComponent() {
        this.controlEvent = new ControlEvent();
        this.lastDirection = new Vector2().set(controlEvent.getDirection());
    }

    @Override
    public void update(Entity e, GameMap gameMap) {
        EntityDef entityDef = gameMap.getEngine().getEntityRegistry().getEntityDef(e.getID());

        //Check if the entity has a velocity component
        if (!entityDef.hasComponent(PhysicsComponent.class))
            return;

        //Get velocity component
        PhysicsComponent physicsComponent = entityDef.getComponent(PhysicsComponent.class);

        controlEvent.getDirection().set(0, 0);

        //Check for input
        if (Gdx.input.isKeyPressed(Input.Keys.A)) {
            physicsComponent.addVelocityX(e, -96f);
            controlEvent.getDirection().set(-1, 0);
        }

        if (Gdx.input.isKeyPressed(Input.Keys.D)) {
            physicsComponent.addVelocityX(e, 96f);
            controlEvent.getDirection().set(1, 0);
        }


        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
            if (physicsComponent.canJump(e) || GameMap.DEBUG) {
                physicsComponent.addVelocityY(e, 210f);
                physicsComponent.setCanJump(e, false);

            }
        }

        //Updates the state of the entity
        if (!lastDirection.equals(controlEvent.getDirection()))
            BehaviorManager.instance().broadcastEntity(EventType.ENTITY_CONTROL, controlEvent, gameMap, e);

        //Update tile placer
        if (gameMap.getTilePlacer().isActive())
            gameMap.getTilePlacer().touchDragged(Gdx.input.getX(), Gdx.input.getY(), 0);

        lastDirection.set(controlEvent.getDirection());
    }

    @Override
    public Behavior getBehavior() {
        return new UserControlBehavior(getClass());
    }

    @Override
    public Component cpy() {
        return new UserControlComponent();
    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {
        if (properties.hasData("player"))
            data.add(new BooleanData("player", properties.getBoolean("player")));
    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {
        DataReader reader = new DataReader(data);
        if (reader.hasData("player"))
            properties.setBoolean("player", reader.getBoolean("player"));
    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {

    }

}
