package at.prored.main.engine.entities;

import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Pool;

/**
 * Created by ProRed on 23.05.2017.
 */

/**
 * This object stores information about the entity
 * It was designed to be pooled
 */
public class EntityProperties implements Pool.Poolable, Properties {

    private ObjectMap<String, Object> data;
    private Vector2 position;

    public EntityProperties() {
        this.data = new ObjectMap<String, Object>();
        this.position = new Vector2();
    }

    /**
     * Adds a integer value to the entity property
     *
     * @param key   key
     * @param value value
     */
    public void setInteger(String key, int value) {
        data.put(key, value);
    }

    /**
     * Adds a integer value to the entity property
     *
     * @param key   key
     * @param value value
     */
    public void setFloat(String key, float value) {
        data.put(key, value);
    }

    /**
     * Adds a object value to the entity property
     *
     * @param key   key
     * @param value value
     */
    public void setData(String key, Object value) {
        data.put(key, value);
    }

    /**
     * Adds a integer value to the entity property
     *
     * @param key   key
     * @param value value
     */
    public void setBoolean(String key, boolean value) {
        data.put(key, value);
    }

    /**
     * Returns a integer from the entity property value set
     *
     * @param key key
     * @return integer
     */
    public int getInteger(String key) {
        return (Integer) data.get(key);
    }

    /**
     * Returns a float from the entity property value set
     *
     * @param key key
     * @return float
     */
    public float getFloat(String key) {
        return (Float) data.get(key);
    }

    /**
     * Returns a boolean from the entity property value set
     *
     * @param key key
     * @return boolean
     */
    public boolean getBoolean(String key) {
        return (Boolean) data.get(key);
    }

    /**
     * Checks if this entity property has key entry
     *
     * @param key key
     * @return true or false
     */
    public boolean hasData(String key) {
        return data.containsKey(key);
    }

    /**
     * Returns an object from the entity property value set
     *
     * @param key key
     * @return object
     */
    public Object getData(String key) {
        return data.get(key);
    }

    /**
     * Clears all data
     */
    @Override
    public void clear() {
        reset();
    }

    /**
     * Returns the position of the entity
     *
     * @return position of entity
     */
    public Vector2 getPosition() {
        return position;
    }

    /**
     * Reset function for pooling
     */
    @Override
    public void reset() {
        data.clear();
        position.set(0, 0);
    }

    @Override
    public String toString() {
        return data.toString();
    }
}
