package at.prored.main.engine.entities;

import at.prored.main.engine.components.Component;
import at.prored.main.engine.utils.Definition;
import com.badlogic.gdx.utils.Array;

import java.util.Iterator;

/**
 * Created by ProRed on 18.05.2017.
 */
public class EntityDef implements Definition {

    private String defID;
    private int id;
    private Array<Component> entityComponents;

    public EntityDef(int id) {
        this.id = id;
        this.entityComponents = new Array<Component>();
    }

    public String getDefID() {
        return defID;
    }

    public void setDefID(String defID) {
        this.defID = defID;
    }

    /**
     * Adds a entity component to this entity definition
     *
     * @param entityComponent entity component to add
     */
    public void addComponent(Component entityComponent) {
        if(hasComponent(entityComponent.getClass()))
            removeComponent(entityComponent.getClass());

        entityComponents.add(entityComponent);
    }

    /**
     * Removes a component from the definition
     * @param component component
     */
    public void removeComponent(Class<? extends Component> component){
        Iterator<Component> it = entityComponents.iterator();
        while(it.hasNext()){
            if(component.isInstance(it.next()))
                it.remove();
        }
    }

    /**
     * Returns a component from a entity definition
     *
     * @param component class of the searched component
     * @return component (or null)
     */
    @SuppressWarnings("unchecked")
    public <T extends Component> T getComponent(Class<T> component){
        for (int i = 0; i < entityComponents.size; i++) {
            if(component.isInstance(entityComponents.get(i)))
                return (T) entityComponents.get(i);
        }

        return null;
    }

    /**
     * Checks if a entity definition has a component
     *
     * @param component class of the component
     * @return true or false
     */
    public boolean hasComponent(Class<? extends Component> component) {
        for (int i = 0; i < entityComponents.size; i++) {
            if(component.isInstance(entityComponents.get(i)))
                return true;
        }

        return false;
    }

    /**
     * Returns all components
     *
     * @return all components
     */
    public Array<Component> getComponents() {
        return entityComponents;
    }

    public int getID() {
        return id;
    }
}
