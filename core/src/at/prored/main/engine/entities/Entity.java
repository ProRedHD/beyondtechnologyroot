package at.prored.main.engine.entities;

import at.prored.main.engine.Engine;
import at.prored.main.engine.utils.Pools;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by ProRed on 23.05.2017.
 */
public class Entity {

    private int id;
    private EntityProperties entityProperties;

    private Entity(int id){
        this.id = id;
        this.entityProperties = Pools.instance().getEntityPropertiesPool().obtain();
    }

    public static Entity createInstance(int id, Engine e){
        Entity entity = new Entity(id);
        e.getEntityRegistry().initEntity(entity);

        return entity;
    }

    public int getID() {
        return id;
    }

    public EntityProperties getEntityProperties() {
        return entityProperties;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        Pools.instance().getEntityPropertiesPool().free(entityProperties);
    }
}
