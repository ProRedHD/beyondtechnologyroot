package at.prored.main.engine.tiles;

import at.prored.main.engine.components.LocalizableComponent;
import at.prored.main.engine.components.behavior.BehaviorManager;
import at.prored.main.engine.components.behavior.EventType;
import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.tiles.storage.Chunk;
import at.prored.main.engine.tiles.storage.GameMap;
import at.prored.main.engine.utils.MapUtils;
import at.prored.main.engine.utils.Pools;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by ProRed on 07.03.2018.
 */
public class RandomTickDistributor {

    /**
     * Interval between random ticks (in seconds)
     */
    private static final float INTERVAL = .125f;

    /**
     * Maximum amount of chunks away from player to tick chunks
     */
    private static final int TICK_CHUNK_RADIUS = 3;

    private float stateTime;

    private GameMap gameMap;

    public RandomTickDistributor(GameMap gameMap) {
        this.gameMap = gameMap;
    }

    /**
     * Updates the random tick distributor
     */
    public void update() {
        stateTime += Graphics.instance().getDeltaTime();

        if (stateTime > INTERVAL) {
            stateTime = 0;
            tick();
        }
    }

    /**
     * Finds a random chunk near the player and ticks a tile in that chunk
     */
    private void tick() {

        //vars
        Chunk c;
        Vector2 help = Pools.instance().getVector2();
        Vector2 playerChunk = gameMap.getPlayerChunk();

        do {
            //generate random offsets
            int offsetX = com.badlogic.gdx.math.MathUtils.random(-TICK_CHUNK_RADIUS, TICK_CHUNK_RADIUS);
            int offsetY = com.badlogic.gdx.math.MathUtils.random(-TICK_CHUNK_RADIUS, TICK_CHUNK_RADIUS);

            //calculate chunk position
            help.set(playerChunk.x + offsetX, playerChunk.y + offsetY);

            //get chunk from arry
            c = gameMap.getChunks().get(help);

        } while (c == null);

        //Tick chunk when not null
        tickChunk(c, (int) help.x, (int) help.y);

        //free pooled vector
        Pools.instance().freeVector2(help);
    }

    /**
     * Ticks a random tile in a chunk (skips if it doesn't find a tile that isn't air in 5 tries)
     *
     * @param chunk chunk
     * @param cx    chunk x
     * @param cy    chunk y
     */
    private void tickChunk(Chunk chunk, int cx, int cy) {
        int x = com.badlogic.gdx.math.MathUtils.random(0, 15);
        int y = com.badlogic.gdx.math.MathUtils.random(0, 15);

        int ax = (cx * Chunk.CHUNK_SIZE) + x;
        int ay = (cy * Chunk.CHUNK_SIZE) + y;

        if (GameMap.DEBUG) {
            int id = MapUtils.getTileType(gameMap, ax, ay, 0);

            TileDef def = gameMap.getEngine().getTileRegistry().getTileDef(id);

            System.out.println("Tile at " + x + " " + y + " received a random tick. (" + def.getComponent(LocalizableComponent.class).getName() + ")");
        }

        //notiy behaviormanager
        BehaviorManager.instance().broadcastTile(EventType.RANDOM_TICK, null, gameMap, ax, ay, 0);
    }


}
