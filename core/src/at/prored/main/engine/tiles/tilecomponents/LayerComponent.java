package at.prored.main.engine.tiles.tilecomponents;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.Component;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.utils.ComponentException;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 04.01.2018.
 */
public class LayerComponent extends TileComponent {

    private Array<Integer> layers;

    public LayerComponent() {
        this.layers = new Array<Integer>();
    }

    public LayerComponent(Array<Integer> layers) {
        this.layers = layers;
    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {
        for (String s : element.getText().split(",")) {
            try {
                this.layers.add(Integer.parseInt(s));
            } catch(NumberFormatException en){
                throw new ComponentException(this.getClass(), en.getMessage());
            }
        }
    }

    @Override
    public Component cpy() {
        Array<Integer> layers = new Array<Integer>();
        layers.addAll(this.layers);
        return new LayerComponent(layers);
    }

    public Array<Integer> getLayers() {
        return layers;
    }

    public boolean isForced(){
        return this.layers.size == 1;
    }

}
