package at.prored.main.engine.tiles.tilecomponents.behaviors.events;

import at.prored.main.engine.components.behavior.ActionEvent;

/**
 * Created by ProRed on 22.03.2018.
 */
public class KineticChangeEvent extends ActionEvent {

    /**
     * Position of the sender
     */
    private int senderX, senderY;

    /**
     * New energy
     */
    private int energy;

    public KineticChangeEvent(int senderX, int senderY, int energy) {
        this.senderX = senderX;
        this.senderY = senderY;
        this.energy = energy;
    }

    public int getSenderX() {
        return senderX;
    }

    public int getSenderY() {
        return senderY;
    }

    public int getEnergy() {
        return energy;
    }
}
