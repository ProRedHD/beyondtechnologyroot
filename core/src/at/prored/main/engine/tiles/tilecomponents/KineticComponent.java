package at.prored.main.engine.tiles.tilecomponents;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.Component;
import at.prored.main.engine.components.behavior.Behavior;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.tiles.tilecomponents.behaviors.KineticBehavior;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 20.03.2018.
 */
public class KineticComponent extends Component {

    private static final String KINETIC_ENERGY = "kinetic_energy";
    private static final String SOURCE_X = "sourceX";
    private static final String SOURCE_Y = "sourceY";


    /**
     * Type of the kinetic
     */
    public enum KineticType {
        GEARBOX, SHAFT, PROVIDER, CONSUMER
    }

    private KineticType kineticType;

    public KineticComponent() {
    }

    public KineticComponent(KineticType kineticType) {
        this.kineticType = kineticType;
    }

    /**
     * Sets the kinetic energy of the component
     * @param p properties
     * @param energy energy
     */
    public void setKineticEnergy(Properties p, int energy){
        p.setInteger(KINETIC_ENERGY, energy);
    }

    /**
     * Returns the kinetic energy of the component
     * @param p properties
     * @return energy
     */
    public int getKineticEnergy(Properties p){
        return p.hasData(KINETIC_ENERGY) ? p.getInteger(KINETIC_ENERGY) : 0;
    }

    /**
     * Sets the x offset to the source of the energy
     * @param p properties
     * @param offsetX new offset
     */
    public void setSourceX(Properties p, int offsetX){
        p.setInteger(SOURCE_X, offsetX);
    }

    /**
     * Returns the x offset to the source of the energy
     * @param p properties
     * @return x offset
     */
    public int getSourceX(Properties p){
        return p.hasData(SOURCE_X) ? p.getInteger(SOURCE_X) : 0;
    }

    /**
     * Sets the y offset to the source of the energy
     * @param p properties
     * @param offsetY new offset
     */
    public void setSourceY(Properties p, int offsetY){
        p.setInteger(SOURCE_Y, offsetY);
    }

    /**
     * Returns the y offset to the source of the energy
     * @param p properties
     * @return y offset
     */
    public int getSourceY(Properties p){
        return p.hasData(SOURCE_Y) ? p.getInteger(SOURCE_Y) : 0;
    }

    /**
     * Returns the kinetic type of the component
     * @return kinetic type
     */
    public KineticType getKineticType() {
        return kineticType;
    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {
        this.kineticType = KineticType.valueOf(element.getAttribute("type", "SHAFT"));
    }

    @Override
    public Behavior getBehavior() {
        return new KineticBehavior(this.getClass());
    }

    @Override
    public Component cpy() {
        return new KineticComponent(kineticType);
    }
}
