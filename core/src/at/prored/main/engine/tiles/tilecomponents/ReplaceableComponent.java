package at.prored.main.engine.tiles.tilecomponents;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.Component;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 12.11.2018.
 *
 * This component is used for tiles that should not be checked when placing other tiles
 * Meaning that tiles that have this component are just replaced and are not checked for while placing
 */
public class ReplaceableComponent extends Component {

    public ReplaceableComponent(){}

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {

    }

    @Override
    public Component cpy() {
        return new ReplaceableComponent();
    }
}
