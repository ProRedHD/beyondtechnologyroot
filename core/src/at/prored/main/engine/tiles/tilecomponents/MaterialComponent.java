package at.prored.main.engine.tiles.tilecomponents;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.Component;
import at.prored.main.engine.items.components.ToolComponent;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 25.12.2017.
 */
public class MaterialComponent extends Component {

    private ToolComponent.TileBreakingTypes breakingType;
    private float hardness;
    private float breakingSpeedMultiplier;

    public MaterialComponent(){}

    public MaterialComponent(ToolComponent.TileBreakingTypes breakingType, float hardness, float breakingSpeedMultiplier) {
        this.breakingType = breakingType;
        this.hardness = hardness;
        this.breakingSpeedMultiplier = breakingSpeedMultiplier;
    }

    public ToolComponent.TileBreakingTypes getBreakingType() {
        return breakingType;
    }

    public float getHardness() {
        return hardness;
    }

    public float getBreakingSpeedMultiplier() {
        return breakingSpeedMultiplier;
    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {
        this.breakingType = ToolComponent.TileBreakingTypes.valueOf(element.getAttribute("breakingtype", "wood").toUpperCase());
        this.hardness = Float.parseFloat(element.getAttribute("hardness", "10"));
        this.breakingSpeedMultiplier = Float.parseFloat(element.getAttribute("speedMulitplier", "1"));
    }

    @Override
    public Component cpy() {
        return new MaterialComponent(breakingType, hardness, breakingSpeedMultiplier);
    }


}
