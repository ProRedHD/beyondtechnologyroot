package at.prored.main.engine.tiles.tilecomponents.behaviors;

import at.prored.main.engine.components.Component;
import at.prored.main.engine.components.behavior.ActionEvent;
import at.prored.main.engine.components.behavior.Behavior;
import at.prored.main.engine.components.behavior.EventType;
import at.prored.main.engine.components.behavior.behaviors.events.TileChangeEvent;
import at.prored.main.engine.entities.Entity;
import at.prored.main.engine.inventory.Inventory;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.tiles.TileDef;
import at.prored.main.engine.tiles.storage.GameMap;
import at.prored.main.engine.tiles.tilecomponents.RestrictedPlacementComponent;
import at.prored.main.engine.utils.MapUtils;

public class RestrictedPlacementBehavior extends Behavior {

    public RestrictedPlacementBehavior(Class<? extends Component> component) {
        super(component);
    }

    @Override
    public void trigger(EventType type, ActionEvent e, GameMap gameMap, Entity entity) {

    }

    @Override
    public void trigger(EventType type, ActionEvent e, GameMap gameMap, int x, int y, int layer) {
        if(type == EventType.TILE_CHANGE){
            TileChangeEvent e1 = (TileChangeEvent) e;

            if(e1.getType() == TileChangeEvent.ChangeType.NEIGHBOUR_ADDED || e1.getType() == TileChangeEvent.ChangeType.NEIGHBOUR_REMOVED){
                int id = MapUtils.getTileType(gameMap, x, y, layer);
                TileDef def = gameMap.getEngine().getTileRegistry().getTileDef(id);

                //Check if placement is valid
                boolean valid = def.getComponent(RestrictedPlacementComponent.class).checkPlacement(gameMap, x, y, layer);

                if(!valid)
                    del(gameMap, x, y, layer);
            }
        }
    }

    /**
     * Deletes the current tile
     * @param gameMap gameMap
     * @param x x
     * @param y y
     * @param layer layer
     */
    private void del(GameMap gameMap, int x, int y, int layer){
        MapUtils.removeTile(gameMap, x, y, layer);
    }

    @Override
    public void trigger(EventType type, ActionEvent e, ItemStack stack, Inventory inventory, GameMap gameMap) {

    }
}
