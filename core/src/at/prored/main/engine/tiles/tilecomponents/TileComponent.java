package at.prored.main.engine.tiles.tilecomponents;

import at.prored.main.engine.components.Component;
import at.prored.main.engine.tiles.TileProperties;
import com.badlogic.gdx.assets.AssetManager;

/**
 * Created by ProRed on 18.05.2017.
 */
public abstract class TileComponent extends Component {

    public TileComponent(){}
}
