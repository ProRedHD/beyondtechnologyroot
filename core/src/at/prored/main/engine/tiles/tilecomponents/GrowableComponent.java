package at.prored.main.engine.tiles.tilecomponents;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.Component;
import at.prored.main.engine.components.RenderableComponent;
import at.prored.main.engine.components.TextureStorageComponent;
import at.prored.main.engine.components.behavior.Behavior;
import at.prored.main.engine.saving.data.IntData;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.tiles.TileProperties;
import at.prored.main.engine.tiles.tilecomponents.behaviors.GrowableBehavior;
import at.prored.main.engine.utils.ComponentException;
import at.prored.main.engine.utils.DataReader;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

import java.util.Arrays;

/**
 * Created by ProRed on 07.03.2018.
 */
public class GrowableComponent extends Component {

    private static final String STAGE = "stage";
    private static final String STAGEPROGRESS = "stageProgress";

    private int stages;
    private String[] stageTextureNames;

    private int stageProgressionCount;

    public GrowableComponent(){

    }

    public GrowableComponent(int stages, String[] stageTextureNames, int stageProgressionCount) {
        this.stages = stages;
        this.stageTextureNames = stageTextureNames;
        this.stageProgressionCount = stageProgressionCount;
    }

    @Override
    public void onFinishedCompLoading(Definition def) {
        if(!def.hasComponent(TextureStorageComponent.class))
            throw new ComponentException(this.getClass(), "Required Component not found: TextureStorageComponent");

        if(def.hasComponent(RenderableComponent.class))
            throw new ComponentException(this.getClass(), "Not compatible with RenderableComponent");

        def.getComponent(TextureStorageComponent.class).setDefaultTexture(stageTextureNames[0]);
    }

    @Override
    public void onTileAdded(TileProperties tileProperties) {
        setStage(tileProperties, 0);
        setStageProgress(tileProperties, 0);
    }

    /**
     * Returns the stage of the growing tile
     * @param properties  properties
     * @return stage progress
     */
    public static int getStage(Properties properties){
        if(!properties.hasData(STAGE))
            return 0;

        return properties.getInteger(STAGE);
    }

    /**
     * Returns the stage progress of the growing tile
     * @param properties properties
     * @return stage progress
     */
    public static int getStageProgress(Properties properties){
        if(!properties.hasData(STAGEPROGRESS))
            return 0;

        return properties.getInteger(STAGEPROGRESS);
    }

    /**
     * Sets the stage of the growing tile
     * @param properties properties
     * @param stage stage
     */
    public void setStage(Properties properties, int stage){
        properties.setInteger(STAGE, stage);
        properties.setData(TextureStorageComponent.ACTIVE_TEXTURE, getStageTextureNames()[stage]);
    }

    /**
     * Sets the stage progress of the growing tile
     * @param properties properties
     * @param progress stage progress
     */
    public void setStageProgress(Properties properties, int progress){
        properties.setInteger(STAGEPROGRESS, progress);
    }

    /**
     * Returns the stages
     * @return stages
     */
    public int getStages() {
        return stages;
    }

    /**
     * Returns the stage texture names
     * @return stage texture name
     */
    public String[] getStageTextureNames() {
        return stageTextureNames;
    }

    /**
     * Returns the stage progression count
     * @return stage progression count
     */
    public int getStageProgressionCount() {
        return stageProgressionCount;
    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {
        data.add(new IntData(STAGE, properties.getInteger(STAGE)));
        data.add(new IntData(STAGEPROGRESS, properties.getInteger(STAGEPROGRESS)));
    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {
        DataReader reader = new DataReader(data);

        properties.setInteger(STAGE, reader.getInt(STAGE));
        properties.setInteger(STAGEPROGRESS, reader.getInt(STAGEPROGRESS));
    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {
        this.stages = Integer.parseInt(element.getAttribute("stageCnt", "0"));
        this.stageProgressionCount = Integer.parseInt(element.getAttribute("stageProgressCnt", "0"));

        this.stageTextureNames = element.getText().split(",");
    }

    @Override
    public Behavior getBehavior() {
        return new GrowableBehavior(getClass());
    }

    @Override
    public Component cpy() {
        String[] cpyStages = Arrays.copyOf(stageTextureNames, stageTextureNames.length);
        return new GrowableComponent(stages, cpyStages, stageProgressionCount);
    }
}
