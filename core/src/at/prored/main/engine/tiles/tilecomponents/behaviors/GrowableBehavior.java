package at.prored.main.engine.tiles.tilecomponents.behaviors;

import at.prored.main.engine.components.Component;
import at.prored.main.engine.components.TextureStorageComponent;
import at.prored.main.engine.components.behavior.ActionEvent;
import at.prored.main.engine.components.behavior.Behavior;
import at.prored.main.engine.components.behavior.EventType;
import at.prored.main.engine.entities.Entity;
import at.prored.main.engine.inventory.Inventory;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.tiles.TileDef;
import at.prored.main.engine.tiles.TileProperties;
import at.prored.main.engine.tiles.storage.GameMap;
import at.prored.main.engine.tiles.tilecomponents.GrowableComponent;
import at.prored.main.engine.utils.MapUtils;

/**
 * Created by ProRed on 07.03.2018.
 */
public class GrowableBehavior extends Behavior {

    public GrowableBehavior(Class<? extends Component> component) {
        super(component);
    }

    @Override
    public void trigger(EventType type, ActionEvent e, GameMap gameMap, Entity entity) {

    }

    @Override
    public void trigger(EventType type, ActionEvent e, GameMap gameMap, int x, int y, int layer) {

        if (type == EventType.RANDOM_TICK) {

            int id = MapUtils.getTileType(gameMap, x, y, layer);
            TileProperties properties = MapUtils.getTileProperties(gameMap, x, y, layer);
            TileDef def = gameMap.getEngine().getTileRegistry().getTileDef(id);
            GrowableComponent gc = def.getComponent(GrowableComponent.class);

            //get current values
            int stage = GrowableComponent.getStage(properties);
            int stageProgress = GrowableComponent.getStageProgress(properties);

            //increase stage progress
            stageProgress++;

            //progress stage
            if(stageProgress >= gc.getStageProgressionCount()){
                if(stage < (gc.getStages()-1)){
                    stage++;
                    stageProgress = 0;
                } else {
                    stageProgress = gc.getStageProgressionCount();
                }
            }

            //set values in properties
            gc.setStage(properties, stage);
            gc.setStageProgress(properties, stageProgress);

            //update texture
            def.getComponent(TextureStorageComponent.class).setActiveTexture(properties, gc.getStageTextureNames()[stage]);
        }

    }

    @Override
    public void trigger(EventType type, ActionEvent e, ItemStack stack, Inventory inventory, GameMap gameMap) {

    }
}
