package at.prored.main.engine.tiles.tilecomponents;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.Component;
import at.prored.main.engine.components.TextureStorageComponent;
import at.prored.main.engine.components.behavior.Behavior;
import at.prored.main.engine.components.behavior.behaviors.ConnectedTextureBehavior;
import at.prored.main.engine.graphics.renderer.CustomTextureRenderer;
import at.prored.main.engine.saving.data.IntData;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.utils.ComponentException;
import at.prored.main.engine.utils.DataReader;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.XmlReader;

import java.util.Arrays;

/**
 * Created by ProRed on 03.01.2018.
 */
public class ConnectedTextureComponent extends Component {

    private String[] textureNames;

    public ConnectedTextureComponent() {

    }

    public ConnectedTextureComponent(String[] textureNames) {
        this.textureNames = textureNames;
    }

    public void setSides(int sides, int orientation, Properties properties) {
        properties.setInteger("sides", sides);
        properties.setInteger("orientation", orientation);
    }

    @Override
    public void onFinishedCompLoading(Definition def) {
        if(!def.hasComponent(TextureStorageComponent.class))
            throw new ComponentException(this.getClass(), "Required Component not found: TextureStorageComponent");

        def.getComponent(TextureStorageComponent.class).setCustomTextureRenderer(new ConnectedTextureRenderer());
    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {
        this.textureNames = new String[6];

        for(int i = 0; i < textureNames.length; i++){
            this.textureNames[i] = String.format(element.getAttribute("name", "name"), i);
        }
    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {
        data.add(new IntData("orientation", properties.getInteger("orientation")));
        data.add(new IntData("sides", properties.getInteger("sides")));
    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {
        DataReader reader = new DataReader(data);

        System.out.println(data);

        if(reader.hasData("orientation")){
            properties.setInteger("orientation", reader.getInt("orientation"));
        }

        if(reader.hasData("sides")){
            properties.setInteger("sides", reader.getInt("sides"));
        }
    }


    @Override
    public Behavior getBehavior() {
        return new ConnectedTextureBehavior(this.getClass());
    }

    @Override
    public Component cpy() {
        return new ConnectedTextureComponent(Arrays.copyOf(textureNames, textureNames.length));
    }

    /**
     * Custom renderer for the connected textures
     */
    class ConnectedTextureRenderer implements CustomTextureRenderer {

        @Override
        public void render(SpriteBatch batch, float x, float y, float scaleX, float scaleY, OrthographicCamera camera, Properties properties, Definition definition, Engine engine, TextureStorageComponent textureStorageComponent) {
            TextureRegion texture = getTexture(properties, definition);

            if(texture == null)
                throw new GdxRuntimeException("Texture is null!");

            float width = texture.getRegionWidth();
            float height = texture.getRegionHeight();

            if (camera.frustum.boundsInFrustum(x + width / 2f, y + height / 2f, 1f, width / 2f, height / 2f, 1f)) {

                if (properties == null || (!properties.hasData("orientation") || !properties.hasData("sides"))) {
                    batch.draw(texture, x, y, width, height);
                } else {

                    float rotation = 0;
                    boolean flipX = false, flipY = false;
                    int orientation = properties.getInteger("orientation");
                    switch (properties.getInteger("sides")) {
                        case 1:
                            switch (orientation) {
                                case 0:
                                    rotation = 180;
                                    break;
                                case 1:
                                    rotation = 0;
                                    break;
                                case 2:
                                    rotation = 270;
                                    break;
                                case 3:
                                    rotation = 90;
                                    break;
                                default:
                            }
                            break;
                        case 2:
                            switch (orientation) {
                                case 0:
                                    rotation = 270;
                                    break;
                                case 3:
                                    flipX = true;
                                    break;
                                case 4:
                                    flipY = true;
                                    break;
                                case 5:
                                    flipX = true;
                                    flipY = true;
                                    break;
                                default:
                            }
                            break;
                        case 3:
                            switch (orientation) {
                                case 1:
                                    flipX = true;
                                    break;
                                case 2:
                                    rotation = 270;
                                    break;
                                case 3:
                                    rotation = 90;
                                    flipY = true;
                                    break;
                                default:
                            }
                            break;
                        default:
                    }

                    batch.draw(texture, x, y, width / 2f, height / 2f, width, height, 1f*(flipX ? -1 : 1), 1f*(flipY ? -1 : 1), rotation);
                }
            }
        }

        /**
         * Returns the currect texture
         * @param properties properties of tile
         * @return texture
         */
        private TextureRegion getTexture(Properties properties, Definition definition) {

            TextureStorageComponent storage = definition.getComponent(TextureStorageComponent.class);

            if (properties == null)
                return storage.getTexture(textureNames[0]);
            try {
                int orientation = properties.getInteger("orientation");
                switch (properties.getInteger("sides")) {
                    case 0:
                        return storage.getTexture(textureNames[0]);
                    case 1:
                        return storage.getTexture(textureNames[1]);
                    case 2:
                        return storage.getTexture(textureNames[orientation > 1 ? 2 : 3]);
                    case 3:
                        return storage.getTexture(textureNames[4]);
                    case 4:
                        return storage.getTexture(textureNames[5]);
                }
            } catch(NullPointerException e){
                return storage.getTexture(textureNames[0]);
            }

            return null;
        }
    }
}
