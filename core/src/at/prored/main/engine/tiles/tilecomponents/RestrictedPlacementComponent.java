package at.prored.main.engine.tiles.tilecomponents;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.Component;
import at.prored.main.engine.components.behavior.Behavior;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.tiles.TileDef;
import at.prored.main.engine.tiles.storage.GameMap;
import at.prored.main.engine.tiles.tilecomponents.behaviors.RestrictedPlacementBehavior;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Direction;
import at.prored.main.engine.utils.MapUtils;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.XmlReader;

public class RestrictedPlacementComponent extends Component {

    private ObjectMap<Direction, Array<String>> directionWhitelist;
    private ObjectMap<Direction, Array<String>> directionBlacklist;
    private ObjectMap<String, TileDef> cache;

    public RestrictedPlacementComponent(){
        this.directionWhitelist = new ObjectMap<>();
        this.directionBlacklist = new ObjectMap<>();
        this.cache = new ObjectMap<>();

        for (Direction direction : Direction.values()) {
            directionWhitelist.put(direction, new Array<>());
            directionBlacklist.put(direction, new Array<>());
        }
    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {
        XmlReader.Element whitelist = element.getChildByName("whitelist");
        processBlackWhiteList(whitelist, directionWhitelist, e);

        XmlReader.Element blacklist = element.getChildByName("blacklist");
        processBlackWhiteList(blacklist, directionBlacklist, e);
    }

    /**
     * Converts a tree of xml elements to proper placement restrictions
     * @param element root of list (whitelist/blacklist)
     * @param map map
     * @param e engine
     */
    private void processBlackWhiteList(XmlReader.Element element, ObjectMap<Direction, Array<String>> map, Engine e){
        if(element != null){
            for (int i = 0; i < element.getChildCount(); i++) {
                XmlReader.Element child = element.getChild(i);

                map.get(Direction.valueOf(child.getName().toUpperCase())).add(child.getAttribute("value"));
            }
        }
    }

    /**
     * Checks if a position is valid given the components settings
     * @param map game map
     * @param x x
     * @param y y
     * @param layer layer
     * @return valid or not
     */
    public boolean checkPlacement(GameMap map, int x, int y, int layer){

        for (Direction direction : Direction.values()) {
            int id = MapUtils.getTileType(map, x + direction.getXoffset(),y + direction.getYoffset(),layer);
            TileDef def = map.getEngine().getTileRegistry().getTileDef(id);

            //Check blacklist
            for (String s : directionBlacklist.get(direction)) {
                TileDef thisdef = getTileDef(s, map.getEngine());

                if(thisdef.equals(def))
                    return false;
            }

            //Check whitelist
            if(directionWhitelist.get(direction).size > 0){
                boolean found = false;
                for (String s : directionWhitelist.get(direction)) {
                    TileDef thisdef = getTileDef(s, map.getEngine());

                    if(thisdef.equals(def))
                        found = true;
                }

                if(!found)
                    return false;
            }
        }

        return true;
    }

    private TileDef getTileDef(String value, Engine e){

        if(cache.containsKey(value))
            return cache.get(value);

        TileDef vdef;
        try {
            int id = Integer.parseInt(value);
            vdef = e.getTileRegistry().getTileDef(id);
        } catch(NumberFormatException ex){
            vdef = e.getTileRegistry().getTileDef(value);
        }

        cache.put(value, vdef);

        return vdef;
    }

    @Override
    public Behavior getBehavior() {
        return new RestrictedPlacementBehavior(getClass());
    }

    @Override
    public Component cpy() {
        return new RestrictedPlacementComponent();
    }

}
