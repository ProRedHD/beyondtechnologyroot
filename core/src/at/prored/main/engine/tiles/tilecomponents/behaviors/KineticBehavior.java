package at.prored.main.engine.tiles.tilecomponents.behaviors;

import at.prored.main.engine.components.Component;
import at.prored.main.engine.components.RotateComponent;
import at.prored.main.engine.components.behavior.ActionEvent;
import at.prored.main.engine.components.behavior.Behavior;
import at.prored.main.engine.components.behavior.BehaviorManager;
import at.prored.main.engine.components.behavior.EventType;
import at.prored.main.engine.components.behavior.behaviors.events.TileChangeEvent;
import at.prored.main.engine.entities.Entity;
import at.prored.main.engine.entities.components.StateComponent;
import at.prored.main.engine.entities.components.StatedAnimationComponent;
import at.prored.main.engine.inventory.Inventory;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.tiles.TileDef;
import at.prored.main.engine.tiles.storage.GameMap;
import at.prored.main.engine.tiles.tilecomponents.KineticComponent;
import at.prored.main.engine.tiles.tilecomponents.behaviors.events.KineticChangeEvent;
import at.prored.main.engine.utils.MapUtils;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by ProRed on 20.03.2018.
 */
public class KineticBehavior extends Behavior {

    public KineticBehavior(Class<? extends Component> component) {
        super(component);
    }

    @Override
    public void trigger(EventType type, ActionEvent e, GameMap gameMap, Entity entity) {

    }

    @Override
    public void trigger(EventType type, ActionEvent e, GameMap gameMap, int x, int y, int layer) {

        if (type == EventType.TILE_CHANGE) {
            TileChangeEvent event = ((TileChangeEvent) e);

            int id = MapUtils.getTileType(gameMap, x, y, layer);
            Properties p = MapUtils.getTileProperties(gameMap, x, y, layer);
            TileDef def = gameMap.getEngine().getTileRegistry().getTileDef(id);

            KineticComponent kc = def.getComponent(KineticComponent.class);

            if (kc.getKineticType() == KineticComponent.KineticType.PROVIDER && event.getType() == TileChangeEvent.ChangeType.ADDED) {
                spreadKineticChange(1000, x, y, 0, 0, layer, gameMap);
            } else if (event.getType() == TileChangeEvent.ChangeType.REMOVED) {
                spreadKineticChange(0, x, y, -kc.getSourceX(p), -kc.getSourceY(p), layer, gameMap);
            }

            if (event.getType() == TileChangeEvent.ChangeType.ADDED) {
                rotateAndInit(x, y, layer, gameMap);
            }

        } else if (type == EventType.KINETIC_ENERGY_CHANGED) {
            KineticChangeEvent ke = ((KineticChangeEvent) e);

            int id = MapUtils.getTileType(gameMap, x, y, layer);
            Properties p = MapUtils.getTileProperties(gameMap, x, y, layer);
            TileDef def = gameMap.getEngine().getTileRegistry().getTileDef(id);

            KineticComponent kc = def.getComponent(KineticComponent.class);
            StateComponent stateComponent = def.getComponent(StateComponent.class);
            StatedAnimationComponent statedAnimationComponent = def.getComponent(StatedAnimationComponent.class);
            RotateComponent rc = def.getComponent(RotateComponent.class);


            if (kc == null)
                return;

            kc.setKineticEnergy(p, ke.getEnergy());
            kc.setSourceX(p, MathUtils.clamp(ke.getSenderX() - x, -1, 1));
            kc.setSourceY(p, MathUtils.clamp(ke.getSenderY() - y, -1, 1));

            if (statedAnimationComponent != null) {
                if(kc.getKineticType() != KineticComponent.KineticType.GEARBOX) {
                    if (kc.getKineticEnergy(p) < 0) {
                        statedAnimationComponent.setPlayMode(p, Animation.PlayMode.REVERSED);
                    } else {
                        statedAnimationComponent.setPlayMode(p, Animation.PlayMode.NORMAL);
                    }
                } else {
                    int rotation = rc.getRotation(p);

                    if(rotation == 90){
                        statedAnimationComponent.setPlayMode(p, Animation.PlayMode.REVERSED);
                    } else {
                        statedAnimationComponent.setPlayMode(p, Animation.PlayMode.NORMAL);
                    }
                }
            }

            if (stateComponent != null) {
                if (kc.getKineticType() == KineticComponent.KineticType.GEARBOX)
                    switchGearboxState(stateComponent, p, ke.getEnergy() != 0 ? 6 : 0);
                else
                    stateComponent.setState(ke.getEnergy() != 0 ? 3 : 0, p);

            }

            switch (kc.getKineticType()) {
                case SHAFT:
                    int rotation = rc.getRotation(p);

                    if (rotation == 90) {
                        if (kc.getSourceX(p) != 1)
                            spreadKineticChange(ke.getEnergy(), x, y, 1, 0, layer, gameMap);

                        if (kc.getSourceX(p) != -1)
                            spreadKineticChange(ke.getEnergy(), x, y, -1, 0, layer, gameMap);
                    } else {
                        if (kc.getSourceY(p) != 1)
                            spreadKineticChange(ke.getEnergy(), x, y, 0, 1, layer, gameMap);

                        if (kc.getSourceY(p) != -1)
                            spreadKineticChange(ke.getEnergy(), x, y, 0, -1, layer, gameMap);
                    }

                    break;
                case GEARBOX:
                    rc = def.getComponent(RotateComponent.class);
                    rotation = rc.getRotation(p);
                    int var = getAnimationVariant(stateComponent, p);

                    if (kc.getSourceX(p) != 1 && ((var == 1 && rotation == 270) || (var == 0 && rotation == 90)))
                        spreadKineticChange(ke.getEnergy() * -1, x, y, 1, 0, layer, gameMap);

                    if (kc.getSourceX(p) != -1 && ((var == 1 && rotation == 90) || (var == 0 && rotation == 270)))
                        spreadKineticChange(ke.getEnergy() * -1, x, y, -1, 0, layer, gameMap);

                    if (kc.getSourceY(p) != 1 && ((var == 0 && rotation == 270) || (var == 1 && rotation == 270)))
                        spreadKineticChange(ke.getEnergy() * -1, x, y, 0, 1, layer, gameMap);

                    if (kc.getSourceY(p) != -1 && ((var == 0 && rotation == 90) || (var == 1 && rotation == 90)))
                        spreadKineticChange(ke.getEnergy() * -1, x, y, 0, -1, layer, gameMap);

                    break;
                case PROVIDER:
                    if (kc.getSourceY(p) != -1)
                        spreadKineticChange(ke.getEnergy(), x, y, 0, -1, layer, gameMap);
                    break;
            }
        }
    }

    @Override
    public void trigger(EventType type, ActionEvent e, ItemStack stack, Inventory inventory, GameMap gameMap) {

    }

    /**
     * Spreads energy change over the whole network
     *
     * @param energy  energy
     * @param x       x
     * @param y       y
     * @param offsetX x offset to start
     * @param offsetY y offset to start
     * @param layer   layer
     * @param gameMap gameMap
     */
    private void spreadKineticChange(int energy, int x, int y, int offsetX, int offsetY, int layer, GameMap gameMap) {
        int id = MapUtils.getTileType(gameMap, x + offsetX, y + offsetY, layer);
        TileDef def = gameMap.getEngine().getTileRegistry().getTileDef(id);

        KineticComponent kc = def.getComponent(KineticComponent.class);

        if (kc != null) {
            KineticChangeEvent e = new KineticChangeEvent(x, y, energy);
            BehaviorManager.instance().broadcastTile(EventType.KINETIC_ENERGY_CHANGED, e, gameMap, x + offsetX, y + offsetY, layer);
        }

    }

    /**
     * Checks all four neighbours for rotation and energy
     *
     * @param x       x
     * @param y       y
     * @param layer   layer
     * @param gameMap gameMap
     */
    private void rotateAndInit(int x, int y, int layer, GameMap gameMap) {
        init(x, y, layer, gameMap);
    }

    /**
     * Checks if the tile has a kinetic component and if it can be connected to
     *
     * @param x       x
     * @param y       y
     * @param ox      cx
     * @param oy      cy
     * @param layer   layer
     * @param gameMap gameMap
     * @return kinetic component / null
     */
    private KineticComponent checkForKineticComponent(int x, int y, int ox, int oy, int layer, GameMap gameMap) {
        int id = MapUtils.getTileType(gameMap, x + ox, y + oy, layer);
        TileDef def = gameMap.getEngine().getTileRegistry().getTileDef(id);

        KineticComponent component = def.getComponent(KineticComponent.class);
        return component != null && canConnect(x, y, ox, oy, layer, gameMap) ? component : null;
    }

    /**
     * Checks all four sides for potential kinetic components
     *
     * @param x
     * @param y
     * @param layer
     * @param gameMap
     * @return
     */
    private Vector2 findKineticComponent(int x, int y, int layer, GameMap gameMap, boolean[] allowedSides) {
        KineticComponent kc;
        Vector2 offset = new Vector2();

        kc = checkForKineticComponent(x, y, 1, 0, layer, gameMap);
        if (kc != null && allowedSides[0]) {
            offset.set(1, 0);
            if (kc.getKineticEnergy(MapUtils.getTileProperties(gameMap, x + 1, y, layer)) > 0)
                return offset;
        }

        kc = checkForKineticComponent(x, y, -1, 0, layer, gameMap);
        if (kc != null && allowedSides[1]) {
            offset.set(-1, 0);
            if (kc.getKineticEnergy(MapUtils.getTileProperties(gameMap, x - 1, y, layer)) > 0)
                return offset;
        }

        kc = checkForKineticComponent(x, y, 0, 1, layer, gameMap);
        if (kc != null && allowedSides[2]) {
            offset.set(0, 1);
            if (kc.getKineticEnergy(MapUtils.getTileProperties(gameMap, x, y + 1, layer)) > 0)
                return offset;
        }

        kc = checkForKineticComponent(x, y, 0, -1, layer, gameMap);
        if (kc != null && allowedSides[3]) {
            offset.set(0, -1);
            if (kc.getKineticEnergy(MapUtils.getTileProperties(gameMap, x, y - 1, layer)) > 0)
                return offset;
        }

        return offset;
    }

    /**
     * Initiates the kinetic tile and sets the energy and rotation
     *
     * @param x       x
     * @param y       y
     * @param layer   layer
     * @param gameMap gameMap
     * @return 0 -> no kc found, 1 -> found, but energy = 0, 2 -> found with energy
     */
    private void init(int x, int y, int layer, GameMap gameMap) {
        Vector2 offset = findKineticComponent(x, y, layer, gameMap, new boolean[]{true, true, true, true});

        if (((int) offset.x) == 0 && ((int) offset.y) == 0)
            return;

        int ox = (int) offset.x, oy = (int) offset.y;
        KineticComponent kc = checkForKineticComponent(x, y, ox, oy, layer, gameMap);

        int id = MapUtils.getTileType(gameMap, x, y, layer);
        Properties p = MapUtils.getTileProperties(gameMap, x, y, layer);
        Properties po = MapUtils.getTileProperties(gameMap, x + ox, y + oy, layer);
        TileDef def = gameMap.getEngine().getTileRegistry().getTileDef(id);
        KineticComponent ownkc = def.getComponent(KineticComponent.class);

        //Set rotation
        switch (ownkc.getKineticType()) {
            case SHAFT:

                //set rotation
                RotateComponent rotateComponent = def.getComponent(RotateComponent.class);

                if (ox != 0)
                    rotateComponent.setRotation(p, 90);
                else
                    rotateComponent.setRotation(p, 0);
                break;
            case GEARBOX:
                rotateComponent = def.getComponent(RotateComponent.class);
                StatedAnimationComponent sac = def.getComponent(StatedAnimationComponent.class);
                StateComponent stateComponent = def.getComponent(StateComponent.class);

                if (offset.x == -1) {
                    Vector2 vec = findKineticComponent(x, y, layer, gameMap, new boolean[]{false, false, true, true});
                    if (vec.y == 1) {
                        rotateComponent.setRotation(p, 270);
                        switchAnimationTexture(stateComponent, p, 0);
                    } else {
                        rotateComponent.setRotation(p, 90);
                        switchAnimationTexture(stateComponent, p, 1);
                    }
                } else if (offset.x == 1) {
                    Vector2 vec = findKineticComponent(x, y, layer, gameMap, new boolean[]{false, false, true, true});
                    if (vec.y == 1) {
                        rotateComponent.setRotation(p, 270);
                        switchAnimationTexture(stateComponent, p, 1);
                    } else {
                        rotateComponent.setRotation(p, 90);
                        switchAnimationTexture(stateComponent, p, 0);
                    }
                } else if (offset.y == -1) {
                    Vector2 vec = findKineticComponent(x, y, layer, gameMap, new boolean[]{true, true, false, false});
                    if (vec.x == 1) {
                        rotateComponent.setRotation(p, 90);
                        switchAnimationTexture(stateComponent, p, 0);
                    } else {
                        rotateComponent.setRotation(p, 90);
                        switchAnimationTexture(stateComponent, p, 1);
                    }
                } else if (offset.y == 1) {
                    Vector2 vec = findKineticComponent(x, y, layer, gameMap, new boolean[]{true, true, false, false});
                    if (vec.x == 1) {
                        rotateComponent.setRotation(p, 270);
                        switchAnimationTexture(stateComponent, p, 1);
                    } else {
                        rotateComponent.setRotation(p, 270);
                        switchAnimationTexture(stateComponent, p, 0);
                    }


                }
        }

        //spread energy
        spreadKineticChange(kc.getKineticEnergy(po), x + ((int) offset.x), y + ((int) offset.y), (int) -offset.x, (int) -offset.y, layer, gameMap);
    }

    /**
     * Checks if a shaft can connect to a kinetic tile
     *
     * @param x       x pos of kinetic tile
     * @param y       y pos of kinetic tile
     * @param ox      x offset to the kinetic tile you want to connect
     * @param oy      y offset to the kinetic tile you want to connect
     * @param layer   layer
     * @param gameMap gameMap
     * @return true/false
     */
    private boolean canConnect(int x, int y, int ox, int oy, int layer, GameMap gameMap) {
        int id = MapUtils.getTileType(gameMap, x + ox, y + oy, layer);
        Properties p = MapUtils.getTileProperties(gameMap, x + ox, y + oy, layer);
        TileDef def = gameMap.getEngine().getTileRegistry().getTileDef(id);

        KineticComponent ownkc = def.getComponent(KineticComponent.class);

        if (ownkc == null)
            return false;

        switch (ownkc.getKineticType()) {
            case SHAFT:
                RotateComponent rotateComponent = def.getComponent(RotateComponent.class);

                if (rotateComponent.getRotation(p) == 90 && ox != 0)
                    return true;

                if (rotateComponent.getRotation(p) == 0 && oy != 0)
                    return true;

                break;
            case GEARBOX:
                rotateComponent = def.getComponent(RotateComponent.class);

                if (rotateComponent.getRotation(p) == 0 && (ox == 1 || oy == 1))
                    return true;

                if (rotateComponent.getRotation(p) == 270 && (ox == 1 || oy == -1))
                    return true;

                if (rotateComponent.getRotation(p) == 180 && (ox == -1 || oy == -1))
                    return true;

                if (rotateComponent.getRotation(p) == 90 && (ox == -1 || oy == 1))
                    return true;

            case PROVIDER:
                if (oy == 1)
                    return true;
        }

        return false;
    }

    private void switchAnimationTexture(StateComponent stateComponent, Properties p, int v) {
        int state = stateComponent.getState(p);

        v = MathUtils.clamp(v, 0, 1);

        if (state == 0 || state == 1) {
            stateComponent.setState(v, p);
        } else if (state == 2 || state == 3) {
            stateComponent.setState(2 + v, p);
        } else if (state == 4 || state == 5) {
            stateComponent.setState(4 + v, p);
        } else if (state == 6 || state == 7) {
            stateComponent.setState(6 + v, p);
        }
    }

    private void switchGearboxState(StateComponent stateComponent, Properties p, int state) {
        int curstate = stateComponent.getState(p);

        int offset = curstate % 2 == 0 ? 0 : 1;

        if (state % 2 != 0)
            state -= 1;

        stateComponent.setState(state + offset, p);
    }

    private int getAnimationVariant(StateComponent stateComponent, Properties p) {
        int curstate = stateComponent.getState(p);

        return curstate % 2 == 0 ? 0 : 1;
    }
}
