package at.prored.main.engine.tiles;

import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.graphics.renderer.RenderModeManager;
import at.prored.main.engine.items.ItemDef;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.items.components.ToolComponent;
import at.prored.main.engine.tiles.light.GameMapTileLighter;
import at.prored.main.engine.tiles.storage.GameMap;
import at.prored.main.engine.tiles.tilecomponents.MaterialComponent;
import at.prored.main.engine.utils.MapUtils;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.GdxRuntimeException;

/**
 * Created by ProRed on 25.12.2017.
 */
public class TileBreaker extends InputAdapter {

    private boolean isActive;

    private GameMap gameMap;
    private Vector2 selectedPosition;

    private ItemStack stack;

    private boolean breaking;
    private float breakingTime;
    private float startBreakingTime;

    private Color progressColor;

    private int airID;

    public TileBreaker(GameMap gameMap) {
        this.gameMap = gameMap;

        airID = gameMap.getEngine().getTileRegistry().getTileDef("air").getID();

        this.selectedPosition = new Vector2();
        this.progressColor = new Color(Color.RED);
    }

    /**
     * Activates the tile breaker
     *
     * @param stack selected itemstack
     */
    public void activate(ItemStack stack) {
        this.isActive = true;
        this.stack = stack;
    }

    /**
     * Turns the tile breaker off
     */
    public void deactivate() {
        this.isActive = false;
        this.breaking = false;
    }

    /**
     * Renders the progress bar and highlight
     */
    public void render() {
        if (isActive) {
            ShapeRenderer shapeRenderer = Graphics.instance().getWorldShapeBatch();
            shapeRenderer.setColor(Color.WHITE);
            shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
            shapeRenderer.rect(selectedPosition.x * 32, selectedPosition.y * 32, 32, 32);
            shapeRenderer.end();
            shapeRenderer.setColor(Color.WHITE);

            if (breaking && startBreakingTime != 0) {
                float v = (startBreakingTime / 4);
                float dt1, dt2, dt3, dt4;
                float reverseTime = startBreakingTime - breakingTime;

                dt1 = Math.min(reverseTime / v, 1);
                dt2 = dt1 >= 1 ? Math.min((reverseTime - v) / v, 1) : 0;
                dt3 = dt2 >= 1 ? Math.min((reverseTime - 2 * v) / v, 1) : 0;
                dt4 = dt3 >= 1 ? Math.min((reverseTime - 3 * v) / v, 1) : 0;

                shapeRenderer.setColor(progressColor);
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.rect(selectedPosition.x * 32 + 4, selectedPosition.y * 32 + (32 - 8), 24 * dt1, 4);
                shapeRenderer.rect(selectedPosition.x * 32 + (32 - 8), selectedPosition.y * 32 + (32 - 8), 4, -20 * dt2);
                shapeRenderer.rect(selectedPosition.x * 32 + (32 - 8), selectedPosition.y * 32 + 4, -20 * dt3, 4);
                shapeRenderer.rect(selectedPosition.x * 32 + 4, selectedPosition.y * 32 + 8, 4, 16 * dt4);
                shapeRenderer.end();
                shapeRenderer.setColor(Color.WHITE);
            }
        }
    }

    /**
     * Updates the tile breaker object
     */
    public void update() {
        if (isActive && breaking && startBreakingTime != 0) {

            this.breakingTime -= Graphics.instance().getDeltaTime();

            if (breakingTime <= 0) {
                int layer = selectLayer((int) selectedPosition.x, (int) selectedPosition.y);

                MapUtils.removeTile(gameMap, (int) selectedPosition.x, (int) selectedPosition.y, layer);
                this.breakingTime = 0;
                this.startBreakingTime = 0;

                GameMapTileLighter.updateTileLights(gameMap);
            }

            progressColor.lerp(Color.GREEN, Graphics.instance().getDeltaTime() / startBreakingTime);
        }
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (isActive && breaking) {
            this.breaking = false;
            this.breakingTime = 0;
            this.startBreakingTime = 0;
        }
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (isActive) {
            breaking = true;
            calculateBreakingTime();
        }
        return false;
    }

    /**
     * Calculates the breaking time of a tile
     */
    private void calculateBreakingTime() {
        progressColor.set(Color.RED);

        int x = (int) selectedPosition.x;
        int y = (int) selectedPosition.y;

        int layer = selectLayer(x, y);
        int id = MapUtils.getTileType(gameMap, x, y, layer);

        if (id != airID) {
            TileDef tdef = gameMap.getEngine().getTileRegistry().getTileDef(id);

            ItemDef iDef = gameMap.getEngine().getItemRegistry().getItemDef(stack.getID());
            ToolComponent toolComponent = iDef.getComponent(ToolComponent.class);

            if (!tdef.hasComponent(MaterialComponent.class))
                throw new GdxRuntimeException(String.format("Tile %d cannot be broken, due to missing material component!", id));

            MaterialComponent materialComponent = tdef.getComponent(MaterialComponent.class);

            ToolComponent.ToolProperties toolProperties = toolComponent.getToolProperties(materialComponent.getBreakingType());
            this.breakingTime = (materialComponent.getHardness() / toolProperties.getEfficiency()) * materialComponent.getBreakingSpeedMultiplier();
            this.startBreakingTime = breakingTime;
        } else {
            this.startBreakingTime = 0;
            this.breakingTime = 0;
        }
    }

    /**
     * Selects the layer it should break depending on the selected layer and so on
     */
    private int selectLayer(int x, int y) {
        RenderModeManager.RenderMode renderMode = RenderModeManager.getRenderMode();

        switch (renderMode) {
            case NORMAL:
                int id = MapUtils.getTileType(gameMap, x, y, 0);

                if (id != airID)
                    return 0;

                //Background Layer
                id = MapUtils.getTileType(gameMap, x, y, 1);


                if (id != airID)
                    return 1;

                //Power layer
                id = MapUtils.getTileType(gameMap, x, y, 2);

                if (id != airID)
                    return 2;
                break;
            case POWER:
                return 2;
        }

        return 0;
    }

    /**
     * Calculates the size and position of the highlight box
     */
    private void calculateSelectedPosition() {
        Vector2 mousePos = Graphics.instance().getCamera().getUnprojectedMouseCoordinates();

        int tX = MathUtils.floor(mousePos.x / 32f);
        int tY = MathUtils.floor(mousePos.y / 32f);

        if (breaking && (selectedPosition.x != tX || selectedPosition.y != tY)) {
            selectedPosition.set(tX, tY);
            calculateBreakingTime();
        }

        selectedPosition.set(tX, tY);
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (isActive) {
            calculateSelectedPosition();
            return true;
        }
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        if (isActive) {
            calculateSelectedPosition();
            return true;
        }
        return false;
    }

    public boolean isBreaking() {
        return breaking;
    }

    public boolean isActive() {
        return isActive;
    }
}
