package at.prored.main.engine.tiles;

import at.prored.main.engine.components.Component;
import at.prored.main.engine.utils.Definition;
import com.badlogic.gdx.utils.Array;

import java.util.Iterator;
import java.util.Objects;

/**
 * Created by ProRed on 18.05.2017.
 */
public class TileDef implements Definition {

    private String defid;
    private int id;
    private Array<Component> tileComponents;

    public TileDef(int id){
        this.id = id;
        this.tileComponents = new Array<>();
    }

    public String getDefID() {
        return defid;
    }

    public void setDefID(String defid) {
        this.defid = defid;
    }

    /**
     * Adds a tile component to this tile definition
      * @param tileComponent tile definition to add
     */
    public void addComponent(Component tileComponent){
        if(hasComponent(tileComponent.getClass()))
            removeComponent(tileComponent.getClass());

        tileComponents.add(tileComponent);
    }

    /**
     * Returns a component from a tile definition
     * @param component class of the searched component
     * @return component (or null)
     */
    @SuppressWarnings("unchecked")
    public <T extends Component> T getComponent(Class<T> component){
        for (int i = 0; i < tileComponents.size; i++) {
            if(component.isInstance(tileComponents.get(i)))
                return (T) tileComponents.get(i);
        }

        return null;
    }

    /**
     * Checks if a tile definition has a component
     * @param component class of the component
     * @return true or false
     */
    public boolean hasComponent(Class<? extends Component> component){
        for (int i = 0; i < tileComponents.size; i++) {
            if(component.isInstance(tileComponents.get(i)))
                return true;
        }

        return false;
    }

    /**
     * Removes a component from the definition
     * @param component component
     */
    public void removeComponent(Class<? extends Component> component){
        Iterator<Component> it = tileComponents.iterator();
        while(it.hasNext()){
            if(component.isInstance(it.next()))
                it.remove();
        }
    }

    @Override
    public Array<Component> getComponents() {
        return tileComponents;
    }

    public int getID() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TileDef tileDef = (TileDef) o;
        return id == tileDef.id;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
