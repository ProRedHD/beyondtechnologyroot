package at.prored.main.engine.tiles;

import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Pool;

/**
 * Created by ProRed on 18.05.2017.
 */

/**
 * This object stores information about the tile
 * It was designed to be pooled
 */
public class TileProperties implements Pool.Poolable, Properties {

    private ObjectMap<String, Object> data;
    private float light;

    public TileProperties(){
        this.data = new ObjectMap<String, Object>();
    }

    /**
     * Adds a integer value to the tile property
     * @param key key
     * @param value value
     */
    public void setInteger(String key, int value){
        data.put(key, value);
    }

    /**
     * Adds a integer value to the tile property
     * @param key key
     * @param value value
     */
    public void setFloat(String key, float value){
        data.put(key, value);
    }

    /**
     * Adds a object value to the tile property
     * @param key key
     * @param value value
     */
    public void setData(String key, Object value){
        data.put(key, value);
    }

    /**
     * Adds a integer value to the tile property
     * @param key key
     * @param value value
     */
    public void setBoolean(String key, boolean value){
        data.put(key, value);
    }

    /**
     * Returns a integer from the tile property value set
     * @param key key
     * @return integer
     */
    public int getInteger(String key){
        return (Integer) data.get(key);
    }

    /**
     * Returns a float from the tile property value set
     * @param key key
     * @return float
     */
    public float getFloat(String key){
        return (Float) data.get(key);
    }

    /**
     * Returns a boolean from the tile property value set
     * @param key key
     * @return booleana
     */
    public boolean getBoolean(String key){
        return (Boolean) data.get(key);
    }

    /**
     * Checks if this tile property has key entry
     * @param key key
     * @return true or false
     */
    public boolean hasData(String key){
        return data.containsKey(key);
    }

    /**
     * Returns an object from the tile property value set
     * @param key key
     * @return object
     */
    public Object getData(String key) {return data.get(key); }

    /**
     * Returns the light value of this block
     * @return light value
     */
    public float getLightValue() {
        return light;
    }

    /**
     * Sets the light value
     * @param light new light value
     */
    public void setLightValue(float light) {
        this.light = light;
    }

    /**
     * Reset function for pooling
     */
    @Override
    public void reset() {
        data.clear();
    }

    /**
     * Clears all data
     */
    @Override
    public void clear() {
        reset();
    }

    @Override
    public String toString() {
        return data.toString();
    }
}
