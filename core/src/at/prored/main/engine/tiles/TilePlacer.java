package at.prored.main.engine.tiles;

import at.prored.main.engine.components.FlipComponent;
import at.prored.main.engine.components.RotateComponent;
import at.prored.main.engine.components.TextureStorageComponent;
import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.inventory.Inventory;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.items.components.TileComponent;
import at.prored.main.engine.tiles.light.GameMapTileLighter;
import at.prored.main.engine.tiles.storage.GameMap;
import at.prored.main.engine.tiles.tilecomponents.LayerComponent;
import at.prored.main.engine.tiles.tilecomponents.ReplaceableComponent;
import at.prored.main.engine.tiles.tilecomponents.RestrictedPlacementComponent;
import at.prored.main.engine.utils.AssetUtils;
import at.prored.main.engine.inventory.InventoryUtils;
import at.prored.main.engine.utils.MapUtils;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by ProRed on 05.06.2017.
 */
public class TilePlacer extends InputAdapter {

    private static final float ALPHASPEED = 0.5f;

    private boolean isActive;
    private GameMap gameMap;

    private Rectangle area;
    private float sX, sY;
    private int lastX, lastY, lastTileWidth, lastTileHeight;
    private boolean clicked;
    private boolean placeable, flippedX, flippedY;

    //components of definition
    private FlipComponent flipComponent;
    private RotateComponent rotateComponent;
    private int rotationSelection = 0;

    private TileDef tileDef;
    private int layer;
    private Inventory inventory;
    private Properties p;

    private float time;
    private Color color;
    private Color errorBorderColor;

    //Textures
    private NinePatch arrow_horizontal;
    private NinePatch arrow_vertical;
    private TextureRegion rotating_symbol;

    private int airID, tileID;

    public TilePlacer(GameMap gameMap) {
        this.gameMap = gameMap;
        this.isActive = false;

        airID = gameMap.getEngine().getTileRegistry().getTileDef("air").getID();
        tileID = gameMap.getEngine().getItemRegistry().getItemDef("tile").getID();

        this.area = new Rectangle(0, 0, 32, 32);
        this.color = new Color(Color.WHITE);
        this.errorBorderColor = new Color(0.85f, 0.35f, 0.35f, 1f);

        this.arrow_horizontal = new NinePatch(AssetUtils.loadTextureRegion(gameMap.getEngine().getAssetManager(), "arrow_horizontal"), 4, 4, 2, 2);
        this.arrow_vertical = new NinePatch(AssetUtils.loadTextureRegion(gameMap.getEngine().getAssetManager(), "arrow_vertical"), 2, 2, 4, 4);
        this.rotating_symbol = AssetUtils.loadTextureRegion(gameMap.getEngine().getAssetManager(), "rotating_symbol");

        p = new TileProperties();
    }

    /**
     * Sets the tile def
     *
     * @param tileDef tile def
     */
    public void setTileDef(TileDef tileDef) {
        this.tileDef = tileDef;

        this.flipComponent = tileDef.getComponent(FlipComponent.class);
        this.rotateComponent = tileDef.getComponent(RotateComponent.class);

        RotateComponent rotateComponent = tileDef.getComponent(RotateComponent.class);
        if (rotateComponent != null) {

        }

        LayerComponent layerComponent = tileDef.getComponent(LayerComponent.class);
        if (layerComponent.isForced())
            layer = layerComponent.getLayers().first();
        else if (layerComponent.getLayers().contains(0, false))
            layer = 0;
        else
            layer = layerComponent.getLayers().first();
    }

    /**
     * Places the selected area of tiles into the game map
     */
    public void placeTiles() {
        int amountX = (int) (area.getWidth() / 32f);
        int amountY = (int) (area.getHeight() / 32f);

        int tX = MathUtils.floor(area.x / 32f);
        int tY = MathUtils.floor(area.y / 32f);

        for (int x = tX; x < tX + amountX; x++) {
            for (int y = tY; y < tY + amountY; y++) {
                MapUtils.placeTile(gameMap, x, y, tileDef.getID(), layer);

                //apply flip
                if (flipComponent != null) {
                    Properties p = MapUtils.getTileProperties(gameMap, x, y, layer);
                    flipComponent.setFlipX(p, flippedX);
                    flipComponent.setFlipY(p, flippedY);
                }

                //apply rotation
                if (rotateComponent != null && rotateComponent.getRotations().size > 0) {
                    Properties p = MapUtils.getTileProperties(gameMap, x, y, layer);
                    rotateComponent.setRotation(p, rotateComponent.getRotations().get(rotationSelection));
                }
            }
        }

        ItemStack stack = ItemStack.createInstance(tileID, amountX * amountY, gameMap.getEngine().getItemRegistry());
        TileComponent.setTileID(tileDef.getID(), stack.getProperties());
        InventoryUtils.removeItemStack(stack, inventory);

        GameMapTileLighter.updateTileLights(gameMap);

        //Check if there is anything left
        stack.getProperties().setAmount(1);
        this.isActive = InventoryUtils.hasItemStack(stack, inventory);
    }

    @Override
    public boolean keyUp(int keycode) {
        if (flipComponent != null) {
            if (keycode == Input.Keys.F && !Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
                this.flippedX = !flippedX;
                return true;
            } else if (keycode == Input.Keys.F && Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
                this.flippedY = !flippedY;
                return true;
            }
        }

        if (rotateComponent != null && rotateComponent.getRotations().size > 0) {
            if (keycode == Input.Keys.R && !Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {

                this.rotationSelection++;

                if (this.rotationSelection >= rotateComponent.getRotations().size)
                    this.rotationSelection = 0;

                return true;
            } else if (keycode == Input.Keys.R && Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {

                this.rotationSelection--;

                if (this.rotationSelection < 0)
                    this.rotationSelection = rotateComponent.getRotations().size - 1;

                return true;
            }
        }

        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (isActive) {
            clicked = true;
            return true;
        }

        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        if (isActive) {

            //Calculate position
            Vector2 mousePos = Graphics.instance().getCamera().getUnprojectedMouseCoordinates();

            int tX = MathUtils.floor(mousePos.x / 32f);
            int tY = MathUtils.floor(mousePos.y / 32f);

            area.setX(tX * 32);
            area.setY(tY * 32);

            sX = area.x;
            sY = area.y;

            if (lastX != (int) sX || lastY != (int) sY) {
                checkPlacement();
                checkInventory();

                lastX = (int) sX;
                lastY = (int) sY;
            }
            return true;
        }

        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (isActive) {
            clicked = false;
            if (placeable) placeTiles();

            //reset area
            area.setSize(32, 32);
            mouseMoved(screenX, screenY);
            return true;
        }

        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (isActive && clicked) {
            resizeSelectedArea();
            return true;
        }

        return false;
    }

    /**
     * Resizes the selected area
     */
    public void resizeSelectedArea() {
        Vector2 mousePos = Graphics.instance().getCamera().getUnprojectedMouseCoordinates();

        int tX = MathUtils.floor(mousePos.x / 32f);
        int tY = MathUtils.floor(mousePos.y / 32f);

        float difX = tX * 32 - sX;
        float difY = tY * 32 - sY;


        if (difX < 0) {
            area.setX(sX + difX);
            area.setWidth(Math.abs(difX) + 32);
        } else {
            area.setX(sX);
            area.setWidth(difX + 32);
        }

        if (difY < 0) {
            area.setY(sY + difY);
            area.setHeight(Math.abs(difY) + 32);
        } else {
            area.setY(sY);
            area.setHeight(difY + 32);
        }

        int amountX = (int) (area.getWidth() / 32f);
        int amountY = (int) (area.getHeight() / 32f);

        if (lastTileWidth != amountX || lastTileHeight != amountY) {
            checkPlacement();
            checkInventory();

            lastTileWidth = amountX;
            lastTileHeight = amountY;
        }
    }

    /**
     * Checks if the area collides with any blocks already placed in the world
     */
    private void checkPlacement() {
        int amountX = (int) (area.getWidth() / 32f);
        int amountY = (int) (area.getHeight() / 32f);

        int tX = MathUtils.floor(area.x / 32f);
        int tY = MathUtils.floor(area.y / 32f);

        boolean hasRestrictedPlacement = tileDef.hasComponent(RestrictedPlacementComponent.class);
        RestrictedPlacementComponent resplac = tileDef.getComponent(RestrictedPlacementComponent.class);

        this.placeable = true;
        for (int x = tX; x < tX + amountX; x++) {
            for (int y = tY; y < tY + amountY; y++) {

                //Check if loop tile is air or has a replaceable component
                int tileType = MapUtils.getTileType(gameMap, x, y, layer);
                TileDef tileDef = gameMap.getEngine().getTileRegistry().getTileDef(tileType);
                if (tileType != airID && !tileDef.hasComponent(ReplaceableComponent.class)) {
                    this.placeable = false;
                    return;
                }

                if (hasRestrictedPlacement) {
                    if(!resplac.checkPlacement(gameMap, x, y, layer)) {
                        this.placeable = false;
                        return;
                    }
                }

            }
        }
    }

    /**
     * Checks if enough items are in the inventory
     */
    private void checkInventory() {
        int amountX = (int) (area.getWidth() / 32f);
        int amountY = (int) (area.getHeight() / 32f);

        ItemStack stack = ItemStack.createInstance(tileID, amountX * amountY, gameMap.getEngine().getItemRegistry());
        TileComponent.setTileID(tileDef.getID(), stack.getProperties());
        boolean enough = InventoryUtils.hasItemStack(stack, inventory);
        this.placeable = this.placeable && enough;
    }

    /**
     * Updates the tile placer
     */
    public void update() {
        if (isActive) {
            //Update alpha animation
            time += Math.PI * Graphics.instance().getDeltaTime() * ALPHASPEED * (placeable ? 1f : 1.25f);
            time %= Math.PI;

            color.set(1f, !placeable ? .35f : 1f, !placeable ? .35f : 1f, (0.35f - 0.65f) * (float) Math.sin(time) + 0.65f);
        }
    }

    /**
     * Renders the selected area and tiles the selected texture onto the screen
     */
    public void render() {
        if (isActive) {

            //Check if the tiledef has a renderable component and updateLightBuffer the stone texture
            if (tileDef.hasComponent(TextureStorageComponent.class)) {
                int amountX = (int) (area.getWidth() / 32f);
                int amountY = (int) (area.getHeight() / 32f);

                SpriteBatch batch = Graphics.instance().getWorldBatch();
                TextureStorageComponent texComp = tileDef.getComponent(TextureStorageComponent.class);

                //set flipped
                if (flipComponent != null) {
                    flipComponent.setFlipX(p, flippedX);
                    flipComponent.setFlipY(p, flippedY);
                }

                //set rotation
                if (rotateComponent != null && rotateComponent.getRotations().size > 0) {
                    rotateComponent.setRotation(p, rotateComponent.getRotations().get(rotationSelection));
                }

                batch.setColor(color);
                batch.begin();
                for (int x = 0; x < amountX; x++) {
                    for (int y = 0; y < amountY; y++) {
                        texComp.render(batch, area.x + x * 32, area.y + y * 32, 1, 1, Graphics.instance().getCamera().getWorldCamera(), tileDef, p, gameMap.getEngine());
                    }
                }
                batch.end();
                batch.setColor(Color.WHITE);
            }

            //Render selected area
            ShapeRenderer shapeRenderer = Graphics.instance().getWorldShapeBatch();
            shapeRenderer.setColor(placeable ? Color.WHITE : errorBorderColor);
            shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
            shapeRenderer.rect(area.x, area.y, area.width, area.height);
            shapeRenderer.end();
            shapeRenderer.setColor(Color.WHITE);

            renderAttachments();
        }
    }

    private void renderAttachments() {

        SpriteBatch batch = Graphics.instance().getWorldBatch();

        batch.begin();
        batch.setColor(placeable ? Color.WHITE : errorBorderColor);
        if (flipComponent != null) {

            //render vertical flip arrow
            if (flipComponent.isUiFlippableY()) {
                arrow_vertical.draw(batch, area.x + area.width + 2, area.y + 3, 5, area.height - 6);
            }

            //render horizontal flip arrows
            if (flipComponent.isUiFlippableX()) {
                arrow_horizontal.draw(batch, area.x + 3, area.y - 7, area.width - 6, 5);
            }
        }

        if (rotateComponent != null) {
            if (rotateComponent.getRotations().size > 0) {
                batch.draw(rotating_symbol, area.x + area.width - 2, area.y + area.height - 2, 16, 16);
            }
        }

        batch.end();
    }

    /**
     * Returns if the tile placer is active
     *
     * @return active state
     */
    public boolean isActive() {
        return isActive;
    }

    public void activate() {
        isActive = true;
        mouseMoved(Gdx.input.getX(), Gdx.input.getY());

        checkPlacement();
        checkInventory();
    }

    public void deactivate() {
        isActive = false;
        tileDef = null;
        inventory = null;
        rotationSelection = 0;
        p.clear();
    }

    public boolean isClicked() {
        return clicked;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }
}
