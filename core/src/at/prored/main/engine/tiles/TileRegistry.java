package at.prored.main.engine.tiles;

import at.prored.main.engine.Engine;
import at.prored.main.engine.init.ContentPackage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by ProRed on 18.05.2017.
 */
public class TileRegistry {

    private Engine engine;

    private ObjectMap<Integer, TileDef> tileDefs;
    private ObjectMap<String, Integer> defIDs;

    public TileRegistry(Engine engine) {
        this.engine = engine;

        this.tileDefs = new ObjectMap<>();
        this.defIDs = new ObjectMap<>();
    }

    public int size(){
        return tileDefs.size;
    }


    /**
     * Registers a tile definition
     *
     * @param id      id of this tile
     * @param tileDef tile def for this tile
     */
    public void registerTileDef(int id, TileDef tileDef) {
        this.tileDefs.put(id, tileDef);

        //Store defid
        ContentPackage contentPackage = engine.getContentPackageRegistry().getContentPackageFromContentID(id);
        this.defIDs.put(contentPackage.getPackageID() + ":" + tileDef.getDefID(), id);
    }

    /**
     * Unregisters a tile definition
     *
     * @param id id of the tile
     */
    public void unregisterTileDef(int id) {
        this.tileDefs.remove(id);
    }

    /**
     * Checks if a tile id is registered / if a tile def is available
     *
     * @param id id of the tile
     * @return true or false
     */
    public boolean hasTileDef(int id) {
        return this.tileDefs.containsKey(id);
    }

    /**
     * Returns a tile definition
     *
     * @param id id of the tile
     * @return tile definition
     */
    public TileDef getTileDef(int id) {
        return this.tileDefs.get(id);
    }

    /**
     * Returns a tile definition
     * @param defid defid of tile
     * @return tile definition
     */
    public TileDef getTileDef(String defid) {
        if (!defid.contains(":"))
            defid = "btcontent:" + defid;


        Integer id = defIDs.get(defid);

        if(id == null)
            return null;

        return getTileDef(id);
    }

    /**
     * Returns all tile defs
     * @return all registered tile definitions
     */
    public Array<TileDef> getTileDefs(){
        return new Array<TileDef>(tileDefs.values().toArray());
    }

    /**
     * Returns engine
     * @return engine
     */
    public Engine getEngine() {
        return engine;
    }
}
