package at.prored.main.engine.tiles.light;

import at.prored.main.engine.tiles.TileDef;
import at.prored.main.engine.tiles.TileProperties;
import at.prored.main.engine.tiles.storage.GameMap;
import at.prored.main.engine.utils.MapUtils;

/**
 * Created by ProRed on 18.06.2017.
 */
public class GameMapTileLighter {

    /**
     * Calculates the tile light for every tile in the active chunks
     * @param gameMap game map
     */
    public static void updateTileLights(GameMap gameMap) {
        int startX = (int) ((gameMap.getPlayerChunk().x - GameMap.CHUNK_RENDER_RADIUS) * 16);
        int endX = (int) ((gameMap.getPlayerChunk().x + GameMap.CHUNK_RENDER_RADIUS + 1) * 16 - 1);

        int startY = (int) ((gameMap.getPlayerChunk().y - GameMap.CHUNK_RENDER_RADIUS) * 16);
        int endY = (int) ((gameMap.getPlayerChunk().y + GameMap.CHUNK_RENDER_RADIUS + 1) * 16 - 1);

        float currentLightValue = 0f;

        TileDef air = gameMap.getEngine().getTileRegistry().getTileDef("air");

        //Top to bottom
        for (int x = startX; x <= endX; x++) {
            for (int y = endY; y >= startY; y--) {
                int tileID = MapUtils.getTileType(gameMap, x, y, 0);
                TileProperties tileProperties = MapUtils.getTileProperties(gameMap, x, y, 0);

                if (tileID == air.getID()) {
                    currentLightValue = 1.0f;
                    continue;
                }

                TileDef def = gameMap.getEngine().getTileRegistry().getTileDef(tileID);

                if (def.hasComponent(LightComponent.class)) {
                    tileProperties.setLightValue(def.getComponent(LightComponent.class).getLightEmitValue());
                }

                if (tileProperties.getLightValue() > currentLightValue) {
                    currentLightValue = tileProperties.getLightValue();
                } else if (currentLightValue != 0) {
                    tileProperties.setLightValue(currentLightValue);
                }

                if (tileProperties.getLightValue() == currentLightValue) {
                    float falloff = def.hasComponent(LightComponent.class) ? def.getComponent(LightComponent.class).getLightFalloffValue() : 0f;
                    currentLightValue -= falloff;
                }
            }
        }

        //Bottom to top
        for (int x = startX; x <= endX; x++) {
            for (int y = startY; y <= endY; y++) {
                int tileID = MapUtils.getTileType(gameMap, x, y, 0);
                TileProperties tileProperties = MapUtils.getTileProperties(gameMap, x, y, 0);

                if (tileID == air.getID()) {
                    currentLightValue = 1.0f;
                    continue;
                }

                TileDef def = gameMap.getEngine().getTileRegistry().getTileDef(tileID);

                if (tileProperties.getLightValue() > currentLightValue) {
                    currentLightValue = tileProperties.getLightValue();
                } else if (currentLightValue != 0) {
                    tileProperties.setLightValue(currentLightValue);
                }

                if (tileProperties.getLightValue() == currentLightValue) {
                    float falloff = def.hasComponent(LightComponent.class) ? def.getComponent(LightComponent.class).getLightFalloffValue() : 0f;
                    currentLightValue -= falloff;
                }
            }
        }

        //Left to right
        for (int y = startY; y <= endY; y++) {
            for (int x = startX; x <= endX; x++) {
                int tileID = MapUtils.getTileType(gameMap, x, y, 0);
                TileProperties tileProperties = MapUtils.getTileProperties(gameMap, x, y, 0);

                if (tileID == air.getID()) {
                    currentLightValue = 1.0f;
                    continue;
                }

                TileDef def = gameMap.getEngine().getTileRegistry().getTileDef(tileID);

                if (tileProperties.getLightValue() > currentLightValue) {
                    currentLightValue = tileProperties.getLightValue();
                } else if (currentLightValue != 0) {
                    tileProperties.setLightValue(currentLightValue);
                }

                if (tileProperties.getLightValue() == currentLightValue) {
                    float falloff = def.hasComponent(LightComponent.class) ? def.getComponent(LightComponent.class).getLightFalloffValue() : 0f;
                    currentLightValue -= falloff;
                }
            }
        }

        //Right to Left
        for (int y = startY; y <= endY; y++) {
            for (int x = endX; x >= startX; x--) {
                int tileID = MapUtils.getTileType(gameMap, x, y, 0);
                TileProperties tileProperties = MapUtils.getTileProperties(gameMap, x, y, 0);

                if (tileID == air.getID()) {
                    currentLightValue = 1.0f;
                    continue;
                }

                TileDef def = gameMap.getEngine().getTileRegistry().getTileDef(tileID);

                if (tileProperties.getLightValue() > currentLightValue) {
                    currentLightValue = tileProperties.getLightValue();
                } else if (currentLightValue != 0) {
                    tileProperties.setLightValue(currentLightValue);
                }

                if (tileProperties.getLightValue() == currentLightValue) {
                    float falloff = def.hasComponent(LightComponent.class) ? def.getComponent(LightComponent.class).getLightFalloffValue() : 0f;
                    currentLightValue -= falloff;
                }
            }
        }

        gameMap.getLightProcessor().updateLightBuffer(gameMap.getEngine());
    }
}
