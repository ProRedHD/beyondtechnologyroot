package at.prored.main.engine.tiles.light;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.Component;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 18.06.2017.
 */
public class LightComponent extends Component {

    private float lightEmitValue;
    private float lightFalloffValue;

    public LightComponent(float lightEmitValue, float lightFalloffValue) {
        this.lightEmitValue = lightEmitValue;
        this.lightFalloffValue = lightFalloffValue;
    }

    public LightComponent() {
    }

    public float getLightEmitValue() {
        return lightEmitValue;
    }

    public float getLightFalloffValue() {
        return lightFalloffValue;
    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {
        this.lightEmitValue = Float.parseFloat(element.getAttribute("emit", "0"));
        this.lightFalloffValue = Float.parseFloat(element.getAttribute("falloff", "0"));
    }

    @Override
    public Component cpy() {
        return new LightComponent(lightEmitValue, lightFalloffValue);
    }

}
