package at.prored.main.engine.tiles.light;

import at.prored.main.engine.Engine;
import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.tiles.TileDef;
import at.prored.main.engine.tiles.TileProperties;
import at.prored.main.engine.tiles.storage.Chunk;
import at.prored.main.engine.tiles.storage.GameMap;
import at.prored.main.engine.utils.AssetUtils;
import at.prored.main.engine.utils.MapUtils;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;

/**
 * Created by ProRed on 18.06.2017.
 */
public class LightProcessor {

    private GameMap gameMap;

    private FrameBuffer tileBuffer;
    private FrameBuffer finalBuffer;

    private OrthographicCamera lightCamera;

    private TextureRegion pixel;
    private TextureRegion texture;
    private Color renderColor;

    public LightProcessor(GameMap gameMap){
        this.gameMap = gameMap;

        this.tileBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, Chunk.CHUNK_SIZE  * (GameMap.CHUNK_RENDER_RADIUS * 2 + 1) , Chunk.CHUNK_SIZE * (GameMap.CHUNK_RENDER_RADIUS * 2 + 1), false);
        this.finalBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, Graphics.V_WIDTH, Graphics.V_HEIGHT, false);

        this.lightCamera = new OrthographicCamera();
        this.lightCamera.setToOrtho(true, tileBuffer.getWidth(), tileBuffer.getHeight());
        this.lightCamera.position.set(tileBuffer.getWidth() / 2f, tileBuffer.getHeight() / 2f, 0);
        this.lightCamera.update();

        this.pixel = AssetUtils.loadAtlas(gameMap.getEngine().getAssetManager()).findRegion("pixel");
        this.renderColor = new Color(Color.BLACK);

        this.texture = new TextureRegion(finalBuffer.getColorBufferTexture());
        this.texture.flip(false, true);
    }

    public void updateLightBuffer(Engine e){
        //Render to tile buffer
        renderToTileBuffer(e);

        //Render to final buffer
        renderTileBufferToFinalBuffer();
    }

    public void renderTileBufferToFinalBuffer(){
        int startX = (int) ((gameMap.getPlayerChunk().x - GameMap.CHUNK_RENDER_RADIUS) * 16);
        int startY = (int) ((gameMap.getPlayerChunk().y - GameMap.CHUNK_RENDER_RADIUS) * 16);

        SpriteBatch worldBatch = Graphics.instance().getWorldBatch();

        tileBuffer.getColorBufferTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        finalBuffer.begin();

        Gdx.gl.glClearColor(0f, 0f, 0f, 0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        worldBatch.begin();

        worldBatch.draw(tileBuffer.getColorBufferTexture(), startX * 32, startY * 32, tileBuffer.getWidth() * 32, tileBuffer.getHeight() * 32);

        worldBatch.end();

        finalBuffer.end();
    }

    public void renderBufferToScreen(){
        SpriteBatch batch = Graphics.instance().getHudBatch();

        batch.begin();
        batch.draw(texture, -640, -360, 1280, 720);
        batch.end();
/*
        int startX = (int) ((gameMap.getPlayerChunk().x - GameMap.CHUNK_RENDER_RADIUS) * 16);
        int startY = (int) ((gameMap.getPlayerChunk().y - GameMap.CHUNK_RENDER_RADIUS) * 16);

        SpriteBatch worldBatch = Graphics.instance().getWorldBatch();

        worldBatch.setBlendFunction(GL20.GL_DST_COLOR, GL20.GL_ONE);

        worldBatch.begin();

        worldBatch.draw(tileBuffer.getColorBufferTexture(), startX * 32, startY * 32, tileBuffer.getWidth() * 32, tileBuffer.getHeight() * 32);

        worldBatch.end();

        worldBatch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA); */
    }

    private void renderToTileBuffer(Engine e){
        int startX = (int) ((gameMap.getPlayerChunk().x - GameMap.CHUNK_RENDER_RADIUS) * 16);
        int endX = (int) ((gameMap.getPlayerChunk().x + GameMap.CHUNK_RENDER_RADIUS + 1) * 16 - 1);

        int startY = (int) ((gameMap.getPlayerChunk().y - GameMap.CHUNK_RENDER_RADIUS) * 16);
        int endY = (int) ((gameMap.getPlayerChunk().y + GameMap.CHUNK_RENDER_RADIUS + 1) * 16 - 1);

        SpriteBatch batch = Graphics.instance().getWorldBatch();

        TileDef def = e.getTileRegistry().getTileDef("air");

        //Render to texture
        tileBuffer.begin();

        Gdx.gl.glClearColor(0f, 0f, 0f, 0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(lightCamera.combined);
        batch.begin();

        for (int y = startY; y <= endY; y++) {
            for (int x = startX; x <= endX; x++) {
                int tileID = MapUtils.getTileType(gameMap, x, y, 0);
                TileProperties tileProperties = MapUtils.getTileProperties(gameMap, x, y, 0);

                if (tileID == def.getID()) {
                    continue;
                }

                //Update color
                renderColor.set(tileProperties.getLightValue(), tileProperties.getLightValue(), tileProperties.getLightValue(), 1f);
                batch.setColor(renderColor);

                //Draw to frame buffer
                batch.draw(pixel, x - startX, y - startY, 16f, 16f);
            }
        }

        batch.end();
        batch.setProjectionMatrix(Graphics.instance().getCamera().getWorldCamera().combined);

        tileBuffer.end();
    }

    public Texture getTexture() {
        return finalBuffer.getColorBufferTexture();
    }
}
