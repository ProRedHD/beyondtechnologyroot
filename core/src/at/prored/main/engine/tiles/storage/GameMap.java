package at.prored.main.engine.tiles.storage;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.InventoryComponent;
import at.prored.main.engine.conduit.GridManager;
import at.prored.main.engine.crafting.queue.CraftingQueue;
import at.prored.main.engine.entities.Entity;
import at.prored.main.engine.generation.WorldGenerator;
import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.graphics.renderer.GameMapRenderer;
import at.prored.main.engine.graphics.renderer.RenderModeManager;
import at.prored.main.engine.guis.GUIManager;
import at.prored.main.engine.guis.inventory.InventoryGui;
import at.prored.main.engine.input.GameMapInputAdapter;
import at.prored.main.engine.physics.Physics;
import at.prored.main.engine.saving.WorldSaver;
import at.prored.main.engine.tiles.RandomTickDistributor;
import at.prored.main.engine.tiles.TileBreaker;
import at.prored.main.engine.tiles.TilePlacer;
import at.prored.main.engine.tiles.layers.LayerRegistry;
import at.prored.main.engine.tiles.light.GameMapTileLighter;
import at.prored.main.engine.tiles.light.LightProcessor;
import at.prored.main.engine.utils.MapUtils;
import at.prored.main.engine.utils.Pools;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;

import java.util.Random;

/**
 * Created by ProRed on 18.05.2017.
 */
public class GameMap {

    public static final int CHUNK_RENDER_RADIUS = 2;
    public static boolean DEBUG = false;

    //World variables
    String name;
    int seed;
    private Engine engine;
    private ObjectMap<Vector2, Chunk> chunks;
    private ObjectMap<Vector2, Chunk> activeChunks;

    //Chunk position of the player
    private Vector2 playerChunk;
    private Entity player;

    private CraftingQueue craftingQueuePlayer;

    //Processors and managers
    private GameMapInputAdapter gameMapInputAdapter;
    private LightProcessor lightProcessor;
    private RenderModeManager renderModeManager;
    private GameMapRenderer renderer;
    private TilePlacer tilePlacer;
    private TileBreaker tileBreaker;
    private RandomTickDistributor randomTickDistributor;
    private GridManager gridManager;

    //Physics
    private Array<Entity> physicsEntities;
    private int lastX, lastY;

    public GameMap(Engine engine, String name) {
        this.seed = new Random().nextInt();
        this.name = name;

        this.engine = engine;

        this.chunks = new ObjectMap<>();
        this.activeChunks = new ObjectMap<>();
        this.playerChunk = new Vector2();

        this.player = Entity.createInstance(1, engine);
        this.player.getEntityProperties().setBoolean("player", true);

        craftingQueuePlayer = new CraftingQueue(5, InventoryComponent.getInventory(player),engine);

        this.gameMapInputAdapter = new GameMapInputAdapter(this);
        this.lightProcessor = new LightProcessor(this);

        this.renderModeManager = new RenderModeManager(this);
        this.renderer = new GameMapRenderer(this);

        this.tilePlacer = new TilePlacer(this);
        this.tileBreaker = new TileBreaker(this);
        this.randomTickDistributor = new RandomTickDistributor(this);

        this.gridManager = new GridManager();

        for (int x = 0; x < 1; x++) {
            for (int y = 0; y < 1; y++) {
                Chunk ch = WorldGenerator.generateChunk(x, y, engine);
                ch.populateTileBoundingBoxes(engine.getTileRegistry(), x, y);
                this.chunks.put(new Vector2(x, y), ch);
            }
        }

        player.getEntityProperties().getPosition().add(32 * 4, 32 * 20);
        MapUtils.addEntity(player, this);

        populateActiveChunks();
        checkEntityPositionForChunkChange(player);

        GameMapTileLighter.updateTileLights(this);
        lightProcessor.updateLightBuffer(engine);
    }

    /**
     * Registers the input processors
     */
    public void registerInputProcessors() {
        engine.getInputMultiplexer().addProcessor(gameMapInputAdapter);
    }

    /**
     * Unregisters the input processors
     */
    public void unregisterInputProcessors() {
        engine.getInputMultiplexer().removeProcessor(gameMapInputAdapter);
    }

    /**
     * Updates the game map
     */
    public void update() {

        //Update camera position
        updateCameraPosition();

        //Update random tick distributor
        randomTickDistributor.update();

        //Update active chunks
        for (Chunk ch : activeChunks.values()) {
            ch.update(this);
        }

        //Update physics
        updatePhysics();

        //Update key binds
        renderModeManager.updateKeybinds();

        //Update Renderer
        renderer.update();

        //Update tile breaker/placer
        tilePlacer.update();
        tileBreaker.update();

        craftingQueuePlayer.update();

        updateInventoryContent();

        //Update Debug Value
        if (Gdx.input.isKeyJustPressed(Input.Keys.F3))
            DEBUG = !DEBUG;

        //Save world to disk
        if (Gdx.input.isKeyJustPressed(Input.Keys.L)) {
            WorldSaver.saveGameMap(this);
        }
    }

    /**
     * Updates the camera position to be centered around the player
     */
    public void updateCameraPosition() {
        OrthographicCamera camera = Graphics.instance().getCamera().getWorldCamera();

        Vector3 cameraPos = camera.position;
        cameraPos.x = camera.position.x + ((player.getEntityProperties().getPosition().x + 16) - camera.position.x) * 2 * Graphics.instance().getDeltaTime();
        cameraPos.y = camera.position.y + ((player.getEntityProperties().getPosition().y + 32) - camera.position.y) * 2 * Graphics.instance().getDeltaTime();
        camera.position.set(cameraPos);
        camera.update();

        //Update tile placer of position change
        if(tilePlacer.isActive() && !tilePlacer.isClicked()){
            tilePlacer.mouseMoved(Gdx.input.getX(), Gdx.input.getY());
        }

        //Update tile breaker of position change
        if(tileBreaker.isActive()){
            tileBreaker.mouseMoved(Gdx.input.getX(), Gdx.input.getY());
        }
    }

    private void updateInventoryContent() {
        int x = (int) Math.floor(player.getEntityProperties().getPosition().x / 32f);
        int y = (int) Math.floor(player.getEntityProperties().getPosition().y / 32f);

        InventoryGui gui = (InventoryGui) engine.getGUIManager().getGUI(GUIManager.GUI.INVENTORY);

        if (x != lastX || y != lastY) {

            gui.clear();
            for (int x1 = x - 3; x1 <= x + 3; x1++) {
                for (int y1 = y - 3; y1 <= y + 3; y1++) {
                    for (int layer = 0; layer < LayerRegistry.getLayerCount(); layer++) {
                        gui.addTile(x1, y1, layer);
                    }
                }
            }

            lastX = x;
            lastY = y;
        }

    }

    /**
     * Update the physics of the game map
     */
    private void updatePhysics() {

        if (physicsEntities == null)
            physicsEntities = new Array<Entity>();

        //Get all entities
        physicsEntities.clear();
        for (Chunk ch : activeChunks.values()) {
            physicsEntities.addAll(ch.getEntities());
        }

        Physics.performPhysics(physicsEntities, activeChunks, engine.getEntityRegistry(), this);
    }

    /**
     * Renders all tiles that should be rendered.
     */
    public void render() {
        //Render renderer
        renderer.render();

        //Render tile buffer to final buffer
        lightProcessor.renderTileBufferToFinalBuffer();

        //Render tile placer/breaker overlays
        tilePlacer.render();
        tileBreaker.render();

        //Render guis
        engine.getGUIManager().render(Graphics.instance().getHudBatch());
    }

    /**
     * Populates a objectmap with all the chunks that "active" and should be rendered
     */
    public void populateActiveChunks() {
        activeChunks.clear();

        Vector2 vec = Pools.instance().getVector2();
        for (int x = (int) (playerChunk.x - CHUNK_RENDER_RADIUS); x <= playerChunk.x + CHUNK_RENDER_RADIUS; x++) {
            for (int y = (int) (playerChunk.y - CHUNK_RENDER_RADIUS); y <= playerChunk.y + CHUNK_RENDER_RADIUS; y++) {
                vec.set(x, y);
                if (!chunks.containsKey(vec)) {
                    chunks.put(new Vector2(vec), WorldGenerator.generateChunk(x, y, engine));
                }

                Chunk ch = chunks.get(vec);
                ch.populateTileBoundingBoxes(engine.getTileRegistry(), x, y);

                activeChunks.put(chunks.findKey(ch, true), ch);
            }
        }


        Pools.instance().getVector2Pool().free(vec);
    }

    /**
     * Checks the position of the entity for a chunk change and changes the chunk the entity is in
     *
     * @param e entity that should be checked
     */
    public void checkEntityPositionForChunkChange(Entity e) {
        int chunkX = (int) Math.floor((MathUtils.round(e.getEntityProperties().getPosition().x) / 32f) / 16f);
        int chunkY = (int) Math.floor((MathUtils.round(e.getEntityProperties().getPosition().y) / 32f) / 16f);

        Vector2 currentChunk = Pools.instance().getVector2Pool().obtain().set(chunkX, chunkY);
        Vector2 entityChunk = searchEntityInActiveChunk(e);

        if (entityChunk == null)
            return;

        if (!currentChunk.equals(entityChunk)) {
            this.chunks.get(entityChunk).removeEntity(e);

            //Create a new chunk if necessary
            if (!this.chunks.containsKey(currentChunk)) {
                this.chunks.put(new Vector2(currentChunk), new Chunk());
            }
            this.chunks.get(currentChunk).addEntity(e);

            if (e.getID() == player.getID()) {
                this.playerChunk.set(currentChunk);
                populateActiveChunks();

                GameMapTileLighter.updateTileLights(this);
                lightProcessor.updateLightBuffer(engine);
            }
        }
    }

    /**
     * Returns world seed
     * @return seed
     */
    public int getSeed() {
        return seed;
    }

    /**
     * Searches an entity in the active chunks of the world
     *
     * @param se searched entity
     * @return chunk position
     */
    public Vector2 searchEntityInActiveChunk(Entity se) {
        for (Vector2 vec : activeChunks.keys()) {
            for (Entity e : activeChunks.get(vec).getEntities()) {
                if (e == se) {
                    return vec;
                }
            }
        }

        return null;
    }

    /**
     * Returns the chunk map of the world
     *
     * @return chunk map
     */
    public ObjectMap<Vector2, Chunk> getChunks() {
        return chunks;
    }

    /**
     * Returns the game engine
     *
     * @return engine
     */
    public Engine getEngine() {
        return engine;
    }

    /**
     * Returns the player chunk position
     *
     * @return player chunk position
     */
    public Vector2 getPlayerChunk() {
        return playerChunk;
    }

    public LightProcessor getLightProcessor() {
        return lightProcessor;
    }

    public TilePlacer getTilePlacer() {
        return tilePlacer;
    }

    public TileBreaker getTileBreaker() {
        return tileBreaker;
    }

    public ObjectMap<Vector2, Chunk> getActiveChunks() {
        return activeChunks;
    }

    public String getName() {
        return name;
    }

    public Entity getPlayer() {
        return player;
    }

    public void setPlayer(Entity player) {
        this.player = player;
    }

    public GridManager getGridManager() {
        return gridManager;
    }

    public CraftingQueue getCraftingQueuePlayer() {
        return craftingQueuePlayer;
    }
}
