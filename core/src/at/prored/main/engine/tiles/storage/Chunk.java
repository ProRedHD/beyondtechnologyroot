package at.prored.main.engine.tiles.storage;

import at.prored.main.engine.collision.AABB;
import at.prored.main.engine.components.Component;
import at.prored.main.engine.components.TextureStorageComponent;
import at.prored.main.engine.entities.Entity;
import at.prored.main.engine.entities.EntityDef;
import at.prored.main.engine.entities.EntityRegistry;
import at.prored.main.engine.entities.components.EntityComponent;
import at.prored.main.engine.entities.components.PhysicsComponent;
import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.tiles.TileDef;
import at.prored.main.engine.tiles.TileProperties;
import at.prored.main.engine.tiles.TileRegistry;
import at.prored.main.engine.tiles.layers.LayerRegistry;
import at.prored.main.engine.utils.MathUtils;
import at.prored.main.engine.utils.Pools;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

/**
 * Created by ProRed on 18.05.2017.
 */
public class Chunk {

    public static final int CHUNK_SIZE = 16;

    private int[][][] tiles;
    private TileProperties[][][] tileProperties;

    private Array<Entity> entities;

    private Array<AABB> tileBoundingBoxes;

    private boolean dirty = true;

    public Chunk() {
        this.tiles = new int[CHUNK_SIZE][CHUNK_SIZE][LayerRegistry.getLayerCount()];
        this.tileProperties = new TileProperties[CHUNK_SIZE][CHUNK_SIZE][LayerRegistry.getLayerCount()];

        this.entities = new Array<Entity>();

        this.tileBoundingBoxes = new Array<AABB>();

        //Reset Tiles
        for (int x = 0; x < CHUNK_SIZE; x++) {
            for (int y = 0; y < CHUNK_SIZE; y++) {
                for (int l = 0; l < LayerRegistry.getLayerCount(); l++) {
                    this.tiles[x][y][l] = 0;
                }
            }
        }
    }

    public void update(GameMap gameMap) {

        //Update entity components
        for (Entity e : entities) {
            EntityDef def = gameMap.getEngine().getEntityRegistry().getEntityDef(e.getID());

            //Iterate through all components
            for (int i = 0; i < def.getComponents().size; i++) {
                Component component = def.getComponents().get(i);
                if (component instanceof EntityComponent) {
                    ((EntityComponent) component).update(e, gameMap);
                }
            }
        }

    }

    public void renderTiles(TileRegistry tileRegistry, int cx, int cy, int layer) {
        SpriteBatch batch = Graphics.instance().getWorldBatch();

        int airID = tileRegistry.getTileDef("air").getID();

        //Render Tiles
        for (int x = 0; x < CHUNK_SIZE; x++) {
            for (int y = 0; y < CHUNK_SIZE; y++) {
                if (this.tiles[x][y][layer] == airID)
                    continue;

                OrthographicCamera camera = Graphics.instance().getCamera().getWorldCamera();

                float renderX = 32 * (x + cx * CHUNK_SIZE);
                float renderY = 32 * (y + cy * CHUNK_SIZE);


                int id = this.tiles[x][y][layer];
                TileDef tileDef = tileRegistry.getTileDef(id);

                if (tileDef.hasComponent(TextureStorageComponent.class)) {
                    TextureStorageComponent tex = tileDef.getComponent(TextureStorageComponent.class);
                    tex.render(batch, renderX, renderY, 1, 1, camera, tileDef, tileProperties[x][y][layer], tileRegistry.getEngine());
                }

            }
        }
    }

    public void renderEntities(EntityRegistry entityRegistry) {
        SpriteBatch batch = Graphics.instance().getWorldBatch();

        batch.setColor(Color.WHITE);

        //Render Entities
        for (Entity e : entities) {
            EntityDef entityDef = entityRegistry.getEntityDef(e.getID());
            if (entityDef.hasComponent(TextureStorageComponent.class)) {
                TextureStorageComponent texComp = entityDef.getComponent(TextureStorageComponent.class);

                //Check if the entity texture is inside screeen
                OrthographicCamera camera = Graphics.instance().getCamera().getWorldCamera();
                Vector2 position = e.getEntityProperties().getPosition();

                //Render Component
                texComp.render(batch, position.x, position.y, 1, 1, camera, entityDef, e.getEntityProperties(), entityRegistry.getEngine());
            }
        }


    }

    /**
     * Renders all bounding boxes in the chunk
     */
    public void renderBoundingBoxes() {
        ShapeRenderer worldShape = Graphics.instance().getWorldShapeBatch();

        worldShape.begin(ShapeRenderer.ShapeType.Line);
        for (AABB aabb : tileBoundingBoxes) {
            worldShape.rect(aabb.getCenter().x - aabb.getHalfSize().x, aabb.getCenter().y - aabb.getHalfSize().y, aabb.getHalfSize().x * 2f, aabb.getHalfSize().y * 2f);
        }
        worldShape.end();
    }

    public void renderEntityBoundingBoxes(EntityRegistry entityRegistry) {
        ShapeRenderer worldShape = Graphics.instance().getWorldShapeBatch();

        worldShape.setColor(Color.GREEN);
        worldShape.begin(ShapeRenderer.ShapeType.Line);
        for (Entity e : entities) {
            EntityDef entityDef = entityRegistry.getEntityDef(e.getID());

            if (entityDef.hasComponent(PhysicsComponent.class)) {
                PhysicsComponent collisionComponent = entityDef.getComponent(PhysicsComponent.class);
                Vector2 pos = e.getEntityProperties().getPosition();
                worldShape.rect(pos.x, pos.y, collisionComponent.getWidth(), collisionComponent.getHeight());
            }
        }
        worldShape.end();
        worldShape.setColor(Color.WHITE);
    }

    /**
     * Sets the tile of rx and ry to the tile id and creates a tile property object
     *
     * @param rx    x - relative to the chunk
     * @param ry    y - relative to the chunk
     * @param id    id of the tile
     * @param layer layer
     */
    public void setTileTypeRelative(int rx, int ry, int id, int layer) {
        this.tiles[rx][ry][layer] = id;

        this.dirty = true;
        // if (this.tileProperties[rx][ry][layer] != null)
        //   Pools.instance().getTilePropertiesPool().free(this.tileProperties[rx][ry][layer]);
    }

    /**
     * Sets the tile of x and < to the tile id and creates a tile property object
     *
     * @param x     x - relative to the world
     * @param y     y - relative to the world
     * @param id    id of the tile
     * @param layer layer
     */
    public void setTileType(int x, int y, int id, int layer) {
        this.setTileTypeRelative(MathUtils.getRelChunkPosX(x), MathUtils.getRelChunkPosY(y), id, layer);
    }

    /**
     * Returns the tile type of position
     *
     * @param rx    x - relative to the chunk
     * @param ry    y - relative to the chunk
     * @param layer layer
     * @return tile type of position
     */
    public int getTileTypeRelative(int rx, int ry, int layer) {
        return tiles[rx][ry][layer];
    }

    /**
     * Returns the tile type of position
     *
     * @param x     x - relative to the world
     * @param y     y - relative to the world
     * @param layer layer
     * @return tile type of position
     */
    public int getTileType(int x, int y, int layer) {
        return this.getTileTypeRelative(MathUtils.getRelChunkPosX(x), MathUtils.getRelChunkPosY(y), layer);
    }

    /**
     * Sets the tile properties object to the given one
     * Use with caution!
     *
     * @param rx         x - relative to the chunk
     * @param ry         y - relative to the chunk
     * @param properties new tile properies object
     * @param layer      layer
     */
    public void setTilePropertiesRelative(int rx, int ry, TileProperties properties, int layer) {
        this.tileProperties[rx][ry][layer] = properties;
        this.dirty = true;
    }

    /**
     * Sets the tile properties object to the given one
     * Use with caution!
     *
     * @param x          x - relative to the world
     * @param y          y - relative to the world
     * @param properties new tile properies object
     * @param layer      layer
     */
    public void setTileProperties(int x, int y, TileProperties properties, int layer) {
        this.setTilePropertiesRelative(MathUtils.getRelChunkPosX(x), MathUtils.getRelChunkPosY(y), properties, layer);
    }

    /**
     * Returns the tile properties of position
     *
     * @param rx    x - relative to the chunk
     * @param ry    y - relative to the chunk
     * @param layer layer
     * @return tile properties of position
     */
    public TileProperties getTilePropertiesRelative(int rx, int ry, int layer) {

        if (this.tileProperties[rx][ry][layer] == null)
            this.tileProperties[rx][ry][layer] = Pools.instance().getTilePropertiesPool().obtain();


        return tileProperties[rx][ry][layer];
    }

    /**
     * Returns the tile properties of position
     *
     * @param x     x - relative to the world
     * @param y     y - relative to the world
     * @param layer layer
     * @return tile properties of position
     */
    public TileProperties getTileProperties(int x, int y, int layer) {
        return this.getTilePropertiesRelative(MathUtils.getRelChunkPosX(x), MathUtils.getRelChunkPosY(y), layer);
    }

    /**
     * Adds an entity to the chunk
     *
     * @param entity entity
     */
    public void addEntity(Entity entity) {
        this.entities.add(entity);
        this.dirty = true;
    }

    /**
     * Removes an entity from the chunk
     *
     * @param entity entity
     */
    public void removeEntity(Entity entity) {
        this.entities.removeValue(entity, true);
    }

    /**
     * Generates and connects aabbs to each other horizontally
     *
     * @param tileRegistry tileregistry
     * @param cx           chunk x
     * @param cy           chunk y
     */
    public void populateTileBoundingBoxes(TileRegistry tileRegistry, int cx, int cy) {
        tileBoundingBoxes.clear();

        AABB cAABB = null;
        PhysicsComponent lastColComp = null;

        TileDef air = tileRegistry.getTileDef("air");
        for (int x = 0; x < CHUNK_SIZE; x++) {
            for (int y = 0; y < CHUNK_SIZE; y++) {
                int tileID = this.tiles[x][y][0];

                TileDef tileDef = tileRegistry.getTileDef(tileID);


                //Skip air blocks
                if (tileID == air.getID() || !tileDef.hasComponent(PhysicsComponent.class)) {
                    if (cAABB != null)
                        tileBoundingBoxes.add(cAABB);
                    cAABB = null;
                    continue;
                }


                PhysicsComponent collisionComponent = tileDef.getComponent(PhysicsComponent.class);

                float tileX = 32 * (x + cx * 16) + collisionComponent.getXOffset();
                float tileY = 32 * (y + cy * 16) + collisionComponent.getYOffset();

                float tileWidth = collisionComponent.getWidth();
                float tileHeight = collisionComponent.getHeight();

                //Create AABB
                if (cAABB == null) {
                    cAABB = new AABB(new Vector2(tileX + tileWidth / 2f, tileY + tileHeight / 2f), new Vector2(tileWidth / 2f, tileHeight / 2f));
                } else {
                    //Check if they can connect
                    if (collisionComponent.getXOffset() == 0 && lastColComp.getHeight() == collisionComponent.getHeight() && lastColComp.getWidth() == 32) {
                        cAABB.extendY(collisionComponent.getHeight());
                    } else {
                        tileBoundingBoxes.add(cAABB);
                        cAABB = new AABB(new Vector2(tileX + tileWidth / 2f, tileY + tileHeight / 2f), new Vector2(tileWidth / 2f, tileHeight / 2f));
                    }
                }


                lastColComp = collisionComponent;
            }
            if (cAABB != null) {
                tileBoundingBoxes.add(cAABB);
                cAABB = null;
            }
        }

        //Simplify
        AABB remove;
        do {
            remove = null;

            here:
            for (int i = 0; i < tileBoundingBoxes.size; i++) {
                AABB aabb = tileBoundingBoxes.get(i);
                for (int i1 = 0; i1 < tileBoundingBoxes.size; i1++) {
                    AABB aabb1 = tileBoundingBoxes.get(i1);
                    if (aabb1.getCenter().y == aabb.getCenter().y) {
                        if (aabb1.getCenter().x == (aabb.getCenter().x + aabb.getHalfSize().x + aabb1.getHalfSize().x) && aabb1.getHalfSize().y == aabb.getHalfSize().y) {
                            aabb.extendX(aabb1.getHalfSize().x * 2);
                            remove = aabb1;
                            break here;
                        }
                    }
                }
            }
            if (remove != null)
                tileBoundingBoxes.removeValue(remove, true);

        } while (remove != null);
    }

    public Array<Entity> getEntities() {
        return entities;
    }

    public Array<AABB> getTileBoundingBoxes() {
        return tileBoundingBoxes;
    }

    public boolean isDirty() {
        return dirty;
    }
}
