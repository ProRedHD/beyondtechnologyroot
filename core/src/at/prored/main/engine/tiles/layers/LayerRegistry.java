package at.prored.main.engine.tiles.layers;

import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by ProRed on 05.06.2017.
 */
public class LayerRegistry {

    private static int layerCount;

    private ObjectMap<String, Integer> layers;

    public LayerRegistry(){
        this.layers = new ObjectMap<String, Integer>();
    }

    public void registerLayer(String name, int id){
        if(layers.containsValue(id, true)){
            throw new GdxRuntimeException("Layer id is already used! (" + layers.findKey(id, true) + " -> " + id + ")");
        }

        layers.put(name, id);
        layerCount++;
    }

    public void unregisterLayer(String name){
        layers.remove(name);
        layerCount--;
    }

    public String getLayerName(int id){
        return layers.findKey(id, true);
    }

    public int getLayerID(String name){
        return layers.get(name);
    }

    public static int getLayerCount(){
        return layerCount;
    }

}
