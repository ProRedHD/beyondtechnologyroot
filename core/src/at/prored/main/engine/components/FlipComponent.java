package at.prored.main.engine.components;

import at.prored.main.engine.Engine;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 19.03.2018.
 */
public class FlipComponent extends Component {

    private static final String FLIPX = "flipX";
    private static final String FLIPY = "flipY";

    private boolean uiFlippableX, uiFlippableY;

    public FlipComponent() {
    }

    public FlipComponent(boolean uiFlippableX, boolean uiFlippableY) {
        this.uiFlippableX = uiFlippableX;
        this.uiFlippableY = uiFlippableY;
    }

    /**
     * Returns if the texture should be flipped
     * @param p properties
     * @return if it should be flipped
     */
    public boolean isFlipX(Properties p) {
        if (!p.hasData(FLIPX)) {
            p.setBoolean(FLIPX, false);
        }

        return p.getBoolean(FLIPX);
    }

    /**
     * Returns if the texture should be flipped
     * @param p properties
     * @return if it should be flipped
     */
    public boolean isFlipY(Properties p) {
        if (!p.hasData(FLIPY)) {
            p.setBoolean(FLIPY, false);
        }

        return p.getBoolean(FLIPY);
    }

    /**
     * Sets the flip property
     * @param p properties
     * @param flipX flipX
     */
    public void setFlipX(Properties p, boolean flipX){
        p.setBoolean(FLIPX, flipX);
    }

    /**
     * Returns if the object is flippable on x axis
     * @return true/false
     */
    public boolean isUiFlippableX() {
        return uiFlippableX;
    }

    /**
     * Returns if the object is flippable on y axis
     * @return true/false
     */
    public boolean isUiFlippableY() {
        return uiFlippableY;
    }

    /**
     * Sets the flip property
     * @param p properties
     * @param flipY flipX
     */
    public void setFlipY(Properties p, boolean flipY){
        p.setBoolean(FLIPY, flipY);
    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {
        this.uiFlippableX = Boolean.valueOf(element.getAttribute("flippableX", "true"));
        this.uiFlippableY = Boolean.valueOf(element.getAttribute("flippableY", "true"));
    }

    @Override
    public Component cpy() {
        return new FlipComponent(uiFlippableX, uiFlippableY);
    }

}
