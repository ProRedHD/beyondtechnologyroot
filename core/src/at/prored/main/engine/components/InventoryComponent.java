package at.prored.main.engine.components;

import at.prored.main.engine.Engine;
import at.prored.main.engine.entities.Entity;
import at.prored.main.engine.guis.inventory.componentRenderers.ComponentUIRenderer;
import at.prored.main.engine.guis.inventory.componentRenderers.InventoryUIRenderer;
import at.prored.main.engine.inventory.Inventory;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.saving.data.ItemStackData;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.tiles.TileProperties;
import at.prored.main.engine.utils.DataReader;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 27.08.2017.
 */
public class InventoryComponent extends Component implements InventoryRenderable {

    private int slotCount;

    public InventoryComponent() {
    }

    public InventoryComponent(int slotCount) {
        this.slotCount = slotCount;
    }

    public static Inventory getInventory(Entity entity) {
        return (Inventory) entity.getEntityProperties().getData("inventory");
    }

    public static Inventory getInventory(TileProperties tileProperties) {
        return (Inventory) tileProperties.getData("inventory");
    }

    public Inventory getInventory(Properties properties) {
        return (Inventory) properties.getData("inventory");
    }


    public int getSlotCount() {
        return slotCount;
    }

    @Override
    public void onEntityAdded(Entity entity) {
        super.onEntityAdded(entity);
        entity.getEntityProperties().setData("inventory", new Inventory(slotCount));
    }

    @Override
    public void onTileAdded(TileProperties tileProperties) {
        super.onTileAdded(tileProperties);
        tileProperties.setData("inventory", new Inventory(slotCount));
    }

    @Override
    public void onItemStackCreated(ItemStack stack, Engine e) {
        super.onItemStackCreated(stack, e);
        stack.getProperties().setData("inventory", new Inventory(slotCount));
    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {
        Inventory inventory = (Inventory) properties.getData("inventory");

        for (int i = 0; i < inventory.getData().length; i++) {
            if (inventory.getData()[i] != null) {
                data.add(new ItemStackData("stack_" + i, inventory.getData()[i]));
            }
        }
    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {
        DataReader reader = new DataReader(data);

        for (int i = 0; i < slotCount; i++) {
            if (reader.hasData("stack_" + i)) {
                ((ItemStack[]) properties.getData("inventory"))[i] = ((ItemStackData) reader.get("stack_" + i)).getValue();
            }
        }
    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {
        this.slotCount = element.getIntAttribute("slotCount", 5);
    }

    @Override
    public ComponentUIRenderer getComponentRenderer() {
        return new InventoryUIRenderer(this, this);
    }

    @Override
    public Component cpy() {
        return new InventoryComponent(slotCount);
    }

}
