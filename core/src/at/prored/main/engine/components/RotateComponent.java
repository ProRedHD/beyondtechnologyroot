package at.prored.main.engine.components;

import at.prored.main.engine.Engine;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.utils.ComponentException;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 19.03.2018.
 */
public class RotateComponent extends Component {

    private static final String ROTATION = "rotation";

    private Array<Integer> rotations;

    public RotateComponent() {
        this.rotations = new Array<Integer>();
    }

    public RotateComponent(Array<Integer> rotations) {
        this.rotations = rotations;
    }

    public int getRotation(Properties p) {
        return p.hasData(ROTATION) ? p.getInteger(ROTATION) : 0;
    }

    public void setRotation(Properties p, int rotation) {
        p.setInteger(ROTATION, rotation);
    }

    public Array<Integer> getRotations() {
        return rotations;
    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {

        //construct rotation angles
        for (String rotation : element.getText().split(",")) {

            //skip empty strings
            if(rotation.isEmpty())
                continue;

            try {
                this.rotations.add(Integer.valueOf(rotation));
            } catch (NumberFormatException ex) {
                throw new ComponentException(RotateComponent.class, "Malformed rotation found! (" + rotation + ")");
            }
        }
    }

    @Override
    public Component cpy() {
        return new RotateComponent(new Array<Integer>(rotations));
    }
}
