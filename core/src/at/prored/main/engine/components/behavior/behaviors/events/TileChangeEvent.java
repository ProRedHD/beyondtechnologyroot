package at.prored.main.engine.components.behavior.behaviors.events;

import at.prored.main.engine.components.behavior.ActionEvent;
import at.prored.main.engine.tiles.TileProperties;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;

/**
 * Created by ProRed on 03.01.2018.
 */
public class TileChangeEvent extends ActionEvent implements Pool.Poolable {

    private ChangeType type;
    private Vector2 src;

    public TileChangeEvent(ChangeType type, int srcOffsetX, int srcOffsetY) {
        this.type = type;
        this.src = new Vector2(srcOffsetX, srcOffsetY);
    }

    public TileChangeEvent() {
        this.type = null;
        this.src = new Vector2();
    }

    @Override
    public void reset() {
        this.type = null;
        src.set(0,0);
    }

    public TileChangeEvent set(ChangeType type, int srcOffsetX, int srcOffsetY) {
        this.type = type;
        this.src.set(srcOffsetX, srcOffsetY);

        return this;
    }

    public ChangeType getType() {
        return type;
    }

    public Vector2 getSourceOffset() {
        return src;
    }

    public enum ChangeType {
        ADDED, REMOVED, CHANGED, NEIGHBOUR_ADDED, NEIGHBOUR_REMOVED, NEIGHBOUR_CHANGED, LAYER_ADDED, LAYER_REMOVED, LAYER_CHANGED
    }
}
