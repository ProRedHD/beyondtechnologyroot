package at.prored.main.engine.components.behavior.behaviors.events;

import at.prored.main.engine.components.behavior.ActionEvent;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.tiles.TileDef;

/**
 * Created by ProRed on 09.12.2017.
 */
public class HotbarItemChangeEvent extends ActionEvent {

    private ItemStack stack;
    private ItemStack[] inventory;

    public HotbarItemChangeEvent(ItemStack stack, ItemStack[] inventory) {
        this.stack = stack;
        this.inventory = inventory;
    }

    public ItemStack getStack() {
        return stack;
    }

    public ItemStack[] getInventory() {
        return inventory;
    }
}
