package at.prored.main.engine.components.behavior.behaviors.events;

import at.prored.main.engine.components.behavior.ActionEvent;
import at.prored.main.engine.utils.Pools;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by ProRed on 22.08.2017.
 */
public class ControlEvent extends ActionEvent {

    private Vector2 direction;

    public ControlEvent(){
        this.direction = Pools.instance().getVector2Pool().obtain();
    }

    public Vector2 getDirection() {
        return direction;
    }

    @Override
    protected void finalize() throws Throwable {
        Pools.instance().getVector2Pool().free(direction);
        super.finalize();
    }
}
