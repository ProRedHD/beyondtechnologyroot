package at.prored.main.engine.components.behavior.behaviors;

import at.prored.main.engine.components.Component;
import at.prored.main.engine.components.behavior.ActionEvent;
import at.prored.main.engine.components.behavior.Behavior;
import at.prored.main.engine.components.behavior.EventType;
import at.prored.main.engine.entities.Entity;
import at.prored.main.engine.inventory.Inventory;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.tiles.TileDef;
import at.prored.main.engine.tiles.TileProperties;
import at.prored.main.engine.tiles.storage.GameMap;
import at.prored.main.engine.tiles.tilecomponents.ConnectedTextureComponent;
import at.prored.main.engine.utils.MapUtils;

/**
 * Created by ProRed on 03.01.2018.
 */
public class ConnectedTextureBehavior extends Behavior {

    public ConnectedTextureBehavior(Class<? extends Component> component) {
        super(component);
    }

    @Override
    public void trigger(EventType type, ActionEvent e, GameMap gameMap, Entity entity) {

    }

    @Override
    public void trigger(EventType type, ActionEvent e, ItemStack stack, Inventory inventory, GameMap gameMap) {

    }

    @Override
    public void trigger(EventType type, ActionEvent e, GameMap gameMap, int x, int y, int layer) {
        if (type == EventType.TILE_CHANGE) {
            process(x, y, layer, gameMap);
            process(x - 1, y, layer, gameMap);
            process(x + 1, y, layer, gameMap);
            process(x, y - 1, layer, gameMap);
            process(x, y + 1, layer, gameMap);
        }
    }

    private void process(int x, int y, int layer, GameMap gameMap) {
        int id = MapUtils.getTileType(gameMap, x, y, layer);
        TileProperties properties = MapUtils.getTileProperties(gameMap, x, y, layer);

        if (id == -1)
            return;

        TileDef def = gameMap.getEngine().getTileRegistry().getTileDef(id);
        if (def.hasComponent(ConnectedTextureComponent.class)) {
            int cnt = 0;
            boolean[] sides = new boolean[4];

            //Find sides that connect
            if (MapUtils.getTileType(gameMap, x + 1, y, layer) == id) {
                cnt++;
                sides[3] = true;
            }

            if (MapUtils.getTileType(gameMap, x - 1, y, layer) == id) {
                cnt++;
                sides[2] = true;
            }

            if (MapUtils.getTileType(gameMap, x, y + 1, layer) == id) {
                cnt++;
                sides[0] = true;
            }

            if (MapUtils.getTileType(gameMap, x, y - 1, layer) == id) {
                cnt++;
                sides[1] = true;
            }

            //Get the orientation
            int orientation = 0;
            switch (cnt) {
                case 1:
                    for(int i = 0; i < 4; i++)
                        orientation = sides[i] ? i : orientation;
                    break;
                case 2:
                    if (sides[0] && sides[1]) {
                        orientation = 0;
                    } else if(sides[2] && sides[3]){
                        orientation = 1;
                    } else {
                        if(sides[0] && sides[3])
                            orientation = 2;
                        else if(sides[0] && sides[2])
                            orientation = 3;
                        else if(sides[1] && sides[3])
                            orientation = 4;
                        else if(sides[1] && sides[2])
                            orientation = 5;
                    }
                    break;
                case 3:
                    if(sides[0] && sides[3] && sides[1])
                        orientation = 0;
                    else if(sides[0] && sides[2] && sides[1])
                        orientation = 1;
                    else if(sides[2] && sides[1] && sides[3])
                        orientation = 2;
                    else if(sides[2] && sides[0] && sides[3])
                        orientation = 3;
                default:
                    break;
            }

            def.getComponent(ConnectedTextureComponent.class).setSides(cnt, orientation, properties);
        }
    }


}
