package at.prored.main.engine.components.behavior.behaviors;

import at.prored.main.engine.components.Component;
import at.prored.main.engine.components.FlipComponent;
import at.prored.main.engine.components.behavior.ActionEvent;
import at.prored.main.engine.components.behavior.Behavior;
import at.prored.main.engine.components.behavior.EventType;
import at.prored.main.engine.components.behavior.behaviors.events.ControlEvent;
import at.prored.main.engine.entities.Entity;
import at.prored.main.engine.entities.EntityDef;
import at.prored.main.engine.entities.components.StateComponent;
import at.prored.main.engine.inventory.Inventory;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.tiles.storage.GameMap;

/**
 * Created by ProRed on 22.08.2017.
 */
public class UserControlBehavior extends Behavior {

    public UserControlBehavior(Class<? extends Component> component) {
        super(component);
    }

    @Override
    public void trigger(EventType type, ActionEvent e, GameMap gameMap, Entity entity) {
        if (type == EventType.ENTITY_CONTROL) {
            EntityDef def = gameMap.getEngine().getEntityRegistry().getEntityDef(entity.getID());

            if (!def.hasComponent(StateComponent.class))
                return;

            StateComponent stateComponent = def.getComponent(StateComponent.class);
            FlipComponent flipComponent = def.getComponent(FlipComponent.class);

            if (((ControlEvent) e).getDirection().x > 0) {
                stateComponent.setState(1, entity.getEntityProperties());
                flipComponent.setFlipX(entity.getEntityProperties(), false);
            } else if (((ControlEvent) e).getDirection().x < 0) {
                stateComponent.setState(1, entity.getEntityProperties());
                flipComponent.setFlipX(entity.getEntityProperties(), true);
            } else {
                stateComponent.setState(0, entity.getEntityProperties());
            }
        }
    }

    @Override
    public void trigger(EventType type, ActionEvent e, GameMap gameMap, int x, int y, int layer) {

    }

    @Override
    public void trigger(EventType type, ActionEvent e, ItemStack stack, Inventory inventory, GameMap gameMap) {

    }
}
