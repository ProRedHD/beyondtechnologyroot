package at.prored.main.engine.components.behavior;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.Component;
import at.prored.main.engine.entities.Entity;
import at.prored.main.engine.entities.EntityDef;
import at.prored.main.engine.inventory.Inventory;
import at.prored.main.engine.items.ItemDef;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.tiles.TileDef;
import at.prored.main.engine.tiles.storage.GameMap;
import at.prored.main.engine.utils.MapUtils;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by ProRed on 16.08.2017.
 */
public class BehaviorManager {

    private static BehaviorManager instance;

    private ObjectMap<Class<? extends Component>, Behavior> behaviors;

    private BehaviorManager() {
        this.behaviors = new ObjectMap<>();
    }

    /**
     * Returns the instance of the class (singleton)
     *
     * @return instance of class
     */
    public static BehaviorManager instance() {
        if (instance == null) instance = new BehaviorManager();
        return instance;
    }

    /**
     * Registers a behavior to the given component
     *
     * @param c component
     */
    public void registerBehavior(Component c) {
        Behavior b = c.getBehavior();

        //Return if b doesn't exit
        if (b == null) {
            return;
        }

        //Get class
        Class<? extends Component> clazz = c.getClass();

        //Store behavior in object map
        behaviors.put(clazz, b);
    }

    /**
     * Returns the behavior for a component
     *
     * @param c component
     * @return behavior
     */
    private Behavior getBehavior(Component c) {
        return behaviors.get(c.getClass());
    }

    /**
     * Checks if a component has a registered behavior
     *
     * @param c component
     * @return true/false
     */
    private boolean hasBehavior(Component c) {
        return behaviors.containsKey(c.getClass());
    }

    /**
     * Dispatches trigger to all registered item behaviors
     *
     * @param type    event type
     * @param e       action event
     * @param stack   stack
     * @param gameMap gameMap
     */
    public void broadcastItem(EventType type, ActionEvent e, ItemStack stack, Inventory inventory, GameMap gameMap) {
        Engine engine = gameMap.getEngine();

        //Return if the stack is null
        if (stack == null)
            return;

        //Get itemdef
        ItemDef def = engine.getItemRegistry().getItemDef(stack.getID());

        for (int i = 0; i < def.getComponents().size; i++) {
            Component component = def.getComponents().get(i);


            //Check if component has no behavior and try to add them
            if (!hasBehavior(component)) {
                Behavior b = component.getBehavior();

                if (b != null) {
                    registerBehavior(component);
                } else {

                    //Continue due to missing behavior
                    continue;
                }
            }

            //Get behavior
            Behavior b = getBehavior(component);

            //Notify behavior
            b.trigger(type, e, stack, inventory, gameMap);
        }
    }

    /**
     * Dispatches trigger to all registered tile behaviors
     *
     * @param type    event type
     * @param e       action event
     * @param gameMap gameMap
     * @param x       x
     * @param y       y
     * @param layer   layer
     */
    public void broadcastTile(EventType type, ActionEvent e, GameMap gameMap, int x, int y, int layer) {
        Engine engine = gameMap.getEngine();

        //Get tiledef
        int id = MapUtils.getTileType(gameMap, x, y, layer);
        TileDef def = engine.getTileRegistry().getTileDef(id);

        for (int i = 0; i < def.getComponents().size; i++) {
            Component component = def.getComponents().get(i);

            //Check if component has no behavior and try to add them
            if (!hasBehavior(component)) {
                Behavior b = component.getBehavior();

                if (b != null) {
                    registerBehavior(component);
                } else {

                    //Continue due to missing behavior
                    continue;
                }
            }

            //Get behavior
            Behavior b = getBehavior(component);

            //Notify behavior
            b.trigger(type, e, gameMap, x, y, layer);
        }
    }

    /**
     * Dispatches trigger to all registered entity behaviors
     *
     * @param type    type
     * @param e       action event
     * @param gameMap gameMap
     * @param entity  entity
     */
    public void broadcastEntity(EventType type, ActionEvent e, GameMap gameMap, Entity entity) {
        Engine engine = gameMap.getEngine();

        //Get itemdef
        EntityDef def = engine.getEntityRegistry().getEntityDef(entity.getID());

        for (Component component : def.getComponents()) {

            //Check if component has no behavior and try to add them
            if (!hasBehavior(component)) {
                Behavior b = component.getBehavior();

                if (b != null) {
                    registerBehavior(component);
                } else {

                    //Continue due to missing behavior
                    continue;
                }
            }

            //Get behavior
            Behavior b = getBehavior(component);

            //Notify behavior
            b.trigger(type, e, gameMap, entity);
        }
    }

}
