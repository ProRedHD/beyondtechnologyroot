package at.prored.main.engine.components.behavior.behaviors;

import at.prored.main.engine.components.Component;
import at.prored.main.engine.components.HotbarComponent;
import at.prored.main.engine.components.behavior.ActionEvent;
import at.prored.main.engine.components.behavior.Behavior;
import at.prored.main.engine.components.behavior.EventType;
import at.prored.main.engine.entities.Entity;
import at.prored.main.engine.inventory.Inventory;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.items.components.TileComponent;
import at.prored.main.engine.tiles.storage.GameMap;

/**
 * Created by ProRed on 09.12.2017.
 */
public class TileBehavior extends Behavior {

    public TileBehavior(Class<? extends Component> component) {
        super(component);
    }

    @Override
    public void trigger(EventType type, ActionEvent e, GameMap gameMap, Entity entity) {

    }

    @Override
    public void trigger(EventType type, ActionEvent e, GameMap gameMap, int x, int y, int layer) {

    }

    @Override
    public void trigger(EventType type, ActionEvent e, ItemStack stack, Inventory inventory, GameMap gameMap) {

        switch (type) {
            case HOTBAR_ITEM_SELECTED:
                int tileID = TileComponent.getTileID(stack.getProperties());
                gameMap.getTilePlacer().setTileDef(gameMap.getEngine().getTileRegistry().getTileDef(tileID));

                Inventory hinventory = HotbarComponent.getHotbarInventory(gameMap.getPlayer().getEntityProperties());
                gameMap.getTilePlacer().setInventory(hinventory);

                gameMap.getTilePlacer().activate();
                break;
            case HOTBAR_ITEM_DESELECTED:
                gameMap.getTilePlacer().deactivate();
                break;
        }

    }
}
