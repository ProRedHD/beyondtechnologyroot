package at.prored.main.engine.components.behavior;

import at.prored.main.engine.components.Component;
import at.prored.main.engine.entities.Entity;
import at.prored.main.engine.inventory.Inventory;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.tiles.storage.GameMap;

/**
 * Created by ProRed on 16.08.2017.
 */
public abstract class Behavior {

    private Class<? extends Component> clazz;

    public Behavior(Class<? extends Component> component) {
        this.clazz = component;
    }

    public Class<? extends Component> getComponent() {
        return clazz;
    }

    public abstract void trigger(EventType type, ActionEvent e, GameMap gameMap, Entity entity);

    public abstract void trigger(EventType type, ActionEvent e, GameMap gameMap, int x, int y, int layer);

    public abstract void trigger(EventType type, ActionEvent e, ItemStack stack, Inventory inventory, GameMap gameMap);
}
