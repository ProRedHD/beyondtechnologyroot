package at.prored.main.engine.components.behavior.behaviors;

import at.prored.main.engine.components.Component;
import at.prored.main.engine.components.behavior.ActionEvent;
import at.prored.main.engine.components.behavior.Behavior;
import at.prored.main.engine.components.behavior.EventType;
import at.prored.main.engine.components.conduit.ConduitComponent;
import at.prored.main.engine.components.conduit.PowerConduitComponent;
import at.prored.main.engine.conduit.EnergyGrid;
import at.prored.main.engine.entities.Entity;
import at.prored.main.engine.inventory.Inventory;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.tiles.TileDef;
import at.prored.main.engine.tiles.TileProperties;
import at.prored.main.engine.tiles.storage.GameMap;
import at.prored.main.engine.utils.MapUtils;
import at.prored.main.engine.utils.Properties;

/**
 * Created by ProRed on 28.02.2018.
 */
public class ConduitBehavior extends Behavior {

    public ConduitBehavior(Class<? extends Component> component) {
        super(component);
    }

    @Override
    public void trigger(EventType type, ActionEvent e, GameMap gameMap, Entity entity) {

    }

    @Override
    public void trigger(EventType type, ActionEvent e, GameMap gameMap, int x, int y, int layer) {
        if(type == EventType.TILE_CHANGE){
            int id = MapUtils.getTileType(gameMap, x, y, layer);
            TileProperties properties = MapUtils.getTileProperties(gameMap, x, y, layer);

            if(!ConduitComponent.hasGridID(properties)){
                EnergyGrid grid = gameMap.getGridManager().createEnergyGrid();
                TileDef def = gameMap.getEngine().getTileRegistry().getTileDef(id);

                PowerConduitComponent powerConduitComponent = def.getComponent(PowerConduitComponent.class);
                grid.addConduit(x,y, powerConduitComponent);


                ConduitComponent.setGridID(properties, grid.getID());
            }

            checkForOtherGrids(gameMap, x, y, layer, properties);
        }
    }

    @Override
    public void trigger(EventType type, ActionEvent e, ItemStack stack, Inventory inventory, GameMap gameMap) {

    }

    /**
     * Checks the four adjacent tile spaces for other conduits
     * @param gameMap gameMap
     * @param x x
     * @param y y
     * @param layer layer
     * @param properties properties
     */
    private void checkForOtherGrids(GameMap gameMap, int x, int y, int layer, Properties properties){
        int id = process(gameMap, x - 1, y, layer);
        processResult(gameMap, id, properties);

        id = process(gameMap, x + 1, y, layer);
        processResult(gameMap, id, properties);

        id = process(gameMap, x, y - 1, layer);
        processResult(gameMap, id, properties);

        id = process(gameMap, x, y + 1, layer);
        processResult(gameMap, id, properties);
    }

    /**
     * Checks the tile space for a power conduit
     * @param gameMap gameMap
     * @param x x
     * @param y y
     * @param layer layer
     * @return id of the grid id
     */
    private int process(GameMap gameMap, int x, int y, int layer){

        int id = MapUtils.getTileType(gameMap, x, y, layer);
        TileDef def = gameMap.getEngine().getTileRegistry().getTileDef(id);

        if(def.hasComponent(PowerConduitComponent.class)){
            Properties p = MapUtils.getTileProperties(gameMap, x, y, layer);

            return ConduitComponent.getGridID(p);
        }

        return -1;
    }

    /**
     * Checks if the grid id is not the same as our grid id and merges them if needed
     * @param gameMap gameMap
     * @param id grid id of the other conduit
     * @param p properties
     */
    private void processResult(GameMap gameMap, int id, Properties p){
        int gridID = ConduitComponent.getGridID(p);

        if(id != -1 && id != gridID){
            int winner = gameMap.getGridManager().mergeEnergyGrids(gameMap, id, gridID);
            ConduitComponent.setGridID(p, winner);
        }
    }
}
