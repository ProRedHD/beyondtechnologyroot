package at.prored.main.engine.components.behavior.behaviors;

import at.prored.main.engine.components.Component;
import at.prored.main.engine.components.RecipeDiscoverComponent;
import at.prored.main.engine.components.behavior.ActionEvent;
import at.prored.main.engine.components.behavior.Behavior;
import at.prored.main.engine.components.behavior.EventType;
import at.prored.main.engine.crafting.Recipe;
import at.prored.main.engine.entities.Entity;
import at.prored.main.engine.inventory.Inventory;
import at.prored.main.engine.inventory.InventoryUtils;
import at.prored.main.engine.items.ItemDef;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.tiles.storage.GameMap;

/**
 * Created by ProRed on 09.10.2018.
 */
public class RecipeDiscoverBehavior extends Behavior {

    public RecipeDiscoverBehavior(Class<? extends Component> component) {
        super(component);
    }

    @Override
    public void trigger(EventType type, ActionEvent e, GameMap gameMap, Entity entity) {

    }

    @Override
    public void trigger(EventType type, ActionEvent e, GameMap gameMap, int x, int y, int layer) {

    }

    @Override
    public void trigger(EventType type, ActionEvent e, ItemStack stack, Inventory inventory, GameMap gameMap) {

        if (type == EventType.HOTBAR_ITEM_SELECTED) {

            ItemDef def = gameMap.getEngine().getItemRegistry().getItemDef(stack.getID());

            if(def.hasComponent(RecipeDiscoverComponent.class)){
                Recipe r = (Recipe) stack.getProperties().getData().get("recipe");
                gameMap.getEngine().getRecipeRegistry().unlockRecipe(r);



                InventoryUtils.removeItemStack(stack, inventory);
            }



        }

    }
}
