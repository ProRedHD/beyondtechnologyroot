package at.prored.main.engine.components.behavior;

/**
 * Created by ProRed on 16.08.2017.
 */
public enum EventType {
    TILE_CHANGE,
    RANDOM_TICK,
    KINETIC_ENERGY_CHANGED,

    ENTITY_CONTROL,

    HOTBAR_ITEM_SELECTED,
    HOTBAR_ITEM_DESELECTED
}
