package at.prored.main.engine.components;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.behavior.Behavior;
import at.prored.main.engine.components.behavior.behaviors.RecipeDiscoverBehavior;
import at.prored.main.engine.crafting.Recipe;
import at.prored.main.engine.crafting.template.RecipeTemplate;
import at.prored.main.engine.decisiontree.Decidable;
import at.prored.main.engine.decisiontree.DecisionTreeElement;
import at.prored.main.engine.decisiontree.DecisionTreeReader;
import at.prored.main.engine.decisiontree.container.RecipeDecision;
import at.prored.main.engine.items.ItemDef;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 06.09.2018.
 */
public class RecipeDiscoverComponent extends Component {

    private DecisionTreeElement recipeTree;

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {
        this.recipeTree = DecisionTreeReader.xmlToLootTableElement(element.getChild(0), e);
    }

    @Override
    public void onItemStackCreated(ItemStack stack, Engine e) {
        super.onItemStackCreated(stack, e);

        Array<Decidable> arr = new Array<>();

        //Decide which recipe
        recipeTree.decide(arr, e);
        RecipeDecision recipeDecision = (RecipeDecision) arr.first();

        //Get and create recipe
        Array<RecipeTemplate> recipeTemplates = e.getRecipeRegistry().getRecipesForRecipeType(recipeDecision.getType()).get(recipeDecision.getTier());
        Recipe r = recipeTemplates.random().createRecipe(e);

        System.out.println("--------------------------------");
        System.out.println("Result: " + r.getResult().toString(e));
        System.out.println();
        System.out.println("Ingredients: ");

        for (ItemStack itemStack : r.getIngredients()) {
            System.out.println("  " + itemStack.toString(e));
        }

        System.out.println("--------------------------------");

        stack.getProperties().setData("recipe", r);

        //Update tooltip
        ItemDef def = e.getItemRegistry().getItemDef(stack.getID());
        if(def.hasComponent(TooltipComponent.class)){
            TooltipComponent tc = def.getComponent(TooltipComponent.class);
            tc.setSupplier(stack.getProperties(), "recipe", ()->r);
        }
    }

    @Override
    public Component cpy() {
        return null;
    }

    @Override
    public Behavior getBehavior() {
        return new RecipeDiscoverBehavior(this.getClass());
    }
}
