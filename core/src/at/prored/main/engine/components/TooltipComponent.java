package at.prored.main.engine.components;

import at.prored.main.engine.Engine;
import at.prored.main.engine.guis.tooltip.elements.*;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.XmlReader;

import java.util.function.Supplier;

/**
 * Created by ProRed on 04.12.2018.
 */
public class TooltipComponent extends Component {


    private XmlReader.Element template;
    private ObjectMap<Properties, Array<TooltipElement>> elements;
    private ObjectMap<Properties, ObjectMap<String, Supplier<?>>> valueSupplier;

    public TooltipComponent(){
        this.elements = new ObjectMap<>();
        this.valueSupplier = new ObjectMap<>();
    }

    public Array<TooltipElement> getChildren(Properties p, Engine e) {

        if(!elements.containsKey(p)){
            elements.put(p, createChildArray(template, e, p));
        }

        return elements.get(p);
    }

    public void setSupplier(Properties p, String id, Supplier<?> sup){
        if(!valueSupplier.containsKey(p))
            valueSupplier.put(p, new ObjectMap<>());

        valueSupplier.get(p).put(id, sup);
    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {
        this.template = element;
    }

    private Array<TooltipElement> createChildArray(XmlReader.Element element, Engine e, Properties p){
        Array<TooltipElement> children = new Array<>();

        for (int i = 0; i < element.getChildCount(); i++) {
            XmlReader.Element el = element.getChild(i);

            switch (el.getName().toLowerCase()){
                case "text":
                    children.add(new TooltipElementLabel(el.getText().trim(), e));
                    break;
                case "progressbar":
                    String id = el.getAttribute("id");
                    float width = el.getFloat("width");
                    float height = el.getFloat("height");
                    Color start = Color.valueOf(el.getAttribute("startColor"));
                    Color end = Color.valueOf(el.getAttribute("endColor"));
                    TooltipElementProgressBar progressBar = new TooltipElementProgressBar(width, height, start, end, e);
                    children.add(progressBar);
                    break;
                case "itemstack":
                    id = el.getAttribute("id");
                    Supplier<ItemStack> supplier = () -> {
                        if(!this.valueSupplier.containsKey(p))
                            throw new GdxRuntimeException("No supplier was found for object");
                        if(!this.valueSupplier.get(p).containsKey(id))
                            throw new GdxRuntimeException("Supplier for tooltip is not assigned!");

                        return (ItemStack) this.valueSupplier.get(p).get(id).get();
                    };
                    TooltipElementItemStack itemStack = new TooltipElementItemStack(supplier, e);
                    children.add(itemStack);
                    break;
                case "recipe":
                    id = el.getAttribute("id");
                    Supplier<IRecipeDisplayable> recipeSupplier = () -> {
                        if(!this.valueSupplier.containsKey(p))
                            throw new GdxRuntimeException("No supplier was found for object");
                        if(!this.valueSupplier.get(p).containsKey(id))
                            throw new GdxRuntimeException("Supplier for tooltip is not assigned!");

                        return (IRecipeDisplayable) this.valueSupplier.get(p).get(id).get();
                    };
                    TooltipElementRecipe recipeElement = new TooltipElementRecipe(recipeSupplier, e);
                    children.add(recipeElement);
                    break;
            }
        }

        return children;
    }

    @Override
    public Component cpy() {
        return null;
    }
}
