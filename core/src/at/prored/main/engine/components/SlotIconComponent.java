package at.prored.main.engine.components;

import at.prored.main.engine.Engine;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.utils.AssetUtils;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 04.09.2018.
 */
public class SlotIconComponent extends Component {

    private TextureRegion icon;

    public SlotIconComponent() {
    }

    public SlotIconComponent(TextureRegion icon) {
        this.icon = icon;
    }

    public TextureRegion getIcon() {
        return icon;
    }

    public void render(SpriteBatch batch, float x, float y, float scaleX, float scaleY, OrthographicCamera camera, Definition definition, Properties properties, Engine engine) {

        if(icon != null) {
            float width = icon.getRegionWidth();
            float height = icon.getRegionHeight();

            if (camera.frustum.boundsInFrustum(x + width / 2f, y + height / 2f, 1f, width / 2f, height / 2f, 1f)) {
                batch.draw(icon, x, y, width/2f, height/2f, width, height, scaleX, scaleY, 0);
            }
        }
    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {
        this.icon = AssetUtils.loadTextureRegion(e.getAssetManager(), element.getAttribute("name", "stone"));
}

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public Component cpy() {
        return new SlotIconComponent(icon);
    }
}
