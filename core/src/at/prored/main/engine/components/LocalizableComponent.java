package at.prored.main.engine.components;

import at.prored.main.engine.Engine;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 18.05.2017.
 */
public class LocalizableComponent extends Component {

    private String name;

    public LocalizableComponent(){}

    public LocalizableComponent(String name) {
        this.name = name;
    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {
        this.name = element.getAttribute("name");
    }

    @Override
    public Component cpy() {
        return new LocalizableComponent(name);
    }

    public String getName() {
        return name;
    }
}
