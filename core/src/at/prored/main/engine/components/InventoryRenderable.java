package at.prored.main.engine.components;

import at.prored.main.engine.inventory.Inventory;
import at.prored.main.engine.utils.Properties;

/**
 * Created by ProRed on 08.07.2018.
 */
public interface InventoryRenderable {

    Inventory getInventory(Properties p);
    int getSlotCount();
}
