package at.prored.main.engine.components;

import at.prored.main.engine.Engine;
import at.prored.main.engine.decisiontree.Decidable;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.items.components.TileComponent;
import at.prored.main.engine.decisiontree.DecisionTreeElement;
import at.prored.main.engine.decisiontree.DecisionTreeItem;
import at.prored.main.engine.decisiontree.DecisionTreeReader;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.tiles.tilecomponents.GrowableComponent;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 03.09.2018.
 */
public class DropComponent extends Component {

    private ObjectMap<Integer, DecisionTreeElement> lootTables;
    private boolean dropSelf;

    public DropComponent() {
        this.lootTables = new ObjectMap<>();
    }

    public DropComponent(DecisionTreeElement root) {
        this.lootTables = new ObjectMap<>();
        lootTables.put(0, root);
    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {

        this.dropSelf = element.getBoolean("dropSelf", false);

        for (int i = 0; i < element.getChildCount(); i++) {
            lootTables.put(i, DecisionTreeReader.xmlToLootTableElement(element.getChild(i), e));
        }

        Gdx.app.debug("DropComponent", "Successfully loaded " + lootTables.size + " decision table(s).");
    }

    /**
     * Generates a decide table
     * @param e engine
     * @param def definition
     * @param properties properties
     * @return array of item stacks
     */
    public Array<ItemStack> generateLoot(Engine e, Definition def, Properties properties) {

        Array<Decidable> drops = new Array<>();

        //Get the right decide table
        DecisionTreeElement lootTable;
        if(dropSelf){
            ItemStack stack = ItemStack.createInstance(e.getItemRegistry().getItemDef("tile").getID(), 1, e.getItemRegistry());
            TileComponent.setTileID(def.getID(), stack.getProperties());

            lootTable = new DecisionTreeItem(100, stack, 1, 1);
        } else if (def.hasComponent(GrowableComponent.class)) {
            int stage = GrowableComponent.getStage(properties);

            lootTable = getLootTable(stage);
        } else {
            lootTable = getLootTable();
        }

        //fill array with items
        if (lootTable != null) {
            lootTable.decide(drops, e);
        }

        Array<ItemStack> result = new Array<>();
        for (Decidable drop : drops) {
            if(drop instanceof ItemStack)
                result.add((ItemStack) drop);
        }

        return result;
    }

    public DecisionTreeElement getLootTable(int i) {
        return lootTables.get(i);
    }

    public DecisionTreeElement getLootTable() {
        return lootTables.get(0);
    }

    @Override
    public Component cpy() {
        return new DropComponent(getLootTable());
    }
}
