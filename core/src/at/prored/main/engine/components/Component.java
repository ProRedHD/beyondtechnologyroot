package at.prored.main.engine.components;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.behavior.Behavior;
import at.prored.main.engine.entities.Entity;
import at.prored.main.engine.guis.inventory.componentRenderers.ComponentUIRenderer;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.tiles.TileProperties;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 21.05.2017.
 */
public abstract class Component implements Cloneable {

    public void onEntityAdded(Entity entity){}
    public void onTileAdded(TileProperties tileProperties){}
    public void onItemStackCreated(ItemStack stack, Engine e){}


    public void onFinishedCompLoading(Definition def){}

    public abstract void saveData(int id, Properties properties, Array<Data> data);
    public abstract void loadData(int id, Properties properties, Array<Data> data);

    public abstract void constructComponent(XmlReader.Element element, Engine e, Definition def);

    public Behavior getBehavior(){
        return null;
    }

    /**
     * Returns the component ui render of the component
     * @return component ui render
     */
    public ComponentUIRenderer getComponentRenderer(){
        return null;
    }

    public abstract Component cpy();

}
