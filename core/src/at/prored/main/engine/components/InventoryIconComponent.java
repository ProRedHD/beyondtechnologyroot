package at.prored.main.engine.components;

import at.prored.main.engine.Engine;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.utils.AssetUtils;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 07.07.2018.
 */
public class InventoryIconComponent extends Component {

    private TextureRegion icon;

    public InventoryIconComponent(TextureRegion icon) {
        this.icon = icon;
    }

    public InventoryIconComponent() {
    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {
        this.icon = AssetUtils.loadTextureRegion(e.getAssetManager(), element.getAttribute("name", "chestInventory"));
    }

    @Override
    public Component cpy() {
        return new InventoryIconComponent(icon);
    }

    public TextureRegion getIcon() {
        return icon;
    }
}
