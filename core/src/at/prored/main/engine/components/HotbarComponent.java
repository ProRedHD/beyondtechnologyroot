package at.prored.main.engine.components;

import at.prored.main.engine.Engine;
import at.prored.main.engine.entities.Entity;
import at.prored.main.engine.guis.inventory.componentRenderers.ComponentUIRenderer;
import at.prored.main.engine.guis.inventory.componentRenderers.InventoryUIRenderer;
import at.prored.main.engine.inventory.Inventory;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.saving.data.ItemStackData;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.utils.DataReader;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 18.06.2018.
 */
public class HotbarComponent extends Component implements InventoryRenderable{

    int hotbarSlotCount;

    public HotbarComponent(int hotbarSlotCount) {
        this.hotbarSlotCount = hotbarSlotCount;
    }

    public HotbarComponent() {
    }

    public Inventory getInventory(Properties p) {
        return getHotbarInventory(p);
    }

    public static Inventory getHotbarInventory(Properties p) {
        return (Inventory) p.getData("hotbarInventory");
    }

    public int getSlotCount() {
        return hotbarSlotCount;
    }

    @Override
    public void onEntityAdded(Entity entity) {
        super.onEntityAdded(entity);
        entity.getEntityProperties().setData("hotbarInventory", new Inventory(hotbarSlotCount));
    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {
        ItemStack[] inventory = getInventory(properties).getData();

        for (int i = 0; i < inventory.length; i++) {
            if (inventory[i] != null) {
                data.add(new ItemStackData("stack_" + i, inventory[i]));
            }
        }
    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {
        DataReader reader = new DataReader(data);

        for (int i = 0; i < hotbarSlotCount; i++) {
            if (reader.hasData("stack_" + i)) {
                getInventory(properties).getData()[i] = ((ItemStackData) reader.get("stack_" + i)).getValue();
            }
        }
    }

    @Override
    public ComponentUIRenderer getComponentRenderer() {
        return new InventoryUIRenderer(this, this);
    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {
        this.hotbarSlotCount = element.getIntAttribute("slotCount", 5);
    }

    @Override
    public Component cpy() {
        return new HotbarComponent(hotbarSlotCount);
    }
}
