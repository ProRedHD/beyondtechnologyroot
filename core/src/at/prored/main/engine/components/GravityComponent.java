package at.prored.main.engine.components;

import at.prored.main.engine.Engine;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 27.05.2017.
 */
public class GravityComponent extends Component {

    private float mass;

    public GravityComponent(float mass) {
        this.mass = mass;
    }

    public GravityComponent() {
    }

    public float getMass() {
        return mass;
    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {
        this.mass = Float.parseFloat(element.getAttribute("mass"));
    }

    @Override
    public Component cpy() {
        return new GravityComponent(mass);
    }

}
