package at.prored.main.engine.components;

import at.prored.main.engine.Engine;
import at.prored.main.engine.graphics.renderer.CustomTextureRenderer;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.utils.AssetUtils;
import at.prored.main.engine.utils.ComponentException;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 07.03.2018.
 */
public class TextureStorageComponent extends Component {

    public static final String ACTIVE_TEXTURE = "activetexture";

    /**
     * Holds all textures
     */
    private ObjectMap<String, TextureRegion> textures;
    private String defaultTexture;

    private CustomTextureRenderer customTextureRenderer;

    public TextureStorageComponent() {
        this.textures = new ObjectMap<String, TextureRegion>();
    }

    public TextureStorageComponent(ObjectMap<String, TextureRegion> textures, String defaultTexture, CustomTextureRenderer customTextureRenderer) {
        this.textures = textures;
        this.defaultTexture = defaultTexture;
        this.customTextureRenderer = customTextureRenderer;
    }

    /**
     * Returns a texture under a given name
     *
     * @param name name of texture
     * @return texture object
     */
    public TextureRegion getTexture(String name) {
        if (name == null)
            return null;

        return textures.get(name);
    }

    /**
     * Returns the default texture
     * @return default texture
     */
    public String getDefaultTexture() {
        return defaultTexture;
    }

    /**
     * Sets the default texture
     * @param defaultTexture default texture
     */
    public void setDefaultTexture(String defaultTexture) {
        this.defaultTexture = defaultTexture;
    }

    /**
     * Returns the rendered texture
     *
     * @return rendered texture
     */
    public TextureRegion getActiveTexture(Properties properties) {
        if(properties == null || !properties.hasData(ACTIVE_TEXTURE))
            return getDefaultTexture() != null ? textures.get(getDefaultTexture()) : null;

        return textures.get((String) properties.getData(ACTIVE_TEXTURE));
    }

    /**
     * Sets the rendered texture to the texture with the given texture
     *
     * @param name
     */
    public void setActiveTexture(Properties properties, String name) {
        properties.setData(ACTIVE_TEXTURE, name);
    }

    /**
     * Sets a custom renderer for rendering
     *
     * @param customTextureRenderer custom renderer
     */
    public void setCustomTextureRenderer(CustomTextureRenderer customTextureRenderer) {
        this.customTextureRenderer = customTextureRenderer;
    }

    /**
     * Renders the active texture to the screen
     * @param batch  sprite batch
     * @param x      x
     * @param y      y
     * @param scaleX
     * @param scaleY
     */
    public void render(SpriteBatch batch, float x, float y, float scaleX, float scaleY, OrthographicCamera camera, Definition definition, Properties properties, Engine engine) {

        TextureRegion activeTexture = getActiveTexture(properties);

        if (activeTexture == null && defaultTexture == null && customTextureRenderer == null)
            return;
        else if(activeTexture == null && defaultTexture != null)
            activeTexture = getTexture(defaultTexture);

        if (customTextureRenderer == null) {
            float width = activeTexture.getRegionWidth();
            float height = activeTexture.getRegionHeight();

            if (camera.frustum.boundsInFrustum(x + width / 2f, y + height / 2f, 1f, width / 2f, height / 2f, 1f)) {
                draw(activeTexture, batch, x, y, scaleX, scaleY, camera, properties, definition, engine);
            }
        } else {
            customTextureRenderer.render(batch, x, y, scaleX, scaleY, camera, properties, definition, engine, this);
        }
    }

    /**
     * Draws a texture and looks at the flip and rotate component
     * @param region texture
     * @param batch batch
     * @param x x
     * @param y y
     * @param camera camera
     * @param properties properties
     * @param definition definition
     * @param engine engine
     */
    public void draw(TextureRegion region, SpriteBatch batch, float x, float y, float scaleX, float scaleY, OrthographicCamera camera, Properties properties, Definition definition, Engine engine){
        RotateComponent rotateComponent = definition.getComponent(RotateComponent.class);
        FlipComponent flipComponent = definition.getComponent(FlipComponent.class);

        //flip texture if flipcomponent is not null
        if(flipComponent != null && properties != null)
            region.flip(flipComponent.isFlipX(properties), flipComponent.isFlipY(properties));

        //get width/height
        float width = region.getRegionWidth();
        float height = region.getRegionHeight();

        //Check if the entity texture is inside screeen
        if (camera.frustum.boundsInFrustum(x + (width / 2f), y + (height / 2f), 1, width / 2f, height / 2f, 1f)) {
            float rotation = rotateComponent != null && properties != null ? rotateComponent.getRotation(properties) : 0;

            batch.draw(region, x, y, width/2f, height/2f, width, height, scaleX, scaleY, rotation);
        }

        //flip texture back
        if(flipComponent != null && properties != null)
            region.flip(flipComponent.isFlipX(properties), flipComponent.isFlipY(properties));

    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {

        if(element.getText() != null) {

            //Split strings into map
            String[] parts = element.getText().split(",");

            for (String s : parts) {
                String name = s.trim();

                if(name.isEmpty())
                    continue;

                TextureAtlas.AtlasRegion texture = AssetUtils.loadAtlas(e.getAssetManager()).findRegion(name);

                if (texture == null)
                    throw new ComponentException(this.getClass(), "Texture not found: " + name);

                this.textures.put(name, texture);
            }
        }
    }

    @Override
    public Component cpy() {
        ObjectMap<String, TextureRegion> textures = new ObjectMap<String, TextureRegion>();

        //Copy textures
        for (String s : this.textures.keys()) {
            textures.put(s, this.textures.get(s));
        }

        return new TextureStorageComponent(textures, defaultTexture, customTextureRenderer);
    }

}
