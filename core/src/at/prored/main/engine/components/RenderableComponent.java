package at.prored.main.engine.components;

import at.prored.main.engine.Engine;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.utils.ComponentException;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 18.05.2017.
 */
public class RenderableComponent extends Component {

    private String textureName;

    public RenderableComponent() {
    }

    public RenderableComponent(String textureName) {
        this.textureName = textureName;
    }

    @Override
    public void onFinishedCompLoading(Definition def) {
        if(!def.hasComponent(TextureStorageComponent.class))
            throw new ComponentException(this.getClass(), "Required Component not found: TextureStorageComponent");

        def.getComponent(TextureStorageComponent.class).setDefaultTexture(textureName);
    }

    public TextureRegion getTexture(Definition def){
        return def.getComponent(TextureStorageComponent.class).getTexture(textureName);
    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {
        this.textureName = element.getAttribute("texture");
    }

    @Override
    public RenderableComponent cpy(){
        return new RenderableComponent(textureName);
    }

}
