package at.prored.main.engine.components;

import at.prored.main.engine.Engine;
import at.prored.main.engine.entities.Entity;
import at.prored.main.engine.guis.inventory.componentRenderers.ComponentUIRenderer;
import at.prored.main.engine.guis.inventory.componentRenderers.InventoryUIRenderer;
import at.prored.main.engine.inventory.Inventory;
import at.prored.main.engine.inventory.InventoryUtils;
import at.prored.main.engine.items.ItemDef;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.items.components.TileComponent;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.tiles.TileDef;
import at.prored.main.engine.tiles.TileProperties;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 27.08.2017.
 */
public class CreativeInventoryComponent extends Component implements InventoryRenderable {

    private Engine e;

    private int slotCount;

    public CreativeInventoryComponent() {
    }

    public CreativeInventoryComponent(Engine e) {
        this.e = e;
    }

    public Inventory getInventory(Entity entity) {
        return (Inventory) entity.getEntityProperties().getData("creativeInventory");
    }

    public Inventory getInventory(TileProperties tileProperties) {
        return (Inventory) tileProperties.getData("creativeInventory");
    }

    public Inventory getInventory(Properties properties) {
        return (Inventory) properties.getData("creativeInventory");
    }

    public int getSlotCount() {
        return slotCount;
    }

    @Override
    public void onEntityAdded(Entity entity) {
        super.onEntityAdded(entity);
        entity.getEntityProperties().setData("creativeInventory", createInventory());
    }

    @Override
    public void onTileAdded(TileProperties tileProperties) {
        super.onTileAdded(tileProperties);
        tileProperties.setData("creativeInventory", createInventory());
    }

    @Override
    public void onItemStackCreated(ItemStack stack, Engine e) {
        super.onItemStackCreated(stack, e);
        stack.getProperties().setData("creativeInventory", createInventory());
    }

    /**
     * Creates a inventory with all registered items and tiles
     *
     * @return inventory
     */
    private Inventory createInventory() {
        Array<ItemDef> itemDefs = e.getItemRegistry().getItemDefs();
        Array<TileDef> tileDefs = e.getTileRegistry().getTileDefs();
        this.slotCount = itemDefs.size + tileDefs.size - 2;

        Inventory inv = new Inventory(slotCount);
        ItemDef tile = e.getItemRegistry().getItemDef("tile");

        //Add itemdefs to inventory
        for (ItemDef itemDef : itemDefs) {
            ItemStack stack = ItemStack.createInstance(itemDef.getID(), 100, e.getItemRegistry());
            InventoryUtils.addItem(stack, inv);
        }

        //Add tiledefs to inventory
        for (TileDef tileDef : tileDefs) {
            if(tileDef.getDefID().equals("air"))
                continue;

            ItemStack stack = ItemStack.createInstance(tile.getID(), 100, e.getItemRegistry());
            TileComponent.setTileID(tileDef.getID(), stack.getProperties());
            InventoryUtils.addItem(stack, inv);
        }

        return inv;
    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {
        this.e = e;
    }

    @Override
    public ComponentUIRenderer getComponentRenderer() {
        return new InventoryUIRenderer(this, this);
    }

    @Override
    public Component cpy() {
        return new CreativeInventoryComponent(e);
    }

}
