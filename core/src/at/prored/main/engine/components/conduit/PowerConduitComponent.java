package at.prored.main.engine.components.conduit;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.Component;
import at.prored.main.engine.components.behavior.Behavior;
import at.prored.main.engine.components.behavior.behaviors.ConduitBehavior;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 27.02.2018.
 */
public class PowerConduitComponent extends ConduitComponent {


    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {

    }

    @Override
    public Behavior getBehavior() {
        return new ConduitBehavior(getClass());
    }

    @Override
    public Component cpy() {
        return new PowerConduitComponent();
    }
}
