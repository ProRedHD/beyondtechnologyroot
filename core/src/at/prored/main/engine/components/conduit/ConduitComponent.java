package at.prored.main.engine.components.conduit;

import at.prored.main.engine.components.Component;
import at.prored.main.engine.utils.Properties;

/**
 * Created by ProRed on 27.02.2018.
 */
public abstract class ConduitComponent extends Component {

    public static void setGridID(Properties p, int gridID){
        p.setInteger("gridID", gridID);
    }

    public static int getGridID(Properties p){
        return p.getInteger("gridID");
    }

    public static boolean hasGridID(Properties p){
        return p.hasData("gridID");
    }

}
