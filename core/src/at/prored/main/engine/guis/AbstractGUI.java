package at.prored.main.engine.guis;

import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by ProRed on 27.08.2017.
 */
public abstract class AbstractGUI {

    private boolean active;
    protected InputMultiplexer multiplexer;

    public AbstractGUI(){}

    public abstract void initializeGUI(AssetManager assetManager);

    public abstract void update(GUIManager guiManager);

    public void updateAlways(GUIManager guiManager) {}

    public abstract void render(SpriteBatch batch);

    public void renderAlways(SpriteBatch batch){}

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    protected void onActivated(GUIManager guiManager){

    }

    protected void onDeactivated(GUIManager guiManager){

    }

    public void registerInputProcessors(InputMultiplexer inputMultiplexer){
        this.multiplexer = inputMultiplexer;
    }

    public void deregisterInputProcessors(InputMultiplexer inputMultiplexer){
        multiplexer = null;
    }
}
