package at.prored.main.engine.guis;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.HotbarComponent;
import at.prored.main.engine.components.behavior.BehaviorManager;
import at.prored.main.engine.components.behavior.EventType;
import at.prored.main.engine.entities.Entity;
import at.prored.main.engine.entities.EntityDef;
import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.graphics.shaders.Shaders;
import at.prored.main.engine.inventory.Inventory;
import at.prored.main.engine.items.ItemDef;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.items.components.HotbarActionComponent;
import at.prored.main.engine.items.hotbaraction.HotbarActionItem;
import at.prored.main.engine.tiles.storage.GameMap;
import at.prored.main.engine.utils.AssetUtils;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

import java.util.Arrays;
import java.util.Iterator;

/**
 * Created by ProRed on 27.08.2017.
 */
public class Hotbar extends AbstractGUI {

    private static final float BACKGROUND_SLIDING_SPEED = .125f;

    private static final int HOTBAR_X_OFFSET = 50;
    private static final int HOTBAR_Y_OFFSET = 50;

    private Engine engine;
    private GameMap gameMap;

    private TextureRegion pixel;
    private TextureRegion itemSlot, itemSlotActivated;

    private NinePatch background;
    private float backgroundWidth, backgroundHeight;

    private Inventory inventory;

    private int activeSlot;

    private Array<HotbarSlot> slots;
    private float x, y;

    private Array<HotbarActionItem> actionItems;

    //Animation
    private int backgroundState;
    private Color opacityColor;
    private float offset;

    public Hotbar(Engine e) {
        this.setActive(false);
        this.engine = e;
        this.activeSlot = -1;

        this.actionItems = new Array<>();
    }

    @Override
    public void initializeGUI(AssetManager assetManager) {
        this.pixel = AssetUtils.loadTextureRegion(assetManager, "pixel");
        this.itemSlot = AssetUtils.loadTextureRegion(assetManager, "item_slot");
        this.itemSlotActivated = AssetUtils.loadTextureRegion(assetManager, "item_slot_activated");

        this.background = AssetUtils.loadNinePatch(assetManager, "crafting_ui_background", 3, 3, 3, 3);
        this.opacityColor = new Color(Color.WHITE);

        this.slots = new Array<>();
        calculatePosition();
    }

    public void syncronizeHotbarToEntity(Entity e) {

        //Get entity def
        EntityDef def = gameMap.getEngine().getEntityRegistry().getEntityDef(e.getID());

        //Update inventory
        setInventory(HotbarComponent.getHotbarInventory(e.getEntityProperties()));
    }

    private void calculatePosition() {
        this.x = -(Graphics.V_WIDTH / 2f) + HOTBAR_X_OFFSET;
        this.y = -(Graphics.V_HEIGHT / 2f);
    }

    private void initSlots() {
        for (int i = 0; i < inventory.getData().length; i++) {
            slots.add(new HotbarSlot(itemSlot, itemSlotActivated, 63 * i, 0));
        }

        //setup sizes
        backgroundWidth = 15 + slots.size * 63;
        backgroundHeight = 68;
    }

    @Override
    public void update(GUIManager guiManager) {
        for (int i = 0; i < slots.size; i++) {
            slots.get(i).setStack(inventory.getData()[i]);
        }

        for (int i = 0; i < slots.size; i++) {
            if (i > 16)
                break;

            if (Gdx.input.isKeyJustPressed(8 + i)) {
                if (activeSlot != i) {
                    if (activeSlot != -1) {

                        //Deactivate slot
                        slots.get(activeSlot).setActivated(false);

                        //Notify of deselect
                        ItemStack stack = slots.get(activeSlot).getStack();
                        BehaviorManager.instance().broadcastItem(EventType.HOTBAR_ITEM_DESELECTED, null, stack, inventory, gameMap);
                    }

                    //Notify all other hotbaractionitems of switch
                    for (HotbarActionItem actionItem : actionItems) {
                        actionItem.markStop();
                    }

                    //Create hotbaractionitem
                    HotbarSlot s = slots.get(i);
                    if (s.getStack() != null) {
                        ItemDef def = engine.getItemRegistry().getItemDef(slots.get(i).getStack().getID());
                        if(def.hasComponent(HotbarActionComponent.class)) {
                            HotbarActionItem item = new HotbarActionItem(s, s.getX(x + 15) + 1, s.getY(y + 10 + offset), def, engine);
                            item.start();
                            actionItems.add(item);
                        }
                    }

                    //Notify of item select
                    ItemStack stack = slots.get(i).getStack();
                    BehaviorManager.instance().broadcastItem(EventType.HOTBAR_ITEM_SELECTED, null, stack, inventory, gameMap);
                }

                activeSlot = i;
                slots.get(activeSlot).setActivated(true);
            }
        }

        //Update selected item
        Iterator<HotbarActionItem> actionItemArrayIterator = actionItems.iterator();
        while (actionItemArrayIterator.hasNext()) {
            HotbarActionItem item = actionItemArrayIterator.next();
            item.update();
            if (item.isDone())
                actionItemArrayIterator.remove();
        }
    }

    @Override
    public void render(SpriteBatch batch) {
        if (inventory == null)
            return;

        batch.setColor(opacityColor);

        background.draw(batch, x, y + offset, backgroundWidth, backgroundHeight);

        for (int i = 0; i < slots.size; i++) {
            HotbarSlot s = slots.get(i);
            s.renderBackground(x + 15, y + 10 + offset, batch);
            s.renderItem(x + 15, y + 10 + offset, batch, Shaders.instance().getSilhouette(), engine);
        }

        batch.setColor(Color.WHITE);

        //Render hotbar actions
        for (HotbarActionItem actionItem : actionItems) {
            actionItem.render(batch);
        }
    }

    @Override
    public void updateAlways(GUIManager guiManager) {
        //Update opacity
        float amount = HOTBAR_Y_OFFSET;
        opacityColor.a = (offset / amount);

        //Update sliding
        switch (backgroundState) {
            case 2:
                offset -= amount * Graphics.instance().getDeltaTime() / BACKGROUND_SLIDING_SPEED;
                if (offset <= 0) {
                    offset = 0;
                    backgroundState = 0;
                    guiManager.closeGUI(GUIManager.GUI.INVENTORY_HOTBAR);
                    actionItems.clear();
                }
                break;
            case 1:
                offset += amount * Graphics.instance().getDeltaTime() / BACKGROUND_SLIDING_SPEED;
                if (offset >= amount) {
                    offset = amount;
                    backgroundState = 0;
                }
                break;
            default:
        }
    }

    public void toggle(GUIManager manager) {
        if (isActive()) {
            backgroundState = 2;

            //Tell action items to stop
            for (HotbarActionItem actionItem : actionItems) {
                actionItem.markStop();
            }

            if (activeSlot != -1)
                BehaviorManager.instance().broadcastItem(EventType.HOTBAR_ITEM_DESELECTED, null, slots.get(activeSlot).getStack(), inventory, gameMap);
        } else {
            manager.openGUI(GUIManager.GUI.INVENTORY_HOTBAR);
            backgroundState = 1;

            //Activate active slot again
            if (activeSlot != -1) {
                HotbarSlot hotbarSlot = slots.get(activeSlot);

                if(hotbarSlot.getStack() == null)
                    return;

                ItemDef def = engine.getItemRegistry().getItemDef(hotbarSlot.getStack().getID());
                HotbarActionItem item = new HotbarActionItem(hotbarSlot, hotbarSlot.getX(x + 15) + 1, hotbarSlot.getY(y + 10 + offset), def, engine);
                item.start();
                actionItems.add(item);

                BehaviorManager.instance().broadcastItem(EventType.HOTBAR_ITEM_SELECTED, null, hotbarSlot.getStack(), inventory, gameMap);
            }
        }
    }

    public boolean isClosed() {
        return backgroundState == 0;
    }

    public void setInventory(Inventory inventory) {
        if (inventory == null || this.inventory == null || !Arrays.equals(inventory.getData(), this.inventory.getData())) {
            this.inventory = inventory;
            initSlots();
        }
    }

    public void setGameMap(GameMap gameMap) {
        this.gameMap = gameMap;
    }
}
