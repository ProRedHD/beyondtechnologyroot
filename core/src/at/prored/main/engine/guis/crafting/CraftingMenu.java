package at.prored.main.engine.guis.crafting;

import at.prored.main.engine.Engine;
import at.prored.main.engine.crafting.Recipe;
import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.graphics.font.Fonts;
import at.prored.main.engine.guis.AbstractGUI;
import at.prored.main.engine.guis.GUIManager;
import at.prored.main.engine.guis.elements.GuiImage;
import at.prored.main.engine.guis.elements.GuiLabel;
import at.prored.main.engine.guis.elements.button.GuiButton;
import at.prored.main.engine.guis.elements.panes.RelativePane;
import at.prored.main.engine.guis.elements.panes.ScrollPane;
import at.prored.main.engine.guis.elements.panes.TabbedPane;
import at.prored.main.engine.guis.elements.tabs.TabText;
import at.prored.main.engine.tiles.storage.GameMap;
import at.prored.main.engine.utils.AssetUtils;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

/**
 * Created by ProRed on 30.01.2018.
 */
public class CraftingMenu extends AbstractGUI {

    //Constant
    private static final int BACKGROUND_PADDING_TOP_BOTTOM = 45;
    private static final int BACKGROUND_PADDING_RIGHT = -1;
    private static final float BACKGROUND_SLIDING_SPEED = .25f;
    private static final Color seperatorColor = new Color(.315f, .315f, .315f, 1f);

    private Engine e;
    private GameMap gameMap;

    private NinePatch crafting_ui_element;

    private Color opacityColor;
    private RelativePane pane;
    private TabbedPane tabbedPane;

    //Background
    private TextureRegion crafting_ui_background, pixel;
    private GuiImage backgroundImage;
    private int backgroundwidth, backgroundheight;
    private int backgroundx, backgroundy;
    private int backgroundState;
    private float offset;

    public CraftingMenu(Engine e) {
        this.e = e;
    }

    @Override
    public void initializeGUI(AssetManager assetManager) {
        this.opacityColor = new Color(Color.WHITE);

        calculateDimensions();

        this.pane = new RelativePane(backgroundx, backgroundy);

        //Background
        this.crafting_ui_background = AssetUtils.loadTextureRegion(assetManager, "crafting_ui_background");
        this.backgroundImage = new GuiImage(crafting_ui_background, 0, 0, backgroundwidth, backgroundheight);
        this.backgroundImage.convertToNinePatch(3, 3, 3, 3);
        pane.add(backgroundImage);

        //Init TabbedPane
        this.tabbedPane = new TabbedPane(0, 0, backgroundwidth, backgroundheight);
        pane.add(tabbedPane);

        //Recipe element
        this.crafting_ui_element = new NinePatch(AssetUtils.loadTextureRegion(assetManager, "crafting_ui_element"), 42, 12, 37, 31);

        //Pixel
        pixel = AssetUtils.loadTextureRegion(assetManager, "pixel");

        setupPlayerTab();
    }

    /**
     * Setup player tab
     */
    public void setupPlayerTab() {
        TabText playerTab = new TabText("player", e);

        playerTab.setContent(new ScrollPane(0, 0, backgroundwidth, backgroundheight, 15));
        playerTab.setText("Player");

        ((ScrollPane) playerTab.getContent()).add(new GuiLabel(0, 0, Fonts.instance().getNormalFont(), "Hier könnte ihr Text stehen."));
        ((ScrollPane) playerTab.getContent()).add(new GuiButton(e, "Hallo", 0, 0, 100, 25));

        tabbedPane.addTab("player", playerTab);
        tabbedPane.switchTab(playerTab);

        updateRecipes();
    }

    public void updateRecipes() {
        ScrollPane sc = ((ScrollPane) tabbedPane.getTabs().get("player").getContent());

        sc.clear();

        Array<Recipe> unlockedRecipes = e.getRecipeRegistry().getUnlockedRecipeForRecipeType("inventory");

        if (unlockedRecipes == null) {
            return;
        }

        for (Recipe recipe : unlockedRecipes) {
            RecipeEntry element = new RecipeEntry(0, 0, recipe, e);
            element.setQueue(gameMap.getCraftingQueuePlayer());
            sc.add(element);
            element.setTexture(crafting_ui_element);

            /*GuiImage image = new GuiImage(pixel, 0, 0, backgroundwidth - 2, 5);
            image.setTint(seperatorColor);
            sc.add(image); */
        }

    }

    /**
     * Calculates the dimensions of the menu
     */
    private void calculateDimensions() {
        this.backgroundwidth = (int) (Graphics.V_WIDTH / 4f);
        this.backgroundheight = Graphics.V_HEIGHT - BACKGROUND_PADDING_TOP_BOTTOM * 2;
        this.backgroundx = Graphics.V_WIDTH - BACKGROUND_PADDING_RIGHT - this.backgroundwidth - Graphics.V_WIDTH / 2;
        this.backgroundy = BACKGROUND_PADDING_TOP_BOTTOM - Graphics.V_HEIGHT / 2;

        this.offset = (float) backgroundwidth + BACKGROUND_PADDING_RIGHT;
    }

    /**
     * Sets the gameMap of the crafting menu
     *
     * @param gameMap
     */
    public void setGameMap(GameMap gameMap) {
        this.gameMap = gameMap;
    }

    /**
     * Updates gui elements
     *
     * @param guiManager guiManager
     */
    @Override
    public void update(GUIManager guiManager) {
        tabbedPane.update();
    }

    /**
     * Updates the gui visibility and the sliding animation
     *
     * @param guiManager guiManager
     */
    @Override
    public void updateAlways(GUIManager guiManager) {
        if (Gdx.input.isKeyJustPressed(Input.Keys.C)) {
            if (isActive()) {
                backgroundState = 2;
            } else {
                guiManager.openGUI(GUIManager.GUI.CRAFTING_MENU);
                backgroundState = 1;
            }
        }

        //Background
        pane.setX((int) (backgroundx + offset));

        //Update opacity
        float amount = backgroundwidth + BACKGROUND_PADDING_RIGHT;
        opacityColor.a = 1 - (offset / amount);

        //Update sliding
        switch (backgroundState) {
            case 1:
                offset -= amount * Graphics.instance().getDeltaTime() / BACKGROUND_SLIDING_SPEED;
                if (offset <= 0) {
                    offset = 0;
                    backgroundState = 0;
                }
                break;
            case 2:
                offset += amount * Graphics.instance().getDeltaTime() / BACKGROUND_SLIDING_SPEED;
                if (offset >= amount) {
                    offset = amount;
                    backgroundState = 0;
                    guiManager.closeGUI(GUIManager.GUI.CRAFTING_MENU);
                }
                break;
            default:
        }
    }

    @Override
    public void render(SpriteBatch batch) {
        batch.setColor(opacityColor);
        pane.render(batch);
        batch.setColor(Color.WHITE);
    }

    @Override
    public void renderAlways(SpriteBatch batch) {

        //Render crafting queue
        gameMap.getCraftingQueuePlayer().render(pane.getX() - 10f, pane.getY(), batch);
    }

    @Override
    public void registerInputProcessors(InputMultiplexer inputMultiplexer) {
        super.registerInputProcessors(inputMultiplexer);
        pane.registerInputProcessors(inputMultiplexer);
    }

    @Override
    public void deregisterInputProcessors(InputMultiplexer inputMultiplexer) {
        super.deregisterInputProcessors(inputMultiplexer);
        pane.deregisterInputProcessors(inputMultiplexer);
    }

    public float getX(){
        return pane.getX();
    }
}
