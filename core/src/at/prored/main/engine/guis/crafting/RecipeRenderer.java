package at.prored.main.engine.guis.crafting;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.LocalizableComponent;
import at.prored.main.engine.crafting.Recipe;
import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.graphics.font.Fonts;
import at.prored.main.engine.guis.elements.InputGuiElement;
import at.prored.main.engine.items.ItemDef;
import at.prored.main.engine.items.ItemRenderer;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.localization.GameTranslator;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by ProRed on 09.10.2018.
 */
public class RecipeRenderer extends InputGuiElement {

    private Engine engine;
    private Recipe r;
    private int width;

    public RecipeRenderer(Recipe r, int width, Engine e){
        this.r = r;
        this.width = width;
        this.engine = e;
    }

    @Override
    public void render(float offsetX, float offsetY, SpriteBatch batch) {
        renderImages(offsetX, offsetY, batch);
        renderFont(offsetX, offsetY, batch);
    }

    private void renderImages(float offsetX, float offsetY, SpriteBatch batch){

        //Render Result
        ItemRenderer.renderItem(batch, offsetX + 8, offsetY + getHeight() - 23, 1f, 1f, engine, Graphics.instance().getCamera().getHudCamera(), r.getResult(), true);

        //Render Ingredients
        for (int i = 0; i < r.getIngredients().size; i++) {
            ItemStack stack = r.getIngredients().get(i);
            ItemRenderer.renderItem(batch, offsetX + 6, offsetY + getHeight() - (66 + i * 26), .75f, .75f, engine, Graphics.instance().getCamera().getHudCamera(), stack, true);
        }
    }


    private void renderFont(float offsetX, float offsetY, SpriteBatch batch){
        BitmapFont font = Fonts.instance().getNormalFont();
        BitmapFont smallFont = Fonts.instance().getSmallFont();

        //Result
        ItemDef def = engine.getItemRegistry().getItemDef(r.getResult().getID());
        String name = def.getComponent(LocalizableComponent.class).getName();
        font.draw(batch, r.getResult().getAmount() + "x " + GameTranslator.instance().get(name), offsetX + 44, offsetY + getHeight());

        //Ingredients
        for (int i = 0; i < r.getIngredients().size; i++) {
            ItemStack stack = r.getIngredients().get(i);
            ItemDef idef = engine.getItemRegistry().getItemDef(stack.getID());
            name = idef.getComponent(LocalizableComponent.class).getName();
            smallFont.draw(batch, stack.getAmount() + "x " + GameTranslator.instance().get(name), offsetX + 42, offsetY + getHeight() - (45 + i * 26));
        }
    }

    @Override
    public void update() {

    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return 67 + r.getIngredients().size * 26;
    }
}
