package at.prored.main.engine.guis.crafting;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.LocalizableComponent;
import at.prored.main.engine.crafting.Recipe;
import at.prored.main.engine.crafting.queue.CraftingQueue;
import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.graphics.font.Fonts;
import at.prored.main.engine.guis.elements.GuiElement;
import at.prored.main.engine.guis.elements.button.GuiButton;
import at.prored.main.engine.items.ItemDef;
import at.prored.main.engine.items.ItemRenderer;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.items.components.TileComponent;
import at.prored.main.engine.localization.GameTranslator;
import at.prored.main.engine.tiles.TileDef;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

public class RecipeEntry extends GuiElement {

    public static final int PADDING_LEFT_RIGHT = 20;

    private NinePatch texture;
    private Engine engine;
    private CraftingQueue queue;

    private int x, y;
    private int width, height;

    private Recipe recipe;

    private GuiButton craftButton, craftMultipleButton, craftAllButton;


    public RecipeEntry(int x, int y, Recipe r, Engine e) {
        this.x = x;
        this.y = y;
        this.recipe = r;
        this.engine = e;

        calculateDimensions();
        initElements();
    }

    /**
     * Sets the queue for the buttons
     * @param queue crafting queue
     */
    public void setQueue(CraftingQueue queue) {
        this.queue = queue;
    }

    /**
     * Calculates the height of a element with the given ingredients
     *
     * @param ingredients array of ingredients
     * @return height
     */
    public static int getHeight(Array<ItemStack> ingredients) {
        return 67 + ingredients.size * 26;
    }

    /**
     * Calculates the dimentsions of the recipe entry
     */
    private void calculateDimensions() {
        width = Graphics.V_WIDTH / 4 - PADDING_LEFT_RIGHT * 2;
        height = getHeight(recipe.getIngredients());
    }

    /**
     * Initiates all gui elements
     */
    private void initElements() {
        String craftName = GameTranslator.instance().get("craft");
        String craftMultipleName = GameTranslator.instance().get("craftMultiple");

        Fonts.getLayout().setText(Fonts.instance().getSmallFont(), craftName);
        float craftWidth = Fonts.getLayout().width;

        Fonts.getLayout().setText(Fonts.instance().getSmallFont(), "5 " + craftMultipleName);
        float craftMultipleWidth = Fonts.getLayout().width;

        Fonts.getLayout().setText(Fonts.instance().getSmallFont(), "13 " + craftMultipleName);
        float craftAllWidth = Fonts.getLayout().width;

        craftButton = new GuiButton(engine, craftName, width - (16 + craftWidth), 4, craftWidth + 10, 20);
        craftMultipleButton = new GuiButton(engine, "5 " + craftMultipleName, craftButton.getX() - (15 + craftMultipleWidth), 4, craftMultipleWidth + 10, 20);
        craftAllButton = new GuiButton(engine, "13 " + craftMultipleName, craftMultipleButton.getX() - (15 + craftAllWidth), 4, craftAllWidth + 10, 20);

        craftButton.setClickListener(() -> queue.addToQueue(recipe));

        addSubElement(craftButton);
        addSubElement(craftMultipleButton);
        addSubElement(craftAllButton);
    }

    /**
     * Sets the texture of the recipe entry
     *
     * @param texture ninepatch
     */
    public void setTexture(NinePatch texture) {
        this.texture = texture;
    }

    /**
     * Renders the entry
     * @param offsetX offsetX
     * @param offsetY offsetY
     * @param batch   batch
     */
    @Override
    public void render(float offsetX, float offsetY, SpriteBatch batch) {
        texture.draw(batch, offsetX + x, offsetY + y, width, height);

        renderResult(offsetX, offsetY, batch);
        renderIngredients(offsetX, offsetY, batch);

        renderText(offsetX, offsetY, batch);

        craftButton.render(offsetX, offsetY, batch);
        craftMultipleButton.render(offsetX, offsetY, batch);
        craftAllButton.render(offsetX, offsetY, batch);
    }

    /**
     * Renders the result item in the upper left corner
     *
     * @param offsetX offsetX
     * @param offsetY offsetY
     * @param batch   batch
     */
    private void renderResult(float offsetX, float offsetY, SpriteBatch batch) {
        ItemRenderer.renderItem(batch, offsetX + x + 3, offsetY + y + height - 35, 1f, 1f, engine, Graphics.instance().getCamera().getHudCamera(), recipe.getResult(), true);
    }

    /**
     * Renders the ingredients textures into the right area
     *
     * @param offsetX offsetX
     * @param offsetY offsetY
     * @param batch   batch
     */
    private void renderIngredients(float offsetX, float offsetY, SpriteBatch batch) {
        for (int i = 0; i < recipe.getIngredients().size; i++) {
            ItemStack stack = recipe.getIngredients().get(i);
            ItemRenderer.renderItem(batch, (float) (offsetX + x + 6), offsetY + y + height - (66 + i * 26), .75f, .75f, engine, Graphics.instance().getCamera().getHudCamera(), stack, true);
        }
    }

    /**
     * Renders the item names to the textures
     *
     * @param offsetX offsetX
     * @param offsetY offsetY
     * @param batch   batch
     */
    private void renderText(float offsetX, float offsetY, SpriteBatch batch) {
        BitmapFont font = Fonts.instance().getNormalFont();
        BitmapFont smallFont = Fonts.instance().getSmallFont();

        //Result
        String name = getName(recipe.getResult(), engine);
        font.draw(batch, recipe.getResult().getAmount() + "x " + GameTranslator.instance().get(name), (float) (offsetX + x + 42), offsetY + y + height - 12);

        //Ingredients
        for (int i = 0; i < recipe.getIngredients().size; i++) {
            ItemStack stack = recipe.getIngredients().get(i);
            name = getName(stack, engine);
            smallFont.draw(batch, stack.getAmount() + "x " + GameTranslator.instance().get(name), (float) (offsetX + x + 42), offsetY + y + height - (45 + i * 26));
        }
    }

    private String getName(ItemStack stack, Engine e){

        //Get localized name of itemstack
        ItemDef def = e.getItemRegistry().getItemDef(stack.getID());

        String name = "";
        if (def.hasComponent(TileComponent.class)) {
            TileDef tileDef = e.getTileRegistry().getTileDef(TileComponent.getTileID(stack.getProperties()));

            if (tileDef.hasComponent(LocalizableComponent.class))
                name = GameTranslator.instance().get(tileDef.getComponent(LocalizableComponent.class).getName());
            else
                name = "tile_" + tileDef.getDefID();
        } else if (def.hasComponent(LocalizableComponent.class)) {
            name = GameTranslator.instance().get(def.getComponent(LocalizableComponent.class).getName());
        }

        return name;
    }

    @Override
    public void update() {
        craftButton.update();
        craftMultipleButton.update();
        craftAllButton.update();
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return getHeight(recipe.getIngredients());
    }
}
