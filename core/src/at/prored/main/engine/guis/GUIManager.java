package at.prored.main.engine.guis;

import at.prored.main.engine.Engine;
import at.prored.main.engine.guis.crafting.CraftingMenu;
import at.prored.main.engine.guis.inventory.InventoryGui;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by ProRed on 27.08.2017.
 */
public class GUIManager {

    private Engine e;

    private ObjectMap<GUI, AbstractGUI> guis;
    private Array<AbstractGUI> activeGuis;

    public GUIManager(Engine engine) {
        this.e = engine;

        this.guis = new ObjectMap<>();
        this.activeGuis = new Array<>();

        this.registerGUI(GUIManager.GUI.INVENTORY_HOTBAR, new Hotbar(engine));
        this.registerGUI(GUIManager.GUI.CRAFTING_MENU, new CraftingMenu(engine));
        this.registerGUI(GUIManager.GUI.INVENTORY, new InventoryGui(engine));
    }

    /**
     * Initializes all GUIs
     *
     * @param assetManager assetManager
     */
    public void init(AssetManager assetManager) {
        for (AbstractGUI abstractGUI : this.guis.values()) {
            abstractGUI.initializeGUI(assetManager);
        }
    }

    /**
     * Opens a gui
     *
     * @param gui gui
     */
    public void openGUI(GUI gui) {
        AbstractGUI abstractGUI = this.guis.get(gui);
        abstractGUI.setActive(true);
        abstractGUI.onActivated(this);
        abstractGUI.registerInputProcessors(e.getInputMultiplexer());

        activeGuis.add(abstractGUI);
    }

    /**
     * Closes a gui
     *
     * @param gui gui
     */
    public void closeGUI(GUI gui) {
        AbstractGUI abstractGUI = this.guis.get(gui);
        abstractGUI.setActive(false);
        abstractGUI.onDeactivated(this);
        abstractGUI.deregisterInputProcessors(e.getInputMultiplexer());

        activeGuis.removeValue(abstractGUI, true);
    }

    /**
     * Registers a gui in the gui manager
     *
     * @param guienum enum that represents the abstractgui instance
     * @param gui     abstractgui instance
     */
    public void registerGUI(GUI guienum, AbstractGUI gui) {
        this.guis.put(guienum, gui);
    }

    /**
     * Returns a abstrct gui instance
     *
     * @param gui gui enum
     * @return abstract gui
     */
    public AbstractGUI getGUI(GUI gui) {
        return this.guis.get(gui);
    }

    /**
     * Updates all active guis
     */
    public void update() {
        for (AbstractGUI gui : guis.values()) {
            if (gui.isActive())
                gui.update(this);
        }

        updateAlways();
    }

    /**
     * Updates all guis
     */
    public void updateAlways() {
        for (AbstractGUI gui : guis.values()) {
            gui.updateAlways(this);
        }
    }

    /**
     * Renders all guis
     */
    public void render(SpriteBatch batch) {
        batch.begin();
        for (AbstractGUI gui : guis.values()) {
            if (gui.isActive())
                gui.render(batch);
        }

        renderAlways(batch);
        batch.end();
    }

    private void renderAlways(SpriteBatch batch){
        for (AbstractGUI gui : guis.values()) {
            gui.renderAlways(batch);
        }
    }

    /**
     * Enum for GUI
     */
    public enum GUI {
        CRAFTING_MENU, INVENTORY, INVENTORY_HOTBAR
    }

}
