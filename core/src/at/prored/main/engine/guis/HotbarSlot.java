package at.prored.main.engine.guis;

import at.prored.main.engine.Engine;
import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.items.ItemRenderer;
import at.prored.main.engine.items.ItemStack;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

/**
 * Created by ProRed on 21.09.2017.
 */
public class HotbarSlot {

    private TextureRegion texture;
    private TextureRegion activatedTexture;
    private float relX, relY;
    private float itemOffsetX = 8, itemOffsetY = 8;

    private ItemStack stack;
    private boolean silhouette = false, activated=false;

    public HotbarSlot(TextureRegion texture, float relX, float relY) {
        this(texture, null, relX, relY);
    }

    public HotbarSlot(TextureRegion texture, TextureRegion activatedTexture, float relX, float relY) {
        this.texture = texture;
        this.activatedTexture = activatedTexture;

        this.relX = relX;
        this.relY = relY;
    }

    public void renderBackground(float guiX, float guiY, SpriteBatch batch) {
        if (activated)
            batch.draw(activatedTexture, guiX + this.relX - 1, guiY + this.relY - 1);
        else
            batch.draw(texture, guiX + this.relX, guiY + this.relY);

    }

    public void renderItem(float guiX, float guiY, SpriteBatch batch, ShaderProgram silshader, Engine engine) {
        if (stack == null)
            return;

        if (this.silhouette) {
            if (batch.getShader() != silshader)
                batch.setShader(silshader);
        }

        float offsetX = this.itemOffsetX;
        float offsetY = this.itemOffsetY;

        if(activated){
            offsetX++;
            offsetY++;
        }

        ItemRenderer.renderItem(batch, guiX + this.relX + offsetX, guiY + this.relY + offsetY, 1, 1, engine, Graphics.instance().getCamera().getHudCamera(), stack, true);

        if (batch.getShader() == silshader)
            batch.setShader(null);
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
        if(activated)
            this.silhouette = true;
    }

    public void setSilhouette(boolean silhouette) {
        this.silhouette = silhouette;
    }

    public ItemStack getStack() {
        return stack;
    }

    public void setStack(ItemStack stack) {
        this.stack = stack;
    }

    /**
     * Position of item on screen
     * @param guiX guiX
     * @return x pos
     */
    public float getX(float guiX){
        return guiX + this.relX + this.itemOffsetX;
    }

    /**
     * Position of item on screen
     * @param guiY guiY
     * @return y pos
     */
    public float getY(float guiY){
        return guiY + this.relY + this.itemOffsetY;
    }
}
