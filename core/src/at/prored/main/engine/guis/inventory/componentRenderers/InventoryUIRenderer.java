package at.prored.main.engine.guis.inventory.componentRenderers;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.Component;
import at.prored.main.engine.components.CreativeInventoryComponent;
import at.prored.main.engine.components.InventoryRenderable;
import at.prored.main.engine.guis.elements.panes.RelativePane;
import at.prored.main.engine.guis.inventory.Slot;
import at.prored.main.engine.inventory.Inventory;
import at.prored.main.engine.inventory.InventoryObserver;
import at.prored.main.engine.inventory.InventoryProperties;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

public class InventoryUIRenderer extends ComponentUIRenderer implements InventoryObserver {

    private Engine e;

    private RelativePane pane;
    private Array<Slot> slots;
    private float width;

    private int rowCnt, collumnCnt;

    private InventoryRenderable i;
    private Properties properties;

    public InventoryUIRenderer(Component c, InventoryRenderable i) {
        super(c);
        this.i = i;

        pane = new RelativePane(0, 0);
        slots = new Array<Slot>();
    }

    public void setup(int width, Engine engine, Properties p, InventoryProperties invProps) {
        this.width = width;
        this.e = engine;
        this.properties = p;

        initSlots(invProps);
    }

    private void initSlots(InventoryProperties p) {
        int slotCount = i.getSlotCount();

        collumnCnt = (int) ((width) / 37);
        rowCnt = (int) Math.ceil(slotCount / (float) collumnCnt);

        int index = 0;
        Inventory inventory = i.getInventory(properties);
        inventory.registerObserver(this);

        for (int i = rowCnt - 1; i >= 0; i--) {
            for (int j = 0; j < collumnCnt; j++) {
                if (slots.size < slotCount) {
                    Slot s = new Slot(e, inventory.getData()[index], 37 * j, i * 37);
                    s.setCreative(getComponent() instanceof CreativeInventoryComponent);

                    s.setInvProps(p);
                    index++;
                    slots.add(s);
                    pane.add(s);
                }
            }
        }
    }

    @Override
    public void render(float offsetX, float offsetY, SpriteBatch batch) {
        pane.render(offsetX, offsetY, batch);
    }

    @Override
    public void renderGlobal(int offsetX, int offsetY, SpriteBatch batch) {

        //Render slots
        for (Slot slot : slots) {
            slot.renderTooltip(batch);
        }
    }

    @Override
    public void update() {
        pane.update();
    }

    private void save() {
        ItemStack[] inventory = getInventory();

        for (int i = 0; i < slots.size; i++) {
            inventory[i] = slots.get(i).getStack();
        }
    }

    private void load() {
        ItemStack[] inventory = getInventory();

        for (int i = 0; i < slots.size; i++) {
            slots.get(i).setStack(inventory[i]);
        }
    }

    private ItemStack[] getInventory() {
        Inventory inventory = i.getInventory(properties);
        return inventory.getData();
    }

    @Override
    public void beforeChange() {
        if (!(getComponent() instanceof CreativeInventoryComponent))
            save();
    }

    @Override
    public void afterChange() {
        load();
    }

    @Override
    public void onAdded() {
        super.onAdded();
        load();
    }

    @Override
    public void onRemoved() {
        super.onRemoved();
        if (!(getComponent() instanceof CreativeInventoryComponent))
            save();
    }

    @Override
    public void registerInputProcessors(InputMultiplexer inputMultiplexer) {
        super.registerInputProcessors(inputMultiplexer);
        pane.registerInputProcessors(inputMultiplexer);
    }

    @Override
    public void deregisterInputProcessors(InputMultiplexer inputMultiplexer) {
        super.deregisterInputProcessors(inputMultiplexer);
        pane.deregisterInputProcessors(inputMultiplexer);
    }

    @Override
    public int getWidth() {
        return collumnCnt * 37;
    }

    @Override
    public int getHeight() {
        return rowCnt * 37;
    }

}
