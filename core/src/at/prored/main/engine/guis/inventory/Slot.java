package at.prored.main.engine.guis.inventory;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.TooltipComponent;
import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.graphics.font.Fonts;
import at.prored.main.engine.guis.elements.InputGuiElement;
import at.prored.main.engine.guis.tooltip.TooltipRenderer;
import at.prored.main.engine.guis.tooltip.elements.TooltipElement;
import at.prored.main.engine.guis.tooltip.elements.TooltipElementItemStack;
import at.prored.main.engine.guis.tooltip.elements.TooltipElementLabel;
import at.prored.main.engine.inventory.InventoryProperties;
import at.prored.main.engine.inventory.InventoryUtils;
import at.prored.main.engine.items.ItemDef;
import at.prored.main.engine.items.ItemRenderer;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.utils.AssetUtils;
import at.prored.main.engine.utils.StringUtils;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Slot extends InputGuiElement {

    private static final int EXTENSION_AMOUNT = 4;

    private static TextureRegion slotTexture;

    private Engine e;
    private ItemStack stack;
    private TextureRegion texture;

    private float x, y;
    private float width, height;

    //Logic
    private InventoryProperties invProps;
    private Rectangle bounds, extendedBounds;
    private boolean creative;

    //Tooltip
    private boolean hovered;
    private TooltipElementItemStack tooltipRoot;

    public Slot(Engine e, ItemStack stack, float x, float y, float width, float height) {
        this.e = e;
        this.stack = stack;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;

        bounds = new Rectangle(x, y, width, height);
        extendedBounds = new Rectangle(x - EXTENSION_AMOUNT, y - EXTENSION_AMOUNT, width + EXTENSION_AMOUNT * 2, height + EXTENSION_AMOUNT * 2);

        //Load slot texture if not loaded already loaded
        if (slotTexture == null) {
            slotTexture = AssetUtils.loadTextureRegion(e.getAssetManager(), "button");
        }

        this.texture = slotTexture;

        tooltipRoot = new TooltipElementItemStack(stack, e);
    }

    public Slot(Engine e, ItemStack stack, float x, float y) {
        this(e, stack, x, y, 32, 32);
    }

    @Override
    public void render(float offsetX, float offsetY, SpriteBatch batch) {
        bounds.setPosition(x + offsetX, y + offsetY);
        extendedBounds.setPosition(x + offsetX - EXTENSION_AMOUNT, y + offsetY - EXTENSION_AMOUNT);

        batch.draw(texture, offsetX + x, offsetY + y, width, height);


        if (stack != null) {

            //Draw amount
            if(stack.getAmount() > 1) {
                String text = String.valueOf(stack.getAmount());
                BitmapFont font = Fonts.instance().getTinyFont();
                font.draw(batch, text, offsetX + x + 32 - StringUtils.getTextWidth(text, font) - 2, offsetY + y + 29);
            }

            //Draw icon
            ItemRenderer.renderItem(batch, offsetX + x, offsetY + y, width / 32, height / 32, e, Graphics.instance().getCamera().getHudCamera(), stack, true);
        }

    }

    public void renderTooltip(SpriteBatch batch) {
        if (hovered && stack != null) {
            TooltipRenderer.renderTooltip(batch, tooltipRoot, e);
        }
    }

    public boolean isCreative() {
        return creative;
    }

    public void setCreative(boolean creative) {
        this.creative = creative;
        updateTooltips();
    }

    /**
     * Updates the tooltip of the stack tooltip
     */
    public void updateTooltips() {

        if (stack != null) {
            tooltipRoot.clearChildren();
            ItemDef def = e.getItemRegistry().getItemDef(stack.getID());

            if (def.hasComponent(TooltipComponent.class)) {
                TooltipComponent tc = def.getComponent(TooltipComponent.class);
                for (TooltipElement tooltipElement : tc.getChildren(stack.getProperties(), e)) {
                    tooltipRoot.addChild(tooltipElement);
                }
            }

            if (creative) {
                tooltipRoot.addChild(new TooltipElementLabel("ID: " + def.getID(), Color.WHITE, Fonts.instance().getSmallFont()));
                tooltipRoot.addChild(new TooltipElementLabel("DefID: " + def.getDefID(), Color.WHITE, Fonts.instance().getSmallFont()));
                tooltipRoot.addChild(new TooltipElementLabel("Properties: " + stack.getProperties().toString(), Color.WHITE, Fonts.instance().getSmallFont()));

            }
        }
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        Vector2 m = Graphics.instance().getCamera().getMouseCoordinates();
        if (bounds.contains(m.x, m.y) && invProps != null) {
            ItemStack floating = invProps.getFloating();
            if (floating == null) {
                if (button == Input.Buttons.LEFT) {
                    if (creative) {
                        invProps.setFloating(ItemStack.createInstance(stack, e.getItemRegistry()));
                    } else {
                        invProps.setFloating(stack);
                        stack = null;
                    }
                } else if (button == Input.Buttons.RIGHT) {

                    if (stack != null) {
                        int amount = (int) Math.ceil(stack.getAmount() / 2d);
                        ItemStack newStack = ItemStack.createInstance(stack, e.getItemRegistry());
                        newStack.getProperties().setAmount(amount);

                        invProps.setFloating(newStack);

                        if (!creative)
                            stack.getProperties().addAmount(-amount);

                        if (stack.getAmount() <= 0)
                            stack = null;
                    }


                }
            } else {

                if (creative) {
                    invProps.setFloating(null);
                } else {

                    //Create stack if right mouse button was used
                    if (stack == null && button == Input.Buttons.RIGHT) {
                        stack = ItemStack.createInstance(floating, e.getItemRegistry());
                        stack.getProperties().setAmount(0);
                    }


                    if (stack != null && InventoryUtils.compareItemStack(floating, stack)) {
                        stack.getProperties().addAmount(button == Input.Buttons.RIGHT ? 1 : floating.getAmount());
                        invProps.setFloating(null);
                    } else {
                        ItemStack help = stack;
                        stack = floating;
                        invProps.setFloating(help);
                    }

                }
            }
        }

        tooltipRoot.setStack(stack);
        updateTooltips();
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {

        Vector2 m = Graphics.instance().getCamera().getMouseCoordinates();

        if (bounds.contains(m.x, m.y) && invProps != null && invProps.getFloating() == null) {
            hovered = true;
        } else if (hovered && !extendedBounds.contains(m.x, m.y)) {
            hovered = false;
        }

        return false;
    }

    @Override
    public void update() {

    }

    @Override
    public int getWidth() {
        return texture.getRegionWidth();
    }

    @Override
    public int getHeight() {
        return texture.getRegionHeight();
    }

    public ItemStack getStack() {
        return stack;
    }

    public void setStack(ItemStack stack) {
        this.stack = stack;
        tooltipRoot.setStack(stack);
        updateTooltips();
    }

    public void setInvProps(InventoryProperties invProps) {
        this.invProps = invProps;
    }
}


