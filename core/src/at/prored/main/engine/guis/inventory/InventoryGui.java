package at.prored.main.engine.guis.inventory;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.Component;
import at.prored.main.engine.components.InventoryIconComponent;
import at.prored.main.engine.entities.EntityDef;
import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.guis.AbstractGUI;
import at.prored.main.engine.guis.GUIManager;
import at.prored.main.engine.guis.Hotbar;
import at.prored.main.engine.guis.elements.GuiImage;
import at.prored.main.engine.guis.elements.IGuiElement;
import at.prored.main.engine.guis.elements.panes.RelativePane;
import at.prored.main.engine.guis.elements.panes.ScrollPane;
import at.prored.main.engine.guis.elements.panes.TabbedPane;
import at.prored.main.engine.guis.elements.tabs.Tab;
import at.prored.main.engine.guis.elements.tabs.TabText;
import at.prored.main.engine.guis.inventory.componentRenderers.ComponentUIRenderer;
import at.prored.main.engine.inventory.InventoryProperties;
import at.prored.main.engine.items.ItemRenderer;
import at.prored.main.engine.tiles.TileDef;
import at.prored.main.engine.tiles.storage.GameMap;
import at.prored.main.engine.utils.AssetUtils;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.MapUtils;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class InventoryGui extends AbstractGUI {

    //Constants
    private static final int BACKGROUND_PADDING_TOP_BOTTOM = 45;
    private static final int BACKGROUND_PADDING_LEFT = -1;
    private static final float BACKGROUND_SLIDING_SPEED = .25f;
    private static final Color seperatorColor = new Color(.315f, .315f, .315f, 1f);

    private Engine e;
    private GameMap gameMap;

    private Color opacityColor;

    private RelativePane pane;
    private TabbedPane tabbedPane;

    //Background
    private TextureRegion crafting_ui_background, pixel;
    private GuiImage backgroundImage;
    private int backgroundwidth, backgroundheight;
    private int backgroundx, backgroundy;
    private int backgroundState;
    private float offset;

    //Inventory
    private InventoryProperties invProps;

    //Component Rendering
    private Array<String> rendererKeys;

    public InventoryGui(Engine e) {
        this.e = e;
    }

    @Override
    public void initializeGUI(AssetManager assetManager) {
        this.opacityColor = new Color(Color.WHITE);
        calculateDimensions();

        this.pane = new RelativePane(backgroundx, backgroundy);

        //Add background
        this.crafting_ui_background = AssetUtils.loadTextureRegion(assetManager, "crafting_ui_background");
        this.backgroundImage = new GuiImage(crafting_ui_background, 0, 0, backgroundwidth, backgroundheight);
        this.backgroundImage.convertToNinePatch(3, 3, 3, 3);
        pane.add(backgroundImage);

        //Init TabbedPane
        this.tabbedPane = new TabbedPane(0, 0, backgroundwidth, backgroundheight);
        pane.add(tabbedPane);

        //init pixel
        pixel = AssetUtils.loadTextureRegion(assetManager, "pixel");

        //Init inventory properties
        invProps = new InventoryProperties();

        //Init rendererKeys list
        rendererKeys = new Array<>();
    }

    /**
     * Calculates the dimensions of the menu
     */
    private void calculateDimensions() {
        this.backgroundwidth = (int) (Graphics.V_WIDTH / 4f);
        this.backgroundheight = Graphics.V_HEIGHT - BACKGROUND_PADDING_TOP_BOTTOM * 2;
        this.backgroundx = BACKGROUND_PADDING_LEFT - this.backgroundwidth - Graphics.V_WIDTH / 2;
        this.backgroundy = BACKGROUND_PADDING_TOP_BOTTOM - Graphics.V_HEIGHT / 2;
    }

    /**
     * Sets the game map of the inventory
     *
     * @param gameMap gameMap
     */
    public void setGameMap(GameMap gameMap) {
        this.gameMap = gameMap;

        //Add player inventory
        setupPlayerTab();
    }

    /**
     * Setup player tab
     */
    private void setupPlayerTab() {

        TabText playerTab = new TabText("Player", e);

        EntityDef pdef = e.getEntityRegistry().getEntityDef(gameMap.getPlayer().getID());
        Array<ComponentUIRenderer> arr = searchDefForComponentRenderers(pdef, gameMap.getPlayer().getEntityProperties());

        ScrollPane sp = new ScrollPane(0, 0, backgroundwidth, backgroundheight, 15);
        for (ComponentUIRenderer componentUIRenderer : arr) {
            componentUIRenderer.onAdded();
            sp.add(componentUIRenderer);

            GuiImage image = new GuiImage(pixel, 0, 0, backgroundwidth-2, 5);
            image.setTint(seperatorColor);
            sp.add(image);
        }

        playerTab.setContent(sp);
        tabbedPane.addTab("player", playerTab);
        tabbedPane.switchTab(playerTab);
    }

    /**
     * Adds a tile to the inventory
     *
     * @param x     x coord
     * @param y     y coord
     * @param layer layer
     */
    public void addTile(int x, int y, int layer) {

        int id = MapUtils.getTileType(gameMap, x, y, layer);
        TileDef def = e.getTileRegistry().getTileDef(id);

        //Check for component renderers
        Array<ComponentUIRenderer> renderer = searchDefForComponentRenderers(def, MapUtils.getTileProperties(gameMap, x, y, layer));

        if (renderer.size == 0) {
            return;
        }

        //Find right icon
        TextureRegion icon;
        if (def.hasComponent(InventoryIconComponent.class)) {
            icon = def.getComponent(InventoryIconComponent.class).getIcon();
        } else {
            icon = AssetUtils.loadTextureRegion(e.getAssetManager(), "chestInventory");
        }

        //Create Tab
        TabText tabText = new TabText(def.getDefID(), e);

        ScrollPane sp = new ScrollPane(0, 0, backgroundwidth, backgroundheight, 15);
        for (ComponentUIRenderer componentUIRenderer : renderer) {
            componentUIRenderer.onAdded();
            sp.add(componentUIRenderer);

            GuiImage image = new GuiImage(pixel, 0, 0, backgroundwidth-2, 5);
            image.setTint(seperatorColor);
            sp.add(image);
        }

        tabText.setContent(sp);

        String key = def.getDefID() + x + y;
        tabbedPane.addTab(key,  tabText);
        rendererKeys.add(key);

    }

    /**
     * Clears all inventories except the players inventory
     */
    public void clear() {
        for (String rendererKey : rendererKeys) {
            tabbedPane.removeTab(rendererKey);
        }
        rendererKeys.clear();
    }

    /**
     * Searches components of definition for ones that can be displayed
     *
     * @param def definition
     * @param p   properties
     * @return array of found component renderers
     */
    private Array<ComponentUIRenderer> searchDefForComponentRenderers(Definition def, Properties p) {
        Array<ComponentUIRenderer> renderer = new Array<>();
        for (Component component : def.getComponents()) {
            ComponentUIRenderer componentUIRenderer = component.getComponentRenderer();
            if (componentUIRenderer != null) {
                componentUIRenderer.setup(backgroundwidth, e, p, invProps);
                renderer.add(componentUIRenderer);
            }
        }

        return renderer;
    }

    @Override
    public void update(GUIManager guiManager) {
        pane.update();
    }

    @Override
    public void updateAlways(GUIManager guiManager) {
        Hotbar hotbar = (Hotbar) guiManager.getGUI(GUIManager.GUI.INVENTORY_HOTBAR);

        here:
        if (Gdx.input.isKeyJustPressed(Input.Keys.TAB)) {
            if (backgroundState != 0)
                break here;

            if (isActive()) {
                backgroundState = 2;
            } else {
                guiManager.openGUI(GUIManager.GUI.INVENTORY);
                backgroundState = 1;

                //Notify renderes of addition
                for (Tab tab : tabbedPane.getTabs().values()) {
                    if(tab.getContent() instanceof ScrollPane){
                        for (IGuiElement ge : ((ScrollPane) tab.getContent()).getElements()) {
                            if(ge instanceof ComponentUIRenderer)
                                ((ComponentUIRenderer) ge).onAdded();
                        }
                    }
                }

                hotbar.toggle(guiManager);
            }
        }

        //Background
        pane.setX((int) (backgroundx + offset));

        //Update opacity
        float amount = backgroundwidth + BACKGROUND_PADDING_LEFT;
        opacityColor.a = (offset / amount);

        //Update sliding
        switch (backgroundState) {
            case 2:
                offset -= amount * Graphics.instance().getDeltaTime() / BACKGROUND_SLIDING_SPEED;
                if (offset <= 0) {
                    offset = 0;
                    backgroundState = 0;
                    guiManager.closeGUI(GUIManager.GUI.INVENTORY);

                    //notify renderers
                    for (Tab tab : tabbedPane.getTabs().values()) {
                        if(tab.getContent() instanceof ScrollPane){
                            for (IGuiElement ge : ((ScrollPane) tab.getContent()).getElements()) {
                                if(ge instanceof ComponentUIRenderer)
                                    ((ComponentUIRenderer) ge).onRemoved();
                            }
                        }
                    }

                    hotbar.toggle(guiManager);
                }
                break;
            case 1:
                if (!hotbar.isClosed())
                    break;

                offset += amount * Graphics.instance().getDeltaTime() / BACKGROUND_SLIDING_SPEED;
                if (offset >= amount) {
                    offset = amount;
                    backgroundState = 0;
                }
                break;
            default:
        }
    }

    @Override
    public void render(SpriteBatch batch) {
        batch.setColor(opacityColor);
        pane.render(batch);
        batch.setColor(Color.WHITE);

        if (invProps.getFloating() != null) {
            Vector2 m = Graphics.instance().getCamera().getMouseCoordinates();

            ItemRenderer.renderItem(batch, m.x, m.y, 1f, 1f, e, Graphics.instance().getCamera().getHudCamera(), invProps.getFloating(),true);
        }

        //Render global
        if (backgroundState == 0) {
            for (Tab tab : tabbedPane.getTabs().values()) {
                if(tab.getContent() instanceof ScrollPane){
                    for (IGuiElement ge : ((ScrollPane) tab.getContent()).getElements()) {
                        if(ge instanceof ComponentUIRenderer)
                            ((ComponentUIRenderer) ge).renderGlobal(0,0,batch);
                    }
                }
            }
        }

    }

    @Override
    public void registerInputProcessors(InputMultiplexer inputMultiplexer) {
        super.registerInputProcessors(inputMultiplexer);
        pane.registerInputProcessors(inputMultiplexer);
    }

    @Override
    public void deregisterInputProcessors(InputMultiplexer inputMultiplexer) {
        super.deregisterInputProcessors(inputMultiplexer);
        pane.deregisterInputProcessors(inputMultiplexer);
    }
}
