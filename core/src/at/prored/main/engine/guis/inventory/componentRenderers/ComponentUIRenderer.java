package at.prored.main.engine.guis.inventory.componentRenderers;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.Component;
import at.prored.main.engine.guis.elements.GuiElement;
import at.prored.main.engine.inventory.InventoryProperties;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract class ComponentUIRenderer extends GuiElement {

    private Component c;

    public ComponentUIRenderer(Component c) {
        this.c = c;
    }

    public Component getComponent() {
        return c;
    }

    public void onAdded(){}

    public void onRemoved(){}

    public abstract void setup(int width, Engine e, Properties p, InventoryProperties invProps);

    public void renderGlobal(int offsetX, int offsetY, SpriteBatch batch){}
}
