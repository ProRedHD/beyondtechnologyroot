package at.prored.main.engine.guis.tooltip.elements;

import at.prored.main.engine.Engine;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

/**
 * Created by ProRed on 14.02.2017.
 */
public abstract class TooltipElement {

    private EnumTooltipElement type;
    private Array<TooltipElement> children;
    private int layer;
    public TooltipElement(EnumTooltipElement type) {
        this.type = type;
    }

    /**
     * Updates the tooltip element
     */
    public void update() {
        //Update children
        if (hasChildren()) {
            for (TooltipElement child : children) {
                child.update();
            }
        }
    }

    /**
     * Draws the tooltip element to the screen
     *
     * @param batch spritebatch used for rendering
     */
    public abstract void render(SpriteBatch batch, float x, float y, Engine e);

    /**
     * Returns the width of the tooltip element
     *
     * @return height of tooltip element
     */
    public abstract float getWidth();

    /**
     * Returns the height of the tooltip element
     *
     * @return height of tooltip element
     */
    public abstract float getHeight();

    /**
     * Adds a child element to the element
     *
     * @param element child element
     */
    public void addChild(TooltipElement element) {
        if (children == null)
            children = new Array<>();

        //Set child layer
        element.setLayer(this.getLayer() + 1);

        children.add(element);
    }

    /**
     * Removes a child from the child array
     *
     * @param child child that should be removed
     */
    public void removeChild(TooltipElement child) {
        if (children == null)
            return;

        children.removeValue(child, true);
    }

    /**
     * Clears all children
     */
    public void clearChildren() {
        if (children != null)
            children.clear();
    }

    /**
     * Returns if the element has children
     *
     * @return whether or not the element has children
     */
    public boolean hasChildren() {
        return children != null && children.size > 0;
    }

    /**
     * Returns how many children the element has
     *
     * @return number of children of the element
     */
    public int getChildCount() {
        if (!hasChildren())
            return 0;

        return children.size;
    }

    public EnumTooltipElement getType() {
        return type;
    }

    public Array<TooltipElement> getChildren() {
        return children;
    }

    public int getLayer() {
        return layer;
    }

    public void setLayer(int layer) {
        this.layer = layer;

        //Update child layers
        if (hasChildren()) {
            for (TooltipElement child : children) {
                child.setLayer(layer + 1);
            }
        }
    }

    public enum EnumTooltipElement {
        PROGRESS_BAR, ITEM_STACK, LABEL, RECIPE
    }
}
