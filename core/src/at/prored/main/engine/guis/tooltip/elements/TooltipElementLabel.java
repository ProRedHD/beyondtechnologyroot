package at.prored.main.engine.guis.tooltip.elements;

import at.prored.main.engine.Engine;
import at.prored.main.engine.graphics.font.Fonts;
import at.prored.main.engine.utils.StringUtils;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.function.Supplier;

/**
 * Created by ProRed on 14.02.2017.
 */
public class TooltipElementLabel extends TooltipElement {

    private Supplier<String> text;
    private Color tint;
    private BitmapFont font;

    public TooltipElementLabel(String text, Engine e) {
        this(text, Color.WHITE, e);
    }

    public TooltipElementLabel(String text, Color tint, Engine e) {
        this(text, tint, Fonts.instance().getNormalFont());
    }

    public TooltipElementLabel(String text, Color tint, BitmapFont font) {
        super(EnumTooltipElement.LABEL);
        this.text = () -> text;
        this.tint = tint;
        this.font = font;
    }

    public TooltipElementLabel(Supplier<String> text, Color tint, BitmapFont font) {
        super(EnumTooltipElement.LABEL);
        this.text = text;
        this.tint = tint;
        this.font = font;
    }

    @Override
    public void render(SpriteBatch batch, float x, float y, Engine e) {
        font.setColor(tint);
        font.draw(batch, text.get(), x, y);
        font.setColor(Color.WHITE);
    }

    @Override
    public float getWidth() {
        return StringUtils.getTextWidth(text.get(), font);
    }

    @Override
    public float getHeight() {
        return StringUtils.getTextHeight(text.get(), font);
    }

    public String getText() {
        return text.get();
    }

    public void setText(String text) {
        this.text = () -> text;
    }

    public Color getTint() {
        return tint;
    }

    public void setTint(Color tint) {
        this.tint = tint;
    }
}
