package at.prored.main.engine.guis.tooltip.elements;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.LocalizableComponent;
import at.prored.main.engine.graphics.font.Fonts;
import at.prored.main.engine.items.ItemDef;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.items.components.TileComponent;
import at.prored.main.engine.localization.GameTranslator;
import at.prored.main.engine.tiles.TileDef;
import at.prored.main.engine.utils.AssetUtils;
import at.prored.main.engine.utils.StringUtils;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.function.Supplier;

/**
 * Created by ProRed on 05.12.2018.
 */
public class TooltipElementRecipe extends TooltipElement {

    private Supplier<IRecipeDisplayable> recipe;

    private TextureRegion pixel;

    private NinePatch background;
    private TextureRegion curl;

    public TooltipElementRecipe(Supplier<IRecipeDisplayable> recipe, Engine e) {
        super(EnumTooltipElement.RECIPE);
        this.recipe = recipe;

        this.pixel = AssetUtils.loadTextureRegion(e.getAssetManager(), "pixel");
        this.background = AssetUtils.loadNinePatch(e.getAssetManager(), "recipe_tooltip",4,4,4,4);
        this.curl = AssetUtils.loadTextureRegion(e.getAssetManager(), "recipe_tooltip_curl");



    }

    @Override
    public void render(SpriteBatch batch, float x, float y, Engine e) {

        //Draw curl top
        batch.draw(curl, x, y-curl.getRegionHeight());

        //Draw curl bottom
        curl.flip(false, true);
        batch.draw(curl, x, y-getHeight());
        curl.flip(false, true);

        x += curl.getRegionWidth();

        //Draw background
        background.draw(batch, x-1, y-getHeight(), getWidth(), getHeight());

        BitmapFont font = Fonts.instance().getRecipeFont();

        //Draw Result
        y -= StringUtils.getTextHeight("Result: ", font) + 5;
        font.draw(batch, "Result:", x + 5, y);


        ItemStack stack = recipe.get().getResult();
        String s = stack.getAmount() + "x " + getName(stack, e);
        s = StringUtils.truncateString(s, getWidth() - 20, font);
        y -= StringUtils.getTextHeight(s, font) + 5;
        font.draw(batch, s, x + 15, y);

        //Spacing
        y -= StringUtils.getTextHeight("Result: ", font) + 5;

        //Draw Ingredients
        y -= StringUtils.getTextHeight("Ingredients: ", font);
        font.draw(batch, "Ingredients:", x + 5, y);

        for (ItemStack itemStack : recipe.get().getIngredients()) {
            s = itemStack.getAmount() + "x " + getName(itemStack, e);
            s = StringUtils.truncateString(s, getWidth() - 25, font);
            y -= StringUtils.getTextHeight(s, font) + 5;
            font.draw(batch, s, x + 15, y);
        }

        //Spacing
        y -= StringUtils.getTextHeight("Result: ", font) + 5;

        y -= StringUtils.getTextHeight("Time: ", font) + 5;
        font.draw(batch, "Time:", x + 5, y);

        float craftingTime = recipe.get().getCraftingTime();
        s = craftingTime + "s";
        y -= StringUtils.getTextHeight(s, font) + 5;
        font.draw(batch, s, x + 15, y);


    }

    private String getName(ItemStack stack, Engine e){

        //Get localized name of itemstack
        ItemDef def = e.getItemRegistry().getItemDef(stack.getID());

        String name = "";
        if (def.hasComponent(TileComponent.class)) {
            TileDef tileDef = e.getTileRegistry().getTileDef(TileComponent.getTileID(stack.getProperties()));

            if (tileDef.hasComponent(LocalizableComponent.class))
                name = GameTranslator.instance().get(tileDef.getComponent(LocalizableComponent.class).getName());
            else
                name = "tile_" + tileDef.getDefID();
        } else if (def.hasComponent(LocalizableComponent.class)) {
            name = GameTranslator.instance().get(def.getComponent(LocalizableComponent.class).getName());
        }

        return name;
    }

    @Override
    public float getWidth() {
        return curl.getRegionWidth()+ 100;
    }

    @Override
    public float getHeight() {
        return 20 + (StringUtils.getTextHeight("T", Fonts.instance().getRecipeFont())+5)*(7+recipe.get().getIngredients().size);
    }
}
