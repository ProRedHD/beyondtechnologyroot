package at.prored.main.engine.guis.tooltip.elements;

import at.prored.main.engine.Engine;
import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.graphics.font.Fonts;
import at.prored.main.engine.utils.AssetUtils;
import at.prored.main.engine.utils.StringUtils;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by ProRed on 15.02.2017.
 */
public class TooltipElementProgressBar extends TooltipElement {

    private static final int FONT_PROGRESSBAR_DISTANCE = 5;

    private static final byte SOLID = 0;
    private static final byte CHANGEING = 1;

    //Dimenstions
    private float width;
    private float height;

    //Color
    private Color currentColor;

    private byte colorType;
    private Color solidColor;

    private Color startColor;
    private Color endColor;

    //Progress
    private float progress;

    //Label
    private String text;
    private BitmapFont font;

    //Misc
    private TextureRegion blank;

    public TooltipElementProgressBar(float width, float height, Color startColor, Color endColor, Engine e) {
        this(width, height, e);
        this.colorType = CHANGEING;
        this.startColor = startColor;
        this.endColor = endColor;

        this.currentColor = startColor.cpy();
    }

    public TooltipElementProgressBar(float width, float height, Color solidColor, Engine e) {
        this(width, height, e);
        this.colorType = SOLID;
        this.solidColor = solidColor;

        this.currentColor = solidColor.cpy();
    }

    public TooltipElementProgressBar(Color startColor, Color endColor, Engine e) {
        this(100, 15, startColor, endColor, e);
    }

    public TooltipElementProgressBar(Color solidColor, Engine e) {
        this(100, 15, solidColor, e);
    }

    public TooltipElementProgressBar(float width, float height, Engine e) {
        super(EnumTooltipElement.PROGRESS_BAR);
        this.width = width;
        this.height = height;
        this.colorType = SOLID;
        this.solidColor = Color.BLACK;

        this.blank = AssetUtils.loadTextureRegion(e.getAssetManager(), "pixel");
    }

    /**
     * Sets the label text and a default font
     *
     * @param text label text
     */
    public void setLabel(String text) {
        this.setLabel(text, Fonts.instance().getNormalFont());
    }

    /**
     * Sets the label text and a label font
     *
     * @param text label text
     * @param font label font
     */
    public void setLabel(String text, BitmapFont font) {
        this.text = text;
        this.font = font;
    }

    /**
     * Adds the given delta progress to the progress value
     *
     * @param deltaProgress delta progress
     */
    public void progress(float deltaProgress) {
        float correctedDelta = Math.max(0, Math.min(1, deltaProgress));

        this.progress += correctedDelta;

        //Fade color
        if (colorType == CHANGEING) {
            if (deltaProgress > 0)
                currentColor.lerp(endColor, correctedDelta);
            else
                currentColor.lerp(startColor, correctedDelta * -1);
        }
    }

    /**
     * Returns the progress
     * @return progress
     */
    public float getProgress() {
        return progress;
    }

    /**
     * Sets the progress to the given new progress value
     * @param progress new progress value
     */
    public void setProgress(float progress) {
        this.progress = 0;

        //Reset color
        if(colorType == CHANGEING){
            this.currentColor = startColor.cpy();
        }

        //Do progress
        progress(progress);
    }

    @Override
    public void update() {
        super.update();

        progress(0.1f * Graphics.instance().getDeltaTime());

        if(getProgress() >= 1f){
            setProgress(0f);
        }
    }

    @Override
    public void render(SpriteBatch batch, float x, float y, Engine e) {

        //Substract height to correct rendering
        y -= getHeight();

        //ProgressBar position
        float pX = text == null ? x : x + StringUtils.getTextWidth(text, font) + FONT_PROGRESSBAR_DISTANCE;

        //Render progressbar borders
        batch.setColor(0.45f, 0.45f, 0.45f, 1f);
        batch.draw(blank, pX, y, width, 1);
        batch.draw(blank, pX, y + height, width, 1);
        batch.draw(blank, pX, y, 1, height);
        batch.draw(blank, pX + width, y, 1, height);
        batch.setColor(Color.WHITE);

        //Render contents
        batch.setColor(currentColor);
        batch.draw(blank, pX + 1, y + 1, (width - 1) * progress, height - 1);
        batch.setColor(Color.WHITE);

        //Render text (if present)
        if (text != null) {
            font.draw(batch, text, x, y + getHeight());
        }
    }

    @Override
    public float getWidth() {
        return text == null ? width : width + FONT_PROGRESSBAR_DISTANCE + StringUtils.getTextWidth(text, font);
    }

    @Override
    public float getHeight() {
        return text == null ? height : Math.max(height, StringUtils.getTextHeight(text, font));
    }
}
