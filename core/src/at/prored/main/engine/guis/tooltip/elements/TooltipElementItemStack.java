package at.prored.main.engine.guis.tooltip.elements;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.LocalizableComponent;
import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.graphics.font.Fonts;
import at.prored.main.engine.items.ItemDef;
import at.prored.main.engine.items.ItemRenderer;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.items.components.TileComponent;
import at.prored.main.engine.localization.GameTranslator;
import at.prored.main.engine.tiles.TileDef;
import at.prored.main.engine.utils.StringUtils;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.function.Supplier;

/**
 * Created by ProRed on 15.02.2017.
 */
public class TooltipElementItemStack extends TooltipElement {

    private static final int ITEM_PADDING = 0;
    private static final int ITEM_FONT_DISTANCE = 4;

    private static final float ITEM_WIDTH = 32;
    private static final float ITEM_HEIGHT = 32;

    private Engine e;

    private Supplier<ItemStack> stackSupplier;
    private BitmapFont font;

    public TooltipElementItemStack(Supplier<ItemStack> stack, Engine e) {
        super(EnumTooltipElement.ITEM_STACK);
        this.e = e;
        this.stackSupplier = stack;
        this.font = Fonts.instance().getNormalFont();
    }

    public TooltipElementItemStack(ItemStack stack, Engine e){
        this(() -> stack, e);
    }

    /**
     * Sets the item stack of the element
     *
     * @param stack new item stack
     */
    public void setStack(ItemStack stack) {
        this.stackSupplier = () -> stack;
    }

    @Override
    public void render(SpriteBatch batch, float x, float y, Engine e) {

        //Skip if nothing to render
        if (stackSupplier == null)
            return;

        //Get localized name of itemstack
        ItemDef def = e.getItemRegistry().getItemDef(stackSupplier.get().getID());

        String name = "";
        if (def.hasComponent(TileComponent.class)) {
            TileDef tileDef = e.getTileRegistry().getTileDef(TileComponent.getTileID(stackSupplier.get().getProperties()));

            if (tileDef.hasComponent(LocalizableComponent.class))
                name = GameTranslator.instance().get(tileDef.getComponent(LocalizableComponent.class).getName());
            else
                name = "tile_" + tileDef.getDefID();
        } else if (def.hasComponent(LocalizableComponent.class)) {
            name = GameTranslator.instance().get(def.getComponent(LocalizableComponent.class).getName());
        }


        //Reduce y by height to correct rendering position
        y -= ITEM_HEIGHT + ITEM_PADDING;

        float height = StringUtils.getTextHeight(stackSupplier.get().getAmount() + "x " + name, font);

        //Render item image
        x += ITEM_PADDING;
        ItemRenderer.renderItem(batch, x, y + Math.max(0, (height - ITEM_HEIGHT) / 2f), ITEM_WIDTH / 32, ITEM_WIDTH / 32, e, Graphics.instance().getCamera().getHudCamera(), stackSupplier.get(), false);
        x += ITEM_WIDTH + ITEM_FONT_DISTANCE;

        //Add height for rendering fonts
        y += ITEM_HEIGHT + ITEM_PADDING;

        font.draw(batch, stackSupplier.get().getAmount() + "x " + name, x, y - Math.max(0, (ITEM_HEIGHT - height) / 2f));
    }

    @Override
    public float getWidth() {

        if (stackSupplier == null)
            return 0;

        //Get localized name of item
        ItemDef def = e.getItemRegistry().getItemDef(stackSupplier.get().getID());

        String name = "";
        if (def.hasComponent(TileComponent.class)) {
            TileDef tileDef = e.getTileRegistry().getTileDef(TileComponent.getTileID(stackSupplier.get().getProperties()));

            if (tileDef.hasComponent(LocalizableComponent.class))
                name = GameTranslator.instance().get(tileDef.getComponent(LocalizableComponent.class).getName());
            else
                name = "tile_" + tileDef.getDefID();
        } else if (def.hasComponent(LocalizableComponent.class)) {
            name = GameTranslator.instance().get(def.getComponent(LocalizableComponent.class).getName());
        }

        return ITEM_PADDING + ITEM_WIDTH + ITEM_FONT_DISTANCE + StringUtils.getTextWidth(stackSupplier.get().getAmount() + "x " + name, font);
    }

    @Override
    public float getHeight() {

        if (stackSupplier == null)
            return 0;

        //Get localized name of item
        ItemDef def = e.getItemRegistry().getItemDef(stackSupplier.get().getID());

        String name = "";
        if (def.hasComponent(TileComponent.class)) {
            TileDef tileDef = e.getTileRegistry().getTileDef(TileComponent.getTileID(stackSupplier.get().getProperties()));

            if (tileDef.hasComponent(LocalizableComponent.class))
                name = GameTranslator.instance().get(tileDef.getComponent(LocalizableComponent.class).getName());
            else
                name = "tile_" + tileDef.getDefID();
        } else if (def.hasComponent(LocalizableComponent.class)) {
            name = GameTranslator.instance().get(def.getComponent(LocalizableComponent.class).getName());
        }

        return Math.max(2 * ITEM_PADDING + ITEM_HEIGHT, StringUtils.getTextHeight(stackSupplier.get().getAmount() + "x " + name, font));
    }
}
