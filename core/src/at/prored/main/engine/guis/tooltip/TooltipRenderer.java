package at.prored.main.engine.guis.tooltip;

import at.prored.main.engine.Engine;
import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.guis.tooltip.elements.TooltipElement;
import at.prored.main.engine.utils.AssetUtils;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by ProRed on 14.02.2017.
 */
public class TooltipRenderer {

    private static final float PADDING = 8;
    private static final float LAYER_XOFFSET = 15;
    private static final float LAYER_YOFFSET = 10;

    private static NinePatch tooltip;

    public static void renderTooltip(SpriteBatch batch, TooltipElement root, Engine e) {
        if (tooltip == null)
            tooltip = AssetUtils.loadNinePatch(e.getAssetManager(), "tooltip", 7, 7, 7, 7);

        //Calculate Dimensions
        float width = getMaxWidth(root);
        float height = getHeight(root);

        //Add Padding to dimensions
        width += 2 * PADDING;
        height += 2 * PADDING;

        //Get Mouse position
        Vector2 pos = Graphics.instance().getCamera().getMouseCoordinates();
        float mX = pos.x;
        float mY = pos.y;

        //Don't go out of screen (y)
        if(mY + 5 + height > Graphics.V_HEIGHT/2){
            mY -= (mY + + 5 + height) - Graphics.V_HEIGHT/2d;
        }

        //Don't go out of screen (x)
        if(mX + 5 + width > Graphics.V_WIDTH/2){
            mX -= (mX + 5 + width) - Graphics.V_WIDTH/2d;
        }

        //Draw background
        tooltip.draw(batch, mX, mY, width, height);

        //Draw elements
        renderTooltipElement(batch, root, height - PADDING, mX + PADDING, mY, e, true);
    }

    public static void renderTooltip(SpriteBatch batch, float x, float y, TooltipElement root, Engine e) {
        if (tooltip == null)
            tooltip = AssetUtils.loadNinePatch(e.getAssetManager(), "tooltip", 7, 7, 7, 7);

        //Calculate Dimensions
        float width = getMaxWidth(root);
        float height = getHeight(root);

        //Add Padding to dimensions
        width += 2 * PADDING;
        height += 2 * PADDING;

        //Draw background
        tooltip.draw(batch, x, y, width, height);

        //Draw elements
        renderTooltipElement(batch, root, height - PADDING, x + PADDING, y, e, true);
    }


    /**
     * Renders a tooltip element and all of its children
     *
     * @param batch sprite batch for rendering
     * @param root  root element
     * @param y     y position of the next element
     * @param e Engine
     */
    private static void renderTooltipElement(SpriteBatch batch, TooltipElement root, float y, float mX, float mY, Engine e, boolean isroot) {
        //Reduce y value
        if (!isroot)
            y -= LAYER_YOFFSET;

        //Render root
        root.render(batch, mX + root.getLayer() * LAYER_XOFFSET, mY + y, e);

        //Reduce y by root height
        y -= root.getHeight();

        //Render children
        if (root.hasChildren()) {
            for (TooltipElement child : root.getChildren()) {
                renderTooltipElement(batch, child, y, mX, mY, e, false);
                y -= getHeight(child) + LAYER_YOFFSET;
            }
        }
    }

    /**
     * Calculates the maximum width of an root element
     *
     * @param root root element
     * @return maximum width
     */
    public static float getMaxWidth(TooltipElement root) {
        float maxWidth = root.getWidth() + root.getLayer() * LAYER_XOFFSET;

        if (root.hasChildren()) {
            for (TooltipElement child : root.getChildren()) {
                maxWidth = Math.max(maxWidth, getMaxWidth(child));
            }
        }

        return maxWidth;
    }

    public static float getHeight(TooltipElement root) {
        float height = root.getHeight();

        if (root.hasChildren()) {
            for (TooltipElement child : root.getChildren()) {
                height += LAYER_YOFFSET;
                height += getHeight(child);
            }
        }

        return height;
    }
}
