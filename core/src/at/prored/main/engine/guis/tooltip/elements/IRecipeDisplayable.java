package at.prored.main.engine.guis.tooltip.elements;

import at.prored.main.engine.items.ItemStack;
import com.badlogic.gdx.utils.Array;

/**
 * Created by ProRed on 05.12.2018.
 *
 * Used by TooltipElementRecipe to display both templates and actual recipes
 */
public interface IRecipeDisplayable {

    ItemStack getResult();

    Array<ItemStack> getIngredients();

    float getCraftingTime();

}
