package at.prored.main.engine.guis.elements.panes;

import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.guis.elements.GuiElement;
import at.prored.main.engine.guis.elements.IGuiElement;
import at.prored.main.engine.guis.elements.InputGuiElement;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.utils.ScissorStack;
import com.badlogic.gdx.utils.Array;

public class ScrollPane extends InputGuiElement {

    private static final int PADDING_TOP = 15;

    private Array<IGuiElement> elements;
    private int spacing;

    private int x, y;
    private int width, height;
    private int targetScrollAmount, scrollAmount, maxScrollHeight;

    //Scrolling stuff
    private Rectangle scissors, bounds;

    public ScrollPane(int x, int y, int width, int height, int spacing) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.spacing = spacing;
        this.elements = new Array<IGuiElement>();

        this.bounds = new Rectangle(x, y, width - 2, height - 4);
        this.scissors = new Rectangle();
    }

    public ScrollPane(int x, int y, int width, int height) {
        this(x, y, width, height, 10);
    }

    /**
     * Returns the spacing of the pane
     *
     * @return spacing in px
     */
    public int getSpacing() {
        return spacing;
    }

    /**
     * Removes all elements from the pane
     */
    public void clear(){
        for (IGuiElement element : elements) {
            removeSubElement((GuiElement) element);
        }

        elements.clear();
        maxScrollHeight = 0;
        scrollAmount = 0;
        targetScrollAmount = 0;
    }

    /**
     * Adds a element to the pane with the given offsets
     *
     * @param element element that should be added
     */
    public void add(IGuiElement element) {
        this.elements.add(element);

        if(element instanceof GuiElement)
            addSubElement((GuiElement) element);

        maxScrollHeight += element.getHeight() + spacing;
    }

    /**
     * Removes a element from the pane
     *
     * @param element element that should be removed
     */
    public void remove(IGuiElement element) {
        elements.removeValue(element, true);

        if(element instanceof GuiElement)
            removeSubElement((GuiElement) element);

        maxScrollHeight -= (element.getHeight() + spacing);
    }

    /**
     * Returns all elements
     * @return all elements
     */
    public Array<IGuiElement> getElements(){
        return elements;
    }

    @Override
    public void render(float offsetX, float offsetY, SpriteBatch batch) {

        bounds.setPosition(offsetX + 2, offsetY + 2);
        OrthographicCamera c = Graphics.instance().getCamera().getHudCamera();
        ScissorStack.calculateScissors(c, batch.getTransformMatrix(), bounds, scissors);

        batch.flush();
        ScissorStack.pushScissors(scissors);

        int ny = height - PADDING_TOP;
        for (IGuiElement element : elements) {
            float x = this.x + ((this.width - element.getWidth()) / 2) + offsetX;
            float y = ny - element.getHeight() + offsetY + scrollAmount;
            element.render(x, y, batch);

            ny -= (element.getHeight() + spacing);
        }

        batch.flush();

        ScissorStack.popScissors();
    }

    @Override
    public boolean scrolled(int amount) {
        super.scrolled(amount);

        Vector2 m = Graphics.instance().getCamera().getMouseCoordinates();

        if(bounds.contains(m.x, m.y)) {
            targetScrollAmount += amount * 50;

            //Check if the scroll amount is over the maximum height
            if(targetScrollAmount > maxScrollHeight - height + spacing){

                //set to 0 when not more than one page
                if(maxScrollHeight < height){
                    this.targetScrollAmount = 0;
                } else {
                    this.targetScrollAmount = maxScrollHeight - height + spacing;
                }

            } else if(targetScrollAmount < 0){
                this.targetScrollAmount = 0;
            }

            return true;
        }
        return false;
    }

    @Override
    public void update() {

        //Lerps scrollamount to target
        if(scrollAmount > targetScrollAmount){
            this.scrollAmount -= (int) (5000f * Graphics.instance().getDeltaTime());

            //Fixes overshooting issue
            if(scrollAmount < targetScrollAmount)
                this.scrollAmount = targetScrollAmount;

        } else if(scrollAmount < targetScrollAmount){
            this.scrollAmount += (int) (5000f * Graphics.instance().getDeltaTime());

            //Fixes overshooting issue
            if(scrollAmount > targetScrollAmount)
                this.scrollAmount = targetScrollAmount;

        }
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }
}
