package at.prored.main.engine.guis.elements.tabs;

import at.prored.main.engine.Engine;
import at.prored.main.engine.guis.elements.GuiElement;
import at.prored.main.engine.utils.AssetUtils;
import at.prored.main.engine.utils.Trigger;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public abstract class Tab {

    protected static NinePatch tabTexture;

    protected boolean active;

    private Trigger trigger;
    private Rectangle rectangle;

    //Content
    private GuiElement content;

    public Tab(Engine e) {
        this.rectangle = new Rectangle();

        if(tabTexture == null){
            tabTexture = AssetUtils.loadNinePatch(e.getAssetManager(), "crafting_ui_tab", 4, 4, 4, 4);
        }
    }

    public Trigger getTrigger() {
        return trigger;
    }

    public void setTrigger(Trigger trigger) {
        this.trigger = trigger;
    }

    public GuiElement getContent() {
        return content;
    }

    public void setContent(GuiElement content) {
        this.content = content;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public abstract void render(float x, float y, SpriteBatch batch);

    public void update() {

    }

    public abstract float getWidth();

    public abstract float getHeight();
}
