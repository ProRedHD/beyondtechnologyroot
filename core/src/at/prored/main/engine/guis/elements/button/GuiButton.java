package at.prored.main.engine.guis.elements.button;

import at.prored.main.engine.Engine;
import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.graphics.font.Fonts;
import at.prored.main.engine.guis.elements.InputGuiElement;
import at.prored.main.engine.utils.AssetUtils;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by ProRed on 08.05.2018.
 */
public class GuiButton extends InputGuiElement {

    private NinePatch currentTexture, texture, pressed_texture;
    private boolean pressed;

    private float x, y;
    private float width, height;
    private Rectangle rect;

    private float textX, textY;
    private String text;
    private IButtonClickListener clickListener;

    public GuiButton(NinePatch texture, NinePatch pressed_texture, String text, float x, float y, float width, float height) {
        this.texture = texture;
        this.pressed_texture = pressed_texture;
        this.text = text;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.rect = new Rectangle(x, y, width, height);

        this.currentTexture = texture;

        calculateDimensions();
    }

    public GuiButton(Engine e, String text, float x, float y, float width, float height) {
        this(new NinePatch(AssetUtils.loadTextureRegion(e.getAssetManager(), "button"),
                        4, 4, 4, 4),
                new NinePatch(AssetUtils.loadTextureRegion(e.getAssetManager(), "button_pressed"),
                        4, 4, 4, 4), text, x, y, width, height);
    }

    public void setClickListener(IButtonClickListener clickListener) {
        this.clickListener = clickListener;
    }

    private void calculateDimensions() {
        Fonts.getLayout().setText(Fonts.instance().getSmallFont(), text);

        float width = Fonts.getLayout().width;
        float height = Fonts.getLayout().height;

        this.textX = this.x + this.width / 2f - width / 2f;
        this.textY = this.y - this.height / 2f + height / 2f;
    }

    @Override
    public void render(float offsetX, float offsetY, SpriteBatch batch) {
        rect.setPosition(x + offsetX, y + offsetY);
        currentTexture.draw(batch, x + offsetX, y + offsetY, width, height);

        Fonts.instance().getSmallFont().draw(batch, text, textX + offsetX, textY + offsetY + height - 2);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        Vector2 m = Graphics.instance().getCamera().getMouseCoordinates();

        if (rect.contains(m.x, m.y)) {
            pressed = true;
            currentTexture = pressed_texture;
            if (clickListener != null)
                clickListener.trigger();
            return true;
        }

        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        if (pressed) {
            pressed = false;
            currentTexture = texture;
            return true;
        }

        return false;
    }

    @Override
    public void update() {

    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    @Override
    public int getWidth() {
        return (int) width;
    }

    @Override
    public int getHeight() {
        return (int) height;
    }
}
