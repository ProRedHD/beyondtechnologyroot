package at.prored.main.engine.guis.elements;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by ProRed on 01.12.2016.
 */
public class GuiLabel extends GuiElement {

    private static GlyphLayout layout = new GlyphLayout();

    private float x, y;
    private BitmapFont font;

    private String text;

    public GuiLabel(float x, float y, BitmapFont font, String text){ ;
        this.x = x;
        this.y = y;
        this.font = font;
        this.text = text;
    }

    public void setText(String text){
        this.text = text;
    }

    @Override
    public void render(float offsetX, float offsetY, SpriteBatch batch) {
        this.font.draw(batch, text, offsetX + x, offsetY + y);
    }

    @Override
    public void update() {

    }

    @Override
    public int getWidth() {
        layout.setText(font, text);
        return (int) layout.width;
    }

    @Override
    public int getHeight() {
        layout.setText(font, text);
        return (int) layout.height;
    }

}
