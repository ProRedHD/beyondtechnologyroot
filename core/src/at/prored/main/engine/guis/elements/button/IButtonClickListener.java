package at.prored.main.engine.guis.elements.button;

/**
 * Created by ProRed on 28.05.2016.
 */
public interface IButtonClickListener {

    void trigger();
}
