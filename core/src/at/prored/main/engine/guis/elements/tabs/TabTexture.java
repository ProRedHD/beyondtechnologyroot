package at.prored.main.engine.guis.elements.tabs;

import at.prored.main.engine.Engine;
import at.prored.main.engine.utils.AssetUtils;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by ProRed on 25.09.2018.
 */
public class TabTexture extends Tab {

    private TextureRegion texture;

    public TabTexture(TextureRegion textureRegion, Engine e) {
        super(e);
        this.texture = textureRegion;
    }

    public TabTexture(String path, Engine e) {
        super(e);
        this.texture = AssetUtils.loadTextureRegion(e.getAssetManager(), path);
    }

    @Override
    public void render(float x, float y, SpriteBatch batch) {
        if (active) {
            tabTexture.draw(batch, x, y, getWidth(), getHeight()+5);
            batch.draw(texture, x + 5, y + 8+5);
        } else {
            tabTexture.draw(batch, x, y, getWidth(), getHeight());
            batch.draw(texture, x + 5, y + 8);
        }
    }

    public float getWidth(){
        return texture.getRegionWidth() + 10;
    }

    public float getHeight(){
        return texture.getRegionWidth() + 15;
    }

}

