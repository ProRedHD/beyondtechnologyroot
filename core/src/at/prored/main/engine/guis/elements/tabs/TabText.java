package at.prored.main.engine.guis.elements.tabs;

import at.prored.main.engine.Engine;
import at.prored.main.engine.graphics.font.Fonts;
import at.prored.main.engine.utils.StringUtils;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by ProRed on 25.09.2018.
 */
public class TabText extends Tab {

    private String text;
    private BitmapFont font;

    public TabText(String text, Engine e) {
        super(e);
        this.text = text;
        this.font = Fonts.instance().getNormalFont();
    }

    @Override
    public void render(float x, float y, SpriteBatch batch) {

        if (active) {
            tabTexture.draw(batch, x, y - 2, getWidth(), getHeight()+7);
            font.draw(batch, text, x + 5, y + getHeight() - 8+5);
        } else {
            tabTexture.draw(batch, x, y, getWidth(), getHeight());
            font.draw(batch, text, x + 5, y + getHeight() - 8);
        }
    }

    public void setText(String text) {
        this.text = text;
    }

    public float getWidth(){
        return StringUtils.getTextWidth(text, font) + 10;
    }

    public float getHeight(){
        return StringUtils.getTextHeight(text, font) + 15;
    }

    public String getText() {
        return text;
    }
}

