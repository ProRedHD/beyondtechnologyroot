package at.prored.main.engine.guis.elements;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class GuiImage extends GuiElement {

    private boolean ninePatchMode = false;

    private TextureRegion region;
    private NinePatch patch;

    private float x, y;
    private float width, height;
    private Color tint;

    public GuiImage(TextureRegion region, float x, float y, float width, float height) {
        this.region = region;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    /**
     * Converts the image to a nine patch
     *
     * @param left   left padding
     * @param right  right padding
     * @param top    top padding
     * @param bottom btoom padding
     */
    public void convertToNinePatch(int left, int right, int top, int bottom) {
        ninePatchMode = true;

        this.patch = new NinePatch(region, left, right, top, bottom);
    }

    public Color getTint() {
        return tint;
    }

    public void setTint(Color tint) {
        this.tint = tint;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public int getWidth() {
        return (int) width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public int getHeight() {
        return (int) height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    @Override
    public void render(float offsetX, float offsetY, SpriteBatch batch) {

        Color c = null;
        if (tint != null) {
            c = batch.getColor();
            batch.setColor(tint);
        }

        if (ninePatchMode) {
            patch.draw(batch, offsetX + x, offsetY + y, width, height);
        } else {
            batch.draw(region, offsetX + x, offsetY + y, width, height);
        }

        if (tint != null)
            batch.setColor(c);
    }

    @Override
    public void update() {

    }
}
