package at.prored.main.engine.guis.elements;

import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

public abstract class GuiElement implements IGuiElement {

    protected InputMultiplexer multiplexer;
    protected Array<GuiElement> subElements;

    public GuiElement() {
        this.subElements = new Array<GuiElement>();
    }

    /**
     * Adds a subelement
     *
     * @param element element that should be added
     */
    public void addSubElement(GuiElement element) {
        this.subElements.add(element);

        if (multiplexer != null)
            element.registerInputProcessors(multiplexer);
    }

    /**
     * Removes a subelement
     *
     * @param element element that should be removed
     */
    public void removeSubElement(GuiElement element) {

        if(element == null)
            return;

        if (multiplexer != null)
            element.deregisterInputProcessors(multiplexer);

        subElements.removeValue(element, true);
    }

    /**
     * Renders the element without offset
     *
     * @param batch sprite batch for drawing
     */
    public void render(SpriteBatch batch) {
        render(0, 0, batch);
    }

    /**
     * Registers all input processors and itself
     * @param inputMultiplexer inputMultiplexer
     */
    public void registerInputProcessors(InputMultiplexer inputMultiplexer) {
        if(this instanceof InputProcessor)
            inputMultiplexer.addProcessor((InputProcessor) this);

        this.multiplexer = inputMultiplexer;
        this.onMultiplexerReceive(multiplexer);

        for (GuiElement subElement : subElements) {
            subElement.registerInputProcessors(inputMultiplexer);
        }
    }

    /**
     * Deregisters alll input processors and itself
     * @param inputMultiplexer inputMultiplexer
     */
    public void deregisterInputProcessors(InputMultiplexer inputMultiplexer) {
        if(this instanceof InputProcessor)
            inputMultiplexer.removeProcessor((InputProcessor) this);

        this.multiplexer = null;

        for (GuiElement subElement : subElements) {
            subElement.deregisterInputProcessors(inputMultiplexer);
        }
    }

    public void onMultiplexerReceive(InputMultiplexer multiplexer){}

}
