package at.prored.main.engine.guis.elements.panes;

import at.prored.main.engine.guis.elements.GuiElement;
import at.prored.main.engine.guis.elements.IGuiElement;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class RelativePane extends GuiElement {

    private int x, y;

    public RelativePane(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Adds a element to the pane with the given offsets
     * @param guiElement element that should be added
     */
    public void add(GuiElement guiElement){
        this.addSubElement(guiElement);
    }

    /**
     * Removes a element from the pane
     * @param element elmenet that should be removed
     */
    public void remove(GuiElement element){
        removeSubElement(element);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void render(float offsetX, float offsetY, SpriteBatch batch) {
        for (IGuiElement e : subElements) {
            e.render(offsetX + x, offsetY + y, batch);
        }
    }

    @Override
    public void update() {
        for (IGuiElement iGuiElement : subElements) {
            iGuiElement.update();
        }
    }

    @Override
    public int getWidth() {
        return 0;
    }

    @Override
    public int getHeight() {
        return 0;
    }
}
