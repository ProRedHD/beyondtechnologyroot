package at.prored.main.engine.guis.elements;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by ProRed on 27.05.2016.
 */
public interface IGuiElement {

    void render(float offsetX, float offsetY, SpriteBatch batch);

    void update();

    int getWidth();
    int getHeight();
}
