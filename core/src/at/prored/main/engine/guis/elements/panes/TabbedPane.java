package at.prored.main.engine.guis.elements.panes;

import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.guis.elements.GuiElement;
import at.prored.main.engine.guis.elements.InputGuiElement;
import at.prored.main.engine.guis.elements.tabs.Tab;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by ProRed on 20.09.2018.
 */
public class TabbedPane extends InputGuiElement {

    private float x, y;
    private float width, height;
    private float tabSpacing = 8;

    private ObjectMap<String, Tab> tabs;
    private ObjectMap<Tab, Rectangle> tabBounds;

    private Tab currentTab;
    private GuiElement currentContent;

    public TabbedPane(float x, float y, float width, float height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;

        this.tabs = new ObjectMap<>();
        this.tabBounds = new ObjectMap<>();
    }

    /**
     * Adds a tab to the tabbed pane
     *
     * @param id     the id of the tab
     * @param guiTab the tab object
     */
    public void addTab(String id, Tab guiTab) {

        if(tabs.containsKey(id))
            throw new IllegalArgumentException("Tab with ID already exists: " + id);

        tabs.put(id, guiTab);
    }

    /**
     * Removes a tab from the pane
     * @param key pane key
     */
    public void removeTab(String key){
        tabs.remove(key);
    }

    /**
     * Returns every tab in the pane
     *
     * @return map of all tabs
     */
    public ObjectMap<String, Tab> getTabs() {
        return tabs;
    }

    /**
     * Changes the current selected tab to the give one via id
     *
     * @param id new tab id
     */
    public void switchTab(String id) {
        switchTab(tabs.get(id));
    }

    /**
     * Changes the current selected tab to the give one
     *
     * @param tab new tab
     */
    public void switchTab(Tab tab) {
        if(currentContent != null && multiplexer != null){
            currentTab.setActive(false);
            currentContent.deregisterInputProcessors(multiplexer);
        }

        currentTab = tab;
        currentContent = tab.getContent();

        if(currentContent != null) {
            currentTab.setActive(true);

            if(multiplexer != null) {
                currentContent.registerInputProcessors(multiplexer);
            }
        }
    }

    @Override
    public void render(float offsetX, float offsetY, SpriteBatch batch) {

        //Render and regenerate tab bounds
        float offX = x + offsetX + 15;
        for (Tab tab : tabs.values()) {

            //Update position
            if (!tabBounds.containsKey(tab))
                tabBounds.put(tab, new Rectangle(offX, y + offsetY + height, tab.getWidth(), tab.getHeight()));
            else
                tabBounds.get(tab).setPosition(offX, y + offsetY + height);

            tab.render(offX, y + offsetY + height, batch);

            offX += tab.getWidth() + tabSpacing;
        }

        //Render content
        if (currentContent != null) {
            currentContent.render(x + offsetX, y + offsetY, batch);
        }

    }

    @Override
    public void onMultiplexerReceive(InputMultiplexer multiplexer) {
        super.onMultiplexerReceive(multiplexer);

        if(currentContent != null)
            currentContent.registerInputProcessors(multiplexer);
    }

    @Override
    public void update() {

        //Update content
        if (currentContent != null) {
            currentContent.update();
        }
    }



    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {


        Vector2 m = Graphics.instance().getCamera().getMouseCoordinates();

        for (Tab tab : tabBounds.keys()) {
            Rectangle rect = tabBounds.get(tab);

            if (rect.contains(m.x, m.y)) {
                switchTab(tab);
                return true;
            }
        }

        return false;
    }

    @Override
    public int getWidth() {
        return (int) width;
    }

    @Override
    public int getHeight() {
        return (int) height;
    }

}
