package at.prored.main.engine.localization;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.I18NBundle;

import java.util.Locale;
import java.util.MissingResourceException;

/**
 * Created by ProRed on 08.05.2018.
 */
public class GameTranslator {

    private static GameTranslator gameTranslator;

    private I18NBundle bundle;

    private GameTranslator(){
        init();
    }

    private void init(){
        FileHandle bundle = Gdx.files.internal("lang/btech_en_US");
        Locale loc = new Locale("en", "US");
        this.bundle = I18NBundle.createBundle(bundle, loc);
    }

    public String get(String key){
        try {
            return bundle.get(key);
        } catch (MissingResourceException e){
            return key;
        }
    }

    public static GameTranslator instance(){
        if(gameTranslator == null)
            gameTranslator = new GameTranslator();

        return gameTranslator;
    }
}
