package at.prored.main.engine.saving.data.base;

import at.prored.main.engine.Engine;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlWriter;

import java.io.IOException;

/**
 * Created by ProRed on 13.12.2016.
 */
public abstract class Data {

    private EnumDataType dataType;
    private String name;

    /**
     * Used to store data in files for blocks and entities
     * @param dataType dataType of the data piece
     * @param name name of the data piece
     */
    protected Data(EnumDataType dataType, String name){
        this.dataType = dataType;
        this.name = name;
    }

    /**
     * Saves all data to xml file
     * @param xmlWriter xml writer for writing
     * @param e
     */
    public abstract void saveData(XmlWriter xmlWriter, Engine e) throws IOException;

    /**
     * Loads data from xml element
     * @param element xml element
     * @param e
     */
    public abstract void loadData(XmlReader.Element element, Engine e);

    /**
     * Returns the name of this data piece
     */
    public String getDataName(){
        return name;
    }

    /**
     * Returns the data type of this data piece (int, float, ...)
     */
    public EnumDataType getDataType(){
        return dataType;
    }
}
