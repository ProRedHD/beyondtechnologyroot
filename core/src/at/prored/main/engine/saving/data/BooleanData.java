package at.prored.main.engine.saving.data;

import at.prored.main.engine.Engine;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.saving.data.base.EnumDataType;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlWriter;

import java.io.IOException;

/**
 * Created by ProRed on 13.12.2016.
 */
public class BooleanData extends Data {

    private boolean value;

    public BooleanData(String name) {
        super(EnumDataType.BOOLEAN, name);
    }

    public BooleanData(String name, boolean value) {
        super(EnumDataType.BOOLEAN, name);
        this.value = value;
    }

    public boolean getValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    @Override
    public void saveData(XmlWriter xmlWriter, Engine e) throws IOException {
        xmlWriter.attribute("value", value);
    }

    @Override
    public void loadData(XmlReader.Element element, Engine e) {
        this.value = Boolean.parseBoolean(element.getAttribute("value"));
    }
}
