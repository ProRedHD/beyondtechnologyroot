package at.prored.main.engine.saving.data.base;

import at.prored.main.engine.Engine;

/**
 * Created by ProRed on 13.12.2016.
 */
public enum EnumDataType {
    INT("IntData"), FLOAT("FloatData"), STRING("StringData"), BOOLEAN("BooleanData"), ITEMSTACK("ItemStackData");

    private String classname;

    EnumDataType(String classname) {
        this.classname = classname;
    }

    public String getClassname() {
        return classname;
    }
}
