package at.prored.main.engine.saving.data;

import at.prored.main.engine.Engine;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.saving.data.base.EnumDataType;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlWriter;

import java.io.IOException;

/**
 * Created by ProRed on 13.12.2016.
 */
public class StringData extends Data {

    private String value;

    public StringData(String name) {
        super(EnumDataType.STRING, name);
    }

    public StringData(String name, String value) {
        super(EnumDataType.STRING, name);
        this.value = value;
    }

    public String getValue(){
        return value;
    }

    public void setValue(String value){
        this.value = value;
    }

    @Override
    public void saveData(XmlWriter xmlWriter, Engine e) throws IOException {
        xmlWriter.attribute("value", value);
    }

    @Override
    public void loadData(XmlReader.Element element, Engine e) {
        this.value = element.getAttribute("value");
    }
}
