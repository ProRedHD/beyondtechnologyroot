package at.prored.main.engine.saving.data;

import at.prored.main.engine.Engine;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.saving.data.base.EnumDataType;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlWriter;

import java.io.IOException;

/**
 * Created by ProRed on 13.12.2016.
 */
public class IntData extends Data {

    private int value;

    public IntData(String name) {
        super(EnumDataType.INT, name);
    }

    public IntData(String name, int value) {
        super(EnumDataType.INT, name);
        this.value = value;
    }

    public int getValue(){
        return value;
    }

    public void setValue(int value){
        this.value = value;
    }

    @Override
    public void saveData(XmlWriter xmlWriter, Engine e) throws IOException {
        xmlWriter.attribute("value", value);
    }

    @Override
    public void loadData(XmlReader.Element element, Engine e) {
        this.value = element.getInt("value");
    }

}
