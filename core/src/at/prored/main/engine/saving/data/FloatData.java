package at.prored.main.engine.saving.data;

import at.prored.main.engine.Engine;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.saving.data.base.EnumDataType;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlWriter;

import java.io.IOException;

/**
 * Created by ProRed on 13.12.2016.
 */
public class FloatData extends Data {

    private float value;

    public FloatData(String name) {
        super(EnumDataType.FLOAT, name);
    }

    public FloatData(String name, float value) {
        super(EnumDataType.FLOAT, name);
        this.value = value;
    }

    public float getValue(){
        return value;
    }

    public void setValue(float value){
        this.value = value;
    }

    @Override
    public void saveData(XmlWriter xmlWriter, Engine e) throws IOException {
        xmlWriter.attribute("value", value);
    }

    @Override
    public void loadData(XmlReader.Element element, Engine e) {
        this.value = element.getFloat("value");
    }
}
