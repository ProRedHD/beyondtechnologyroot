package at.prored.main.engine.saving.data;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.Component;
import at.prored.main.engine.entities.EntityDef;
import at.prored.main.engine.items.ItemDef;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.saving.WorldLoader;
import at.prored.main.engine.saving.WorldSaver;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.saving.data.base.EnumDataType;
import at.prored.main.engine.tiles.TileDef;
import at.prored.main.engine.tiles.TileProperties;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlWriter;

import java.io.IOException;

/**
 * Created by ProRed on 25.02.2018.
 */
public class ItemStackData extends Data {

    private ItemStack value;

    public ItemStackData(String name) {
        super(EnumDataType.ITEMSTACK, name);
    }

    public ItemStackData(String name, ItemStack value) {
        super(EnumDataType.ITEMSTACK, name);
        this.value = value;
    }

    public ItemStack getValue(){
        return value;
    }

    public void setValue(ItemStack value){
        this.value = value;
    }

    @Override
    public void saveData(XmlWriter xmlWriter, Engine e) throws IOException {

        if (value == null) {
            return;
        }

        int id = value.getID();
        int amount = value.getAmount();

        xmlWriter.attribute("id", id).attribute("amount", amount);

        ItemDef def = e.getItemRegistry().getItemDef(value.getID());

        //Find data
        Array<Data> dataArray = new Array<Data>();
        for (Component component : def.getComponents()) {
            component.saveData(id, value.getProperties(), dataArray);
        }

        //Save data
        for (Data data : dataArray) {
            xmlWriter.element(data.getDataType().toString()).attribute("name", data.getDataName());
            data.saveData(xmlWriter, e);
            xmlWriter.pop();
        }
    }

    @Override
    public void loadData(XmlReader.Element element, Engine e) {
        int id = element.getInt("id");
        int amount = element.getInt("amount");

        ItemStack stack = ItemStack.createInstance(id, amount, e.getItemRegistry());

        if(element.getChildCount() != 0){
            Array<Data> data = WorldLoader.loadData(element, e);

            ItemDef def = e.getItemRegistry().getItemDef(id);

            //Notify every component of tile
            for (Component component : def.getComponents()) {
                component.loadData(stack.getID(), stack.getProperties(), data);
            }
        }

        this.value = stack;
    }
}
