package at.prored.main.engine.saving;

import at.prored.main.engine.Engine;
import at.prored.main.engine.generation.WorldGenerator;
import at.prored.main.engine.components.Component;
import at.prored.main.engine.entities.Entity;
import at.prored.main.engine.entities.EntityDef;
import at.prored.main.engine.saving.data.BooleanData;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.saving.data.base.EnumDataType;
import at.prored.main.engine.tiles.TileDef;
import at.prored.main.engine.tiles.TileProperties;
import at.prored.main.engine.tiles.storage.Chunk;
import at.prored.main.engine.tiles.storage.GameMap;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by ProRed on 20.02.2018.
 */
public class WorldLoader {

    /**
     * Loads a 5x5 chunk radius around the player as well as other data
     *
     * @param gameMap gameMap
     */
    public static void loadGameMap(GameMap gameMap) {
        String worldname = gameMap.getName();

        //Load data
        GameMapData mapData = loadGameMapData(worldname);

        if(mapData == null)
            return;

        //Load 5x5 area around player
        for (int x = mapData.getCurrentChunkX() - 2; x < mapData.getCurrentChunkX() + 2; x++) {
            for (int y = mapData.getCurrentChunkY() - 2; y < mapData.getCurrentChunkY() + 2; y++) {
                Chunk ch = loadChunk(worldname, x, y, gameMap);

                //Generate chunk if not loaded
                if (ch == null) {
                    ch = WorldGenerator.generateChunk(x,y, gameMap.getEngine());
                }

                //calculate tile bounding boxes
                ch.populateTileBoundingBoxes(gameMap.getEngine().getTileRegistry(), x, y);

                //Save chunk in gamemap
                gameMap.getChunks().put(new Vector2(x,y), ch);
            }
        }

        //Update active chunks of gamemap
        gameMap.populateActiveChunks();
    }

    /**
     * Loads game map data from file
     *
     * @param name name
     * @return data
     */
    private static GameMapData loadGameMapData(String name) {
        XmlReader reader = new XmlReader();
        FileHandle f = Gdx.files.external("/AppData/Roaming/btech/worlds/" + name + "/worldinfo.btw");

        if (!f.exists())
            return null;

        XmlReader.Element root = null;

        try {
            root = reader.parse(f);
        } catch (IOException e) {
            e.printStackTrace();
        }

        GameMapData data = null;

        if (root != null) {
            int seed = Integer.parseInt(root.getAttribute("seed"));
            int cX = Integer.parseInt(root.getAttribute("cX"));
            int cY = Integer.parseInt(root.getAttribute("cY"));

            data = new GameMapData(seed, cX, cY);
        }

        return data;
    }

    /**
     * Create chunk from the chunk file
     *
     * @param name world name
     * @param x    x of chunk
     * @param y    y of chunk
     * @return created chunk
     */
    private static Chunk loadChunk(String name, int x, int y, GameMap gameMap) {
        Engine engine = gameMap.getEngine();

        FileHandle f = Gdx.files.external("/AppData/Roaming/btech/worlds/" + name + String.format("/data/chunk_%d_%d.btc", x, y));
        XmlReader reader = new XmlReader();

        if (!f.exists())
            return null;

        XmlReader.Element root = null;

        //Chunk
        Chunk c = new Chunk();

        try {
            root = reader.parse(f);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (root != null) {

            //read layers
            Array<XmlReader.Element> layers = root.getChildrenByName("layer");

            //for every layer read tiles
            for (XmlReader.Element e : layers) {
                if (e.getChildCount() != 0) {

                    int layer = Integer.parseInt(e.getAttribute("value"));

                    //read tiles
                    Array<XmlReader.Element> tiles = e.getChildrenByName("tile");

                    for (XmlReader.Element tile : tiles) {
                        int id = Integer.parseInt(tile.getAttribute("id"));
                        int rx = Integer.parseInt(tile.getAttribute("rx"));
                        int ry = Integer.parseInt(tile.getAttribute("ry"));

                        c.setTileTypeRelative(rx, ry, id, layer);

                        //Load custom data
                        if(tile.getChildCount() != 0){
                            Array<Data> data = loadData(tile, engine);

                            TileDef def = engine.getTileRegistry().getTileDef(id);
                            TileProperties properties = c.getTilePropertiesRelative(rx, ry, layer);

                            //Notify every component of tile
                            for (Component component : def.getComponents()) {
                                component.loadData(id, properties, data);
                            }
                        }

                    }
                }
            }

            //read entities

            XmlReader.Element entitiesElement = root.getChildByName("entities");

            Array<XmlReader.Element> entities = entitiesElement.getChildrenByName("entity");

            for (XmlReader.Element entity : entities) {
                System.out.println(entity);

                int id = entity.getInt("id");
                float xpos = entity.getFloat("x");
                float ypos = entity.getFloat("y");

                Entity entityInstance = Entity.createInstance(id, engine);
                entityInstance.getEntityProperties().getPosition().set(xpos, ypos);

                if(entity.getChildCount() != 0) {
                    Array<Data> data = loadData(entity, engine);

                    //check if entity is player
                    for (Data d : data) {
                        if(d instanceof BooleanData && d.getDataName().equals("player") && ((BooleanData) d).getValue()){
                            gameMap.setPlayer(entityInstance);
                        }
                    }

                    EntityDef def = engine.getEntityRegistry().getEntityDef(id);

                    //Notify every component of tile
                    for (Component component : def.getComponents()) {
                        component.loadData(id, entityInstance.getEntityProperties(), data);
                    }
                }

                c.addEntity(entityInstance);
            }
        }

        System.out.println(c.getEntities());

        return c;
    }

    /**
     * Creates a data array from the children of the given element
     * @param element parent element
     * @return array of data
     */
    public static Array<Data> loadData(XmlReader.Element element, Engine e){

        Array<Data> dataArray = new Array<Data>();

        //Save all data to array
        for (int i = 0; i < element.getChildCount(); i++) {
            XmlReader.Element child = element.getChild(i);

            String datatype = child.getName();
            String name = child.get("name");

            //Creates data instance and loads values
            Data d = createDataInstance(datatype, name);
            d.loadData(child, e);

            //Save data object into array
            dataArray.add(d);
        }

        return dataArray;
    }

    /**
     * Creates a data instance
     * @param datatype datatype e.g. INT, STRING,...
     * @param name name of the data
     * @return data instance
     */
    public static Data createDataInstance(String datatype, String name){
        String classname = EnumDataType.valueOf(datatype).getClassname();

        Data d = null;

        try {
            Class<?> c = Class.forName("at.prored.main.engine.saving.data." + classname);

            d = (Data) c.getConstructor(String.class).newInstance(name);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        return d;
    }

}
