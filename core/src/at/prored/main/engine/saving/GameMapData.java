package at.prored.main.engine.saving;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by ProRed on 22.02.2018.
 */
public class GameMapData {

    private int seed;
    private int currentChunkX, currentChunkY;

    public GameMapData(int seed, int currentChunkX, int currentChunkY) {
        this.seed = seed;
        this.currentChunkX = currentChunkX;
        this.currentChunkY = currentChunkY;
    }

    public int getSeed() {
        return seed;
    }

    public int getCurrentChunkX() {
        return currentChunkX;
    }

    public int getCurrentChunkY() {
        return currentChunkY;
    }
}
