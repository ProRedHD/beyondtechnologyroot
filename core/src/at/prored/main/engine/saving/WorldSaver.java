package at.prored.main.engine.saving;

import at.prored.main.engine.components.Component;
import at.prored.main.engine.entities.Entity;
import at.prored.main.engine.entities.EntityDef;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.tiles.TileDef;
import at.prored.main.engine.tiles.TileProperties;
import at.prored.main.engine.tiles.layers.LayerRegistry;
import at.prored.main.engine.tiles.storage.Chunk;
import at.prored.main.engine.tiles.storage.GameMap;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.XmlWriter;

import java.io.IOException;

/**
 * Created by ProRed on 10.02.2018.
 */
public class WorldSaver {

    public static void saveGameMap(GameMap gameMap) {
        saveGameMapData(gameMap);

        for (ObjectMap.Entry<Vector2, Chunk> cve : gameMap.getChunks()) {
            if(cve.value.isDirty()){
                saveChunk(cve.key, cve.value, gameMap);
            }
        }

        Gdx.app.log("WorldSaver", "Saved world! World name: " + gameMap.getName());
    }

    private static void saveGameMapData(GameMap gameMap){
        FileHandle fileHandle = Gdx.files.external("/AppData/Roaming/btech/worlds/" + gameMap.getName() + "/worldinfo.btw");

        try {
            XmlWriter xmlWriter = new XmlWriter(fileHandle.writer(false));

            xmlWriter.element("gamemap")
                    .attribute("seed", gameMap.getSeed())
                    .attribute("cX", (int)gameMap.getPlayerChunk().x)
                    .attribute("cY", (int)gameMap.getPlayerChunk().y).pop().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void saveChunk(Vector2 pos, Chunk ch, GameMap gameMap){
        FileHandle fileHandle = Gdx.files.external("/AppData/Roaming/btech/worlds/" + gameMap.getName() + String.format("/data/chunk_%d_%d.btc", (int)pos.x, (int)pos.y));

        try {
            XmlWriter xmlWriter = new XmlWriter(fileHandle.writer(false));

            xmlWriter.element("chunk").attribute("x", pos.x).attribute("y", pos.y);

            //Save tiles
            for(int l = 0; l < LayerRegistry.getLayerCount(); l++){
                xmlWriter.element("layer").attribute("value", l);
                for (int x = 0; x < 16; x++) {
                    for (int y = 0; y < 16; y++) {
                        int id = ch.getTileTypeRelative(x,y,l);

                        if(id == -1)
                            continue;

                        TileProperties properties = ch.getTilePropertiesRelative(x,y,l);
                        TileDef def = gameMap.getEngine().getTileRegistry().getTileDef(id);

                        //Save data
                        Array<Data> dataArray = new Array<Data>();
                        for (Component component : def.getComponents()) {
                            component.saveData(id, properties, dataArray);
                        }

                        xmlWriter.element("tile").attribute("id", id).attribute("rx", x).attribute("ry",y);

                        for (Data data : dataArray) {
                            xmlWriter.element(data.getDataType().toString()).attribute("name", data.getDataName());
                            data.saveData(xmlWriter, gameMap.getEngine());
                            xmlWriter.pop();
                        }
                        xmlWriter.pop();
                    }
                }
                xmlWriter.pop();
            }

            xmlWriter.element("entities");

            for (Entity entity : ch.getEntities()) {

                int id = entity.getID();

                EntityDef def = gameMap.getEngine().getEntityRegistry().getEntityDef(id);

                //Save data
                Array<Data> dataArray = new Array<Data>();
                for (Component component : def.getComponents()) {
                    component.saveData(id, entity.getEntityProperties(), dataArray);
                }

                xmlWriter.element("entity").attribute("id", entity.getID())
                        .attribute("x", entity.getEntityProperties().getPosition().x)
                        .attribute("y", entity.getEntityProperties().getPosition().y);

                for (Data data : dataArray) {
                    xmlWriter.element(data.getDataType().toString()).attribute("name", data.getDataName());
                    data.saveData(xmlWriter, gameMap.getEngine());
                    xmlWriter.pop();
                }
                xmlWriter.pop();
            }

            xmlWriter.pop().pop().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
