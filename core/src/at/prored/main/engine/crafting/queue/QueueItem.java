package at.prored.main.engine.crafting.queue;

import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.items.ItemStack;

public class QueueItem {

    private float totalTime, elapsedTime;
    private ItemStack result;

    public QueueItem(float totalTime, ItemStack result) {
        this.totalTime = totalTime;
        this.result = result;
    }

    /**
     * Updates the queue item to pass time
     */
    public void update(){
        elapsedTime += Graphics.instance().getDeltaTime();
    }

    /**
     * Returns if the queue item is finished crafting
     * @return status
     */
    public boolean isFinished(){
        return elapsedTime >= totalTime;
    }

    /**
     * Returns the result itemstack
     * @return itemstack
     */
    public ItemStack getResult() {
        return result;
    }

    /**
     * Returns the total time
     * @return total time
     */
    public float getTotalTime() {
        return totalTime;
    }

    /**
     * Returns the elapsed time
     * @return elapsed time
     */
    public float getElapsedTime() {
        return elapsedTime;
    }
}
