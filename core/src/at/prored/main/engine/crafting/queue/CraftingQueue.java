package at.prored.main.engine.crafting.queue;

import at.prored.main.engine.Engine;
import at.prored.main.engine.crafting.Recipe;
import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.guis.elements.GuiElement;
import at.prored.main.engine.inventory.Inventory;
import at.prored.main.engine.items.ItemRenderer;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.utils.AssetUtils;
import at.prored.main.engine.inventory.InventoryUtils;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

public class CraftingQueue extends GuiElement {

    private static NinePatch backgroundTexture, elementTexture;
    private static TextureRegion pixel;

    private int queueSlotCount;
    private Array<QueueItem> queueItems;
    private Inventory targetInventory;
    private Color c;
    private float stateTime;
    private Engine e;

    public CraftingQueue(int queueSlotCount, Inventory targetInventory, Engine e) {
        this.queueSlotCount = queueSlotCount;
        this.queueItems = new Array<QueueItem>();
        this.targetInventory = targetInventory;
        this.e = e;
        this.c = new Color();

        if (backgroundTexture == null) {
            backgroundTexture = new NinePatch(AssetUtils.loadTextureRegion(e.getAssetManager(), "crafting_ui_background"), 3, 3, 3, 3);
            elementTexture = new NinePatch(AssetUtils.loadTextureRegion(e.getAssetManager(), "queue_element"), 4, 4, 4, 20);
            pixel = AssetUtils.loadTextureRegion(e.getAssetManager(), "pixel");
        }
    }

    /**
     * Adds a recipe to the queue
     * @param r recipe
     * @return success
     */
    public boolean addToQueue(Recipe r) {

        //Skip the queue if recipe is instant
        if (r.getCraftingTime() == 0) {
            InventoryUtils.addItem(ItemStack.createInstance(r.getResult(), e.getItemRegistry()), targetInventory);
            return true;
        }

        //Add to queue if space is there
        if (queueItems.size < queueSlotCount) {
            queueItems.add(new QueueItem(r.getCraftingTime(), r.getResult()));
            return true;
        }

        return false;
    }

    /**
     * Returns the queue slot count
     * @return queue slot count
     */
    public int getQueueSlotCount() {
        return queueSlotCount;
    }

    @Override
    public void render(float x, float y, SpriteBatch batch) {
        float width = 20 + Math.min(queueSlotCount, 3) * 53;
        float height = 20 + (int) Math.ceil(queueSlotCount / 3f) * 53;

        backgroundTexture.draw(batch, x - width - 10, y, width, height);

        for (int i = 0; i < queueItems.size; i++) {
            QueueItem item = queueItems.get(i);

            elementTexture.draw(batch, x - 68 - i % 3 * 53, y + 10 + i / 3 * 53, 48, 48);
            ItemRenderer.renderItem(batch, x - 68 - i % 3 * 53 + 8, y + 10 + i / 3 * 53 + 12, e, Graphics.instance().getCamera().getHudCamera(), item.getResult());
        }

        for (int i = 0; i < queueItems.size; i++) {
            QueueItem item = queueItems.get(i);

            if(i == 0) {
                c.set(Color.RED);
                c.lerp(Color.GREEN, item.getElapsedTime() / item.getTotalTime());
            } else {
                float value = (0.2f - 0.3f) * (float) Math.sin(stateTime) + .25f;
                c.set(1f,value, value, .9f);
            }

            Color or = batch.getColor();
            batch.setColor(c.mul(batch.getColor()));
            batch.draw(pixel, x - 68 - i % 3 * 53 + 2, y + 10 + i / 3 * 53 + 2, i > 0 ? 44 : item.getElapsedTime() / item.getTotalTime() * 44, 4);
            batch.setColor(or);
        }
    }

    @Override
    public void update() {

        //Update first
        if (queueItems.size > 0) {
            QueueItem queueItem = queueItems.get(0);
            queueItem.update();

            if (queueItem.isFinished()) {
                InventoryUtils.addItem(ItemStack.createInstance(queueItem.getResult(), e.getItemRegistry()), targetInventory);
                queueItems.removeIndex(0);
            }
        }

        //Update stateTime
        stateTime += Graphics.instance().getDeltaTime() * Math.PI;
    }


    @Override
    public int getWidth() {
        return 20 + Math.min(queueItems.size, 3) * 53;
    }

    @Override
    public int getHeight() {
        return 20 + (int) Math.ceil(queueItems.size / 3f) * 53;
    }
}
