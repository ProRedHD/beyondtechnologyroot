package at.prored.main.engine.crafting.template;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.LocalizableComponent;
import at.prored.main.engine.items.ItemDef;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.tiles.TileDef;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 05.09.2018.
 */
public class ItemStackTemplate {

    private String itemID;
    private ObjectMap<String, Object> properties;

    private int minamount, maxamount;

    public ItemStackTemplate(int itemID, int minamount, int maxamount) {
        this(String.valueOf(itemID), minamount, maxamount);
    }

    public ItemStackTemplate(String itemID, int minamount, int maxamount) {
        this.itemID = itemID;
        this.properties = new ObjectMap<>();
        this.minamount = minamount;
        this.maxamount = maxamount;
    }

    /**
     * Creates a template from a xml element
     * @param element xml element
     * @return template
     */
    public static ItemStackTemplate createFromXML(XmlReader.Element element){
        String id = element.getAttribute("id");

        int minAmount, maxAmount;
        try {
            minAmount = maxAmount = element.getInt("amount");
        } catch(GdxRuntimeException gre) {
            minAmount = element.getInt("min", 1);
            maxAmount = element.getInt("max", 1);
        }

        ItemStackTemplate temp = new ItemStackTemplate(id, minAmount, maxAmount);

        //Create itemstack
        for (String s : element.getAttributes().keys()) {
            if(!s.equals("probability") && !s.equals("id") && !s.equals("min") && !s.equals("max") && !s.equals("tileID")) {
                char type = s.charAt(0);
                String rest = s.substring(1);

                String value = element.getAttributes().get(s);

                //save based on type
                switch (type) {
                    case 'i':
                        temp.getProperties().put(rest, Integer.parseInt(value));
                        break;
                    case 'f':
                        temp.getProperties().put(rest, Float.parseFloat(value));
                        break;
                    case 'b':
                        temp.getProperties().put(rest, Boolean.parseBoolean(value));
                        break;
                    case 's':
                        temp.getProperties().put(rest, value);
                        break;
                }
            }
        }

        //Check for and handle tileID
        if(element.getAttribute("tileID", null) != null){
            temp.getProperties().put("tileID", element.getAttribute("tileID"));
        }

        return temp;
    }

    public String getItemID() {
        return itemID;
    }

    public ObjectMap<String, Object> getProperties() {
        return properties;
    }

    public ItemStack createItemStack(Engine engine){

        //Id
        ItemDef itemDef;
        try {
            itemDef = engine.getItemRegistry().getItemDef(Integer.parseInt(itemID));
        } catch(NumberFormatException ex){
            itemDef = engine.getItemRegistry().getItemDef(itemID);
        }

        if(itemDef == null)
            return null;

        //Amount
        int amount = com.badlogic.gdx.math.MathUtils.random(minamount, maxamount);

        ItemStack stack = ItemStack.createInstance(itemDef.getID(), amount, engine.getItemRegistry());

        for (String s : properties.keys()) {
            if(s.equals("tileID")){

                //Handle tiles
                TileDef tileDef;
                try {
                    tileDef = engine.getTileRegistry().getTileDef(Integer.parseInt(String.valueOf(properties.get(s))));
                } catch(NumberFormatException ex){
                    tileDef = engine.getTileRegistry().getTileDef(String.valueOf(properties.get(s)));
                }

                if(tileDef != null) {
                    stack.getProperties().setData("tileID", tileDef.getID());
                }
            } else {
                stack.getProperties().setData(s, properties.get(s));
            }
        }

        return stack;
    }

    public int getMinamount() {
        return minamount;
    }

    public int getMaxamount() {
        return maxamount;
    }

    public String toString(Engine e) {
        ItemDef def = e.getItemRegistry().getItemDef(itemID);

        if(!def.hasComponent(LocalizableComponent.class))
            return String.format("%d-%dx %d", minamount, maxamount, itemID);

        return String.format("%d-%dx %s (%s)", minamount, maxamount, def.getComponent(LocalizableComponent.class).getName(), getProperties());
    }
}
