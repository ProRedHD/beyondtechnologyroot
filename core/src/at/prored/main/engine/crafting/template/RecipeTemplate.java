package at.prored.main.engine.crafting.template;

import at.prored.main.engine.Engine;
import at.prored.main.engine.crafting.Recipe;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;

/**
 * Created by ProRed on 05.09.2018.
 */
public class RecipeTemplate {

    private ItemStackTemplate result;
    private Array<ItemStackTemplate> ingredients;

    private float minTime, maxTime;

    private String contentPackageID;
    private String type;
    private int tier;

    public RecipeTemplate(float minTime, float maxTime, String type, int tier, String contentPackageID) {
        this.minTime = minTime;
        this.maxTime = maxTime;
        this.type = type;
        this.tier = tier;
        this.contentPackageID = contentPackageID;

        this.ingredients = new Array<>();
    }

    /**
     * Creates a recipe from this template
     * @param e engine
     * @return recipe
     */
    public Recipe createRecipe(Engine e){
        float time = MathUtils.random(minTime, maxTime);

        Recipe r = new Recipe(time, type);

        //Create result
        r.setResult(result.createItemStack(e));

        //Set content package
        r.setContentPackageID(contentPackageID);

        //Create ingredients
        for (ItemStackTemplate ingredient : ingredients) {
            r.getIngredients().add(ingredient.createItemStack(e));
        }

        return r;
    }

    public ItemStackTemplate getResult() {
        return result;
    }

    public void setResult(ItemStackTemplate result) {
        this.result = result;
    }

    public Array<ItemStackTemplate> getIngredients() {
        return ingredients;
    }

    public float getMinTime() {
        return minTime;
    }

    public float getMaxTime() {
        return maxTime;
    }

    public String getType() {
        return type;
    }

    public int getTier() {
        return tier;
    }
}
