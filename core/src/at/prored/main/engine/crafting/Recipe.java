package at.prored.main.engine.crafting;

import at.prored.main.engine.guis.tooltip.elements.IRecipeDisplayable;
import at.prored.main.engine.items.ItemStack;
import com.badlogic.gdx.utils.Array;

/**
 * Created by ProRed on 05.03.2018.
 */
public class Recipe implements IRecipeDisplayable {

    private ItemStack result;
    private Array<ItemStack> ingredients;

    private float craftingTime;
    private String recipeType;
    private String contentPackageID;

    public Recipe(float craftingTime, String recipeType) {
        this.craftingTime = craftingTime;
        this.recipeType = recipeType;
        this.ingredients = new Array<>();
    }

    public Array<ItemStack> getIngredients() {
        return ingredients;
    }

    public ItemStack getResult() {
        return result;
    }

    public float getCraftingTime() {
        return craftingTime;
    }

    public String getRecipeType() {
        return recipeType;
    }

    public void setResult(ItemStack result) {
        this.result = result;
    }

    public void setContentPackageID(String contentPackageID) {
        this.contentPackageID = contentPackageID;
    }

    public String getContentPackageID() {
        return contentPackageID;
    }
}
