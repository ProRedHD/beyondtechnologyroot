package at.prored.main.engine.crafting;

import at.prored.main.engine.Engine;
import at.prored.main.engine.crafting.template.RecipeTemplate;
import at.prored.main.engine.guis.GUIManager;
import at.prored.main.engine.guis.crafting.CraftingMenu;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by ProRed on 05.03.2018.
 */
public class RecipeRegistry {

    private Engine engine;
    private ObjectMap<String, ObjectMap<Integer, Array<RecipeTemplate>>> recipes;
    private ObjectMap<String, Array<Recipe>> unlockedRecipes;

    public RecipeRegistry(Engine engine) {
        this.engine = engine;
        this.recipes = new ObjectMap<>();
        this.unlockedRecipes = new ObjectMap<>();
    }

    public int size(){
        return recipes.size;
    }


    /**
     * Unlocks a recipe
     * @param r recipe
     */
    public void unlockRecipe(Recipe r){
        if(!unlockedRecipes.containsKey(r.getRecipeType()))
            unlockedRecipes.put(r.getRecipeType(), new Array<>());

        unlockedRecipes.get(r.getRecipeType()).add(r);

        ((CraftingMenu) engine.getGUIManager().getGUI(GUIManager.GUI.CRAFTING_MENU)).updateRecipes();
    }

    /**
     * Returns all the recipe that have been unlocked for the recipe type
     * @param type type
     * @return unlocked recipes
     */
    public Array<Recipe> getUnlockedRecipeForRecipeType(String type){
        return unlockedRecipes.get(type);
    }

    /**
     * Registers a recipe
     *
     * @param r recipe
     */
    public void registerRecipe(RecipeTemplate r) {
        if (!recipes.containsKey(r.getType()))
            recipes.put(r.getType(), new ObjectMap<>());

        if(!recipes.get(r.getType()).containsKey(r.getTier()))
            recipes.get(r.getType()).put(r.getTier(), new Array<>());


        this.recipes.get(r.getType()).get(r.getTier()).add(r);
    }

    /**
     * Returns all recipes for a recipe tyoe
     *
     * @param type recipe type
     * @return all recipes
     */
    public ObjectMap<Integer, Array<RecipeTemplate>> getRecipesForRecipeType(String type) {
        return recipes.get(type);
    }
}
