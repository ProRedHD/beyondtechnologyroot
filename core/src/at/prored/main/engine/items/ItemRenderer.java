package at.prored.main.engine.items;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.SlotIconComponent;
import at.prored.main.engine.components.TextureStorageComponent;
import at.prored.main.engine.items.components.TileComponent;
import at.prored.main.engine.tiles.TileDef;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by ProRed on 27.08.2017.
 */
public class ItemRenderer {

    /**
     * Renders item with the specified camera
     * @param batch batch
     * @param x x
     * @param y y
     * @param scaleX scaleX
     * @param scaleY scaleY
     * @param engine engine
     * @param camera camera
     * @param stack stack
     * @param slotTexture
     */
    public static void renderItem(SpriteBatch batch, float x, float y, float scaleX, float scaleY, Engine engine, OrthographicCamera camera, ItemStack stack, boolean slotTexture){
        ItemDef def = engine.getItemRegistry().getItemDef(stack.getID());

        if(slotTexture){
            if(def.hasComponent(TileComponent.class)){
                //Get tile id off item
                int tileID = TileComponent.getTileID(stack.getProperties());

                //Get TileDef from tile id
                TileDef tileDef = engine.getTileRegistry().getTileDef(tileID);

                if(tileDef.hasComponent(SlotIconComponent.class)){
                    tileDef.getComponent(SlotIconComponent.class).render(batch, x, y, scaleX, scaleY, camera, def, stack.getProperties(), engine);
                    return;
                }
            } else if(def.hasComponent(SlotIconComponent.class)){
                def.getComponent(SlotIconComponent.class).render(batch, x, y, scaleX, scaleY, camera, def, stack.getProperties(), engine);
                return;
            }
        }

        if(def.hasComponent(TextureStorageComponent.class)){
            //Render Item
            def.getComponent(TextureStorageComponent.class).render(batch, x, y, scaleX, scaleY, camera, def, stack.getProperties(), engine);
        } else if(def.hasComponent(TileComponent.class)){
            //Render Tile
            TileComponent.renderTile(batch, x, y, scaleX * .5f, scaleY * .5f, engine, camera, stack);
        }
    }

    /**
     * Render item with the specified camera
     * @param batch batch
     * @param x x
     * @param y y
     * @param engine engine
     * @param camera camera
     * @param stack stack
     */
    public static void renderItem(SpriteBatch batch, float x, float y, Engine engine, OrthographicCamera camera, ItemStack stack){
        renderItem(batch, x, y, 1, 1, engine, camera, stack, false);
    }
}
