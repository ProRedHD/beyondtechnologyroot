package at.prored.main.engine.items;

import at.prored.main.engine.components.Component;
import at.prored.main.engine.utils.Definition;
import com.badlogic.gdx.utils.Array;

import java.util.Iterator;

/**
 * Created by ProRed on 23.08.2017.
 */
public class ItemDef implements Definition {

    private String defID;
    private int id;
    private Array<Component> itemComponents;

    public ItemDef(int id) {
        this.id = id;
        this.itemComponents = new Array<Component>();
    }

    public String getDefID() {
        return defID;
    }

    public void setDefID(String defID) {
        this.defID = defID;
    }

    /**
     * Adds a item component to this item definition
     *
     * @param itemComponent item component to add
     */
    public void addComponent(Component itemComponent) {
        if(hasComponent(itemComponent.getClass()))
            removeComponent(itemComponent.getClass());

        itemComponents.add(itemComponent);
    }

    /**
     * Returns a component from a item definition
     *
     * @param component class of the searched component
     * @return component (or null)
     */
    @SuppressWarnings("unchecked")
    public <T extends Component> T getComponent(Class<T> component){
        for (int i = 0; i < itemComponents.size; i++) {
            if(component.isInstance(itemComponents.get(i)))
                return (T) itemComponents.get(i);
        }

        return null;
    }

    /**
     * Removes a component from the definition
     * @param component component
     */
    public void removeComponent(Class<? extends Component> component){
        Iterator<Component> it = itemComponents.iterator();
        while(it.hasNext()){
            if(component.isInstance(it.next()))
                it.remove();
        }
    }

    /**
     * Checks if a item definition has a component
     *
     * @param component class of the component
     * @return true or false
     */
    public boolean hasComponent(Class<? extends Component> component) {
        for (int i = 0; i < itemComponents.size; i++) {
            if(component.isInstance(itemComponents.get(i)))
                return true;
        }

        return false;
    }

    /**
     * Returns all components
     *
     * @return all components
     */
    public Array<Component> getComponents() {
        return itemComponents;
    }

    public int getID() {
        return id;
    }

}
