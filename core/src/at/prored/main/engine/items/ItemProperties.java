package at.prored.main.engine.items;

import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Pool;

/**
 * Created by ProRed on 23.08.2017.
 */
public class ItemProperties implements Pool.Poolable, Properties {

    private ObjectMap<String, Object> data;
    private int amount;

    public ItemProperties() {
        this.data = new ObjectMap<String, Object>();
        this.amount = 0;
    }

    /**
     * Adds a integer value to the item property
     *
     * @param key   key
     * @param value value
     */
    public void setInteger(String key, int value) {
        data.put(key, value);
    }

    /**
     * Adds a integer value to the item property
     *
     * @param key   key
     * @param value value
     */
    public void setFloat(String key, float value) {
        data.put(key, value);
    }

    @Override
    public void setData(String key, Object value) {
        data.put(key, value);
    }

    /**
     * Adds a integer value to the item property
     *
     * @param key   key
     * @param value value
     */
    public void setBoolean(String key, boolean value) {
        data.put(key, value);
    }

    /**
     * Returns a integer from the item property value set
     *
     * @param key key
     * @return integer
     */
    public int getInteger(String key) {
        return (Integer) data.get(key);
    }

    /**
     * Returns a float from the item property value set
     *
     * @param key key
     * @return float
     */
    public float getFloat(String key) {
        return (Float) data.get(key);
    }

    /**
     * Returns a boolean from the item property value set
     *
     * @param key key
     * @return boolean
     */
    public boolean getBoolean(String key) {
        return (Boolean) data.get(key);
    }

    /**
     * Checks if this item property has key entry
     *
     * @param key key
     * @return true or false
     */
    public boolean hasData(String key) {
        return data.containsKey(key);
    }

    public ObjectMap<String, Object> getData(){
        return data;
    }

    @Override
    public Object getData(String key) {
        return data.get(key);
    }

    /**
     * Returns the amount of items in stack
     *
     * @return amount of items
     */
    public int getAmount() {
        return amount;
    }

    /**
     * Sets the amount of items in the stack
     *
     * @param amount amount of items
     */
    public void setAmount(int amount) {
        this.amount = amount;
    }

    /**
     * Adds the given amount of items to the amount of the stack
     *
     * @param amount amount of items to add
     */
    public void addAmount(int amount) {
        this.amount += amount;
    }

    /**
     * Sets this property object to p
     * @param p other property object
     */
    public void set(ItemProperties p){
        amount = p.amount;

        data.clear();
        for (String s : p.data.keys()) {
            data.put(s, p.data.get(s));
        }
    }

    /**
     * Reset function for pooling
     */
    @Override
    public void reset() {
        data.clear();
        amount = 0;
    }

    /**
     * Clears all data
     */
    @Override
    public void clear() {
        reset();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ItemProperties that = (ItemProperties) o;

        return data != null ? data.equals(that.data) : that.data == null;
    }

    @Override
    public int hashCode() {
        return data != null ? data.hashCode() : 0;
    }

    @Override
    public String toString() {

        return "amount=" + amount + (data.size == 0 ? "" : ", " + data.toString().substring(1, data.toString().length() - 1));
    }
}
