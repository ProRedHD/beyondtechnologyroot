package at.prored.main.engine.items.hotbaraction;

import at.prored.main.engine.Engine;
import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.guis.HotbarSlot;
import at.prored.main.engine.items.ItemDef;
import at.prored.main.engine.items.ItemRenderer;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.items.components.HotbarActionComponent;
import at.prored.main.engine.utils.Pools;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by ProRed on 23.12.2017.
 */
public class HotbarActionItem {

    private ItemStack stack;
    private HotbarSlot s;
    private Vector2 pos;
    private ItemDef def;
    private Engine engine;
    private HotbarAction action;
    private ObjectMap<String, Object> storage;

    private boolean stopping = false;

    public HotbarActionItem(HotbarSlot s, float posX, float posY, ItemDef def, Engine engine) {
        this.stack = s.getStack();
        this.s = s;
        this.def = def;
        this.engine = engine;
        this.action = def.getComponent(HotbarActionComponent.class).getHotbarAction().getAction();
        this.pos = Pools.instance().getVector2().set(posX, posY);
        this.storage = Pools.instance().getHotbarItemStoragePool().obtain();

        //Just in case
        this.storage.clear();
    }

    public void start() {
        action.start(pos, storage);
    }

    public void update() {

        action.update(pos, storage, stopping);
    }

    public void render(SpriteBatch batch) {
        ItemRenderer.renderItem(batch, pos.x, pos.y, 1, 1, engine, Graphics.instance().getCamera().getHudCamera(), stack, true);
    }

    public void stop() {
        action.update(pos, storage, stopping);
    }

    public final void markStop(){
        stop();
        this.stopping = true;
    }

    public boolean isDone(){
        boolean done = (stopping && action.isDone(pos, storage)) || (stack == null || stack.getAmount() == 0);

        if(done)
            s.setSilhouette(false);

        return done;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();

        this.storage.clear();
        Pools.instance().freeVector2(pos);
        Pools.instance().getHotbarItemStoragePool().free(storage);
    }
}
