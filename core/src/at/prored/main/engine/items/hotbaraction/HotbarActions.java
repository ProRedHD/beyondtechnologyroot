package at.prored.main.engine.items.hotbaraction;

import at.prored.main.engine.items.hotbaraction.hotbaractions.HotbarActionCircle;
import at.prored.main.engine.items.hotbaraction.hotbaractions.HotbarActionStay;

/**
 * Created by ProRed on 24.10.2017.
 */
public enum HotbarActions {
    STAY(new HotbarActionStay()),
    CIRCLE(new HotbarActionCircle());

    private HotbarAction action;

    HotbarActions(HotbarAction action) {
        this.action = action;
    }

    public HotbarAction getAction() {
        return action;
    }
}
