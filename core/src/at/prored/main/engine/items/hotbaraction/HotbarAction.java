package at.prored.main.engine.items.hotbaraction;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by ProRed on 24.10.2017.
 */
public interface HotbarAction {

    void start(Vector2 position, ObjectMap<String, Object> storage);

    void update(Vector2 position, ObjectMap<String, Object> storage, boolean stopping);

    void stop(Vector2 position, ObjectMap<String, Object> storage);

    boolean isDone(Vector2 position, ObjectMap<String, Object> storage);
}

