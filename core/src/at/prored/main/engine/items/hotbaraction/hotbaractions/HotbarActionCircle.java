package at.prored.main.engine.items.hotbaraction.hotbaractions;

import at.prored.main.engine.items.hotbaraction.HotbarAction;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by ProRed on 18.12.2017.
 */
public class HotbarActionCircle implements HotbarAction {

    @Override
    public void start(Vector2 position, ObjectMap<String, Object> storage) {

    }

    @Override
    public void update(Vector2 position, ObjectMap<String, Object> storage, boolean stopping) {

    }

    @Override
    public void stop(Vector2 position, ObjectMap<String, Object> storage) {

    }

    @Override
    public boolean isDone(Vector2 position, ObjectMap<String, Object> storage) {
        return false;
    }
}
