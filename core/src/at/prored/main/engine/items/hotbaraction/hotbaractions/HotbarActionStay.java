package at.prored.main.engine.items.hotbaraction.hotbaractions;

import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.items.hotbaraction.HotbarAction;
import at.prored.main.engine.utils.VecUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by ProRed on 23.12.2017.
 */
public class HotbarActionStay implements HotbarAction {

    @Override
    public void start(Vector2 position, ObjectMap<String, Object> storage) {
        storage.put("startPos", position.cpy());
    }

    @Override
    public void update(Vector2 position, ObjectMap<String, Object> storage, boolean stopping) {
        Vector2 m = Graphics.instance().getCamera().getMouseCoordinates();
        Vector2 start = (Vector2) storage.get("startPos");
        Vector2 mStart = VecUtils.sub(m, start);

        if (stopping) {
            Vector2 dir = VecUtils.sub(start, position).setLength(mStart.len()).scl(Graphics.instance().getDeltaTime() * 4);
            position.add(dir);
            VecUtils.free(dir);
            return;
        }

        if (storage.get("reached") == null) {
            Vector2 dir = VecUtils.sub(m, position).setLength(mStart.len()).scl(Graphics.instance().getDeltaTime() * 4);

            if (Vector2.dst(m.x, m.y, position.x, position.y) > 50) {
                position.add(dir);
            } else {
                storage.put("reached", true);
            }

            VecUtils.free(dir);
        } else {
            position.set(m);
        }


        VecUtils.free(mStart);
    }

    @Override
    public void stop(Vector2 position, ObjectMap<String, Object> storage) {
        storage.put("direction", position.sub((Vector2) storage.get("startPos")));
    }

    @Override
    public boolean isDone(Vector2 position, ObjectMap<String, Object> storage) {
        Vector2 v = (Vector2) storage.get("startPos");
        return Vector2.dst(v.x, v.y, position.x, position.y) < 10;
    }

}
