package at.prored.main.engine.items;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.Component;
import at.prored.main.engine.init.ContentPackage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by ProRed on 18.05.2017.
 */
public class ItemRegistry {

    private Engine engine;

    private ObjectMap<Integer, ItemDef> itemdefs;
    private ObjectMap<String, Integer> defIDs;

    public ItemRegistry(Engine engine) {
        this.engine = engine;

        this.itemdefs = new ObjectMap<>();
        this.defIDs = new ObjectMap<>();
    }

    public int size(){
        return itemdefs.size;
    }

    /**
     * Loads up values to the items properties object
     *
     * @param i itemstack
     */
    public void initItem(ItemStack i) {
        if (!hasItemDef(i.getID()))
            return;

        ItemDef def = getItemDef(i.getID());

        for (Component component : def.getComponents()) {
            component.onItemStackCreated(i, engine);
        }
    }

    /**
     * Registers a item definition
     *
     * @param id      id of this entity
     * @param itemdef entity def for this entity
     */
    public void registerItemDef(int id, ItemDef itemdef) {
        this.itemdefs.put(id, itemdef);

        //Store defid
        ContentPackage contentPackage = engine.getContentPackageRegistry().getContentPackageFromContentID(id);
        this.defIDs.put(contentPackage.getPackageID() + ":" + itemdef.getDefID(), id);
    }

    /**
     * Unregisters a item definition
     *
     * @param id id of the item
     */
    public void unregisterItemDef(int id) {
        this.itemdefs.remove(id);
    }

    /**
     * Checks if a item id is registered / if a item def is available
     *
     * @param id id of the item
     * @return true or false
     */
    public boolean hasItemDef(int id) {
        return this.itemdefs.containsKey(id);
    }

    /**
     * Returns a item definition
     *
     * @param id id of the item
     * @return item definition
     */
    public ItemDef getItemDef(int id) {
        return this.itemdefs.get(id);
    }

    /**
     * Returns a item definition
     * @param defid defid of the item
     * @return item definition
     */
    public ItemDef getItemDef(String defid) {
        if (!defid.contains(":"))
            defid = "btcontent:" + defid;


        Integer id = defIDs.get(defid);

        if (id == null)
            return null;

        return getItemDef(id);
    }

    /**
     * Returns all item defs
     * @return all registered item definitions
     */
    public Array<ItemDef> getItemDefs(){
        return new Array<>(itemdefs.values().toArray());
    }
}
