package at.prored.main.engine.items.components;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.Component;
import at.prored.main.engine.items.hotbaraction.HotbarActions;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.utils.ComponentException;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 24.10.2017.
 */
public class HotbarActionComponent extends Component {

    private HotbarActions action;

    public HotbarActionComponent() {}

    public HotbarActionComponent(HotbarActions action) {
        this.action = action;
    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {
        this.action = HotbarActions.valueOf(element.getAttribute("name","STAY"));

        if(this.action == null){
            throw new ComponentException(HotbarActionComponent.class, element.getAttribute("name") + " cannot be found!");
        }
    }

    @Override
    public Component cpy() {
        return new HotbarActionComponent(action);
    }

    public HotbarActions getHotbarAction() {
        return action;
    }
}
