package at.prored.main.engine.items.components;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.Component;
import at.prored.main.engine.components.TextureStorageComponent;
import at.prored.main.engine.components.behavior.Behavior;
import at.prored.main.engine.components.behavior.behaviors.TileBehavior;
import at.prored.main.engine.items.ItemComponent;
import at.prored.main.engine.items.ItemDef;
import at.prored.main.engine.items.ItemProperties;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.saving.data.IntData;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.tiles.TileDef;
import at.prored.main.engine.utils.DataReader;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 23.08.2017.
 */
public class TileComponent extends ItemComponent {

    public TileComponent() {
    }

    /**
     * Renders a texture of a tile stored within a item with tile component
     *
     * @param batch  batch
     * @param x      x
     * @param y      y
     * @param engine engine
     * @param stack  itemstack
     */
    public static void renderTile(SpriteBatch batch, float x, float y, float scaleX, float scaleY, Engine engine, OrthographicCamera camera, ItemStack stack) {
        ItemDef def = engine.getItemRegistry().getItemDef(stack.getID());

        //Check if item has TileComponent
        if (!def.hasComponent(TileComponent.class))
            return;

        //Get tile id off item
        int tileID = getTileID(stack.getProperties());

        //Get TileDef from tile id
        TileDef tileDef = engine.getTileRegistry().getTileDef(tileID);

        //Check for a renderable component
        if (!tileDef.hasComponent(TextureStorageComponent.class))
            return;

        tileDef.getComponent(TextureStorageComponent.class).render(batch, x, y, scaleX, scaleY, camera, tileDef, null, engine);
    }

    public static void setTileID(int id, ItemProperties itemProperties) {
        itemProperties.setInteger("tileID", id);
    }

    public static int getTileID(ItemProperties properties) {
        return properties.hasData("tileID") ? properties.getInteger("tileID") : -1;
    }

    @Override
    public void onItemStackCreated(ItemStack stack, Engine e) {
        stack.getProperties().setInteger("tileID", 4);
    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {
        data.add(new IntData("tileID", properties.getInteger("tileID")));
    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {
        DataReader reader = new DataReader(data);
        properties.setInteger("tileID", reader.getInt("tileID"));
    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {

    }

    @Override
    public Behavior getBehavior() {
        return new TileBehavior(getClass());
    }

    @Override
    public Component cpy() {
        return new TileComponent();
    }

}
