package at.prored.main.engine.items.components;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.Component;
import at.prored.main.engine.components.behavior.Behavior;
import at.prored.main.engine.components.behavior.behaviors.ToolBehavior;
import at.prored.main.engine.items.ItemComponent;
import at.prored.main.engine.saving.data.base.Data;
import at.prored.main.engine.utils.Definition;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by ProRed on 24.12.2017.
 */
public class ToolComponent extends ItemComponent {

    private ObjectMap<TileBreakingTypes, ToolProperties> properties;

    public ToolComponent(){
        this.properties = new ObjectMap<TileBreakingTypes, ToolProperties>();
    }

    public ToolComponent(ObjectMap<TileBreakingTypes, ToolProperties> properties) {
        this.properties = properties;
    }

    public ToolProperties getToolProperties(TileBreakingTypes type){
        return properties.get(type);
    }

    @Override
    public void saveData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void loadData(int id, Properties properties, Array<Data> data) {

    }

    @Override
    public void constructComponent(XmlReader.Element element, Engine e, Definition def) {

        XmlReader.Element wood = element.getChildByName("wood");
        float efficiency = Float.parseFloat(wood.getAttribute("efficiency"));
        float dropModifier = Float.parseFloat(wood.getAttribute("dropModifier"));
        ToolProperties woodProperty = new ToolProperties(efficiency, dropModifier);

        XmlReader.Element dirt = element.getChildByName("dirt");
        efficiency = Float.parseFloat(dirt.getAttribute("efficiency"));
        dropModifier = Float.parseFloat(dirt.getAttribute("dropModifier"));
        ToolProperties dirtProperty = new ToolProperties(efficiency, dropModifier);

        XmlReader.Element stone = element.getChildByName("stone");
        efficiency = Float.parseFloat(stone.getAttribute("efficiency"));
        dropModifier = Float.parseFloat(stone.getAttribute("dropModifier"));
        ToolProperties stoneProperty = new ToolProperties(efficiency, dropModifier);

        this.properties.put(TileBreakingTypes.WOOD, woodProperty);
        this.properties.put(TileBreakingTypes.DIRT, dirtProperty);
        this.properties.put(TileBreakingTypes.STONE, stoneProperty);
    }

    @Override
    public Behavior getBehavior() {
        return new ToolBehavior(getClass());
    }

    @Override
    public Component cpy() {
        ObjectMap<TileBreakingTypes, ToolProperties> props = new ObjectMap<TileBreakingTypes, ToolProperties>();
        for (TileBreakingTypes tileBreakingTypes : properties.keys()) {
            props.put(tileBreakingTypes, properties.get(tileBreakingTypes));
        }
        return new ToolComponent(props);
    }

    public enum TileBreakingTypes {
        WOOD,DIRT,STONE
    }

    public class ToolProperties {
        private float efficiency;
        private float dropModifier;

        public ToolProperties(float efficiency, float dropModifier) {
            this.efficiency = efficiency;
            this.dropModifier = dropModifier;
        }

        public float getEfficiency() {
            return efficiency;
        }

        public float getDropModifier() {
            return dropModifier;
        }
    }
}
