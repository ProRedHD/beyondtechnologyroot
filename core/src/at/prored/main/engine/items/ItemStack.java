package at.prored.main.engine.items;

import at.prored.main.engine.Engine;
import at.prored.main.engine.components.LocalizableComponent;
import at.prored.main.engine.decisiontree.Decidable;
import at.prored.main.engine.utils.Pools;

/**
 * Created by ProRed on 23.08.2017.
 */
public class ItemStack implements Decidable {

    private int id;
    private ItemProperties properties;

    private ItemStack(int id, int amount) {
        this.id = id;
       // this.properties = Pools.instance().getItemPropertiesPool().obtain();
        this.properties = new ItemProperties();
        this.properties.setAmount(amount);
    }

    private ItemStack(int id){
        this(id, 0);
    }

    /**
     * Creates an itemstack instance
     * @param id id
     * @param amount amount
     * @param registry item registry
     * @return
     */
    public static ItemStack createInstance(int id, int amount, ItemRegistry registry){
        ItemStack stack = new ItemStack(id, amount);
        registry.initItem(stack);

        return stack;
    }

    /**
     * Creates a copy of stack
     * @param stack stack
     * @param registry item registry
     * @return
     */
    public static ItemStack createInstance(ItemStack stack, ItemRegistry registry){
        ItemStack base = createInstance(stack.getID(), stack.getAmount(), registry);
        base.getProperties().set(stack.getProperties());

        return base;
    }

    public int getID() {
        return id;
    }

    public int getAmount(){
        return this.properties.getAmount();
    }

    public ItemProperties getProperties() {
        return properties;
    }

    @Override
    public String toString() {
        return String.format("{id=%d, %s}", id, properties);
    }

    public String toString(Engine e) {
        ItemDef def = e.getItemRegistry().getItemDef(id);

        if(!def.hasComponent(LocalizableComponent.class))
            return String.format("%dx %d", getAmount(), id);

        return String.format("%dx %s (%s)", getAmount(), def.getComponent(LocalizableComponent.class).getName(), getProperties());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ItemStack itemStack = (ItemStack) o;
        return id == itemStack.id && (properties != null ? properties.equals(itemStack.properties) : itemStack.properties == null);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (properties != null ? properties.hashCode() : 0);
        return result;
    }

    @Override
    protected void finalize() throws Throwable {
        Pools.instance().getItemPropertiesPool().free(properties);
        super.finalize();
    }
}
