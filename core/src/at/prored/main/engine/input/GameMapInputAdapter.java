package at.prored.main.engine.input;

import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.tiles.storage.GameMap;
import at.prored.main.engine.utils.MapUtils;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by ProRed on 04.06.2017.
 */
public class GameMapInputAdapter extends InputAdapter {

    private GameMap gameMap;

    public GameMapInputAdapter(GameMap map){
        this.gameMap = map;
    }

    @Override
    public boolean keyUp(int keycode) {
        return gameMap.getTilePlacer().keyUp(keycode);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if(button == Input.Buttons.MIDDLE){

            //Calculate position
            Vector2 mousePos = Graphics.instance().getCamera().getUnprojectedMouseCoordinates();

            int tX = MathUtils.floor(mousePos.x / 32f);
            int tY = MathUtils.floor(mousePos.y / 32f);

            int id = MapUtils.getTileType(gameMap, tX, tY, 0);
            Properties p = MapUtils.getTileProperties(gameMap, tX, tY, 0);

            System.out.println("-----------------------------");
            System.out.println("ID: " + id);
            System.out.println("Properties: " + p.toString());
            System.out.println("-----------------------------");
            return true;
        }


        if(gameMap.getTilePlacer().touchDown(screenX, screenY, pointer, button))
            return true;

        if(gameMap.getTileBreaker().touchDown(screenX, screenY, pointer, button))
            return true;

        return super.touchDown(screenX, screenY, pointer, button);
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if(gameMap.getTilePlacer().touchUp(screenX, screenY, pointer, button))
            return true;

        if(gameMap.getTileBreaker().touchUp(screenX, screenY, pointer, button))
            return true;

        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if(gameMap.getTilePlacer().touchDragged(screenX, screenY, pointer))
            return true;

        if(gameMap.getTileBreaker().touchDragged(screenX, screenY, pointer))
            return true;

        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        if(gameMap.getTileBreaker().mouseMoved(screenX, screenY))
            return true;

        if(gameMap.getTilePlacer().mouseMoved(screenX, screenY))
            return true;

        return false;
    }
}
