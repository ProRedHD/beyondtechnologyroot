package at.prored.main.engine.decisiontree;

import at.prored.main.engine.Engine;
import com.badlogic.gdx.utils.XmlReader;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by ProRed on 03.09.2018.
 */
public class DecisionTreeReader {

    /**
     * Converts a xml tree to a object decide table
     * @param element root of decide table
     * @param engine engine
     * @return decide table element
     */
    public static DecisionTreeElement xmlToLootTableElement(XmlReader.Element element, Engine engine){

        DecisionTreeElement lootTable;
        switch (element.getName().toUpperCase()){
            case "ITEM":
                lootTable = createNewLootTableItem();
                DecisionTreeItem item = ((DecisionTreeItem) lootTable);

                //Probability
                int probability = element.getInt("probability", 100);

                //id
                String itemID = element.getAttribute("id");

                //Min/Max
                int min = element.getInt("min", 1);
                int max = element.getInt("max", 1);

                //Create itemstack
                for (String s : element.getAttributes().keys()) {
                    if(!s.equals("probability") && !s.equals("id") && !s.equals("min") && !s.equals("max") && !s.equals("tileID")) {
                        char type = s.charAt(0);
                        String rest = s.substring(1);

                        String value = element.getAttributes().get(s);

                        //save based on type
                        switch (type) {
                            case 'i':
                                item.getProperties().put(rest, Integer.parseInt(value));
                                break;
                            case 'f':
                                item.getProperties().put(rest, Float.parseFloat(value));
                                break;
                            case 'b':
                                item.getProperties().put(rest, Boolean.parseBoolean(value));
                                break;
                            case 's':
                                item.getProperties().put(rest, value);
                                break;
                        }
                    }
                }

                //Check for and handle tileID
                if(element.getAttribute("tileID", null) != null){
                    item.getProperties().put("tileID", element.getAttribute("tileID"));
                }


                //Finalize
                item.setProbability(probability);
                item.setItemID(itemID);
                item.setAmountRangeMin(min);
                item.setAmountRangeMax(max);

                return item;
            case "BUNDLE":
                lootTable = createNewLootTableBundle();
                DecisionTreeBundle bundle = ((DecisionTreeBundle) lootTable);

                //Probability
                probability = element.getInt("probability", 100);
                bundle.setProbability(probability);

                //Empty Probability
                int emptyProbability = element.getInt("empty_probability", 0);
                bundle.setEmptyProbability(emptyProbability);

                for (int i = 0; i < element.getChildCount(); i++) {
                    bundle.addElement(xmlToLootTableElement(element.getChild(i), engine));
                }

                return bundle;
            case "RECIPE":
                lootTable = createNewLootTableRecipe();
                DecisionTreeRecipe recipe = ((DecisionTreeRecipe) lootTable);

                //Probability
                probability = element.getInt("probability", 100);
                recipe.setProbability(probability);

                //Tier
                int tier = element.getInt("tier", 100);
                recipe.setTier(tier);

                //Type
                String type = element.getAttribute("type", "inventory");
                recipe.setType(type);

                return recipe;
        }

        return null;
    }

    /**
     * Create new decide table item
     * @return decide table item
     */
    private static DecisionTreeItem createNewLootTableItem(){
        try {
            return DecisionTreeItem.class.getConstructor().newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Create new decide table bundle
     * @return decide table bundle
     */
    private static DecisionTreeRecipe createNewLootTableRecipe(){
        try {
            return DecisionTreeRecipe.class.getConstructor().newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Create new decide table bundle
     * @return decide table bundle
     */
    private static DecisionTreeBundle createNewLootTableBundle(){
        try {
            return DecisionTreeBundle.class.getConstructor().newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        return null;
    }
}
