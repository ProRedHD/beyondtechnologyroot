package at.prored.main.engine.decisiontree;

import at.prored.main.engine.Engine;
import at.prored.main.engine.items.ItemDef;
import at.prored.main.engine.items.ItemStack;
import at.prored.main.engine.tiles.TileDef;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by ProRed on 03.09.2018.
 */
public class DecisionTreeItem extends DecisionTreeElement {

    private String itemID;
    private ObjectMap<String, Object> properties;

    private int amountRangeMin, amountRangeMax;

    public DecisionTreeItem() {
        this.properties = new ObjectMap<>();

    }

    public DecisionTreeItem(int probability, ItemStack stack, int min, int max) {
        super(probability);

        this.itemID = String.valueOf(stack.getID());
        this.properties = stack.getProperties().getData();

        this.amountRangeMin = min;
        this.amountRangeMax = max;
    }

    @Override
    public void decide(Array<Decidable> decision, Engine e) {

        //Id
        ItemDef itemDef;
        try {
            itemDef = e.getItemRegistry().getItemDef(Integer.parseInt(itemID));
        } catch(NumberFormatException ex){
            itemDef = e.getItemRegistry().getItemDef(itemID);
        }

        if(itemDef == null)
            return;

        //Create stack
        int amount = com.badlogic.gdx.math.MathUtils.random(amountRangeMin, amountRangeMax);
        ItemStack stack = ItemStack.createInstance(itemDef.getID(), amount, e.getItemRegistry());

        //Transfer properties
        for (String s : properties.keys()) {

            if(s.equals("tileID")){

                //Handle tiles
                TileDef tileDef;
                try {
                    tileDef = e.getTileRegistry().getTileDef(Integer.parseInt(String.valueOf(properties.get(s))));
                } catch(NumberFormatException ex){
                    tileDef = e.getTileRegistry().getTileDef(String.valueOf(properties.get(s)));
                }

                if(tileDef != null) {
                    stack.getProperties().setData("tileID", tileDef.getID());
                }
            } else {
                stack.getProperties().setData(s, properties.get(s));
            }
        }

        decision.add(stack);
    }

    public String getItemID() {
        return itemID;
    }

    public void setItemID(String itemID) {
        this.itemID = itemID;
    }

    public ObjectMap<String, Object> getProperties() {
        return properties;
    }

    public void setProperties(ObjectMap<String, Object> properties) {
        this.properties = properties;
    }

    public int getAmountRangeMin() {
        return amountRangeMin;
    }

    public void setAmountRangeMin(int amountRangeMin) {
        this.amountRangeMin = amountRangeMin;
    }

    public int getAmountRangeMax() {
        return amountRangeMax;
    }

    public void setAmountRangeMax(int amountRangeMax) {
        this.amountRangeMax = amountRangeMax;
    }
}
