package at.prored.main.engine.decisiontree.container;

import at.prored.main.engine.decisiontree.Decidable;

/**
 * Created by ProRed on 06.09.2018.
 */
public class RecipeDecision implements Decidable {

    private int tier;
    private String type;

    public RecipeDecision(int tier, String type) {
        this.tier = tier;
        this.type = type;
    }

    public int getTier() {
        return tier;
    }

    public String getType() {
        return type;
    }

}
