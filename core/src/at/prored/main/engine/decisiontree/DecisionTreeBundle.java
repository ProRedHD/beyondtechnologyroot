package at.prored.main.engine.decisiontree;

import at.prored.main.engine.Engine;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by ProRed on 03.09.2018.
 */
public class DecisionTreeBundle extends DecisionTreeElement {

    private Array<DecisionTreeElement> elements;
    private int emptyProbability;

    public DecisionTreeBundle() {
        this.elements = new Array<>();
    }

    public DecisionTreeBundle(int probability, int emptyProbability, DecisionTreeElement... elements) {
        super(probability);
        this.emptyProbability = emptyProbability;
        this.elements = new Array<>(elements);
    }

    @Override
    public void decide(Array<Decidable> decision, Engine e) {

        //Calculate sum
        int probsum = emptyProbability;
        for (DecisionTreeElement element : elements) {
            if (element.getProbability() == -1)
                continue;

            probsum += element.getProbability();
        }

        //Define ranges
        ObjectMap<Integer, DecisionTreeElement> ranges = new ObjectMap<>();
        Array<Integer> rangeEnds = new Array<>();

        int curEnd = 0;
        for (DecisionTreeElement element : elements) {

            curEnd += element.getProbability();

            ranges.put(curEnd, element);
            rangeEnds.add(curEnd);
        }

        //Get winner
        int randomNumber = MathUtils.random(probsum);

        rangeEnds.sort();
        DecisionTreeElement winner = null;
        for (int i = 0; i < rangeEnds.size; i++) {
            if(i == 0){
                if(randomNumber <= rangeEnds.get(i)){
                    winner = ranges.get(rangeEnds.get(i));
                }
            } else {
                if(randomNumber > rangeEnds.get(i-1) && randomNumber <= rangeEnds.get(i)){
                    winner = ranges.get(rangeEnds.get(i));
                }
            }
        }

        //Loot winner
        if (winner != null)
            winner.decide(decision, e);

        //Check for always drop
        for (DecisionTreeElement element : elements) {
            if (element.getProbability() == -1)
                element.decide(decision, e);
        }
    }

    public Array<DecisionTreeElement> getElements() {
        return elements;
    }

    public void setElements(Array<DecisionTreeElement> elements) {
        this.elements = elements;
    }

    public void addElement(DecisionTreeElement element) {
        elements.add(element);
    }

    public int getEmptyProbability() {
        return emptyProbability;
    }

    public void setEmptyProbability(int emptyProbability) {
        this.emptyProbability = emptyProbability;
    }


}
