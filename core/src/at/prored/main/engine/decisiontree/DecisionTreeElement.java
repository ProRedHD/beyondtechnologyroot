package at.prored.main.engine.decisiontree;

import at.prored.main.engine.Engine;
import com.badlogic.gdx.utils.Array;

/**
 * Created by ProRed on 03.09.2018.
 */
public abstract class DecisionTreeElement {

    private int probability;

    public DecisionTreeElement() {
    }

    public DecisionTreeElement(int probability){
        this.probability = probability;
    }

    public abstract void decide(Array<Decidable> decision, Engine e);

    public int getProbability() {
        return probability;
    }

    public void setProbability(int probability) {
        this.probability = probability;
    }

}
