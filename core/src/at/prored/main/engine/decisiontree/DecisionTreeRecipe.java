package at.prored.main.engine.decisiontree;

import at.prored.main.engine.Engine;
import at.prored.main.engine.decisiontree.container.RecipeDecision;
import com.badlogic.gdx.utils.Array;

/**
 * Created by ProRed on 06.09.2018.
 */
public class DecisionTreeRecipe extends DecisionTreeElement{

    private int tier;
    private String type;

    public DecisionTreeRecipe(){}

    public DecisionTreeRecipe(int tier, String type) {
        this.tier = tier;
        this.type = type;
    }

    public DecisionTreeRecipe(int probability, int tier, String type) {
        super(probability);
        this.tier = tier;
        this.type = type;
    }

    @Override
    public void decide(Array<Decidable> decision, Engine e) {
        decision.add(new RecipeDecision(tier, type));
    }

    public void setTier(int tier) {
        this.tier = tier;
    }

    public void setType(String type) {
        this.type = type;
    }
}
