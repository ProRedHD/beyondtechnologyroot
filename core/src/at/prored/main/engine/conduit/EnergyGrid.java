package at.prored.main.engine.conduit;

import at.prored.main.engine.components.conduit.PowerConduitComponent;
import at.prored.main.engine.conduit.power.interfaces.IEnergyConsumer;
import at.prored.main.engine.conduit.power.interfaces.IEnergyProvider;
import at.prored.main.engine.utils.Pools;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by ProRed on 01.03.2018.
 */
public class EnergyGrid {

    private ObjectMap<Vector2, PowerConduitComponent> conduits;

    private ObjectMap<Vector2, IEnergyProvider> providers;
    private ObjectMap<Vector2, IEnergyConsumer> consumers;

    /**
     * ID of the energy grid
     */
    private int id;

    protected EnergyGrid(int id) {
        this.id = id;

        this.conduits = new ObjectMap<Vector2, PowerConduitComponent>();

        this.providers = new ObjectMap<Vector2, IEnergyProvider>();
        this.consumers = new ObjectMap<Vector2, IEnergyConsumer>();
    }

    /**
     * Returns the id of the grid
     * @return id
     */
    public int getID() {
        return id;
    }

    public int getGridSize(){
        return conduits.size + providers.size + consumers.size;
    }

    /**
     * Adds a conduit to the grid
     *
     * @param x x
     * @param y y
     * @param component component
     */
    public void addConduit(int x, int y, PowerConduitComponent component) {
        conduits.put(new Vector2(x, y), component);
    }

    /**
     * Adds a conduit to the grid
     * @param vec vector
     * @param component component
     */
    public void addConduit(Vector2 vec, PowerConduitComponent component) {
        conduits.put(vec, component);
    }

    /**
     * Removes a conduit to the grid
     *
     * @param x x
     * @param y y
     * @return success
     */
    public boolean removeConduit(int x, int y) {
        Vector2 help = Pools.instance().getVector2().set(x, y);

        if (conduits.containsKey(help)) {
            conduits.remove(help);
            return true;
        }

        Pools.instance().freeVector2(help);
        return false;
    }

    /**
     * Checks if a conduit is registered at a specific position
     *
     * @param x x
     * @param y y
     * @return true/false
     */
    public boolean hasConduit(int x, int y) {
        Vector2 help = Pools.instance().getVector2().set(x, y);

        boolean sucess = conduits.containsKey(help);

        Pools.instance().freeVector2(help);

        return sucess;
    }

    /**
     * Adds a energy provider at a specific position
     *
     * @param x        x
     * @param y        y
     * @param provider provider
     */
    public void addProvider(int x, int y, IEnergyProvider provider) {
        providers.put(new Vector2(x, y), provider);
    }

    /**
     * Adds a energy provider at a specific position
     *
     * @param vec vector
     * @param provider provider
     */
    public void addProvider(Vector2 vec, IEnergyProvider provider) {
        providers.put(vec, provider);
    }

    /**
     * Adds a energy consumer at a specific position
     *
     * @param x        x
     * @param y        y
     * @param consumer consumer
     */
    public void addConsumer(int x, int y, IEnergyConsumer consumer) {
        consumers.put(new Vector2(x, y), consumer);
    }

    /**
     * Adds a energy consumer at a specific position
     *
     * @param vec vector
     * @param consumer consumer
     */
    public void addConsumer(Vector2 vec, IEnergyConsumer consumer) {
        consumers.put(vec, consumer);
    }

    /**
     * Removes a provider from the grid
     * @param x x
     * @param y y
     */
    public void removeProvider(int x, int y){
        Vector2 help = Pools.instance().getVector2().set(x, y);

        if(providers.containsKey(help))
            providers.remove(help);

        Pools.instance().freeVector2(help);
    }

    /**
     * Removes a consumer from the grid
     * @param x x
     * @param y y
     */
    public void removeConsumer(int x, int y){
        Vector2 help = Pools.instance().getVector2().set(x, y);

        if(consumers.containsKey(help))
            consumers.remove(help);

        Pools.instance().freeVector2(help);
    }

    public ObjectMap<Vector2, PowerConduitComponent> getConduits() {
        return conduits;
    }

    public ObjectMap<Vector2, IEnergyProvider> getProviders() {
        return providers;
    }

    public ObjectMap<Vector2, IEnergyConsumer> getConsumers() {
        return consumers;
    }
}
