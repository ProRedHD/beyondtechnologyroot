package at.prored.main.engine.conduit;

import at.prored.main.engine.components.conduit.ConduitComponent;
import at.prored.main.engine.components.conduit.PowerConduitComponent;
import at.prored.main.engine.conduit.power.interfaces.IEnergyConsumer;
import at.prored.main.engine.conduit.power.interfaces.IEnergyProvider;
import at.prored.main.engine.tiles.storage.GameMap;
import at.prored.main.engine.utils.MapUtils;
import at.prored.main.engine.utils.Properties;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by ProRed on 01.03.2018.
 */
public class GridManager {

    private ObjectMap<Integer, EnergyGrid> energyGrids;
    private int highestGridID = 0;

    public GridManager() {
        this.energyGrids = new ObjectMap<Integer, EnergyGrid>();
    }

    /**
     * Creates a new energy grid and registers it
     * @return
     */
    public EnergyGrid createEnergyGrid(){
        Gdx.app.debug("GridManager", "Created new energy grid with ID " + highestGridID);

        EnergyGrid energyGrid = new EnergyGrid(highestGridID++);
        addEnergyGrid(energyGrid);

        return energyGrid;
    }



    /**
     * Registers a energy grid
     * @param grid energy grid
     */
    public void addEnergyGrid(EnergyGrid grid){
        energyGrids.put(grid.getID(), grid);
    }

    /**
     * Returns a registered energy grid
     * @param id id of the grid
     * @return energy grid
     */
    public EnergyGrid getEnergyGrid(int id){
        return energyGrids.get(id);
    }

    /**
     * Merges two energy grids together
     * @param id1 grid1
     * @param id2 grid2
     * @return the remaining of the two
     */
    public int mergeEnergyGrids(GameMap gameMap, int id1, int id2) {
        EnergyGrid grid1 = getEnergyGrid(id1);
        EnergyGrid grid2 = getEnergyGrid(id2);

        int size1 = grid1.getGridSize();
        int size2 = grid2.getGridSize();

        EnergyGrid sourceGrid = size1 > size2 ? grid2 : grid1;
        EnergyGrid targetGrid = size1 > size2 ? grid1 : grid2;

        //Transfer conduits
        for (ObjectMap.Entry<Vector2, PowerConduitComponent> entry : sourceGrid.getConduits().entries()) {
            targetGrid.addConduit(entry.key, entry.value);

            //Update grid ids
            Properties p = MapUtils.getTileProperties(gameMap, (int)entry.key.x, (int)entry.key.y, 2);
            ConduitComponent.setGridID(p, targetGrid.getID());
        }

        //Transfer providers
        for (ObjectMap.Entry<Vector2, IEnergyProvider> entry : sourceGrid.getProviders().entries()) {
            targetGrid.addProvider(entry.key, entry.value);
        }

        //Transfer consumers
        for (ObjectMap.Entry<Vector2, IEnergyConsumer> entry : sourceGrid.getConsumers().entries()) {
            targetGrid.addConsumer(entry.key, entry.value);
        }

        //Remove smaller grid
        energyGrids.remove(sourceGrid.getID());

        Gdx.app.log("GridManager", "Merged energy grid " + id1 + " and " + id2 + " to " + targetGrid.getID());

        return targetGrid.getID();
    }

    public ObjectMap<Integer, EnergyGrid> getEnergyGrids() {
        return energyGrids;
    }
}

