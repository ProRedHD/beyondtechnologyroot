package at.prored.main.engine.physics;

import at.prored.main.engine.collision.AABB;
import at.prored.main.engine.collision.Collision;
import at.prored.main.engine.components.GravityComponent;
import at.prored.main.engine.entities.Entity;
import at.prored.main.engine.entities.EntityDef;
import at.prored.main.engine.entities.EntityRegistry;
import at.prored.main.engine.entities.components.PhysicsComponent;
import at.prored.main.engine.graphics.camera.Graphics;
import at.prored.main.engine.tiles.storage.Chunk;
import at.prored.main.engine.tiles.storage.GameMap;
import at.prored.main.engine.utils.Pools;
import at.prored.main.engine.utils.VecUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by ProRed on 27.05.2017.
 */
public class Physics {

    private static Array<AABB> tiles = new Array<AABB>();
    private static AABB entityAABB = new AABB();

    public static void performPhysics(Array<Entity> entities, ObjectMap<Vector2, Chunk> activeChunks, EntityRegistry entityRegistry, GameMap gameMap) {

        Vector2 acceleration = Pools.instance().getVector2();
        Vector2 velocity = Pools.instance().getVector2();

        for (Entity entity : entities) {

            //Fill tiles with data
            fillTilePhysicsArray(entity, tiles, activeChunks, gameMap);

            //Get entitydef
            EntityDef entityDef = entityRegistry.getEntityDef(entity.getID());

            //Get physicscomponent
            PhysicsComponent physicsComponent = entityDef.getComponent(PhysicsComponent.class);

            //Get gravity component
            GravityComponent gravityComponent = entityDef.getComponent(GravityComponent.class);

            acceleration.set(physicsComponent.getAccelerationX(entity), physicsComponent.getAccelerationY(entity));
            velocity.set(physicsComponent.getVelocityX(entity), physicsComponent.getVelocityY(entity));

            //Calculate entity aabb
            float halfWidth = physicsComponent.getWidth() / 2f;
            float halfHeight = physicsComponent.getHeight() / 2f;
            float centerX = entity.getEntityProperties().getPosition().x + halfWidth;
            float centerY = entity.getEntityProperties().getPosition().y + halfHeight;
            entityAABB.set(centerX, centerY, halfWidth, halfHeight);

            float dt = Graphics.instance().getDeltaTime();

            //Apply gravity
            if (gravityComponent != null)
                velocity.add(0, gravityComponent.getMass() * -9.81f * dt);

            //Process acceleration
            Vector2 velChange = VecUtils.scl(acceleration, dt * dt);
            acceleration.sub(velChange);
            velocity.add(velChange);
            VecUtils.free(velChange);

            //Process velocity
            Vector2 posChange = VecUtils.scl(velocity, dt);
            entityAABB.getCenter().add(posChange);
            VecUtils.free(posChange);

            velocity.x = 0f;

            for (AABB tile : tiles) {

                Collision collision = tile.getCollision(entityAABB);

                if (collision.isIntersecting()) {
                    Vector2 direction = entityAABB.correctPosition(tile, collision);

                    //Adjust velocity
                    if (direction.x == 1) {
                        velocity.x = 0;
                    } else if (direction.x == -1) {
                        velocity.x = 0;
                    } else if (direction.y == 1) {
                        velocity.y = 0;
                        physicsComponent.setCanJump(entity, true);
                    } else if (direction.y == -1) {
                        velocity.y = 0;
                    }

                    Pools.instance().freeVector2(direction);
                }

                Pools.instance().getCollisionPool().free(collision);
            }

            //Adjust values in properties
            physicsComponent.setAccelerationX(entity, acceleration.x);
            physicsComponent.setAccelerationY(entity, acceleration.y);
            physicsComponent.setVelocityX(entity, velocity.x);
            physicsComponent.setVelocityY(entity, velocity.y);

            //Adjust entity position
            float eX = entityAABB.getCenter().x - entityAABB.getHalfSize().x;
            float eY = entityAABB.getCenter().y - entityAABB.getHalfSize().y;
            entity.getEntityProperties().getPosition().set(eX, eY);

            //Notify gameMap of potential chunk change
            gameMap.checkEntityPositionForChunkChange(entity);
        }

        Pools.instance().freeVector2(acceleration);
        Pools.instance().freeVector2(velocity);
    }

    private static void fillTilePhysicsArray(Entity e, Array<AABB> tiles, ObjectMap<Vector2, Chunk> activeChunks, GameMap gameMap) {
        tiles.clear();

        //Find chunk section
        Vector2 section = getPhysicsChunkSection(e);

        //Search for the entity in the active chunks to get the current chunk
        Vector2 currentChunk = gameMap.searchEntityInActiveChunk(e);

        Vector2 help = Pools.instance().getVector2Pool().obtain();

        if (currentChunk == null) {
            return;
        }

        //Add all aabbs of the current chunk
        if (activeChunks.containsKey(currentChunk)) {
            tiles.addAll(activeChunks.get(currentChunk).getTileBoundingBoxes());
        }

        if (activeChunks.containsKey(help.set(currentChunk.x, currentChunk.y - 1))) {
            tiles.addAll(activeChunks.get(help).getTileBoundingBoxes());
        }

        if (section.x == 1 && activeChunks.containsKey(help.set(currentChunk.x - 1, currentChunk.y))) {
            tiles.addAll(activeChunks.get(help).getTileBoundingBoxes());
        }

        if (section.x == 3 && activeChunks.containsKey(help.set(currentChunk.x + 1, currentChunk.y))) {
            tiles.addAll(activeChunks.get(help).getTileBoundingBoxes());
        }

        if (section.y == 3 && activeChunks.containsKey(help.set(currentChunk.x, currentChunk.y + 1))) {
            tiles.addAll(activeChunks.get(help).getTileBoundingBoxes());
        }

        Pools.instance().getVector2Pool().free(section);
        Pools.instance().getVector2Pool().free(help);
    }


    /**
     * Calculates the physics section that the entity is in
     *
     * @param e entity
     * @return physics section
     */
    public static Vector2 getPhysicsChunkSection(Entity e) {
        float eX = e.getEntityProperties().getPosition().x;
        float eY = e.getEntityProperties().getPosition().y;

        eX %= (Chunk.CHUNK_SIZE * 32);
        eY %= (Chunk.CHUNK_SIZE * 32);

        if (eX < 0) {
            eX += Chunk.CHUNK_SIZE * 32;
        }

        if (eY < 0) {
            eY += Chunk.CHUNK_SIZE * 32;
        }

        float xStart2 = (Chunk.CHUNK_SIZE * 32) / 3f;
        float xStart3 = xStart2 * 2;

        float yStart2 = (Chunk.CHUNK_SIZE * 32) / 3f;
        float yStart3 = yStart2 * 2;

        Vector2 result = Pools.instance().getVector2Pool().obtain();

        //Find x section
        if (eX > xStart3) {
            result.set(3, result.y);
        } else if (eX > xStart2) {
            result.set(2, result.y);
        } else {
            result.set(1, result.y);
        }

        //Find y section
        if (eY > yStart3) {
            result.set(result.x, 3);
        } else if (eY > yStart2) {
            result.set(result.x, 2);
        } else {
            result.set(result.x, 1);
        }

        return result;
    }

}
