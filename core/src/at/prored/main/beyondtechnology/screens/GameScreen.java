package at.prored.main.beyondtechnology.screens;

import at.prored.main.beyondtechnology.BeyondTechnology;
import at.prored.main.engine.graphics.screens.AbstractScreen;
import at.prored.main.engine.guis.crafting.CraftingMenu;
import at.prored.main.engine.guis.GUIManager;
import at.prored.main.engine.guis.Hotbar;
import at.prored.main.engine.guis.inventory.InventoryGui;
import at.prored.main.engine.tiles.storage.GameMap;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;

/**
 * Created by ProRed on 18.05.2017.
 */
public class GameScreen extends AbstractScreen {

    private BeyondTechnology btech;

    private GameMap gameMap;

    public GameScreen(BeyondTechnology btech){
        this.btech = btech;

        this.btech.getEngine().getLayerRegistry().registerLayer("main", 0);
        this.btech.getEngine().getLayerRegistry().registerLayer("background", 1);
        this.btech.getEngine().getLayerRegistry().registerLayer("power", 2);

        this.gameMap = new GameMap(btech.getEngine(), "player");
        //WorldLoader.loadGameMap(gameMap);

        //Synchronize hotbar with gameMap
        Hotbar gui = (Hotbar) gameMap.getEngine().getGUIManager().getGUI(GUIManager.GUI.INVENTORY_HOTBAR);
        gui.setGameMap(gameMap);
        gui.syncronizeHotbarToEntity(gameMap.getPlayer());
        gui.toggle(gameMap.getEngine().getGUIManager());

        CraftingMenu craftingMenu = (CraftingMenu) gameMap.getEngine().getGUIManager().getGUI(GUIManager.GUI.CRAFTING_MENU);
        craftingMenu.setGameMap(gameMap);

        InventoryGui inventoryGui = (InventoryGui) gameMap.getEngine().getGUIManager().getGUI(GUIManager.GUI.INVENTORY);
        inventoryGui.setGameMap(gameMap);
    }

    @Override
    public void show() {
        gameMap.registerInputProcessors();
    }

    @Override
    public void update() {
        if(Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE))
            Gdx.app.exit();

        gameMap.update();
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        gameMap.render();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        gameMap.unregisterInputProcessors();
    }

    @Override
    public void dispose() {

    }
}
