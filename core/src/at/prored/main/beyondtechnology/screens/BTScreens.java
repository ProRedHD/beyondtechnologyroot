package at.prored.main.beyondtechnology.screens;

import at.prored.main.beyondtechnology.BeyondTechnology;
import at.prored.main.engine.Engine;

/**
 * Created by ProRed on 18.05.2017.
 */
public class BTScreens {

    public static final String GAME = "game";

    private static GameScreen gameScreen;

    public static void setupScreens(BeyondTechnology btech){
        initScreens(btech);
        registerScreens(btech.getEngine());
    }

    private static void initScreens(BeyondTechnology btech){
        gameScreen = new GameScreen(btech);
    }

    private static void registerScreens(Engine engine){
        engine.getGameScreenManager().registerScreen(GAME, gameScreen);
    }
}
