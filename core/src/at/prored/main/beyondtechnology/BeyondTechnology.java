package at.prored.main.beyondtechnology;

import at.prored.main.beyondtechnology.screens.BTScreens;
import at.prored.main.engine.Engine;
import at.prored.main.engine.graphics.camera.Graphics;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

/**
 * Created by ProRed on 18.05.2017.
 */
public class BeyondTechnology extends Game {

    private Engine engine;

    public BeyondTechnology(){
        this.engine = new Engine(this);
    }

    @Override
    public void create() {
        Gdx.app.setLogLevel(Application.LOG_DEBUG);

        engine.setAssetLoader(assetManager -> assetManager.load("assets.atlas", TextureAtlas.class));

        engine.init();

        engine.getContentPackageRegistry().readAllContentPackages();

        BTScreens.setupScreens(this);

        engine.getGameScreenManager().setScreen(BTScreens.GAME);
    }

    private void update(){
        engine.update();
    }

    @Override
    public void render() {
        super.render();
        update();

        engine.render();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        Graphics.instance().getCamera().update(width, height);
    }

    public Engine getEngine() {
        return engine;
    }
}
