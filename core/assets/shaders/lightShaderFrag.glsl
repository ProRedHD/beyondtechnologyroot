#version 120

#ifdef GL_ES
precision mediump float;
#endif

uniform sampler2D u_texture;
uniform sampler2D u_tileLight;

uniform vec2 u_resolution;
uniform vec4 u_ambientColor;
uniform int u_renderMode;

varying vec2 v_texCoords;

void main()
{
  vec2 tileColorPosition = (gl_FragCoord.xy / u_resolution.xy);

  vec4 textureColor = texture2D(u_texture, v_texCoords);
  vec4 tileLightColor = texture2D(u_tileLight, tileColorPosition);

  vec4 finaltileLightColor = textureColor * vec4(tileLightColor.xyz, 1);

  gl_FragColor = finaltileLightColor;
}