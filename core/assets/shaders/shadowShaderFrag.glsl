#version 120

#ifdef GL_ES
precision mediump float;
#endif

uniform sampler2D u_texture;

varying vec2 v_texCoords;

void main()
{
  vec4 texColor = texture2D(u_texture, v_texCoords);
  float l = (0.2126*texColor.r + 0.7152*texColor.g + 0.0722*texColor.b);
  gl_FragColor = vec4(0.5f,0.5f,0.5f,texColor.a);
}