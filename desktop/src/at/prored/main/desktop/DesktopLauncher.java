package at.prored.main.desktop;

import at.prored.main.beyondtechnology.BeyondTechnology;
import at.prored.main.engine.graphics.camera.Graphics;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = Graphics.V_WIDTH;
		config.height = Graphics.V_HEIGHT;
		config.foregroundFPS = 0;
		config.backgroundFPS = 0;
		config.samples = 16;
		config.vSyncEnabled = false;
		new LwjglApplication(new BeyondTechnology(), config);
	}
}
